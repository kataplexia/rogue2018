﻿namespace Rogue2018.Interfaces
{
    using RogueSharp;
    using UnityEngine;

    public interface IDrawable
    {
        #region Properties

        Color DrawColor
        {
            get;
            set;
        }

        Color BackgroundDrawColor
        {
            get;
            set;
        }

        Color DrawColorFOV
        {
            get;
            set;
        }

        Color BackgroundDrawColorFOV
        {
            get;
            set;
        }

        string Symbol
        {
            get;
            set;
        }

        string GlyphString
        {
            get;
            set;
        }

        int X
        {
            get;
            set;
        }

        int Y
        {
            get;
            set;
        }

        #endregion

        #region Methods

        void Draw(int xOffset, int yOffset, IMap map);

        #endregion
    }
}