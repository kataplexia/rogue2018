﻿namespace Rogue2018.Interfaces
{
    public interface ISchedulable
    {
        #region Properties

        int Time
        {
            get;
        }

        #endregion
    }
}