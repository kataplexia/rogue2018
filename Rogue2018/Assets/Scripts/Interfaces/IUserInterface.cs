﻿namespace Rogue2018.Interfaces
{
    using PhiOS;

    public interface IUserInterface
    {
        #region Properties

        string[] Handles
        {
            get;
            set;
        }

        PhiCellPropertiesList[] PhiCellPropertiesLists
        {
            get;
            set;
        }

        #endregion

        #region Methods

        void Draw();

        #endregion
    }
}