﻿namespace Rogue2018.Interfaces.ItemInterfaces
{
    public interface IImplicit
    {
        #region Properties

        string EffectInfoString
        {
            get;
            set;
        }

        int EffectValue
        {
            get;
            set;
        }

        #endregion

        #region Methods

        void ApplyEffect();

        #endregion
    }
}