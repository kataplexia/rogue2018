﻿namespace Rogue2018.Interfaces
{
    public enum WeaponStyles
    {
        None = 0,
        OneHanded = 1,
        TwoHanded = 2,
        Ranged = 3
    }

    public enum WeaponTypes
    {
        None = 0,
        Sword = 1,
        Axe = 2,
        Mace = 3,
        Staff = 4,
        Bow = 5,
        Dagger = 6,
        Sceptre = 8,
        Wand = 9
    }

    public enum DamageTypes
    {
        None = 0,
        Cut = 1,
        Pierce = 2,
        Blunt = 3
    }

    public interface IWeapon
    {
        #region Properties

        WeaponStyles WeaponStyle
        {
            get;
            set;
        }

        WeaponTypes WeaponType
        {
            get;
            set;
        }

        DamageTypes DamageType
        {
            get;
            set;
        }

        string WeaponStyleString
        {
            get;
            set;
        }

        string WeaponTypeString
        {
            get;
            set;
        }

        string DamageTypeString
        {
            get;
            set;
        }

        int PhysicalDamage
        {
            get;
            set;
        }

        int HitChance
        {
            get;
            set;
        }

        #endregion
    }
}