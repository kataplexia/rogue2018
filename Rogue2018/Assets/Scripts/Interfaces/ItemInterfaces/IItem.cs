﻿namespace Rogue2018.Interfaces
{
    using UnityEngine;

    public enum ItemQualities
    {
        None = 0,
        Normal = 1,
        Magic = 2,
        Rare = 3,
        Unique = 4
    }

    public interface IItem
    {
        #region Properties

        ItemQualities ItemQuality
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        #endregion

        #region Properties

        void DrawName(int xOffset, int yOffset, Color drawColor);

        void Drop(bool dropAll = false);

        #endregion
    }
}