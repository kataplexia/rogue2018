﻿namespace Rogue2018.Interfaces
{
    public enum AccessoryTypes
    {
        None = 0,
        Ring = 1,
        Belt = 2,
        Quiver = 3,
        Focus = 4
    }

    public interface IAccessory
    {
        #region Properties

        AccessoryTypes AccessoryType
        {
            get;
            set;
        }

        string AccessoryTypeString
        {
            get;
            set;
        }

        #endregion
    }
}