﻿namespace Rogue2018.Interfaces
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore;

    public enum EquipableItemBaseTypes
    {
        Belt = 0,
        Quiver = 1,
        Ring = 2,
        HeavyChestArmour = 3,
        MediumChestArmour = 4,
        LightChestArmour = 5,
        HeavyFeetArmour = 6,
        MediumFeetArmour = 7,
        LightFeetArmour = 8,
        HeavyHandsArmour = 9,
        MediumHandsArmour = 10,
        LightHandsArmour = 11,
        HeavyHeadArmour = 12,
        MediumHeadArmour = 13,
        LightHeadArmour = 14,
        HeavyLegsArmour = 15,
        MediumLegsArmour = 16,
        LightLegsArmour = 17,
        HeavyShield = 18,
        MediumShield = 19,
        LightShield = 20,
        OneHandedAxe = 21,
        TwoHandedAxe = 22,
        Bow = 23,
        Dagger = 24,
        OneHandedMace = 25,
        TwoHandedMace = 26,
        Sceptre = 27,
        Staff = 28,
        OneHandedSwordGeneric = 29,
        OneHandedSwordPierce = 30,
        TwoHandedSword = 31,
        Wand = 32,
        Focus = 33
    }

    public enum EquipmentLocations
    {
        Head = 0,
        Chest = 1,
        Waist = 2,
        Legs = 3,
        Feet = 4,
        Hands = 5,
        MainHand = 6,
        OffHand = 7,
        EitherHand = 8,
        Ring = 9
    }

    public interface IEquipable
    {
        #region Properties

        EquipableItemBaseTypes EquipableItemBaseType
        {
            get;
            set;
        }

        int ItemLevel
        {
            get;
            set;
        }

        bool Equipped
        {
            get;
            set;
        }

        bool EquippedMainHand
        {
            get;
            set;
        }

        bool EquippedOffHand
        {
            get;
            set;
        }

        bool EquippedLeftRing
        {
            get;
            set;
        }

        bool EquippedRightRing
        {
            get;
            set;
        }

        EquipmentLocations EquipmentLocation
        {
            get;
            set;
        }

        string EquipmentLocationString
        {
            get;
            set;
        }

        List<Prefix> Prefixes
        {
            get;
            set;
        }

        List<Suffix> Suffixes
        {
            get;
            set;
        }

        Implicit Implicit
        {
            get;
            set;
        }

        #endregion

        #region Methods

        void Construct();

        void Equip();

        void Unequip();

        void SetupAffixes();

        void AddPrefixes(int prefixCount);

        void AddSuffixes(int suffixCount);

        void RenameItem(int prefixCount, int suffixCount);

        #endregion
    }
}