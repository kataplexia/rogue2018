﻿namespace Rogue2018.Interfaces
{
    using UnityEngine;

    public interface IUsable
    {
        #region Properties

        System.Type ParentType
        {
            get;
            set;
        }

        int MaxStackCount
        {
            get;
            set;
        }

        int CurrentStackCount
        {
            get;
            set;
        }

        #endregion

        #region Methods

        void Construct();

        void DrawStacks(int xOffset, int yOffset, Color drawColor);

        void Use();

        void Drop(bool dropAll = false);

        #endregion
    }
}