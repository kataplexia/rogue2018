﻿namespace Rogue2018.Interfaces
{
    public interface IActor
    {
        #region Properties

        int Attack
        {
            get;
            set;
        }

        int HitChance
        {
            get;
            set;
        }

        int Awareness
        {
            get;
            set;
        }

        int Defence
        {
            get;
            set;
        }

        int DefenceChance
        {
            get;
            set;
        }

        int Gold
        {
            get;
            set;
        }

        int Health
        {
            get;
            set;
        }

        int MaxHealth
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        int Speed
        {
            get;
            set;
        }

        #endregion
    }
}