﻿namespace Rogue2018.Interfaces
{
    using Rogue2018.Core;
    using Rogue2018.Systems;

    public interface IBehaviour
    {
        #region Methods

        bool Act(Monster monster, CommandSystem commandSystem);

        #endregion
    }
}