﻿namespace Rogue2018.Core
{
    using PhiOS;
    using Rogue2018.Interfaces;
    using RogueSharp;
    using UnityEngine;

    public abstract class Actor : IActor, IDrawable, ISchedulable
    {
        #region Fields

        private int attack;
        private int attackChance;
        private int awareness;
        private int defence;
        private int defenceChance;
        private int gold;
        private int health;
        private int maxHealth;
        private int speed;
        private string glyphString;
        private string name;

        private Color backgroundDrawColor;
        private Color backgroundDrawColorFOV;
        private Color drawColor;
        private Color drawColorFOV;
        private int x;
        private int y;
        private string symbol;

        #endregion

        #region Properties

        public int Attack
        {
            get { return attack; }
            set { attack = value; }
        }

        public int Awareness
        {
            get { return awareness; }
            set { awareness = value; }
        }

        public Color BackgroundDrawColor
        {
            get { return backgroundDrawColor; }
            set { backgroundDrawColor = value; }
        }

        public Color BackgroundDrawColorFOV
        {
            get { return backgroundDrawColorFOV; }
            set { backgroundDrawColorFOV = value; }
        }

        public int Defence
        {
            get { return defence; }
            set { defence = value; }
        }

        public int DefenceChance
        {
            get { return defenceChance; }
            set { defenceChance = value; }
        }

        public Color DrawColor
        {
            get { return drawColor; }
            set { drawColor = value; }
        }

        public Color DrawColorFOV
        {
            get { return drawColorFOV; }
            set { drawColorFOV = value; }
        }

        public string GlyphString
        {
            get { return glyphString; }
            set { glyphString = value; }
        }

        public int Gold
        {
            get { return gold; }
            set { gold = value; }
        }

        public int Health
        {
            get { return health; }
            set { health = value; }
        }

        public int HitChance
        {
            get { return attackChance; }
            set { attackChance = value; }
        }

        public int MaxHealth
        {
            get { return maxHealth; }
            set { maxHealth = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }

        public int Time
        {
            get { return Speed; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        #endregion

        #region Methods

        public void Draw(int xOffset, int yOffset, IMap map)
        {
            if (!Game.Singleton.Tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
            {
                return;
            }

            if (!map.GetCell(X, Y).IsExplored)
            {
                return;
            }

            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = Game.Singleton.CurrentMap.BackgroundDrawColorFOV(X, Y);
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.ActorLayer, X + xOffset, Y + yOffset);

                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }
            }
            else
            {
                BackgroundDrawColor = Game.Singleton.CurrentMap.BackgroundDrawColor(X, Y);
                DrawColor = Game.Singleton.CurrentMap.DrawColor(X, Y);
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.ActorLayer, X + xOffset, Y + yOffset);

                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? Game.Singleton.GlyphStrings.Floor : Game.Singleton.CurrentMap.Symbol(X, Y);
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }
        }

        #endregion
    }
}