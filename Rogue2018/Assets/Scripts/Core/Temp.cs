﻿namespace Rogue2018.Core
{
    using System.Diagnostics.CodeAnalysis;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    public class Temp : MonoBehaviour
    {
        public static Temp Singleton;

        public void ClearTemp()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

        private void Awake()
        {
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }
    }
}