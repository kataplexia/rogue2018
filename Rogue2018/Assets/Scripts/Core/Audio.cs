﻿namespace Rogue2018.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    public class Audio : MonoBehaviour
    {
        #region Fields

        public static Audio Singleton;

        public AudioClip MonsterDeath;
        public AudioClip PlayerDeath;
        public AudioClip PlayerHit;
        public AudioClip PlayerMiss;
        public AudioClip PlayerMove;
        public AudioClip PlayerOpenDoor;
        public AudioClip PlayerStairs;
        public AudioClip PlayerStuck;
        public AudioClip PlayerTakeDamage;

        private AudioSource audioSource;
        private List<AudioClip> clipSchedule;
        private int maxClips = 1;

        #endregion

        #region Methods

        public void QueueAudio(AudioClip clip)
        {
            if (clipSchedule.Count < maxClips)
            {
                clipSchedule.Add(clip);
            }
            else
            {
                clipSchedule.Clear();
                clipSchedule.Add(clip);
            }
        }

        public IEnumerator Start()
        {
            audioSource = GetComponent<AudioSource>();
            clipSchedule = new List<AudioClip>();
            yield return null;
        }

        private void Awake()
        {
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }

        private void PlayAudio(AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.Play();
            clipSchedule.Remove(clip);
        }

        private void Update()
        {
            if (clipSchedule.Count == 0)
            {
                return;
            }

            if (!audioSource.isPlaying)
            {
                PlayAudio(clipSchedule[0]);
            }
        }

        #endregion
    }
}