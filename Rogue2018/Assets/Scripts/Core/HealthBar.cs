﻿namespace Rogue2018.Core
{
    using PhiOS;
    using UnityEngine;

    public class HealthBar
    {
        #region Fields

        private GameObject healthBarObject;
        private SpriteRenderer spriteRenderer;
        private Texture2D texture;
        private Sprite sprite;

        private int healthBarHeight = 20;
        private int healthBarBorderThickness = 4;
        private int percentageHealth = 100;
        private Color healthColor = Color.green;
        private Color backgroundColor = Color.gray;
        private int lastHealth;
        private bool drawRequired;

        #endregion

        #region Constructor

        public HealthBar(string actorName)
        {
            healthBarObject = new GameObject();
            healthBarObject.layer = 5;

            healthBarObject.transform.name = actorName + "_HealthBar";
            healthBarObject.transform.parent = Temp.Singleton.transform;

            spriteRenderer = healthBarObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sortingOrder = 1;

            texture = new Texture2D(100, 100);

            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Point;
        }

        #endregion

        #region Properties

        public bool IsEnabled
        {
            get { return spriteRenderer.enabled; }
            set { spriteRenderer.enabled = value; }
        }

        #endregion

        #region Methods

        public void UpdateHealthBarPosition(int actorX, int actorY, int drawOffsetX, int drawOffsetY)
        {
            float xPosition = (actorX + drawOffsetX - (PhiDisplay.Singleton.displayWidth / 2)) + ((1f - healthBarObject.transform.localScale.x) / 2f);
            float yPosition = (PhiDisplay.Singleton.displayHeight / 2) - (actorY + drawOffsetY) - 0.075f;

            Vector2 position = new Vector2(xPosition, yPosition);
            healthBarObject.transform.position = position;
        }

        public void UpdateHealthBarValues(int actorHealth, int actorMaxHealth)
        {
            if (lastHealth != actorHealth)
            {
                lastHealth = actorHealth;
                drawRequired = true;
            }
            else
            {
                return;
            }

            percentageHealth = (int)(((float)actorHealth / (float)actorMaxHealth) * 100f);

            if (percentageHealth <= 100 && percentageHealth > 75)
            {
                healthColor = Color.green;
            }
            else if (percentageHealth <= 75 && percentageHealth > 50)
            {
                healthColor = Color.yellow;
            }
            else if (percentageHealth <= 50 && percentageHealth > 25)
            {
                healthColor = Game.Singleton.Swatch.DbBrightWood;
            }
            else if (percentageHealth <= 25)
            {
                healthColor = Color.red;
            }
        }

        public void Draw()
        {
            if (drawRequired)
            {
                ClearHealthBar();
                SetHealthBarPixels();
                DrawHealthBar();
                drawRequired = false;
            }
        }

        public void DestroyHealthBar()
        {
            GameObject.Destroy(healthBarObject);
        }

        private void ClearHealthBar()
        {
            for (int x = 0; x < texture.width; x++)
            {
                for (int y = 0; y < texture.height; y++)
                {
                    texture.SetPixel(x, y, Color.clear);
                }
            }
        }

        private void SetHealthBarPixels()
        {
            for (int x = 0; x < texture.width; x++)
            {
                for (int y = 0; y < healthBarHeight; y++)
                {
                    Color drawColor = healthColor;
                    if (x <= healthBarBorderThickness ||
                        y <= healthBarBorderThickness ||
                        x >= texture.width - healthBarBorderThickness - 1 ||
                        y >= healthBarHeight - healthBarBorderThickness - 1)
                    {
                        drawColor = Color.black;
                    }
                    else
                    {
                        if (x > percentageHealth)
                        {
                            drawColor = backgroundColor;
                        }
                    }

                    texture.SetPixel(x, y, drawColor);
                }
            }
        }

        private void DrawHealthBar()
        {
            texture.Apply();
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0), 100);
            spriteRenderer.sprite = sprite;
        }

        #endregion
    }
}