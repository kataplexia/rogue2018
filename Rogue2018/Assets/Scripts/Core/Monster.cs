﻿namespace Rogue2018.Core
{
    using Rogue2018.Behaviours;
    using Rogue2018.Interfaces;
    using Rogue2018.Systems;

    public class Monster : Actor
    {
        #region Constructor

        public Monster()
        {
            MoveAndAttackBehaviour = new GoalMapMoveAttack();
        }

        #endregion

        #region Properties

        public int? TurnsAlerted
        {
            get;
            set;
        }

        public GoalMapMoveAttack MoveAndAttackBehaviour
        {
            get;
            set;
        }

        public HealthBar HealthBar
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void PerformAction(CommandSystem commandSystem)
        {
            IBehaviour behaviour = MoveAndAttackBehaviour;
            behaviour.Act(this, commandSystem);
        }

        public void DrawHealthBar(int xOffset, int yOffset)
        {
            if (HealthBar == null)
            {
                HealthBar = new HealthBar(Name);
            }

            if (!HealthBar.IsEnabled)
            {
                HealthBar.IsEnabled = true;
            }

            HealthBar.UpdateHealthBarPosition(X, Y, xOffset, yOffset);
            HealthBar.UpdateHealthBarValues(Health, MaxHealth);
            HealthBar.Draw();
        }

        #endregion
    }
}