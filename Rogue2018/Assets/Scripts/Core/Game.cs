﻿namespace Rogue2018.Core
{
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using PhiOS;
    using Rogue2018.Actors;
    using Rogue2018.Core.ItemCore;
    using Rogue2018.Core.MapCore;
    using Rogue2018.Systems;
    using Rogue2018.UserInterface;
    using RogueSharp;
    using RogueSharp.Random;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    [RequireComponent(typeof(Swatch))]
    [RequireComponent(typeof(MiniMap))]
    public class Game : MonoBehaviour
    {
        #region Fields

        public static Game Singleton;

        public static int MapConsoleDrawXOffset;
        public static int MapConsoleDrawYOffset;
        public readonly int ActorLayer = 0;
        public readonly int MapConsoleHeight = 90;
        public readonly int MapConsoleWidth = 120;
        public readonly int MapLayer = 0;
        public readonly int MessageConsoleHeight = 19;
        public readonly int MessageConsoleWidth = 36;
        public readonly int MessageLayer = 0;
        public readonly int StatsConsoleHeight = 30;
        public readonly int StatsConsoleWidth = 36;
        public readonly int StatsLayer = 0;

        #endregion

        #region Properties

        public int ActNumber
        {
            get;
            private set;
        }

        public CommandSystem CommandSystem
        {
            get;
            private set;
        }

        public CurrentMap CurrentMap
        {
            get;
            private set;
        }

        public GlyphStrings GlyphStrings
        {
            get;
            private set;
        }

        public InputSystem InputSystem
        {
            get;
            private set;
        }

        public bool IsDrawingMapConsole
        {
            get;
            private set;
        }

        public bool IsDrawRequired
        {
            get;
            private set;
        }

        public bool IsGameStarted
        {
            get;
            private set;
        }

        public bool IsInitialized
        {
            get;
            private set;
        }

        public ItemCollection ItemCollection
        {
            get;
            private set;
        }

        public ItemGenerator ItemGenerator
        {
            get;
            private set;
        }

        public ItemNameGenerator ItemNameGenerator
        {
            get;
            private set;
        }

        public ItemPrefixes ItemPrefixes
        {
            get;
            private set;
        }

        public ItemSuffixes ItemSuffixes
        {
            get;
            private set;
        }

        public MapCollection MapCollection
        {
            get;
            private set;
        }

        public string MapName
        {
            get;
            private set;
        }

        public MessageSystem MessageSystem
        {
            get;
            private set;
        }

        public MiniMap MiniMap
        {
            get;
            private set;
        }

        public PhiMouse PhiMouse
        {
            get;
            private set;
        }

        public Player Player
        {
            get;
            set;
        }

        public IRandom Random
        {
            get;
            private set;
        }

        public SchedulingSystem SchedulingSystem
        {
            get;
            private set;
        }

        public int Seed
        {
            get;
            private set;
        }

        public Swatch Swatch
        {
            get;
            private set;
        }

        public Symbols Symbols
        {
            get;
            private set;
        }

        public Tools Tools
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        public string ArrivedMessage()
        {
            string mapName = MapCollection.GetMapProperties(ActNumber, MapName).MapName;
            return string.Format("{0} arrives at {1}", Player.Name, mapName);
        }

        public void ChangeMap(string newMapName)
        {
            CurrentMap.DestroyHealthBars();

            MapCollection.StoreMap(ActNumber, MapName, CurrentMap, Player);
            CurrentMap.RemoveMonstersFromSchedule();
            MapName = newMapName;

            if (MapCollection.CheckForStoredMap(ActNumber, MapName))
            {
                Biomes currentBiome = MapCollection.GetBiome(ActNumber, MapName);
                Swatch.SetSwatchForBiome(currentBiome);
                GlyphStrings.SetGlyphStringsForBiome(currentBiome);

                CurrentMap = MapCollection.GetStoredMap(ActNumber, MapName);
                CurrentMap.AddMonstersToSchedule();

                Point storedPlayerPosition = MapCollection.GetStoredPlayerPosition(ActNumber, MapName);
                Player.X = storedPlayerPosition.X;
                Player.Y = storedPlayerPosition.Y;

                CurrentMap.UpdatePlayerFOV();
            }
            else
            {
                GenerateMap();
            }

            MessageSystem = new MessageSystem();
            CommandSystem = new CommandSystem();

            MessageSystem.Add(ArrivedMessage(), Color.magenta);

            Audio.Singleton.QueueAudio(Audio.Singleton.PlayerStairs);
        }

        public void ClearMapConsole()
        {
            Tools.ClearConsole(MapLayer, MapConsoleWidth, MapConsoleHeight, 0, 0);
        }

        public void NewGame()
        {
            Tools.ClearConsole(0, 160, 90, 0, 0);

            CommandSystem = new CommandSystem();
            MessageSystem = new MessageSystem();
            SchedulingSystem = new SchedulingSystem();

            ActNumber = 1;
            MapName = MapCollection.GetFirstMapNameOfAct(ActNumber);

            GenerateMap();

            MessageSystem.Add(ArrivedMessage(), Color.magenta);

            UIManager.Singleton.SideConsoleUI.Draw();

            IsDrawingMapConsole = true;
            IsDrawRequired = true;
            IsGameStarted = true;
        }

        public void ResetGame()
        {
            IsGameStarted = false;
            SceneManager.LoadScene(0);
        }

        public void SetDrawMapConsole(bool value)
        {
            IsDrawingMapConsole = value;
        }

        public void SetDrawRequired(bool value)
        {
            IsDrawRequired = value;
        }

        public IEnumerator Start()
        {
            while (!PhiDisplay.IsInitialized())
            {
                yield return null;
            }

            Seed = (int)DateTime.UtcNow.Ticks;
            Random = new DotNetRandom(Seed);

            GlyphStrings = new GlyphStrings();
            ItemCollection = new ItemCollection();
            ItemGenerator = new ItemGenerator();
            ItemNameGenerator = new ItemNameGenerator();
            ItemPrefixes = new ItemPrefixes();
            ItemSuffixes = new ItemSuffixes();
            MapCollection = new MapCollection();
            MiniMap = GetComponent<MiniMap>();
            PhiMouse = PhiMouse.Singleton;
            Swatch = GetComponent<Swatch>();
            Symbols = new Symbols();
            Tools = new Tools();

            InputSystem = new InputSystem();

            yield return new WaitForFixedUpdate();

            IsInitialized = true;
        }

        public void StartZoomRoutine(int zoomLevel)
        {
            StartCoroutine(ZoomRoutine(zoomLevel));
        }

        private void Awake()
        {
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }

        private void GenerateMap()
        {
            Biomes currentBiome = MapCollection.GetBiome(ActNumber, MapName);
            Swatch.SetSwatchForBiome(currentBiome);
            GlyphStrings.SetGlyphStringsForBiome(currentBiome);

            MapGenerator mapGenerator = new MapGenerator(ActNumber, MapName);
            CurrentMap = mapGenerator.GenerateMap();

            CurrentMap.UpdatePlayerFOV();
        }

        private void Update()
        {
            if (!PhiDisplay.IsInitialized() || !IsInitialized)
            {
                return;
            }

            if (IsDrawRequired)
            {
                UpdateDrawable();
                IsDrawRequired = false;
            }

            if (!IsGameStarted)
            {
                return;
            }

            InputSystem.UpdateInputSystem();
            CommandSystem.UpdateCommandSystem();
        }

        private void UpdateDrawable()
        {
            if (IsDrawingMapConsole)
            {
                Tools.ClearConsole(MapLayer, MapConsoleWidth, MapConsoleHeight, 0, 0);
                MapConsoleDrawXOffset = Tools.GetMapConsoleDrawXOffset(Player, MapConsoleWidth);
                MapConsoleDrawYOffset = Tools.GetMapConsoleDrawYOffset(Player, MapConsoleHeight);

                CurrentMap.Draw(MapConsoleDrawXOffset, MapConsoleDrawYOffset);
                Player.Draw(MapConsoleDrawXOffset, MapConsoleDrawYOffset, CurrentMap);
            }

            Tools.ClearConsole(StatsLayer, StatsConsoleWidth, StatsConsoleHeight, MapConsoleWidth + 2, 3);
            Player.DrawStats(MapConsoleWidth + 2, 3);

            Tools.ClearConsole(MessageLayer, MessageConsoleWidth, MessageConsoleHeight, MapConsoleWidth + 2, StatsConsoleHeight + 9);
            MessageSystem.Draw(MapConsoleWidth + 2, StatsConsoleHeight + 9);

            CurrentMap.DrawHealthBars(MapConsoleDrawXOffset, MapConsoleDrawYOffset);
            MiniMap.Draw();
        }

        private IEnumerator ZoomRoutine(int zoomLevel)
        {
            SetDrawRequired(true);

            yield return new WaitForFixedUpdate();

            PhiDisplay.Singleton.SetZoom(PhiDisplay.Singleton.currentZoomLevel);
        }

        #endregion
    }
}