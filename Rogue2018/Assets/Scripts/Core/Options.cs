﻿namespace Rogue2018.Core
{
    using System.Diagnostics.CodeAnalysis;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    public class Options : MonoBehaviour
    {
        #region Fields

        public static Options Singleton;

        public bool DrawTiles = true;

        #endregion

        #region Methods

        private void Awake()
        {
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }

        #endregion
    }
}