﻿namespace Rogue2018.Core
{
    using Rogue2018.Core.MapCore;

    public class GlyphStrings
    {
        #region Constructor

        public GlyphStrings()
        {
            DoorClosed = "DOOR_CLOSED";
            DoorOpen = "DOOR_OPEN";
            Floor_Cave = "CAVE_FLOOR";
            Floor_Dungeon = "DUNGEON_FLOOR";
            Floor_Forest = "FOREST_FLOOR";
            Floor_Swamp = "SWAMP_FLOOR";
            Kobold = "KOBOLD";
            Player = "PLAYER";
            StairsDown = "STAIRS_DOWN";
            StairsUp = "STAIRS_UP";
            Tree = "TREE";
            Wall_Cave = "CAVE_WALL_BITMAP_";
            Wall_Dungeon = "DUNGEON_WALL_BITMAP_";
            Wall_Forest = "TREE";
            Wall_Swamp = "SWAMP_WALL_BITMAP_";
            Water = "WATER";
        }

        #endregion

        #region Properties

        public string DoorClosed
        {
            get;
            private set;
        }

        public string DoorOpen
        {
            get;
            private set;
        }

        public string Floor
        {
            get;
            private set;
        }

        public string Floor_Cave
        {
            get;
            private set;
        }

        public string Floor_Dungeon
        {
            get;
            private set;
        }

        public string Floor_Forest
        {
            get;
            private set;
        }

        public string Floor_Swamp
        {
            get;
            private set;
        }

        public string Kobold
        {
            get;
            private set;
        }

        public string Player
        {
            get;
            private set;
        }

        public string StairsDown
        {
            get;
            private set;
        }

        public string StairsUp
        {
            get;
            private set;
        }

        public string Tree
        {
            get;
            private set;
        }

        public string Wall
        {
            get;
            private set;
        }

        public string Wall_Cave
        {
            get;
            private set;
        }

        public string Wall_Dungeon
        {
            get;
            private set;
        }

        public string Wall_Forest
        {
            get;
            private set;
        }

        public string Wall_Swamp
        {
            get;
            private set;
        }

        public string Water
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        public void SetGlyphStringsForBiome(Biomes biome)
        {
            if (biome == Biomes.Cave)
            {
                Wall = Wall_Cave;
                Floor = Floor_Cave;
            }
            else if (biome == Biomes.Dungeon)
            {
                Wall = Wall_Dungeon;
                Floor = Floor_Dungeon;
            }
            else if (biome == Biomes.Forest)
            {
                Wall = Wall_Forest;
                Floor = Floor_Forest;
            }
            else if (biome == Biomes.Swamp)
            {
                Wall = Wall_Swamp;
                Floor = Floor_Swamp;
            }
        }

        #endregion
    }
}