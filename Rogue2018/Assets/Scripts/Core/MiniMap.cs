﻿namespace Rogue2018.Core
{
    using System.Diagnostics.CodeAnalysis;
    using Rogue2018.Actors;
    using Rogue2018.Core.MapCore;
    using RogueSharp;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    public class MiniMap : MonoBehaviour
    {
        #region Fields

        public SpriteRenderer MiniMapSpriteRenderer;
        public int CurrentScaleFactor = 2;

        private Texture2D texture;
        private Sprite sprite;
        private int minimumScaleFactor = 1;
        private int maxiumumScaleFactor = 5;

        #endregion

        #region Methods

        public void Draw()
        {
            if (!Game.Singleton.IsInitialized)
            {
                return;
            }

            ClearMiniMap();
            SetMapCellPixels();
            DrawMiniMap();
        }

        public void ClearMiniMap()
        {
            for (int x = 0; x < texture.width; x++)
            {
                for (int y = 0; y < texture.height; y++)
                {
                    texture.SetPixel(x, y, new Color(0, 0, 0, 1));
                }
            }
        }

        public void SetScale(int scaleFactor)
        {
            if (CurrentScaleFactor != scaleFactor)
            {
                CurrentScaleFactor = scaleFactor;
            }

            CurrentScaleFactor = Mathf.Clamp(CurrentScaleFactor, minimumScaleFactor, maxiumumScaleFactor);
            Draw();
        }

        private void Start()
        {
            texture = new Texture2D(50, 50);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Point;
            MiniMapSpriteRenderer.enabled = true;
        }

        private void SetMapCellPixels()
        {
            Color drawColor = Color.white;

            foreach (Cell cell in Game.Singleton.CurrentMap.GetCellsInSquare(Game.Singleton.Player.X, Game.Singleton.Player.Y, 30))
            {
                if (cell.IsExplored)
                {
                    bool isInFOV = Game.Singleton.CurrentMap.IsInFOV(cell.X, cell.Y);
                    drawColor = isInFOV
                        ? Game.Singleton.CurrentMap.MiniMapDrawColorFOV(cell.X, cell.Y)
                        : Game.Singleton.CurrentMap.MiniMapDrawColor(cell.X, cell.Y);

                    foreach (Exit exit in Game.Singleton.CurrentMap.Exits)
                    {
                        if (exit.X == cell.X && exit.Y == cell.Y)
                        {
                            drawColor = isInFOV ? Color.magenta : Color.magenta * 0.75f;
                        }
                    }

                    foreach (Door door in Game.Singleton.CurrentMap.Doors)
                    {
                        if (door.X == cell.X && door.Y == cell.Y && !door.IsOpen)
                        {
                            drawColor = isInFOV ? Game.Singleton.Swatch.DbWood : Game.Singleton.Swatch.ComplimentDarker;
                        }
                    }

                    if (Game.Singleton.Player.X == cell.X && Game.Singleton.Player.Y == cell.Y)
                    {
                        drawColor = Game.Singleton.Swatch.Player;
                    }

                    Monster monster = Game.Singleton.CurrentMap.GetMonsterAt(cell.X, cell.Y);
                    if (monster != null)
                    {
                        drawColor = isInFOV ? monster.DrawColorFOV : Game.Singleton.Swatch.MiniMapFloor;
                    }

                    int miniMapDrawXOffset = GetMiniMapDrawXOffset(Game.Singleton.Player, texture.width);
                    int miniMapDrawYOffset = GetMiniMapDrawYOffset(Game.Singleton.Player, texture.height);

                    int xPosOrigin = cell.X * CurrentScaleFactor;
                    int yPosOrigin = cell.Y * CurrentScaleFactor;
                    Vector2 blockOrigin = new Vector2(xPosOrigin + miniMapDrawXOffset, yPosOrigin + miniMapDrawYOffset);

                    int xPosFinal = (cell.X * CurrentScaleFactor) + (CurrentScaleFactor - 1);
                    int yPosFinal = (cell.Y * CurrentScaleFactor) + (CurrentScaleFactor - 1);
                    Vector2 blockFinal = new Vector2(xPosFinal + miniMapDrawXOffset, yPosFinal + miniMapDrawYOffset);

                    Color[] drawColors = new Color[CurrentScaleFactor * CurrentScaleFactor];
                    for (int i = 0; i < drawColors.Length; i++)
                    {
                        drawColors[i] = drawColor;
                    }

                    if (CheckDrawable(blockOrigin) && CheckDrawable(blockFinal))
                    {
                        texture.SetPixels((int)blockOrigin.x, (int)blockOrigin.y, CurrentScaleFactor, CurrentScaleFactor, drawColors);
                    }
                }
            }
        }

        private bool CheckDrawable(Vector2 pixelPosition)
        {
            if (pixelPosition.x >= 0 &&
                pixelPosition.x < texture.width &&
                pixelPosition.y >= 0 &&
                pixelPosition.y < texture.height)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int GetMiniMapDrawXOffset(Player player, int miniMapWidth)
        {
            int scaledPlayerX = player.X * CurrentScaleFactor;
            if (scaledPlayerX > (miniMapWidth / 2))
            {
                return -1 * (scaledPlayerX - (miniMapWidth / 2));
            }
            else if (scaledPlayerX < (miniMapWidth / 2))
            {
                return (miniMapWidth / 2) - scaledPlayerX;
            }

            return 0;
        }

        private int GetMiniMapDrawYOffset(Player player, int miniMapHeight)
        {
            int scaledPlayerY = player.Y * CurrentScaleFactor;
            if (scaledPlayerY > (miniMapHeight / 2))
            {
                return -1 * (scaledPlayerY - (miniMapHeight / 2));
            }
            else if (scaledPlayerY < (miniMapHeight / 2))
            {
                return (miniMapHeight / 2) - scaledPlayerY;
            }

            return 0;
        }

        private void DrawMiniMap()
        {
            texture.Apply();
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0), 100);
            MiniMapSpriteRenderer.sprite = sprite;
        }

        #endregion
    }
}