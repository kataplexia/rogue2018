﻿namespace Rogue2018.Core.MapCore
{
    using PhiOS;
    using Rogue2018.Interfaces;
    using RogueSharp;
    using UnityEngine;

    public class Door : IDrawable
    {
        #region Constructor

        public Door()
        {
            Symbol = Game.Singleton.Symbols.DoorClosed;
            DrawColor = Game.Singleton.Swatch.Door;
            BackgroundDrawColor = Game.Singleton.Swatch.DoorBackground;
        }

        #endregion

        #region Properties

        public bool IsOpen
        {
            get;
            set;
        }

        public Color DrawColor
        {
            get;
            set;
        }

        public Color BackgroundDrawColor
        {
            get;
            set;
        }

        public Color DrawColorFOV
        {
            get;
            set;
        }

        public Color BackgroundDrawColorFOV
        {
            get;
            set;
        }

        public string Symbol
        {
            get;
            set;
        }

        public string GlyphString
        {
            get;
            set;
        }

        public int X
        {
            get;
            set;
        }

        public int Y
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void Draw(int xOffset, int yOffset, IMap map)
        {
            if (!Game.Singleton.Tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
            {
                return;
            }

            if (!map.GetCell(X, Y).IsExplored)
            {
                return;
            }

            Symbol = IsOpen ? Game.Singleton.Symbols.DoorOpen : Game.Singleton.Symbols.DoorClosed;
            GlyphString = IsOpen ? Game.Singleton.GlyphStrings.DoorOpen : Game.Singleton.GlyphStrings.DoorClosed;

            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV * 0.8f;
                DrawColorFOV = Game.Singleton.Swatch.AlternateDarker;
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }
            }
            else
            {
                BackgroundDrawColor = Game.Singleton.Swatch.FloorBackground * 0.8f;
                DrawColor = Game.Singleton.Swatch.AlternateDarkest;
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }
        }

        #endregion
    }
}