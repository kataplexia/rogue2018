﻿namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;

    public class MapPropertiesList
    {
        #region Fields

        public List<MapProperties> PropertiesList { get; set; }

        #endregion
    }
}