﻿namespace Rogue2018.Core.MapCore
{
    using System;
    using System.Collections.Generic;
    using Rogue2018.Actors;
    using Rogue2018.Actors.Monsters;
    using RogueSharp;
    using RogueSharp.Algorithms;
    using RogueSharp.DiceNotation;

    public class CellularGenerator
    {
        #region Fields

        private List<MapSection> divisionMapSections;

        #endregion

        #region Constructor

        public CellularGenerator(
            MapGenerator mapGenerator,
            CurrentMap map,
            int mapWidth,
            int mapHeight,
            bool transparentWalls,
            int fillProbability,
            int iterationCount,
            int bigAreaFillCutoff,
            string[] exitStrings)
        {
            int padding = 0;
            if (transparentWalls)
            {
                if (Game.Singleton.Player != null)
                {
                    padding = 2 * Game.Singleton.Player.Awareness;
                }
                else
                {
                    padding = 50;
                }
            }

            map.Initialize(mapWidth + padding, mapHeight + padding);
            CreateMap(map, transparentWalls, padding, fillProbability, iterationCount, bigAreaFillCutoff);
            CreateExits(mapGenerator, map, padding, exitStrings);
            mapGenerator.SetBitmaskForMap();
            mapGenerator.SetCellDrawPropertiesForMap();
            PlacePlayer(map);
            //// PlaceMonsters(map);
            mapGenerator.CreateGoalMaps();
        }

        #endregion

        #region Methods

        private void CreateMap(CurrentMap map, bool transparentWalls, int padding, int fillProbability, int iterationCount, int bigAreaFillCutoff)
        {
            RandomlyFillCells(map, transparentWalls, padding, fillProbability);

            for (int i = 0; i < iterationCount; i++)
            {
                if (i < bigAreaFillCutoff)
                {
                    CellularAutomataBigAreaAlgorithm(map, transparentWalls, padding);
                }
                else if (i >= bigAreaFillCutoff)
                {
                    CellularAutomaNearestNeighborsAlgorithm(map, transparentWalls, padding);
                }
            }

            ConnectCellularAreas(map);
        }

        private void RandomlyFillCells(CurrentMap map, bool transparentWalls, int padding, int fillProbability)
        {
            foreach (ICell cell in map.GetAllCells())
            {
                if (map.IsBorderCell(cell, padding))
                {
                    map.SetCellProperties(cell.X, cell.Y, transparentWalls, false);
                }
                else if (Game.Singleton.Random.Next(1, 100) < fillProbability)
                {
                    map.SetCellProperties(cell.X, cell.Y, true, true);
                }
                else
                {
                    map.SetCellProperties(cell.X, cell.Y, transparentWalls, false);
                }
            }
        }

        private void CellularAutomataBigAreaAlgorithm(CurrentMap map, bool transparentWalls, int padding)
        {
            var updatedMap = map.Clone();

            foreach (ICell cell in map.GetAllCells())
            {
                if (map.IsBorderCell(cell, padding))
                {
                    continue;
                }

                if ((map.CountWallsNear(cell, 1) >= 5) || (map.CountWallsNear(cell, 2) <= 2))
                {
                    updatedMap.SetCellProperties(cell.X, cell.Y, transparentWalls, false);
                }
                else
                {
                    updatedMap.SetCellProperties(cell.X, cell.Y, true, true);
                }
            }

            map.UpdateMap(updatedMap);
        }

        private void CellularAutomaNearestNeighborsAlgorithm(CurrentMap map, bool transparentWalls, int padding)
        {
            var updatedMap = map.Clone();

            foreach (ICell cell in map.GetAllCells())
            {
                if (map.IsBorderCell(cell, padding))
                {
                    continue;
                }

                if (map.CountWallsNear(cell, 1) >= 5)
                {
                    updatedMap.SetCellProperties(cell.X, cell.Y, transparentWalls, false);
                }
                else
                {
                    updatedMap.SetCellProperties(cell.X, cell.Y, true, true);
                }
            }

            map.UpdateMap(updatedMap);
        }

        private void ConnectCellularAreas(CurrentMap map)
        {
            var floodFillAnalyzer = new FloodFillAnalyzer(map);
            List<MapSection> mapSections = floodFillAnalyzer.GetMapSections();
            var unionFind = new UnionFind(mapSections.Count);
            while (unionFind.Count > 1)
            {
                for (int i = 0; i < mapSections.Count; i++)
                {
                    int closestMapSectionIndex = FindNearestMapSection(mapSections, i, unionFind);
                    MapSection closestMapSection = mapSections[closestMapSectionIndex];

                    IEnumerable<ICell> tunnelCells
                        = map.GetCellsAlongLine(
                            mapSections[i].Bounds.Center.X,
                            mapSections[i].Bounds.Center.Y,
                            closestMapSection.Bounds.Center.X,
                            closestMapSection.Bounds.Center.Y);

                    ICell previousCell = null;
                    foreach (ICell cell in tunnelCells)
                    {
                        map.SetCellProperties(cell.X, cell.Y, true, true);
                        if (previousCell != null)
                        {
                            if (cell.X != previousCell.X || cell.Y != previousCell.Y)
                            {
                                map.SetCellProperties(cell.X + 1, cell.Y, true, true);
                            }
                        }

                        previousCell = cell;
                    }

                    unionFind.Union(i, closestMapSectionIndex);
                }
            }
        }

        private int FindNearestMapSection(IList<MapSection> mapSections, int mapSectionIndex, UnionFind unionFind)
        {
            MapSection start = mapSections[mapSectionIndex];
            int closestIndex = mapSectionIndex;
            int distance = Int32.MaxValue;
            for (int i = 0; i < mapSections.Count; i++)
            {
                if (i == mapSectionIndex)
                {
                    continue;
                }

                if (unionFind.Connected(i, mapSectionIndex))
                {
                    continue;
                }

                int distanceBetween = DistanceBetween(start, mapSections[i]);
                if (distanceBetween < distance)
                {
                    distance = distanceBetween;
                    closestIndex = i;
                }
            }

            return closestIndex;
        }

        private int DistanceBetween(MapSection startMapSection, MapSection destinationMapSection)
        {
            return Math.Abs(
                startMapSection.Bounds.Center.X - destinationMapSection.Bounds.Center.X) +
                Math.Abs(startMapSection.Bounds.Center.Y - destinationMapSection.Bounds.Center.Y);
        }

        private void CreateExits(MapGenerator mapGenerator, CurrentMap map, int padding, string[] exitStrings)
        {
            DivisionAnalyzer divisionAnalyzer = new DivisionAnalyzer(map, padding, 5, 5);
            divisionMapSections = divisionAnalyzer.GetMapSections();

            int exitIndex = 0;
            foreach (string exitString in exitStrings)
            {
                if (exitIndex == 0 && exitString != "DEADEND")
                {
                    bool exitCreated = false;
                    for (int i = 0; i < divisionMapSections.Count; i++)
                    {
                        if (exitCreated)
                        {
                            break;
                        }

                        if (!divisionMapSections[i].HasExit)
                        {
                            foreach (ICell cell in divisionMapSections[i].Cells)
                            {
                                if (cell.IsWalkable && map.CountWallsNear(cell, 1) == 0)
                                {
                                    map.Exits.Add(mapGenerator.CreateExit(cell.X, cell.Y, exitString, true));
                                    divisionMapSections[i].HasExit = true;
                                    divisionMapSections[i].ExitPointX = cell.X;
                                    divisionMapSections[i].ExitPointY = cell.Y;
                                    exitCreated = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (exitIndex == exitStrings.Length - 1 && exitString != "DEADEND")
                {
                    bool exitCreated = false;
                    for (int i = divisionMapSections.Count - 1; i >= 0; i--)
                    {
                        if (exitCreated)
                        {
                            break;
                        }

                        if (!divisionMapSections[i].HasExit)
                        {
                            foreach (ICell cell in divisionMapSections[i].Cells)
                            {
                                if (cell.IsWalkable && map.CountWallsNear(cell, 1) == 0)
                                {
                                    map.Exits.Add(mapGenerator.CreateExit(cell.X, cell.Y, exitString, false));
                                    divisionMapSections[i].HasExit = true;
                                    divisionMapSections[i].ExitPointX = cell.X;
                                    divisionMapSections[i].ExitPointY = cell.Y;
                                    exitCreated = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (exitString != "DEADEND")
                {
                    MapSection randomSuitableMapLocation = map.GetRandomSuitableMapSection(divisionMapSections);
                    if (randomSuitableMapLocation != null)
                    {
                        foreach (ICell cell in randomSuitableMapLocation.Cells)
                        {
                            if (cell.IsWalkable && map.CountWallsNear(cell, 1) == 0)
                            {
                                map.Exits.Add(mapGenerator.CreateExit(cell.X, cell.Y, exitString, false));
                                randomSuitableMapLocation.HasExit = true;
                                randomSuitableMapLocation.ExitPointX = cell.X;
                                randomSuitableMapLocation.ExitPointY = cell.Y;
                                break;
                            }
                        }
                    }
                }

                exitIndex++;
            }
        }

        private void PlacePlayer(CurrentMap map)
        {
            Player player = Game.Singleton.Player;
            if (player == null)
            {
                player = new Player();
            }

            for (int i = 0; i < divisionMapSections.Count; i++)
            {
                if (divisionMapSections[i].HasExit)
                {
                    player.X = divisionMapSections[i].ExitPointX;
                    player.Y = divisionMapSections[i].ExitPointY;
                    break;
                }
            }

            map.AddPlayer(player);
        }

        private void PlaceMonsters(CurrentMap map)
        {
            int sectionIndex = 0;
            foreach (MapSection mapSection in divisionMapSections)
            {
                if (Dice.Roll("1D10") < 5)
                {
                    var numberOfMonsters = Dice.Roll("1D8");
                    for (int i = 0; i < numberOfMonsters; i++)
                    {
                        Point randomMapSectionLocation
                            = map.GetRandomWalkableLocationInMapSection(mapSection);
                        if (randomMapSectionLocation != null)
                        {
                            var monster = Kobold.Create(1);
                            monster.X = randomMapSectionLocation.X;
                            monster.Y = randomMapSectionLocation.Y;
                            map.AddMonster(monster);
                        }
                    }
                }

                sectionIndex++;
            }
        }

        #endregion
    }
}