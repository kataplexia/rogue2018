﻿namespace Rogue2018.Core.MapCore
{
    using PhiOS;
    using Rogue2018.Interfaces;
    using RogueSharp;
    using UnityEngine;

    public class Exit : IDrawable
    {
        #region Properties

        public string ExitToName
        {
            get;
            set;
        }

        public bool IsUp
        {
            get;
            set;
        }

        public Color DrawColor
        {
            get;
            set;
        }

        public Color BackgroundDrawColor
        {
            get;
            set;
        }

        public Color DrawColorFOV
        {
            get;
            set;
        }

        public Color BackgroundDrawColorFOV
        {
            get;
            set;
        }

        public string Symbol
        {
            get;
            set;
        }

        public string GlyphString
        {
            get;
            set;
        }

        public int X
        {
            get;
            set;
        }

        public int Y
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void Draw(int xOffset, int yOffset, IMap map)
        {
            if (!Game.Singleton.Tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
            {
                return;
            }

            if (!map.GetCell(X, Y).IsExplored)
            {
                return;
            }

            Symbol = IsUp ? Game.Singleton.Symbols.StairsUp : Game.Singleton.Symbols.StairsDown;
            GlyphString = IsUp ? Game.Singleton.GlyphStrings.StairsUp : Game.Singleton.GlyphStrings.StairsDown;

            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = IsUp ? Game.Singleton.Swatch.FloorBackgroundFOV * 0.8f : Color.clear;
                if (!Options.Singleton.DrawTiles)
                {
                    BackgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV * 0.8f;
                }

                DrawColorFOV = Game.Singleton.Swatch.StairsFOV;
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }
            }
            else
            {
                BackgroundDrawColor = IsUp ? Game.Singleton.Swatch.FloorBackground * 0.8f : Color.clear;
                if (!Options.Singleton.DrawTiles)
                {
                    BackgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackground * 0.8f;
                }

                DrawColor = Game.Singleton.Swatch.Stairs;
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }
        }

        #endregion
    }
}