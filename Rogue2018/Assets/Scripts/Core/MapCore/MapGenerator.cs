﻿namespace Rogue2018.Core.MapCore
{
    using RogueSharp;
    using UnityEngine;

    public class MapGenerator
    {
        #region Fields

        private readonly string mapName;
        private readonly string[] exitStrings;
        private readonly GenerationStyles generationStyle;
        private readonly Biomes biome;
        private readonly int mapWidth;
        private readonly int mapHeight;

        private readonly int maxRoomCount;
        private readonly int maxRoomSize;
        private readonly int minRoomSize;

        private readonly int fillProbability;
        private readonly int iterationCount;
        private readonly int bigAreaFillCutoff;
        private readonly bool transparentWalls;

        private readonly string mapRepresentationPath;
        private readonly string mapRepresentation;
        private readonly string[,] exitStringsAdvanced;
        private readonly Point startingPoint;

        private readonly CurrentMap map;
        private readonly int actNumber;

        #endregion

        #region Constructor

        public MapGenerator(int actNumber, string mapName)
        {
            this.actNumber = actNumber;
            MapProperties mapProperties = Game.Singleton.MapCollection.GetMapProperties(actNumber, mapName);
            this.mapName = mapProperties.MapName;
            exitStrings = mapProperties.ExitStrings;
            generationStyle = mapProperties.GenerationStyle;
            biome = mapProperties.Biome;
            mapWidth = mapProperties.MapWidth;
            mapHeight = mapProperties.MapHeight;

            if (generationStyle == GenerationStyles.RandomRooms)
            {
                maxRoomCount = mapProperties.MaxRoomCount;
                maxRoomSize = mapProperties.MaxRoomSize;
                minRoomSize = mapProperties.MinRoomSize;
            }
            else if (generationStyle == GenerationStyles.Cellular)
            {
                fillProbability = mapProperties.FillProbability;
                iterationCount = mapProperties.IterationCount;
                bigAreaFillCutoff = mapProperties.BigAreaFillCutoff;
                transparentWalls = mapProperties.TransparentWalls;
            }
            else if (generationStyle == GenerationStyles.FromString)
            {
                mapRepresentationPath = mapProperties.MapRepresentationPath;
                TextAsset textAsset = Resources.Load(mapRepresentationPath) as TextAsset;
                mapRepresentation = textAsset.text;
                exitStringsAdvanced = mapProperties.ExitStringsAdvanced;
                startingPoint = mapProperties.StartingPoint;
            }

            map = new CurrentMap();
        }

        #endregion

        #region Methods

        public CurrentMap GenerateMap()
        {
            switch (generationStyle)
            {
                case GenerationStyles.BorderOnly:

                    new BorderOnlyGenerator(this, map, mapWidth, mapHeight, exitStrings);
                    break;

                case GenerationStyles.RandomRooms:

                    new RandomRoomsGenerator(this, map, maxRoomCount, maxRoomSize, minRoomSize, mapWidth, mapHeight, exitStrings);
                    break;

                case GenerationStyles.Cellular:

                    new CellularGenerator(this, map, mapWidth, mapHeight, transparentWalls, fillProbability, iterationCount, bigAreaFillCutoff, exitStrings);
                    break;

                case GenerationStyles.FromString:

                    new FromStringGenerator(this, map, mapWidth, mapHeight, mapRepresentation, exitStringsAdvanced, startingPoint);
                    break;
            }

            Game.Singleton.MapCollection.StoreMap(actNumber, mapName, map);

            return map;
        }

        public void CreateGoalMaps()
        {
            map.MonsterGoalMap = new GoalMap(map, true);
            map.MonsterGoalMap.AddGoal(Game.Singleton.Player.X, Game.Singleton.Player.Y, 1);

            foreach (Monster monster in map.Monsters)
            {
                map.MonsterGoalMap.AddObstacle(monster.X, monster.Y);
            }

            map.MonsterGoalMap.ComputeCellWeightsLimited(map.GetCell(Game.Singleton.Player.X, Game.Singleton.Player.Y), 20);
        }

        public void CreateDoor(int x, int y)
        {
            map.SetCellProperties(x, y, false, true);
            map.Doors.Add(new Door { X = x, Y = y, IsOpen = false });
        }

        public bool IsPotentialDoor(CurrentMap map, RogueSharp.Cell rogueCell)
        {
            if (!rogueCell.IsWalkable)
            {
                return false;
            }

            RogueSharp.Cell rogueCellRight = map.GetCell(rogueCell.X + 1, rogueCell.Y) as RogueSharp.Cell;
            RogueSharp.Cell rogueCellLeft = map.GetCell(rogueCell.X - 1, rogueCell.Y) as RogueSharp.Cell;
            RogueSharp.Cell rogueCellUp = map.GetCell(rogueCell.X, rogueCell.Y - 1) as RogueSharp.Cell;
            RogueSharp.Cell rogueCellDown = map.GetCell(rogueCell.X, rogueCell.Y + 1) as RogueSharp.Cell;

            if (map.GetDoorAt(rogueCell.X, rogueCell.Y) != null ||
                map.GetDoorAt(rogueCellRight.X, rogueCellRight.Y) != null ||
                map.GetDoorAt(rogueCellLeft.X, rogueCellLeft.Y) != null ||
                map.GetDoorAt(rogueCellUp.X, rogueCellUp.Y) != null ||
                map.GetDoorAt(rogueCellDown.X, rogueCellDown.Y) != null)
            {
                return false;
            }

            if (rogueCellRight.IsWalkable &&
                rogueCellLeft.IsWalkable &&
                !rogueCellUp.IsWalkable &&
                !rogueCellDown.IsWalkable)
            {
                return true;
            }

            if (!rogueCellRight.IsWalkable &&
                !rogueCellLeft.IsWalkable &&
                rogueCellUp.IsWalkable &&
                rogueCellDown.IsWalkable)
            {
                return true;
            }

            return false;
        }

        public Exit CreateExit(int x, int y, string exitString, bool isUp)
        {
            return new Exit
            {
                X = x,
                Y = y,
                ExitToName = exitString,
                IsUp = isUp
            };
        }

        public void SetBitmaskForMap(int padding = 0)
        {
            foreach (RogueSharp.Cell rogueCell in map.GetAllCells())
            {
                map.SetBitmaskValue(rogueCell.X, rogueCell.Y, CreateBitmaskValue(rogueCell.X, rogueCell.Y, padding));
            }
        }

        public int CreateBitmaskValue(int x, int y, int padding)
        {
            bool aNotWalkable = true;
            bool bNotWalkable = true;
            bool cNotWalkable = true;
            bool dNotWalkable = true;

            if (y > 0)
            {
                aNotWalkable = !map.IsWalkable(x, y - 1);
            }

            if (x < mapWidth + padding - 1)
            {
                bNotWalkable = !map.IsWalkable(x + 1, y);
            }

            if (y < mapHeight + padding - 1)
            {
                cNotWalkable = !map.IsWalkable(x, y + 1);
            }

            if (x > 0)
            {
                dNotWalkable = !map.IsWalkable(x - 1, y);
            }

            int a = 1 * aNotWalkable.GetHashCode();
            int b = 2 * bNotWalkable.GetHashCode();
            int c = 4 * cNotWalkable.GetHashCode();
            int d = 8 * dNotWalkable.GetHashCode();

            return d + c + b + a;
        }

        public void SetCellDrawPropertiesForMap()
        {
            float colorRandomModifier = 0.825f;
            foreach (RogueSharp.Cell rogueCell in map.GetAllCells())
            {
                if (rogueCell.IsWalkable)
                {
                    Color drawColorFOV = Game.Singleton.Swatch.FloorFOV;
                    float randomDCFOV_R = UnityEngine.Random.Range(drawColorFOV.r, drawColorFOV.r * colorRandomModifier);
                    float randomDCFOV_G = UnityEngine.Random.Range(drawColorFOV.g, drawColorFOV.g * colorRandomModifier);
                    float randomDCFOV_B = UnityEngine.Random.Range(drawColorFOV.b, drawColorFOV.b * colorRandomModifier);

                    float randomDCFOV_R_Dif = drawColorFOV.r - randomDCFOV_R;
                    float randomDCFOV_G_Dif = drawColorFOV.g - randomDCFOV_G;
                    float randomDCFOV_B_Dif = drawColorFOV.b - randomDCFOV_B;

                    drawColorFOV = new Color(randomDCFOV_R, randomDCFOV_G, randomDCFOV_B);

                    Color backgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV;
                    float randomBGDCFOV_R = UnityEngine.Random.Range(backgroundDrawColorFOV.r, backgroundDrawColorFOV.r * colorRandomModifier);
                    float randomBGDCFOV_G = UnityEngine.Random.Range(backgroundDrawColorFOV.g, backgroundDrawColorFOV.g * colorRandomModifier);
                    float randomBGDCFOV_B = UnityEngine.Random.Range(backgroundDrawColorFOV.b, backgroundDrawColorFOV.b * colorRandomModifier);

                    float randomBGDCFOV_R_Dif = backgroundDrawColorFOV.r - randomBGDCFOV_R;
                    float randomBGDCFOV_G_Dif = backgroundDrawColorFOV.g - randomBGDCFOV_G;
                    float randomBGDCFOV_B_Dif = backgroundDrawColorFOV.b - randomBGDCFOV_B;

                    backgroundDrawColorFOV = new Color(randomBGDCFOV_R, randomBGDCFOV_G, randomBGDCFOV_B);

                    Color drawColor = Game.Singleton.Swatch.Floor;
                    float randomDC_R = drawColor.r - randomDCFOV_R_Dif;
                    float randomDC_G = drawColor.g - randomDCFOV_G_Dif;
                    float randomDC_B = drawColor.b - randomDCFOV_B_Dif;
                    drawColor = new Color(randomDC_R, randomDC_G, randomDC_B);

                    Color backgroundDrawColor = Game.Singleton.Swatch.FloorBackground;
                    float randomBGDC_R = backgroundDrawColor.r - randomBGDCFOV_R_Dif;
                    float randomBGDC_G = backgroundDrawColor.g - randomBGDCFOV_G_Dif;
                    float randomBGDC_B = backgroundDrawColor.b - randomBGDCFOV_B_Dif;
                    backgroundDrawColor = new Color(randomBGDC_R, randomBGDC_G, randomBGDC_B);

                    string symbol = Game.Singleton.Symbols.Floor;
                    string glyphString = Game.Singleton.GlyphStrings.Floor;

                    if (biome == Biomes.Cave)
                    {
                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Cave_Floors.Length - 1);
                        symbol = Game.Singleton.Symbols.Cave_Floors[randomIndex];
                    }
                    else if (biome == Biomes.Dungeon)
                    {
                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Dungeon_Floors.Length - 1);
                        symbol = Game.Singleton.Symbols.Dungeon_Floors[randomIndex];
                    }
                    else if (biome == Biomes.Forest)
                    {
                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Forest_Floors.Length - 1);
                        symbol = Game.Singleton.Symbols.Forest_Floors[randomIndex];
                    }
                    else if (biome == Biomes.Swamp)
                    {
                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Swamp_Floors.Length - 1);
                        symbol = Game.Singleton.Symbols.Swamp_Floors[randomIndex];
                    }

                    map.SetDrawColor(rogueCell.X, rogueCell.Y, drawColor);
                    map.SetBackgroundDrawColor(rogueCell.X, rogueCell.Y, backgroundDrawColor);
                    map.SetDrawColorFOV(rogueCell.X, rogueCell.Y, drawColorFOV);
                    map.SetBackgroundDrawColorFOV(rogueCell.X, rogueCell.Y, backgroundDrawColorFOV);

                    map.SetGlyphString(rogueCell.X, rogueCell.Y, glyphString);
                    map.SetSymbol(rogueCell.X, rogueCell.Y, symbol);

                    map.SetMiniMapDrawColor(rogueCell.X, rogueCell.Y, Game.Singleton.Swatch.MiniMapFloor);
                    map.SetMiniMapDrawColorFOV(rogueCell.X, rogueCell.Y, Game.Singleton.Swatch.MiniMapFloorFOV);
                }
                else
                {
                    Color drawColorFOV = Game.Singleton.Swatch.WallFOV;
                    float randomDCFOV_R = UnityEngine.Random.Range(drawColorFOV.r, drawColorFOV.r * colorRandomModifier);
                    float randomDCFOV_G = UnityEngine.Random.Range(drawColorFOV.g, drawColorFOV.g * colorRandomModifier);
                    float randomDCFOV_B = UnityEngine.Random.Range(drawColorFOV.b, drawColorFOV.b * colorRandomModifier);

                    float randomDCFOV_R_Dif = (drawColorFOV.r - randomDCFOV_R) * 0.25f;
                    float randomDCFOV_G_Dif = (drawColorFOV.g - randomDCFOV_G) * 0.25f;
                    float randomDCFOV_B_Dif = (drawColorFOV.b - randomDCFOV_B) * 0.25f;

                    drawColorFOV = new Color(randomDCFOV_R, randomDCFOV_G, randomDCFOV_B);

                    Color backgroundDrawColorFOV = Game.Singleton.Swatch.WallBackgroundFOV;
                    float randomBGDCFOV_R = UnityEngine.Random.Range(backgroundDrawColorFOV.r, backgroundDrawColorFOV.r * colorRandomModifier);
                    float randomBGDCFOV_G = UnityEngine.Random.Range(backgroundDrawColorFOV.g, backgroundDrawColorFOV.g * colorRandomModifier);
                    float randomBGDCFOV_B = UnityEngine.Random.Range(backgroundDrawColorFOV.b, backgroundDrawColorFOV.b * colorRandomModifier);

                    float randomBGDCFOV_R_Dif = (backgroundDrawColorFOV.r - randomBGDCFOV_R) * 0.25f;
                    float randomBGDCFOV_G_Dif = (backgroundDrawColorFOV.g - randomBGDCFOV_G) * 0.25f;
                    float randomBGDCFOV_B_Dif = (backgroundDrawColorFOV.b - randomBGDCFOV_B) * 0.25f;

                    backgroundDrawColorFOV = new Color(randomBGDCFOV_R, randomBGDCFOV_G, randomBGDCFOV_B);

                    Color drawColor = Game.Singleton.Swatch.Wall;
                    float randomDC_R = drawColor.r - randomDCFOV_R_Dif;
                    float randomDC_G = drawColor.g - randomDCFOV_G_Dif;
                    float randomDC_B = drawColor.b - randomDCFOV_B_Dif;
                    drawColor = new Color(randomDC_R, randomDC_G, randomDC_B);

                    Color backgroundDrawColor = Game.Singleton.Swatch.WallBackground;
                    float randomBGDC_R = backgroundDrawColor.r - randomBGDCFOV_R_Dif;
                    float randomBGDC_G = backgroundDrawColor.g - randomBGDCFOV_G_Dif;
                    float randomBGDC_B = backgroundDrawColor.b - randomBGDCFOV_B_Dif;
                    backgroundDrawColor = new Color(randomBGDC_R, randomBGDC_G, randomBGDC_B);

                    string symbol = Game.Singleton.Symbols.Wall;
                    Biomes biome = Game.Singleton.MapCollection.GetBiome(Game.Singleton.ActNumber, Game.Singleton.MapName);
                    if (biome == Biomes.Forest)
                    {
                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Trees.Length - 1);
                        symbol = Game.Singleton.Symbols.Trees[randomIndex];
                    }
                    else if (biome == Biomes.Swamp)
                    {
                        symbol = Game.Singleton.Symbols.Water;
                    }

                    string glyphString = Game.Singleton.GlyphStrings.Wall;
                    if (glyphString[glyphString.Length - 1] == '_')
                    {
                        glyphString += map.BitmaskValue(rogueCell.X, rogueCell.Y).ToString();
                    }

                    map.SetDrawColor(rogueCell.X, rogueCell.Y, drawColor);
                    map.SetBackgroundDrawColor(rogueCell.X, rogueCell.Y, backgroundDrawColor);
                    map.SetDrawColorFOV(rogueCell.X, rogueCell.Y, drawColorFOV);
                    map.SetBackgroundDrawColorFOV(rogueCell.X, rogueCell.Y, backgroundDrawColorFOV);

                    map.SetGlyphString(rogueCell.X, rogueCell.Y, glyphString);
                    map.SetSymbol(rogueCell.X, rogueCell.Y, symbol);

                    map.SetMiniMapDrawColor(rogueCell.X, rogueCell.Y, Game.Singleton.Swatch.MiniMapWall);
                    map.SetMiniMapDrawColorFOV(rogueCell.X, rogueCell.Y, Game.Singleton.Swatch.MiniMapWallFOV);
                }
            }
        }

        #endregion
    }
}