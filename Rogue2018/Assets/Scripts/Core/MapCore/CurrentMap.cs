﻿namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using System.Linq;
    using PhiOS;
    using Rogue2018.Actors;
    using RogueSharp;
    using UnityEngine;

    public class CurrentMap : Map
    {
        #region Fields

        private GoalMap monsterGoalMap;

        #endregion

        #region Constructor

        public CurrentMap()
        {
            Monsters = new List<Monster>();
            Rooms = new List<Rectangle>();
            Doors = new List<Door>();
            Exits = new List<Exit>();
            Game.Singleton.SchedulingSystem.Clear();
        }

        #endregion

        #region Properties

        public List<Door> Doors
        {
            get;
            set;
        }

        public List<Exit> Exits
        {
            get;
            set;
        }

        public bool IsMapExplored
        {
            get;
            set;
        }

        public GoalMap MonsterGoalMap
        {
            get { return monsterGoalMap; }
            set { monsterGoalMap = value; }
        }

        public List<Monster> Monsters
        {
            get;
            set;
        }

        public List<Rectangle> Rooms
        {
            get; set;
        }

        #endregion

        #region Methods

        public void AddMonster(Monster monster)
        {
            Monsters.Add(monster);
            SetIsWalkable(monster.X, monster.Y, false);
            Game.Singleton.SchedulingSystem.Add(monster);
        }

        public void AddMonstersToSchedule()
        {
            foreach (Monster monster in Monsters)
            {
                Game.Singleton.SchedulingSystem.Add(monster);
            }
        }

        public void AddPlayer(Player player)
        {
            Game.Singleton.Player = player;
            SetIsWalkable(player.X, player.Y, false);
            Game.Singleton.SchedulingSystem.Add(player);
        }

        public bool AreMonstersAlerted()
        {
            foreach (Monster monster in Monsters)
            {
                if (monster.TurnsAlerted.HasValue)
                {
                    return true;
                }
            }

            return false;
        }

        public bool AreMonstersInFOV()
        {
            foreach (Monster monster in Monsters)
            {
                if (IsInFOV(monster.X, monster.Y))
                {
                    return true;
                }
            }

            return false;
        }

        public bool CanMoveDownStairs()
        {
            if (OnExit() == null)
            {
                return false;
            }

            if (OnExit().IsUp)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CanMoveUpStairs()
        {
            if (OnExit() == null)
            {
                return false;
            }

            if (OnExit().IsUp)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int CountWallsNear(ICell cell, int distance)
        {
            int count = 0;
            foreach (ICell nearbyCell in GetCellsInSquare(cell.X, cell.Y, distance))
            {
                if (nearbyCell.X == cell.X && nearbyCell.Y == cell.Y)
                {
                    continue;
                }

                if (!nearbyCell.IsWalkable)
                {
                    count++;
                }
            }

            return count;
        }

        public void DestroyHealthBars()
        {
            foreach (Monster monster in Monsters)
            {
                if (monster.HealthBar != null)
                {
                    monster.HealthBar.DestroyHealthBar();
                    monster.HealthBar = null;
                }
            }
        }

        public bool DoesMapSectionHaveWalkableSpace(MapSection mapSection)
        {
            for (int x = mapSection.Bounds.Left; x < mapSection.Bounds.Right - 1; x++)
            {
                for (int y = mapSection.Bounds.Top; y < mapSection.Bounds.Bottom - 1; y++)
                {
                    if (IsWalkable(x, y))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool DoesRoomHaveWalkableSpace(Rectangle room)
        {
            for (int x = 1; x <= room.Width - 2; x++)
            {
                for (int y = 1; y <= room.Height - 2; y++)
                {
                    if (IsWalkable(x + room.X, y + room.Y))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void Draw(int xOffset, int yOffset)
        {
            int drawWidth = (int)PhiDisplay.Singleton.zoomDisplayRects[PhiDisplay.Singleton.currentZoomLevel].width;
            int drawHeight = (int)PhiDisplay.Singleton.zoomDisplayRects[PhiDisplay.Singleton.currentZoomLevel].height;
            IEnumerable<ICell> cellsToDraw = GetCellsInRect(Game.Singleton.Player.X, Game.Singleton.Player.Y, drawWidth, drawHeight, true);

            foreach (ICell cell in cellsToDraw)
            {
                DrawMapCell(cell as RogueSharp.Cell, xOffset, yOffset);
            }

            foreach (Door door in Doors)
            {
                door.Draw(xOffset, yOffset, this);
            }

            foreach (Exit exit in Exits)
            {
                exit.Draw(xOffset, yOffset, this);
            }

            foreach (Monster monster in Monsters)
            {
                monster.Draw(xOffset, yOffset, this);
            }
        }

        public void DrawHealthBars(int xOffset, int yOffset)
        {
            foreach (Monster monster in Monsters)
            {
                if (IsInFOV(monster.X, monster.Y))
                {
                    monster.DrawHealthBar(xOffset, yOffset);
                }
                else if (monster.HealthBar != null)
                {
                    if (monster.HealthBar.IsEnabled)
                    {
                        monster.HealthBar.IsEnabled = false;
                    }
                }
            }
        }

        public Door GetDoorAt(int x, int y)
        {
            return Doors.SingleOrDefault(d => d.X == x && d.Y == y);
        }

        public Exit GetExitAt(int x, int y)
        {
            return Exits.SingleOrDefault(e => e.X == x && e.Y == y);
        }

        public Monster GetMonsterAt(int x, int y)
        {
            return Monsters.FirstOrDefault(m => m.X == x && m.Y == y);
        }

        public MapSection GetRandomSuitableMapSection(List<MapSection> mapSections)
        {
            int randomIndex = Game.Singleton.Random.Next(0, mapSections.Count - 1);

            if (mapSections[randomIndex].HasExit)
            {
                return GetRandomSuitableMapSection(mapSections);
            }

            foreach (ICell cell in mapSections[randomIndex].Cells)
            {
                if (cell.IsWalkable && CountWallsNear(cell, 1) == 0)
                {
                    return mapSections[randomIndex];
                }
            }

            return GetRandomSuitableMapSection(mapSections);
        }

        public Point GetRandomWalkableLocationInMap()
        {
            for (int i = 0; i < 100; i++)
            {
                int x = Game.Singleton.Random.Next(1, Width - 2);
                int y = Game.Singleton.Random.Next(1, Height - 2);

                if (IsWalkable(x, y))
                {
                    return new Point(x, y);
                }
            }

            return null;
        }

        public Point GetRandomWalkableLocationInMapSection(MapSection mapSection)
        {
            if (DoesMapSectionHaveWalkableSpace(mapSection))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = Game.Singleton.Random.Next(mapSection.Bounds.Left, mapSection.Bounds.Right - 1);
                    int y = Game.Singleton.Random.Next(mapSection.Bounds.Top, mapSection.Bounds.Bottom - 1);

                    if (IsWalkable(x, y))
                    {
                        return new Point(x, y);
                    }
                }
            }

            return null;
        }

        public Point GetRandomWalkableLocationInRoom(Rectangle room)
        {
            if (DoesRoomHaveWalkableSpace(room))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = Game.Singleton.Random.Next(1, room.Width - 2) + room.X;
                    int y = Game.Singleton.Random.Next(1, room.Height - 2) + room.Y;

                    if (IsWalkable(x, y))
                    {
                        return new Point(x, y);
                    }
                }
            }

            return null;
        }

        public void HideHealthBars()
        {
            foreach (Monster monster in Monsters)
            {
                if (monster.HealthBar != null)
                {
                    if (monster.HealthBar.IsEnabled)
                    {
                        monster.HealthBar.IsEnabled = false;
                    }
                }
            }
        }

        public bool IsBorderCell(ICell cell, int padding)
        {
            return cell.X <= (int)(padding / 2) ||
                   cell.X >= Width - 1 - (int)(padding / 2) ||
                   cell.Y <= (int)(padding / 2) ||
                   cell.Y >= Height - 1 - (int)(padding / 2);
        }

        public Exit OnExit()
        {
            Player player = Game.Singleton.Player;
            foreach (Exit exit in Exits)
            {
                if (exit.X == player.X && exit.Y == player.Y)
                {
                    return exit;
                }
            }

            return null;
        }

        public void OpenDoor(Actor actor, int x, int y)
        {
            Door door = GetDoorAt(x, y);
            if (door != null && !door.IsOpen)
            {
                door.IsOpen = true;
                ICell rogueCell = GetCell(x, y);

                SetCellProperties(x, y, true, rogueCell.IsWalkable, rogueCell.IsExplored);

                Audio.Singleton.QueueAudio(Audio.Singleton.PlayerOpenDoor);
            }
        }

        public void RemoveMonster(Monster monster)
        {
            Monsters.Remove(monster);
            SetIsWalkable(monster.X, monster.Y, true);
            Game.Singleton.SchedulingSystem.Remove(monster);
        }

        public void RemoveMonstersFromSchedule()
        {
            foreach (Monster monster in Monsters)
            {
                Game.Singleton.SchedulingSystem.Remove(monster);
            }
        }

        public bool SetActorPosition(Actor actor, int x, int y)
        {
            if (actor is Player && x == actor.X && y == actor.Y)
            {
                UpdateMonsterGoalMap();
                return true;
            }

            if (GetCell(x, y).IsWalkable)
            {
                SetIsWalkable(actor.X, actor.Y, true);

                actor.X = x;
                actor.Y = y;

                if (actor is Player)
                {
                    UpdateMonsterGoalMap();
                }

                SetIsWalkable(actor.X, actor.Y, false);
                OpenDoor(actor, x, y);

                if (actor is Player)
                {
                    Audio.Singleton.QueueAudio(Audio.Singleton.PlayerMove);
                    UpdatePlayerFOV();
                }

                return true;
            }

            return false;
        }

        public void SetIsWalkable(int x, int y, bool isWalkable)
        {
            ICell rogueCell = GetCell(x, y);
            SetCellProperties(rogueCell.X, rogueCell.Y, rogueCell.IsTransparent, isWalkable, rogueCell.IsExplored);
        }

        public void UpdateMap(IMap source)
        {
            foreach (ICell sourceCell in source.GetAllCells())
            {
                SetCellProperties(sourceCell.X, sourceCell.Y, sourceCell.IsTransparent, sourceCell.IsWalkable, sourceCell.IsExplored);
            }
        }

        public void UpdateMonsterGoalMap()
        {
            MonsterGoalMap.ClearGoals();
            MonsterGoalMap.AddGoal(Game.Singleton.Player.X, Game.Singleton.Player.Y, 1);

            MonsterGoalMap.ClearObstacles();
            foreach (Monster monster in Monsters)
            {
                MonsterGoalMap.AddObstacle(monster.X, monster.Y);
            }

            if (Game.Singleton.CurrentMap.AreMonstersAlerted())
            {
                MonsterGoalMap.ComputeCellWeightsLimited(GetCell(Game.Singleton.Player.X, Game.Singleton.Player.Y), 20);
            }
        }

        public void UpdatePlayerFOV()
        {
            Player player = Game.Singleton.Player;
            ComputeFOV(player.X, player.Y, player.Awareness, true);

            IEnumerable<ICell> awarenessCells = GetCellsInDiamond(player.X, player.Y, player.Awareness);
            foreach (ICell cell in awarenessCells)
            {
                if (IsInFOV(cell.X, cell.Y))
                {
                    SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
                }
            }
        }

        private void DrawMapCell(RogueSharp.Cell rogueCell, int xOffset, int yOffset)
        {
            if (!rogueCell.IsExplored)
            {
                return;
            }

            if (IsInFOV(rogueCell.X, rogueCell.Y))
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(0, rogueCell.X + xOffset, rogueCell.Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString(rogueCell.X, rogueCell.Y) : Symbol(rogueCell.X, rogueCell.Y);
                    Color drawColor = DrawColorFOV(rogueCell.X, rogueCell.Y);
                    Color backgroundDrawColor = BackgroundDrawColorFOV(rogueCell.X, rogueCell.Y);
                    if (!rogueCell.IsWalkable && !Options.Singleton.DrawTiles)
                    {
                        string symbol = Symbol(rogueCell.X, rogueCell.Y);
                        if (symbol == "#" || symbol == "≈")
                        {
                            backgroundDrawColor = drawColor * 0.5f;
                        }
                    }

                    phiCell.SetContent(phiCellContent, backgroundDrawColor, drawColor);
                }
            }
            else
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(0, rogueCell.X + xOffset, rogueCell.Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.Singleton.DrawTiles ? GlyphString(rogueCell.X, rogueCell.Y) : Symbol(rogueCell.X, rogueCell.Y);
                    Color drawColor = DrawColor(rogueCell.X, rogueCell.Y);
                    Color backgroundDrawColor = BackgroundDrawColor(rogueCell.X, rogueCell.Y);
                    if (!rogueCell.IsWalkable && !Options.Singleton.DrawTiles)
                    {
                        string symbol = Symbol(rogueCell.X, rogueCell.Y);
                        if (symbol == "#" || symbol == "≈")
                        {
                            backgroundDrawColor = drawColor * 0.5f;
                        }
                    }

                    phiCell.SetContent(phiCellContent, backgroundDrawColor, drawColor);
                }
            }
        }

        #endregion
    }
}