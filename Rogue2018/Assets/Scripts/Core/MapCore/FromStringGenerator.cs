﻿namespace Rogue2018.Core.MapCore
{
    using Rogue2018.Actors;
    using RogueSharp;
    using UnityEngine;

    public class FromStringGenerator
    {
        #region Fields

        #endregion

        #region Constructor

        public FromStringGenerator(
            MapGenerator mapGenerator,
            CurrentMap map,
            int mapWidth,
            int mapHeight,
            string mapRepresentation,
            string[,] exitStringsAdvanced,
            Point startingPoint)
        {
            map.Initialize(mapWidth, mapHeight);
            CreateMap(mapGenerator, map, mapWidth, mapHeight, mapRepresentation, exitStringsAdvanced);
            PlacePlayer(map, startingPoint);
            mapGenerator.CreateGoalMaps();
        }

        #endregion

        #region Methods

        private void CreateMap(MapGenerator mapGenerator, CurrentMap map, int mapWidth, int mapHeight, string mapRepresentation, string[,] exitStringsAdvanced)
        {
            string[] lines = mapRepresentation.Replace(" ", string.Empty).Replace("\r", string.Empty).Split('\n');

            for (int y = 0; y < mapHeight; y++)
            {
                float colorRandomModifier = 0.825f;
                float differenceModifier = 1.0f;

                string line = lines[y];
                for (int x = 0; x < mapWidth; x++)
                {
                    Color drawColor = Color.clear;
                    Color backgroundDrawColor = Color.clear;
                    Color drawColorFOV = Color.clear;
                    Color backgroundDrawColorFOV = Color.clear;

                    Color miniMapDrawColor = Color.clear;
                    Color miniMapDrawColorFOV = Color.clear;

                    string symbol = string.Empty;
                    string glyphString = string.Empty;

                    if (line[x] == '.')
                    {
                        map.SetCellProperties(x, y, true, true);

                        drawColor = Game.Singleton.Swatch.Floor_Dungeon;
                        backgroundDrawColor = Game.Singleton.Swatch.FloorBackground_Dungeon;
                        drawColorFOV = Game.Singleton.Swatch.FloorFOV_Dungeon;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV_Dungeon;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapFloor_Dungeon;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapFloorFOV_Dungeon;

                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Dungeon_Floors.Length - 1);
                        symbol = Game.Singleton.Symbols.Dungeon_Floors[randomIndex];

                        glyphString = Game.Singleton.GlyphStrings.Floor_Dungeon;
                    }
                    else if (line[x] == ',')
                    {
                        map.SetCellProperties(x, y, true, true);

                        drawColor = Game.Singleton.Swatch.Floor_Forest;
                        backgroundDrawColor = Game.Singleton.Swatch.FloorBackground_Forest;
                        drawColorFOV = Game.Singleton.Swatch.FloorFOV_Forest;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV_Forest;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapFloor_Forest;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapFloorFOV_Forest;

                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Forest_Floors.Length - 1);
                        symbol = Game.Singleton.Symbols.Forest_Floors[randomIndex];

                        glyphString = Game.Singleton.GlyphStrings.Floor_Forest;
                    }
                    else if (line[x] == '+')
                    {
                        mapGenerator.CreateDoor(x, y);

                        backgroundDrawColor = Game.Singleton.Swatch.FloorBackground_Dungeon;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV_Dungeon;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapFloor_Dungeon;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapFloorFOV_Dungeon;
                    }
                    else if (line[x] == '≈')
                    {
                        map.SetCellProperties(x, y, true, false);

                        drawColor = Game.Singleton.Swatch.Wall_Swamp;
                        backgroundDrawColor = Game.Singleton.Swatch.WallBackground_Swamp;
                        drawColorFOV = Game.Singleton.Swatch.WallFOV_Swamp;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.WallBackgroundFOV_Swamp;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapWall_Swamp;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapWallFOV_Swamp;

                        symbol = Game.Singleton.Symbols.Water;

                        differenceModifier = 0.25f;
                    }
                    else if (line[x] == '#')
                    {
                        map.SetCellProperties(x, y, false, false);

                        drawColor = Game.Singleton.Swatch.Wall_Dungeon;
                        backgroundDrawColor = Game.Singleton.Swatch.WallBackground_Dungeon;
                        drawColorFOV = Game.Singleton.Swatch.WallFOV_Dungeon;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.WallBackgroundFOV_Dungeon;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapWall_Dungeon;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapWallFOV_Dungeon;

                        symbol = Game.Singleton.Symbols.Wall;

                        differenceModifier = 0.25f;
                    }
                    else if (line[x] == 'T')
                    {
                        map.SetCellProperties(x, y, false, false);

                        drawColor = Game.Singleton.Swatch.Wall_Forest;
                        backgroundDrawColor = Game.Singleton.Swatch.WallBackground_Forest;
                        drawColorFOV = Game.Singleton.Swatch.WallFOV_Forest;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.WallBackgroundFOV_Forest;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapWall_Forest;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapWallFOV_Forest;

                        int randomIndex = Game.Singleton.Random.Next(0, Game.Singleton.Symbols.Trees.Length - 1);
                        symbol = Game.Singleton.Symbols.Trees[randomIndex];

                        differenceModifier = 0.25f;
                    }
                    else if (line[x] == '<')
                    {
                        map.SetCellProperties(x, y, true, true);
                        map.Exits.Add(mapGenerator.CreateExit(x, y, exitStringsAdvanced[x, y], true));

                        backgroundDrawColor = Game.Singleton.Swatch.FloorBackground_Dungeon;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV_Dungeon;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapFloor_Dungeon;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapFloorFOV_Dungeon;
                    }
                    else if (line[x] == '>')
                    {
                        map.SetCellProperties(x, y, true, true);
                        map.Exits.Add(mapGenerator.CreateExit(x, y, exitStringsAdvanced[x, y], false));

                        backgroundDrawColor = Game.Singleton.Swatch.FloorBackground_Dungeon;
                        backgroundDrawColorFOV = Game.Singleton.Swatch.FloorBackgroundFOV_Dungeon;

                        miniMapDrawColor = Game.Singleton.Swatch.MiniMapFloor_Dungeon;
                        miniMapDrawColorFOV = Game.Singleton.Swatch.MiniMapFloorFOV_Dungeon;
                    }

                    float randomDCFOV_R = UnityEngine.Random.Range(drawColorFOV.r, drawColorFOV.r * colorRandomModifier);
                    float randomDCFOV_G = UnityEngine.Random.Range(drawColorFOV.g, drawColorFOV.g * colorRandomModifier);
                    float randomDCFOV_B = UnityEngine.Random.Range(drawColorFOV.b, drawColorFOV.b * colorRandomModifier);

                    float randomDCFOV_R_Dif = (drawColorFOV.r - randomDCFOV_R) * differenceModifier;
                    float randomDCFOV_G_Dif = (drawColorFOV.g - randomDCFOV_G) * differenceModifier;
                    float randomDCFOV_B_Dif = (drawColorFOV.b - randomDCFOV_B) * differenceModifier;

                    drawColorFOV = new Color(randomDCFOV_R, randomDCFOV_G, randomDCFOV_B);

                    float randomBGDCFOV_R = UnityEngine.Random.Range(backgroundDrawColorFOV.r, backgroundDrawColorFOV.r * colorRandomModifier);
                    float randomBGDCFOV_G = UnityEngine.Random.Range(backgroundDrawColorFOV.g, backgroundDrawColorFOV.g * colorRandomModifier);
                    float randomBGDCFOV_B = UnityEngine.Random.Range(backgroundDrawColorFOV.b, backgroundDrawColorFOV.b * colorRandomModifier);

                    float randomBGDCFOV_R_Dif = (backgroundDrawColorFOV.r - randomBGDCFOV_R) * differenceModifier;
                    float randomBGDCFOV_G_Dif = (backgroundDrawColorFOV.g - randomBGDCFOV_G) * differenceModifier;
                    float randomBGDCFOV_B_Dif = (backgroundDrawColorFOV.b - randomBGDCFOV_B) * differenceModifier;

                    backgroundDrawColorFOV = new Color(randomBGDCFOV_R, randomBGDCFOV_G, randomBGDCFOV_B);

                    float randomDC_R = drawColor.r - randomDCFOV_R_Dif;
                    float randomDC_G = drawColor.g - randomDCFOV_G_Dif;
                    float randomDC_B = drawColor.b - randomDCFOV_B_Dif;
                    drawColor = new Color(randomDC_R, randomDC_G, randomDC_B);

                    float randomBGDC_R = backgroundDrawColor.r - randomBGDCFOV_R_Dif;
                    float randomBGDC_G = backgroundDrawColor.g - randomBGDCFOV_G_Dif;
                    float randomBGDC_B = backgroundDrawColor.b - randomBGDCFOV_B_Dif;
                    backgroundDrawColor = new Color(randomBGDC_R, randomBGDC_G, randomBGDC_B);

                    map.SetDrawColor(x, y, drawColor);
                    map.SetBackgroundDrawColor(x, y, backgroundDrawColor);
                    map.SetDrawColorFOV(x, y, drawColorFOV);
                    map.SetBackgroundDrawColorFOV(x, y, backgroundDrawColorFOV);

                    map.SetMiniMapDrawColor(x, y, miniMapDrawColor);
                    map.SetMiniMapDrawColorFOV(x, y, miniMapDrawColorFOV);

                    if (glyphString != string.Empty)
                    {
                        map.SetGlyphString(x, y, glyphString);
                    }

                    map.SetSymbol(x, y, symbol);
                }
            }

            int padding = 0;
            foreach (RogueSharp.Cell rogueCell in map.GetAllCells())
            {
                map.SetBitmaskValue(rogueCell.X, rogueCell.Y, mapGenerator.CreateBitmaskValue(rogueCell.X, rogueCell.Y, padding));
            }

            for (int y = 0; y < mapHeight; y++)
            {
                string line = lines[y];
                for (int x = 0; x < mapWidth; x++)
                {
                    string glyphString = string.Empty;

                    if (line[x] == '≈')
                    {
                        glyphString = Game.Singleton.GlyphStrings.Wall_Swamp;
                        if (glyphString[glyphString.Length - 1] == '_')
                        {
                            glyphString += map.BitmaskValue(x, y).ToString();
                        }
                    }
                    else if (line[x] == '#')
                    {
                        glyphString = Game.Singleton.GlyphStrings.Wall_Dungeon;
                        if (glyphString[glyphString.Length - 1] == '_')
                        {
                            glyphString += map.BitmaskValue(x, y).ToString();
                        }
                    }
                    else if (line[x] == 'T')
                    {
                        glyphString = Game.Singleton.GlyphStrings.Wall_Forest;
                        if (glyphString[glyphString.Length - 1] == '_')
                        {
                            glyphString += map.BitmaskValue(x, y).ToString();
                        }
                    }

                    if (glyphString != string.Empty)
                    {
                        map.SetGlyphString(x, y, glyphString);
                    }
                }
            }
        }

        private void PlacePlayer(CurrentMap map, Point startingPoint)
        {
            Player player = Game.Singleton.Player;
            if (player == null)
            {
                player = new Player();
            }

            player.X = startingPoint.X;
            player.Y = startingPoint.Y;

            map.AddPlayer(player);
        }

        #endregion
    }
}