﻿namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using System.Linq;
    using Rogue2018.Actors;
    using RogueSharp;

    public class BorderOnlyGenerator
    {
        #region Constructor

        public BorderOnlyGenerator(MapGenerator mapGenerator, CurrentMap map, int mapWidth, int mapHeight, string[] exitStrings)
        {
            map.Initialize(mapWidth, mapHeight);
            CreateMap(map, mapWidth, mapHeight);
            CreateExits(mapGenerator, map, mapWidth, mapHeight, exitStrings);
            mapGenerator.SetBitmaskForMap();
            mapGenerator.SetCellDrawPropertiesForMap();
            PlacePlayer(map);
            //// PlaceMonsters();
            mapGenerator.CreateGoalMaps();
        }

        #endregion

        #region Methods

        private void CreateMap(CurrentMap map, int mapWidth, int mapHeight)
        {
            map.Clear(true, true);

            foreach (ICell cell in map.GetCellsInRows(0, mapHeight - 1))
            {
                map.SetCellProperties(cell.X, cell.Y, false, false);
            }

            foreach (ICell cell in map.GetCellsInColumns(0, mapWidth - 1))
            {
                map.SetCellProperties(cell.X, cell.Y, false, false);
            }
        }

        private void CreateExits(MapGenerator mapGenerator, CurrentMap map, int mapWidth, int mapHeight, string[] exitStrings)
        {
            if (exitStrings.Length > 4)
            {
                return;
            }

            List<Point> cornerPoints = new List<Point>(4);
            cornerPoints.Add(new Point(2, 2));
            cornerPoints.Add(new Point(mapWidth - 3, 2));
            cornerPoints.Add(new Point(mapWidth - 3, mapHeight - 3));
            cornerPoints.Add(new Point(2, mapHeight - 3));

            int exitIndex = 0;
            foreach (string exitString in exitStrings)
            {
                if (exitIndex == 0 && exitString != "DEADEND")
                {
                    map.Exits.Add(mapGenerator.CreateExit(cornerPoints.First().X, cornerPoints.First().Y, exitString, true));
                }
                else if (exitIndex == exitStrings.Length - 1 && exitString != "DEADEND")
                {
                    map.Exits.Add(mapGenerator.CreateExit(cornerPoints.Last().X, cornerPoints.Last().Y, exitString, false));
                }
                else if (exitString != "DEADEND")
                {
                    map.Exits.Add(mapGenerator.CreateExit(cornerPoints[exitIndex].X, cornerPoints[exitIndex].Y, exitString, false));
                }

                exitIndex++;
            }
        }

        private void PlacePlayer(CurrentMap map)
        {
            Player player = Game.Singleton.Player;
            if (player == null)
            {
                player = new Player();
            }

            player.X = map.Exits[0].X;
            player.Y = map.Exits[0].Y;

            map.AddPlayer(player);
        }

        #endregion
    }
}