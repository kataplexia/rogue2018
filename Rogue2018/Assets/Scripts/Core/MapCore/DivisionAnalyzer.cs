﻿namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using RogueSharp;

    public class DivisionAnalyzer
    {
        #region Fields

        private readonly IMap map;
        private readonly List<MapSection> mapSections;
        private readonly int padding;
        private readonly int divisionsX;
        private readonly int divisionsY;
        private readonly int divisionSizeX;
        private readonly int divisionSizeY;

        #endregion

        #region Constructor

        public DivisionAnalyzer(IMap map, int padding, int divisionsX, int divisionsY)
        {
            this.map = map;
            this.mapSections = new List<MapSection>();
            this.padding = padding;
            this.divisionsX = divisionsX;
            this.divisionsY = divisionsY;
            this.divisionSizeX = (map.Width - padding) / divisionsX;
            this.divisionSizeY = (map.Height - padding) / divisionsY;
        }

        #endregion

        #region Methods

        public List<MapSection> GetMapSections()
        {
            for (int x = 0; x < divisionsX; x++)
            {
                for (int y = 0; y < divisionsY; y++)
                {
                    MapSection mapSection = new MapSection();
                    for (int row = 0; row < divisionSizeY; row++)
                    {
                        int xOrigin = (x * divisionSizeX) + (padding / 2);
                        int yOrigin = (y * divisionSizeY) + (padding / 2) + row;
                        int xDestination = (x * divisionSizeX) + (padding / 2) + (divisionSizeX - 1);
                        int yDestination = yOrigin;

                        IEnumerable<ICell> cells = map.GetCellsAlongLine(xOrigin, yOrigin, xDestination, yDestination);
                        foreach (ICell cell in cells)
                        {
                            mapSection.AddCell(cell);
                        }
                    }

                    mapSections.Add(mapSection);
                }
            }

            return mapSections;
        }

        #endregion
    }
}