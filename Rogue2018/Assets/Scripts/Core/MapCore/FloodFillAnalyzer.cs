﻿namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using RogueSharp;

    public class FloodFillAnalyzer
    {
        #region Fields

        private readonly IMap map;
        private readonly List<MapSection> mapSections;

        private readonly int[][] offsets =
        {
            new[] { 0, -1 }, new[] { -1, 0 }, new[] { 1, 0 }, new[] { 0, 1 }
         };

        private readonly bool[][] visited;

        #endregion

        #region Constructor

        public FloodFillAnalyzer(IMap map)
        {
            this.map = map;
            mapSections = new List<MapSection>();
            visited = new bool[map.Height][];
            for (int i = 0; i < visited.Length; i++)
            {
                visited[i] = new bool[map.Width];
            }
        }

        #endregion

        #region Methods

        public List<MapSection> GetMapSections()
        {
            IEnumerable<ICell> cells = map.GetAllCells();
            foreach (ICell cell in cells)
            {
                MapSection section = Visit(cell);
                if (section.Cells.Count > 0)
                {
                    mapSections.Add(section);
                }
            }

            return mapSections;
        }

        private MapSection Visit(ICell cell)
        {
            Stack<ICell> stack = new Stack<ICell>(new List<ICell>());
            MapSection mapsection = new MapSection();
            stack.Push(cell);
            while (stack.Count != 0)
            {
                cell = stack.Pop();
                if (visited[cell.Y][cell.X] || !cell.IsWalkable)
                {
                    continue;
                }

                mapsection.AddCell(cell);
                visited[cell.Y][cell.X] = true;
                foreach (ICell neighbor in GetNeighbors(cell))
                {
                    if (cell.IsWalkable == neighbor.IsWalkable && !visited[neighbor.Y][neighbor.X])
                    {
                        stack.Push(neighbor);
                    }
                }
            }

            return mapsection;
        }

        private ICell GetCell(int x, int y)
        {
            if (x < 0 || y < 0)
            {
                return null;
            }

            if (x >= map.Width || y >= map.Height)
            {
                return null;
            }

            return map.GetCell(x, y);
        }

        private IEnumerable<ICell> GetNeighbors(ICell cell)
        {
            List<ICell> neighbors = new List<ICell>(8);
            foreach (int[] offset in offsets)
            {
                var neighbor = GetCell(cell.X + offset[0], cell.Y + offset[1]);
                if (neighbor == null)
                {
                    continue;
                }

                neighbors.Add(neighbor);
            }

            return neighbors;
        }

        #endregion
    }
}