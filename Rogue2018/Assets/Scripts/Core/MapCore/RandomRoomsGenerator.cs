﻿namespace Rogue2018.Core.MapCore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Rogue2018.Actors;
    using Rogue2018.Actors.Monsters;
    using RogueSharp;
    using RogueSharp.DiceNotation;

    public class RandomRoomsGenerator
    {
        #region Constructor

        public RandomRoomsGenerator(MapGenerator mapGenerator, CurrentMap map, int maxRoomCount, int maxRoomSize, int minRoomSize, int mapWidth, int mapHeight, string[] exitStrings)
        {
            map.Initialize(mapWidth, mapHeight);
            CreateMap(mapGenerator, map, maxRoomCount, maxRoomSize, minRoomSize, mapWidth, mapHeight);
            CreateExits(mapGenerator, map, exitStrings);
            mapGenerator.SetBitmaskForMap(0);
            mapGenerator.SetCellDrawPropertiesForMap();
            PlacePlayer(map);
            //// PlaceMonsters(map);
            mapGenerator.CreateGoalMaps();
        }

        #endregion

        #region Methods

        private void CreateMap(MapGenerator mapGenerator, CurrentMap map, int maxRoomCount, int maxRoomSize, int minRoomSize, int mapWidth, int mapHeight)
        {
            for (int r = maxRoomCount; r > 0; r--)
            {
                int roomWidth = Game.Singleton.Random.Next(minRoomSize, maxRoomSize);
                int roomHeight = Game.Singleton.Random.Next(minRoomSize, maxRoomSize);
                int roomXPosition = Game.Singleton.Random.Next(0, mapWidth - roomWidth - 1);
                int roomYPosition = Game.Singleton.Random.Next(0, mapHeight - roomHeight - 1);

                var newRoom = new Rectangle(roomXPosition, roomYPosition, roomWidth, roomHeight);

                bool newRoomIntersects = map.Rooms.Any(room => newRoom.Intersects(room));

                if (!newRoomIntersects)
                {
                    map.Rooms.Add(newRoom);
                }
            }

            foreach (Rectangle room in map.Rooms)
            {
                CreateRoom(map, room);
            }

            for (int r = 1; r < map.Rooms.Count; r++)
            {
                int previousRoomCenterX = map.Rooms[r - 1].Center.X;
                int previousRoomCenterY = map.Rooms[r - 1].Center.Y;
                int currentRoomCenterX = map.Rooms[r].Center.X;
                int currentRoomCenterY = map.Rooms[r].Center.Y;

                if (Game.Singleton.Random.Next(1, 2) == 1)
                {
                    CreateHorizontalTunnel(map, previousRoomCenterX, currentRoomCenterX, previousRoomCenterY);
                    CreateVerticalTunnel(map, previousRoomCenterY, currentRoomCenterY, currentRoomCenterX);
                }
                else
                {
                    CreateHorizontalTunnel(map, previousRoomCenterX, currentRoomCenterX, currentRoomCenterY);
                    CreateVerticalTunnel(map, previousRoomCenterY, currentRoomCenterY, previousRoomCenterX);
                }
            }

            foreach (Rectangle room in map.Rooms)
            {
                CreateDoors(mapGenerator, map, room);
            }
        }

        private void CreateRoom(CurrentMap map, Rectangle room)
        {
            for (int x = room.Left + 1; x < room.Right; x++)
            {
                for (int y = room.Top + 1; y < room.Bottom; y++)
                {
                    map.SetCellProperties(x, y, true, true, false);
                }
            }
        }

        private void CreateHorizontalTunnel(CurrentMap map, int xStart, int xEnd, int yPos)
        {
            for (int x = Math.Min(xStart, xEnd); x <= Math.Max(xStart, xEnd); x++)
            {
                map.SetCellProperties(x, yPos, true, true);
            }
        }

        private void CreateVerticalTunnel(CurrentMap map, int yStart, int yEnd, int xPos)
        {
            for (int y = Math.Min(yStart, yEnd); y <= Math.Max(yStart, yEnd); y++)
            {
                map.SetCellProperties(xPos, y, true, true);
            }
        }

        private void CreateDoors(MapGenerator mapGenerator, CurrentMap map, Rectangle room)
        {
            int xMin = room.Left;
            int xMax = room.Right;
            int yMin = room.Top;
            int yMax = room.Bottom;

            List<ICell> borderRogueCells = map.GetCellsAlongLine(xMin, yMin, xMax, yMin).ToList();
            borderRogueCells.AddRange(map.GetCellsAlongLine(xMin, yMin, xMin, yMax));
            borderRogueCells.AddRange(map.GetCellsAlongLine(xMin, yMax, xMax, yMax));
            borderRogueCells.AddRange(map.GetCellsAlongLine(xMax, yMin, xMax, yMax));

            foreach (RogueSharp.Cell rogueCell in borderRogueCells)
            {
                if (mapGenerator.IsPotentialDoor(map, rogueCell))
                {
                    mapGenerator.CreateDoor(rogueCell.X, rogueCell.Y);
                }
            }
        }

        private void CreateExits(MapGenerator mapGenerator, CurrentMap map, string[] exitStrings)
        {
            int exitIndex = 0;
            foreach (string exitString in exitStrings)
            {
                if (exitIndex == 0 && exitString != "DEADEND")
                {
                    map.Exits.Add(mapGenerator.CreateExit(map.Rooms.First().Center.X, map.Rooms.First().Center.Y, exitString, true));
                }
                else if (exitIndex == exitStrings.Length - 1 && exitString != "DEADEND")
                {
                    map.Exits.Add(mapGenerator.CreateExit(map.Rooms.Last().Center.X, map.Rooms.Last().Center.Y, exitString, false));
                }
                else if (exitString != "DEADEND")
                {
                    int roomIndex = (map.Rooms.Count - 1) - ((exitStrings.Length - 1) - exitIndex);
                    map.Exits.Add(mapGenerator.CreateExit(map.Rooms[roomIndex].Center.X, map.Rooms[roomIndex].Center.Y, exitString, false));
                }

                exitIndex++;
            }
        }

        private void PlacePlayer(CurrentMap map)
        {
            Player player = Game.Singleton.Player;
            if (player == null)
            {
                player = new Player();
            }

            player.X = map.Rooms[0].Center.X;
            player.Y = map.Rooms[0].Center.Y;

            map.AddPlayer(player);
        }

        private void PlaceMonsters(CurrentMap map)
        {
            foreach (var room in map.Rooms)
            {
                if (Dice.Roll("1D10") < 7)
                {
                    var numberOfMonsters = Dice.Roll("1D4");
                    for (int i = 0; i < numberOfMonsters; i++)
                    {
                        Point randomRoomLocation = map.GetRandomWalkableLocationInRoom(room);
                        if (randomRoomLocation != null)
                        {
                            var monster = Kobold.Create(1);
                            monster.X = randomRoomLocation.X;
                            monster.Y = randomRoomLocation.Y;
                            map.AddMonster(monster);
                        }
                    }
                }
            }
        }

        #endregion
    }
}