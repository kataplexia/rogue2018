﻿namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using RogueSharp;

    public class MapSection
    {
        #region Fields

        private int exitPointX;
        private int exitPointY;
        private int top;
        private int bottom;
        private int right;
        private int left;
        private bool hasExit;

        #endregion

        #region Constructor

        public MapSection()
        {
            Cells = new HashSet<ICell>();
            top = int.MaxValue;
            left = int.MaxValue;
        }

        #endregion

        #region Properties

        public bool HasExit
        {
            get { return hasExit; }
            set { hasExit = value; }
        }

        public int ExitPointX
        {
            get { return exitPointX; }
            set { exitPointX = value; }
        }

        public int ExitPointY
        {
            get { return exitPointY; }
            set { exitPointY = value; }
        }

        public Rectangle Bounds
        {
            get { return new Rectangle(left, top, right - left + 1, bottom - top + 1); }
        }

        public HashSet<ICell> Cells
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        public void AddCell(ICell cell)
        {
            Cells.Add(cell);
            UpdateBounds(cell);
        }

        public override string ToString()
        {
            return string.Format("Bounds: {0}", Bounds);
        }

        private void UpdateBounds(ICell cell)
        {
            if (cell.X > right)
            {
                right = cell.X;
            }

            if (cell.X < left)
            {
                left = cell.X;
            }

            if (cell.Y > bottom)
            {
                bottom = cell.Y;
            }

            if (cell.Y < top)
            {
                top = cell.Y;
            }
        }

        #endregion
    }
}