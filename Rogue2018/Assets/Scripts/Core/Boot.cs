﻿namespace Rogue2018.Core
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class Boot : MonoBehaviour
    {
        #region Methods

        private void Awake()
        {
            Screen.SetResolution(1600, 900, false);
            SceneManager.LoadScene("Game");
        }

        #endregion
    }
}