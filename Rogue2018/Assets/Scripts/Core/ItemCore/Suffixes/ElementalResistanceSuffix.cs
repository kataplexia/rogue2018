﻿namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class ElementalResistanceSuffix : Suffix
    {
        #region Constructor

        public ElementalResistanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);

            MinimumItemLevel = 12;
            MaximumItemLevel = 100;
            NumberOfTiers = 5;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Elemental Resistances", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(24);
            TierMinimumItemLevels.Add(36);
            TierMinimumItemLevels.Add(48);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("of the Crystal");
            TierNameStrings.Add("of the Prism");
            TierNameStrings.Add("of the Kaleidoscope");
            TierNameStrings.Add("of Variegation");
            TierNameStrings.Add("of the Rainbow");

            TierEffectValues.Add(Game.Singleton.Random.Next(3, 5));
            TierEffectValues.Add(Game.Singleton.Random.Next(6, 8));
            TierEffectValues.Add(Game.Singleton.Random.Next(9, 11));
            TierEffectValues.Add(Game.Singleton.Random.Next(12, 14));
            TierEffectValues.Add(Game.Singleton.Random.Next(15, 16));
        }

        #endregion
    }
}