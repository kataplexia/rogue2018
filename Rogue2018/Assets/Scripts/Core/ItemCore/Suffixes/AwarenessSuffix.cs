﻿namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class AwarenessSuffix : Suffix
    {
        #region Constructor

        public AwarenessSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            MinimumItemLevel = 8;
            MaximumItemLevel = 100;
            NumberOfTiers = 3;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1} to Awareness", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(15);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("of Shining");
            TierNameStrings.Add("of Light");
            TierNameStrings.Add("of Radiance");

            TierEffectValues.Add(5);
            TierEffectValues.Add(10);
            TierEffectValues.Add(15);
        }

        #endregion
    }
}
