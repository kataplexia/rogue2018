﻿namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class SpellCriticalChanceSuffix : Suffix
    {
        #region Constructor

        public SpellCriticalChanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 11;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Critical Chance for Spells", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(21);
            TierMinimumItemLevels.Add(28);
            TierMinimumItemLevels.Add(41);
            TierMinimumItemLevels.Add(59);
            TierMinimumItemLevels.Add(76);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("of Menace");
            TierNameStrings.Add("of Havoc");
            TierNameStrings.Add("of Disaster");
            TierNameStrings.Add("of Calamity");
            TierNameStrings.Add("of Ruin");
            TierNameStrings.Add("of Unmaking");

            TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            TierEffectValues.Add(Game.Singleton.Random.Next(20, 39));
            TierEffectValues.Add(Game.Singleton.Random.Next(40, 59));
            TierEffectValues.Add(Game.Singleton.Random.Next(60, 79));
            TierEffectValues.Add(Game.Singleton.Random.Next(80, 99));
            TierEffectValues.Add(Game.Singleton.Random.Next(100, 109));
        }

        #endregion
    }
}