﻿namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class PlagueResistanceSuffix : Suffix
    {
        #region Constructor

        public PlagueResistanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Plague Resistance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(44);
            TierMinimumItemLevels.Add(56);
            TierMinimumItemLevels.Add(65);
            TierMinimumItemLevels.Add(81);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("of the Lost");
            TierNameStrings.Add("of Banishment");
            TierNameStrings.Add("of Eviction");
            TierNameStrings.Add("of Expulsion");
            TierNameStrings.Add("of Exile");
            TierNameStrings.Add("of Bameth");

            TierEffectValues.Add(Game.Singleton.Random.Next(5, 10));
            TierEffectValues.Add(Game.Singleton.Random.Next(11, 15));
            TierEffectValues.Add(Game.Singleton.Random.Next(16, 20));
            TierEffectValues.Add(Game.Singleton.Random.Next(21, 25));
            TierEffectValues.Add(Game.Singleton.Random.Next(26, 30));
            TierEffectValues.Add(Game.Singleton.Random.Next(31, 35));
        }

        #endregion
    }
}