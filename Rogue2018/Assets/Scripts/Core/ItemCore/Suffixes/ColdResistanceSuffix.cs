﻿namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class ColdResistanceSuffix : Suffix
    {
        #region Constructor

        public ColdResistanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 8;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Cold Resistance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(12);
            TierMinimumItemLevels.Add(24);
            TierMinimumItemLevels.Add(36);
            TierMinimumItemLevels.Add(48);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(72);
            TierMinimumItemLevels.Add(84);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("of the Inuit");
            TierNameStrings.Add("of the Seal");
            TierNameStrings.Add("of the Penguin");
            TierNameStrings.Add("of the Yeti");
            TierNameStrings.Add("of the Walrus");
            TierNameStrings.Add("of the Polar Bear");
            TierNameStrings.Add("of the Ice");
            TierNameStrings.Add("of Haast");

            TierEffectValues.Add(Game.Singleton.Random.Next(6, 11));
            TierEffectValues.Add(Game.Singleton.Random.Next(12, 17));
            TierEffectValues.Add(Game.Singleton.Random.Next(18, 23));
            TierEffectValues.Add(Game.Singleton.Random.Next(24, 29));
            TierEffectValues.Add(Game.Singleton.Random.Next(30, 35));
            TierEffectValues.Add(Game.Singleton.Random.Next(36, 41));
            TierEffectValues.Add(Game.Singleton.Random.Next(42, 45));
            TierEffectValues.Add(Game.Singleton.Random.Next(46, 48));
        }

        #endregion
    }
}