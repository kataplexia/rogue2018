﻿namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class CriticalChanceSuffix : Suffix
    {
        #region Constructor

        public CriticalChanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Critical Chance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(20);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(44);
            TierMinimumItemLevels.Add(59);
            TierMinimumItemLevels.Add(73);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("of Needling");
            TierNameStrings.Add("of Stinging");
            TierNameStrings.Add("of Piercing");
            TierNameStrings.Add("of Rupturing");
            TierNameStrings.Add("of Penetrating");
            TierNameStrings.Add("of Incision");

            TierEffectValues.Add(Game.Singleton.Random.Next(10, 14));
            TierEffectValues.Add(Game.Singleton.Random.Next(15, 19));
            TierEffectValues.Add(Game.Singleton.Random.Next(20, 24));
            TierEffectValues.Add(Game.Singleton.Random.Next(25, 29));
            TierEffectValues.Add(Game.Singleton.Random.Next(30, 34));
            TierEffectValues.Add(Game.Singleton.Random.Next(35, 38));
        }

        #endregion
    }
}