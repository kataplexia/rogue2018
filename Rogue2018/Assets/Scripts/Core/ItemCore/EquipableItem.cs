﻿namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;
    using UnityEngine;

    [System.Serializable]
    public abstract class EquipableItem : IItem, IEquipable
    {
        #region Fields

        private string description;
        private EquipableItemBaseTypes equipableItemBaseType;
        private EquipmentLocations equipmentLocation;
        private string equipmentLocationString;
        private bool equipped;
        private bool equippedLeftRing;
        private bool equippedMainHand;
        private bool equippedOffHand;
        private bool equippedRightRing;
        private Implicit itemImplicit;
        private int itemLevel;
        private List<Prefix> itemPrefixes;
        private ItemQualities itemQuality;
        private List<Suffix> itemSuffixes;
        private string name;

        #endregion

        #region Constructor

        public EquipableItem()
        {
            Equipped = false;
            EquippedLeftRing = false;
            EquippedMainHand = false;
            EquippedOffHand = false;
            EquippedRightRing = false;
        }

        #endregion

        #region Properties

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public EquipableItemBaseTypes EquipableItemBaseType
        {
            get { return equipableItemBaseType; }
            set { equipableItemBaseType = value; }
        }

        public EquipmentLocations EquipmentLocation
        {
            get { return equipmentLocation; }
            set { equipmentLocation = value; }
        }

        public string EquipmentLocationString
        {
            get { return equipmentLocationString; }
            set { equipmentLocationString = value; }
        }

        public bool Equipped
        {
            get { return equipped; }
            set { equipped = value; }
        }

        public bool EquippedLeftRing
        {
            get { return equippedLeftRing; }
            set { equippedLeftRing = value; }
        }

        public bool EquippedMainHand
        {
            get { return equippedMainHand; }
            set { equippedMainHand = value; }
        }

        public bool EquippedOffHand
        {
            get { return equippedOffHand; }
            set { equippedOffHand = value; }
        }

        public bool EquippedRightRing
        {
            get { return equippedRightRing; }
            set { equippedRightRing = value; }
        }

        public Implicit Implicit
        {
            get { return itemImplicit; }
            set { itemImplicit = value; }
        }

        public int ItemLevel
        {
            get { return itemLevel; }
            set { itemLevel = value; }
        }

        public ItemQualities ItemQuality
        {
            get { return itemQuality; }
            set { itemQuality = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int PrefixCount
        {
            get;
            private set;
        }

        public List<Prefix> Prefixes
        {
            get { return itemPrefixes; }
            set { itemPrefixes = value; }
        }

        public int SuffixCount
        {
            get;
            private set;
        }

        public List<Suffix> Suffixes
        {
            get { return itemSuffixes; }
            set { itemSuffixes = value; }
        }

        #endregion

        #region Methods

        public void AddPrefixes(int prefixCount)
        {
            if (prefixCount == 0)
            {
                return;
            }

            for (int i = 0; i < prefixCount; i++)
            {
                Prefixes.Add(Game.Singleton.ItemPrefixes.GetRandomPrefix(Prefixes, ItemLevel, EquipableItemBaseType));
            }
        }

        public void AddSuffixes(int suffixCount)
        {
            if (suffixCount == 0)
            {
                return;
            }

            for (int i = 0; i < suffixCount; i++)
            {
                Suffixes.Add(Game.Singleton.ItemSuffixes.GetRandomSuffix(Suffixes, ItemLevel, EquipableItemBaseType));
            }
        }

        public abstract void Construct();

        public void DrawName(int xOffset, int yOffset, Color drawColor)
        {
            Color backgroundDrawColor = Game.Singleton.Tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            Game.Singleton.Tools.DrawString(xOffset, yOffset, Name, drawColor, backgroundDrawColor, 0);
        }

        public void Drop(bool dropAll = false)
        {
            if (Equipped)
            {
                RemoveItemAtEquipmentLocation(EquipmentLocation);
            }
            else
            {
                Game.Singleton.Player.InventoryList.Remove(this);
            }

            //// TODO: Place item inside list for current cell's contents

            string droppedMessage = string.Format("You dropped {0} on the ground", Name);
            Game.Singleton.MessageSystem.Add(droppedMessage, Color.cyan, false);
            Game.Singleton.SetDrawRequired(true);
        }

        public void Equip()
        {
            string checkString = CheckFreeSlotToEquipString(EquipmentLocation);
            if (checkString.Contains("False: "))
            {
                string cannotEquipMessage = checkString.Substring(7);
                Game.Singleton.MessageSystem.Add(cannotEquipMessage, Color.cyan, false);
                Game.Singleton.SetDrawRequired(true);
                return;
            }

            switch (EquipmentLocation)
            {
                case EquipmentLocations.Chest:

                    Game.Singleton.Player.EquipmentChest = this;

                    break;

                case EquipmentLocations.EitherHand:

                    if (Game.Singleton.Player.EquipmentMainHand == null)
                    {
                        Game.Singleton.Player.EquipmentMainHand = this;
                        EquippedMainHand = true;
                    }
                    else if (Game.Singleton.Player.EquipmentOffHand == null)
                    {
                        Game.Singleton.Player.EquipmentOffHand = this;
                        EquippedOffHand = true;
                    }

                    break;

                case EquipmentLocations.Feet:

                    Game.Singleton.Player.EquipmentFeet = this;

                    break;

                case EquipmentLocations.Hands:

                    Game.Singleton.Player.EquipmentHands = this;

                    break;

                case EquipmentLocations.Head:

                    Game.Singleton.Player.EquipmentHead = this;

                    break;

                case EquipmentLocations.Legs:

                    Game.Singleton.Player.EquipmentLegs = this;

                    break;

                case EquipmentLocations.MainHand:

                    Game.Singleton.Player.EquipmentMainHand = this;

                    break;

                case EquipmentLocations.OffHand:

                    Game.Singleton.Player.EquipmentOffHand = this;

                    break;

                case EquipmentLocations.Ring:

                    if (Game.Singleton.Player.EquipmentLeftRing == null)
                    {
                        Game.Singleton.Player.EquipmentLeftRing = this;
                        EquippedLeftRing = true;
                    }
                    else if (Game.Singleton.Player.EquipmentRightRing == null)
                    {
                        Game.Singleton.Player.EquipmentRightRing = this;
                        EquippedRightRing = true;
                    }

                    break;

                case EquipmentLocations.Waist:

                    Game.Singleton.Player.EquipmentWaist = this;

                    break;
            }

            Equipped = true;

            Game.Singleton.Player.InventoryList.Remove(this);

            string equippedMessage = string.Format("You equipped {0}", Name);
            Game.Singleton.MessageSystem.Add(equippedMessage, Color.cyan, false);
            Game.Singleton.SetDrawRequired(true);
        }

        public void RenameItem(int prefixCount, int suffixCount)
        {
            if (ItemQuality == ItemQualities.Magic)
            {
                if (prefixCount > 0 && suffixCount == 0)
                {
                    string preName = Prefixes[0].PrefixNameString;
                    Name = string.Format("{0} {1}", preName, Name);
                }
                else if (suffixCount > 0 && prefixCount == 0)
                {
                    string postName = Suffixes[0].SuffixNameString;
                    Name = string.Format("{0} {1}", Name, postName);
                }
                else if (suffixCount > 0 && prefixCount > 0)
                {
                    string preName = Prefixes[0].PrefixNameString;
                    string postName = Suffixes[0].SuffixNameString;
                    Name = string.Format("{0} {1} {2}", preName, Name, postName);
                }
            }
            else if (ItemQuality == ItemQualities.Rare)
            {
                string randomName = Game.Singleton.ItemNameGenerator.CreateRandomName(this);
                Name = string.Format("{0} ({1})", randomName, Name);
            }
        }

        public string SetupAccessoryTypeString(AccessoryTypes accessoryType)
        {
            switch (accessoryType)
            {
                case AccessoryTypes.Belt:

                    return "Belt";

                case AccessoryTypes.Ring:

                    return "Ring";

                case AccessoryTypes.Quiver:

                    return "Quiver";

                case AccessoryTypes.None:
                default:

                    return string.Empty;
            }
        }

        public void SetupAffixes()
        {
            PrefixCount = 0;
            SuffixCount = 0;

            if (ItemQuality == ItemQualities.Magic)
            {
                int affixCount = Game.Singleton.Random.Next(1, 2);
                if (affixCount == 1)
                {
                    int prefixOrSuffix = Game.Singleton.Random.Next(0, 1);
                    if (prefixOrSuffix == 0)
                    {
                        PrefixCount = 1;
                    }
                    else
                    {
                        SuffixCount = 1;
                    }
                }
                else if (affixCount == 2)
                {
                    PrefixCount = 1;
                    SuffixCount = 1;
                }
            }
            else if (ItemQuality == ItemQualities.Rare)
            {
                int prefixOrSuffix = Game.Singleton.Random.Next(0, 1);
                if (prefixOrSuffix == 0)
                {
                    PrefixCount = 2 + Game.Singleton.Random.Next(0, 1);
                    SuffixCount = 1 + Game.Singleton.Random.Next(0, 2);
                }
                else
                {
                    PrefixCount = 1 + Game.Singleton.Random.Next(0, 2);
                    SuffixCount = 2 + Game.Singleton.Random.Next(0, 1);
                }
            }

            Prefixes = new List<Prefix>(PrefixCount);
            Suffixes = new List<Suffix>(SuffixCount);

            AddPrefixes(PrefixCount);
            AddSuffixes(SuffixCount);

            RenameItem(PrefixCount, SuffixCount);
        }

        public string SetupArmourStyleString(ArmourStyles armourStyle)
        {
            switch (armourStyle)
            {
                case ArmourStyles.Heavy:

                    return "Heavy";

                case ArmourStyles.Light:

                    return "Light";

                case ArmourStyles.Medium:

                    return "Medium";

                case ArmourStyles.None:
                default:

                    return string.Empty;
            }
        }

        public string SetupDamageTypeString(DamageTypes damageType)
        {
            switch (damageType)
            {
                case DamageTypes.Blunt:

                    return "Blunt";

                case DamageTypes.Cut:

                    return "Cut";

                case DamageTypes.Pierce:

                    return "Pierce";

                case DamageTypes.None:
                default:

                    return string.Empty;
            }
        }

        public string SetupEquipmentLocationString(EquipmentLocations equipmentLocation)
        {
            switch (equipmentLocation)
            {
                case EquipmentLocations.Chest:

                    return "Chest";

                case EquipmentLocations.EitherHand:

                    return "Either Hand";

                case EquipmentLocations.Feet:

                    return "Feet";

                case EquipmentLocations.Hands:

                    return "Hands";

                case EquipmentLocations.Head:

                    return "Head";

                case EquipmentLocations.Legs:

                    return "Legs";

                case EquipmentLocations.MainHand:

                    return "Main Hand";

                case EquipmentLocations.OffHand:

                    return "Off Hand";

                case EquipmentLocations.Ring:

                    return "Ring";

                default:

                    return string.Empty;
            }
        }

        public string SetupWeaponStyleString(WeaponStyles weaponStyle)
        {
            switch (weaponStyle)
            {
                case WeaponStyles.OneHanded:

                    return "One Handed";

                case WeaponStyles.TwoHanded:

                    return "Two Handed";

                case WeaponStyles.Ranged:

                    return "Ranged";

                case WeaponStyles.None:
                default:

                    return string.Empty;
            }
        }

        public string SetupWeaponTypeString(WeaponTypes weaponType)
        {
            switch (weaponType)
            {
                case WeaponTypes.Axe:

                    return "Axe";

                case WeaponTypes.Bow:

                    return "Bow";

                case WeaponTypes.Mace:

                    return "Mace";

                case WeaponTypes.Staff:

                    return "Staff";

                case WeaponTypes.Sword:

                    return "Sword";

                case WeaponTypes.Dagger:

                    return "Dagger";

                case WeaponTypes.Sceptre:

                    return "Sceptre";

                case WeaponTypes.Wand:

                    return "Wand";

                default:

                    return string.Empty;
            }
        }

        public void Unequip()
        {
            if (!Game.Singleton.Player.CheckInventoryCapacity())
            {
                return;
            }

            RemoveItemAtEquipmentLocation(EquipmentLocation);
            Game.Singleton.Player.AddItemToInventory(this as IItem);

            string unequippedMessage = string.Format("You unequipped {0}", Name);
            Game.Singleton.MessageSystem.Add(unequippedMessage, Color.cyan, false);
            Game.Singleton.SetDrawRequired(true);
        }

        private string CheckFreeSlotToEquipString(EquipmentLocations equipmentLocation)
        {
            switch (equipmentLocation)
            {
                case EquipmentLocations.Chest:

                    if (Game.Singleton.Player.EquipmentChest == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.EitherHand:

                    if (Game.Singleton.Player.EquipmentMainHand == null &&
                        Game.Singleton.Player.EquipmentOffHand != null)
                    {
                        IEquipable offHandEquipable = Game.Singleton.Player.EquipmentOffHand as IEquipable;
                        if (offHandEquipable is IAccessory)
                        {
                            IAccessory offHandAccessory = offHandEquipable as IAccessory;
                            if (offHandAccessory.AccessoryType == AccessoryTypes.Quiver)
                            {
                                return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                            }

                            return "True";
                        }

                        return "True";
                    }
                    else if (Game.Singleton.Player.EquipmentMainHand != null &&
                             Game.Singleton.Player.EquipmentOffHand == null)
                    {
                        IWeapon mainHandWeapon = Game.Singleton.Player.EquipmentMainHand as IWeapon;
                        if (mainHandWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                        {
                            return string.Format("False: Cannot equip {0}. Main Hand equipment is Two Handed", Name);
                        }
                        else
                        {
                            if (mainHandWeapon.WeaponStyle == WeaponStyles.Ranged)
                            {
                                return string.Format("False: Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);
                            }

                            return "True";
                        }
                    }
                    else if (Game.Singleton.Player.EquipmentMainHand != null &&
                             Game.Singleton.Player.EquipmentOffHand != null)
                    {
                        return string.Format("False: Cannot equip {0}. No equipment slot available", Name);
                    }

                    return "True";

                case EquipmentLocations.Feet:

                    if (Game.Singleton.Player.EquipmentFeet == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Hands:

                    if (Game.Singleton.Player.EquipmentHands == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Head:

                    if (Game.Singleton.Player.EquipmentHead == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Legs:

                    if (Game.Singleton.Player.EquipmentLegs == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.MainHand:

                    if (Game.Singleton.Player.EquipmentMainHand == null)
                    {
                        IWeapon thisWeapon = this as IWeapon;
                        if (thisWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                        {
                            if (Game.Singleton.Player.EquipmentOffHand != null)
                            {
                                return string.Format("False: Cannot equip {0}. No equipment slot available", Name);
                            }

                            return "True";
                        }
                        else if (thisWeapon.WeaponStyle == WeaponStyles.Ranged)
                        {
                            if (Game.Singleton.Player.EquipmentOffHand != null)
                            {
                                IEquipable offHandEquipable = Game.Singleton.Player.EquipmentOffHand as IEquipable;
                                if (thisWeapon.WeaponType == WeaponTypes.Bow)
                                {
                                    if (offHandEquipable is IAccessory)
                                    {
                                        IAccessory offHandAccessory = offHandEquipable as IAccessory;
                                        if (offHandAccessory.AccessoryType == AccessoryTypes.Focus)
                                        {
                                            return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                                        }

                                        return "True";
                                    }

                                    return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                                }
                                else if (thisWeapon.WeaponType == WeaponTypes.Wand)
                                {
                                    if (offHandEquipable is IAccessory)
                                    {
                                        IAccessory offHandAccessory = offHandEquipable as IAccessory;
                                        if (offHandAccessory.AccessoryType == AccessoryTypes.Quiver)
                                        {
                                            return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                                        }

                                        return "True";
                                    }

                                    return "True";
                                }
                                else if (offHandEquipable is IWeapon)
                                {
                                    return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                                }

                                return "True";
                            }

                            return "True";
                        }

                        return "True";
                    }

                    goto default;

                case EquipmentLocations.OffHand:

                    if (Game.Singleton.Player.EquipmentOffHand == null)
                    {
                        if (Game.Singleton.Player.EquipmentMainHand != null)
                        {
                            IWeapon mainHandWeapon = Game.Singleton.Player.EquipmentMainHand as IWeapon;
                            if (mainHandWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                            {
                                return string.Format("False: Cannot equip {0}. Main Hand equipment is Two Handed", Name);
                            }

                            if (mainHandWeapon.WeaponType == WeaponTypes.Bow &&
                                this is IArmour)
                            {
                                return string.Format("False: Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);
                            }
                            else if (mainHandWeapon.WeaponType != WeaponTypes.Bow &&
                                this is IAccessory)
                            {
                                IAccessory equipableAccessory = this as IAccessory;
                                if (equipableAccessory.AccessoryType == AccessoryTypes.Quiver)
                                {
                                    return string.Format("False: Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);
                                }

                                return "True";
                            }

                            return "True";
                        }

                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Ring:

                    if (Game.Singleton.Player.EquipmentLeftRing == null ||
                        Game.Singleton.Player.EquipmentRightRing == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Waist:

                    if (Game.Singleton.Player.EquipmentWaist == null)
                    {
                        return "True";
                    }

                    goto default;

                default:

                    return string.Format("False: Cannot equip {0}. No equipment slot available", Name);
            }
        }

        private void RemoveItemAtEquipmentLocation(EquipmentLocations equipmentLocation)
        {
            switch (equipmentLocation)
            {
                case EquipmentLocations.Chest:

                    Game.Singleton.Player.EquipmentChest = null;
                    Equipped = false;

                    break;

                case EquipmentLocations.EitherHand:

                    if (EquippedMainHand)
                    {
                        Game.Singleton.Player.EquipmentMainHand = null;
                        Equipped = false;
                        EquippedMainHand = false;
                    }
                    else if (EquippedOffHand)
                    {
                        Game.Singleton.Player.EquipmentOffHand = null;
                        Equipped = false;
                        EquippedOffHand = false;
                    }

                    break;

                case EquipmentLocations.Feet:

                    Game.Singleton.Player.EquipmentFeet = null;
                    Equipped = false;

                    break;

                case EquipmentLocations.Hands:

                    Game.Singleton.Player.EquipmentHands = null;
                    Equipped = false;

                    break;

                case EquipmentLocations.Head:

                    Game.Singleton.Player.EquipmentHead = null;
                    Equipped = false;

                    break;

                case EquipmentLocations.Legs:

                    Game.Singleton.Player.EquipmentLegs = null;
                    Equipped = false;

                    break;

                case EquipmentLocations.MainHand:

                    Game.Singleton.Player.EquipmentMainHand = null;
                    Equipped = false;
                    EquippedMainHand = false;

                    break;

                case EquipmentLocations.OffHand:

                    Game.Singleton.Player.EquipmentOffHand = null;
                    Equipped = false;
                    EquippedOffHand = false;

                    break;

                case EquipmentLocations.Ring:

                    if (EquippedLeftRing)
                    {
                        Game.Singleton.Player.EquipmentLeftRing = null;
                        Equipped = false;
                        EquippedLeftRing = false;
                    }
                    else if (EquippedRightRing)
                    {
                        Game.Singleton.Player.EquipmentRightRing = null;
                        Equipped = false;
                        EquippedRightRing = false;
                    }

                    break;

                case EquipmentLocations.Waist:

                    Game.Singleton.Player.EquipmentWaist = null;
                    Equipped = false;

                    break;
            }
        }

        #endregion
    }
}