﻿namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class MaxHealthPrefix : Prefix
    {
        #region Constructor

        public MaxHealthPrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 8;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1} to Maximum Health", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(5);
            TierMinimumItemLevels.Add(11);
            TierMinimumItemLevels.Add(18);
            TierMinimumItemLevels.Add(24);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(36);
            TierMinimumItemLevels.Add(44);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("Hale");
            TierNameStrings.Add("Healthy");
            TierNameStrings.Add("Sanguine");
            TierNameStrings.Add("Stalwart");
            TierNameStrings.Add("Stout");
            TierNameStrings.Add("Robust");
            TierNameStrings.Add("Rotund");
            TierNameStrings.Add("Virile");

            TierEffectValues.Add(Game.Singleton.Random.Next(3, 9));
            TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            TierEffectValues.Add(Game.Singleton.Random.Next(20, 29));
            TierEffectValues.Add(Game.Singleton.Random.Next(30, 39));
            TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            TierEffectValues.Add(Game.Singleton.Random.Next(70, 79));
        }

        #endregion
    }
}