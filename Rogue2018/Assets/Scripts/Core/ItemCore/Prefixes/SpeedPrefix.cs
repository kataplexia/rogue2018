﻿namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class SpeedPrefix : Prefix
    {
        #region Constructor

        public SpeedPrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Speed", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(15);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(40);
            TierMinimumItemLevels.Add(55);
            TierMinimumItemLevels.Add(86);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("Runner's");
            TierNameStrings.Add("Sprinter's");
            TierNameStrings.Add("Stallion's");
            TierNameStrings.Add("Gazelle's");
            TierNameStrings.Add("Cheetah's");
            TierNameStrings.Add("Hellion's");

            TierEffectValues.Add(10);
            TierEffectValues.Add(15);
            TierEffectValues.Add(20);
            TierEffectValues.Add(25);
            TierEffectValues.Add(30);
            TierEffectValues.Add(35);
        }

        #endregion
    }
}