﻿namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class MaxManaPrefix : Prefix
    {
        #region Constructor

        public MaxManaPrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 12;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1} to Maximum Mana", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(11);
            TierMinimumItemLevels.Add(17);
            TierMinimumItemLevels.Add(23);
            TierMinimumItemLevels.Add(29);
            TierMinimumItemLevels.Add(35);
            TierMinimumItemLevels.Add(42);
            TierMinimumItemLevels.Add(51);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(69);
            TierMinimumItemLevels.Add(75);
            TierMinimumItemLevels.Add(81);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("Beryl");
            TierNameStrings.Add("Cobalt");
            TierNameStrings.Add("Azure");
            TierNameStrings.Add("Sapphire");
            TierNameStrings.Add("Cerulean");
            TierNameStrings.Add("Aqua");
            TierNameStrings.Add("Opalescent");
            TierNameStrings.Add("Gentian");
            TierNameStrings.Add("Chalybeous");
            TierNameStrings.Add("Mazarine");
            TierNameStrings.Add("Blue");
            TierNameStrings.Add("Zaffre");

            TierEffectValues.Add(Game.Singleton.Random.Next(3, 9));
            TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            TierEffectValues.Add(Game.Singleton.Random.Next(20, 29));
            TierEffectValues.Add(Game.Singleton.Random.Next(30, 39));
            TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            TierEffectValues.Add(Game.Singleton.Random.Next(70, 79));
            TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            TierEffectValues.Add(Game.Singleton.Random.Next(70, 79));
        }

        #endregion
    }
}