﻿namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class SpellDamagePrefix : Prefix
    {
        #region Constructor

        public SpellDamagePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 2;
            MaximumItemLevel = 100;
            NumberOfTiers = 8;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Spell Damage", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(11);
            TierMinimumItemLevels.Add(23);
            TierMinimumItemLevels.Add(35);
            TierMinimumItemLevels.Add(46);
            TierMinimumItemLevels.Add(58);
            TierMinimumItemLevels.Add(64);
            TierMinimumItemLevels.Add(84);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("Apprentice's");
            TierNameStrings.Add("Adept's");
            TierNameStrings.Add("Scholar's");
            TierNameStrings.Add("Professor's");
            TierNameStrings.Add("Occultist's");
            TierNameStrings.Add("Incanter's");
            TierNameStrings.Add("Glyphic");
            TierNameStrings.Add("Runic");

            TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            TierEffectValues.Add(Game.Singleton.Random.Next(20, 29));
            TierEffectValues.Add(Game.Singleton.Random.Next(30, 39));
            TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            TierEffectValues.Add(Game.Singleton.Random.Next(70, 74));
            TierEffectValues.Add(Game.Singleton.Random.Next(75, 79));
        }

        #endregion
    }
}
