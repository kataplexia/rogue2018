﻿namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class PhysicalDamagePrefix : Prefix
    {
        #region Constructor

        public PhysicalDamagePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 8;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1}% to Physical Damage", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(11);
            TierMinimumItemLevels.Add(23);
            TierMinimumItemLevels.Add(35);
            TierMinimumItemLevels.Add(46);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(73);
            TierMinimumItemLevels.Add(83);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("Heavy");
            TierNameStrings.Add("Serrated");
            TierNameStrings.Add("Wicked");
            TierNameStrings.Add("Vicious");
            TierNameStrings.Add("Bloodthirsty");
            TierNameStrings.Add("Cruel");
            TierNameStrings.Add("Tyrannical");
            TierNameStrings.Add("Merciless");

            TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            TierEffectValues.Add(Game.Singleton.Random.Next(50, 64));
            TierEffectValues.Add(Game.Singleton.Random.Next(65, 84));
            TierEffectValues.Add(Game.Singleton.Random.Next(85, 109));
            TierEffectValues.Add(Game.Singleton.Random.Next(110, 134));
            TierEffectValues.Add(Game.Singleton.Random.Next(135, 154));
            TierEffectValues.Add(Game.Singleton.Random.Next(155, 169));
            TierEffectValues.Add(Game.Singleton.Random.Next(170, 179));
        }

        #endregion
    }
}
