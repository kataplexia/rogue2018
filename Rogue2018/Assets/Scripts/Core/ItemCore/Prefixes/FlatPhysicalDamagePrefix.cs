﻿namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class FlatPhysicalDamagePrefix : Prefix
    {
        #region Constructor

        public FlatPhysicalDamagePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            MinimumItemLevel = 5;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct(int itemLevel)
        {
            SetupLists();
            ConstructTiers();
            SetupNameAndValue(itemLevel);
            EffectInfoString = string.Format("{0} +{1} to Physical Damage", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(13);
            TierMinimumItemLevels.Add(19);
            TierMinimumItemLevels.Add(28);
            TierMinimumItemLevels.Add(35);
            TierMinimumItemLevels.Add(44);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            TierNameStrings.Add("Glinting");
            TierNameStrings.Add("Burnished");
            TierNameStrings.Add("Polished");
            TierNameStrings.Add("Honed");
            TierNameStrings.Add("Gleaming");
            TierNameStrings.Add("Annealed");

            TierEffectValues.Add(Game.Singleton.Random.Next(1, 2));
            TierEffectValues.Add(Game.Singleton.Random.Next(2, 5));
            TierEffectValues.Add(Game.Singleton.Random.Next(3, 7));
            TierEffectValues.Add(Game.Singleton.Random.Next(4, 10));
            TierEffectValues.Add(Game.Singleton.Random.Next(5, 12));
            TierEffectValues.Add(Game.Singleton.Random.Next(6, 15));
        }

        #endregion
    }
}