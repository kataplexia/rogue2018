﻿namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces.ItemInterfaces;

    [System.Serializable]
    public abstract class Implicit : IImplicit
    {
        #region Fields

        private string effectInfoString;
        private int effectValue;

        #endregion

        #region Properties

        public string EffectInfoString
        {
            get { return effectInfoString; }
            set { effectInfoString = value; }
        }

        public int EffectValue
        {
            get { return effectValue; }
            set { effectValue = value; }
        }

        #endregion

        #region Methods

        public abstract void ApplyEffect();

        public abstract void Construct();

        #endregion
    }
}