﻿namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    public class ItemCollection
    {
        #region Fields

        private List<IItem> allItems;

        private List<EquipableItem> rings;
        private List<EquipableItem> belts;
        private List<EquipableItem> quivers;
        private List<EquipableItem> foci;
        private List<EquipableItem> heavyChestArmours;
        private List<EquipableItem> mediumChestArmours;
        private List<EquipableItem> lightChestArmours;
        private List<EquipableItem> heavyFeetArmours;
        private List<EquipableItem> mediumFeetArmours;
        private List<EquipableItem> lightFeetArmours;
        private List<EquipableItem> heavyHandsArmours;
        private List<EquipableItem> mediumHandsArmours;
        private List<EquipableItem> lightHandsArmours;
        private List<EquipableItem> heavyHeadArmours;
        private List<EquipableItem> mediumHeadArmours;
        private List<EquipableItem> lightHeadArmours;
        private List<EquipableItem> heavyLegsArmours;
        private List<EquipableItem> mediumLegsArmours;
        private List<EquipableItem> lightLegsArmours;
        private List<EquipableItem> heavyShields;
        private List<EquipableItem> mediumShields;
        private List<EquipableItem> lightShields;
        private List<EquipableItem> oneHandedGenericSwords;
        private List<EquipableItem> oneHandedPierceSwords;
        private List<EquipableItem> twoHandedSwords;
        private List<EquipableItem> oneHandedAxes;
        private List<EquipableItem> twoHandedAxes;
        private List<EquipableItem> oneHandedMaces;
        private List<EquipableItem> twoHandedMaces;
        private List<EquipableItem> bows;
        private List<EquipableItem> daggers;
        private List<EquipableItem> sceptres;
        private List<EquipableItem> staves;
        private List<EquipableItem> wands;

        #endregion

        #region Constructor

        public ItemCollection()
        {
            InitializeLists();

            AddRings();
            AddBelts();
            AddQuivers();
            AddFoci();
            AddChestArmours();
            AddFeetArmours();
            AddHandsArmours();
            AddHeadArmours();
            AddLegsArmours();
            AddShields();
            AddSwords();
            AddAxes();
            AddMaces();
            AddBows();
            AddDaggers();
            AddSceptres();
            AddStaves();
            AddWands();

            AddAllItems();
        }

        #endregion

        #region Methods

        public EquipableItem GetRandomEquipableItem()
        {
            int randomIndex = Game.Singleton.Random.Next(0, allItems.Count - 1);
            if (!(allItems[randomIndex] is EquipableItem))
            {
                return GetRandomEquipableItem();
            }

            IItem returnItem = Game.Singleton.Tools.DeepCopy<IItem>(allItems[randomIndex]);
            EquipableItem returnEquipable = returnItem as EquipableItem;
            returnEquipable.Construct();
            return returnEquipable;
        }

        private void InitializeLists()
        {
            rings = new List<EquipableItem>();
            belts = new List<EquipableItem>();
            quivers = new List<EquipableItem>();
            foci = new List<EquipableItem>();
            heavyChestArmours = new List<EquipableItem>();
            mediumChestArmours = new List<EquipableItem>();
            lightChestArmours = new List<EquipableItem>();
            heavyFeetArmours = new List<EquipableItem>();
            mediumFeetArmours = new List<EquipableItem>();
            lightFeetArmours = new List<EquipableItem>();
            heavyHandsArmours = new List<EquipableItem>();
            mediumHandsArmours = new List<EquipableItem>();
            lightHandsArmours = new List<EquipableItem>();
            heavyHeadArmours = new List<EquipableItem>();
            mediumHeadArmours = new List<EquipableItem>();
            lightHeadArmours = new List<EquipableItem>();
            heavyLegsArmours = new List<EquipableItem>();
            mediumLegsArmours = new List<EquipableItem>();
            lightLegsArmours = new List<EquipableItem>();
            heavyShields = new List<EquipableItem>();
            mediumShields = new List<EquipableItem>();
            lightShields = new List<EquipableItem>();
            oneHandedGenericSwords = new List<EquipableItem>();
            oneHandedPierceSwords = new List<EquipableItem>();
            twoHandedSwords = new List<EquipableItem>();
            oneHandedAxes = new List<EquipableItem>();
            twoHandedAxes = new List<EquipableItem>();
            oneHandedMaces = new List<EquipableItem>();
            twoHandedMaces = new List<EquipableItem>();
            bows = new List<EquipableItem>();
            daggers = new List<EquipableItem>();
            sceptres = new List<EquipableItem>();
            staves = new List<EquipableItem>();
            wands = new List<EquipableItem>();
        }

        private void AddRings()
        {
            rings.Add(new AgateRing());
            rings.Add(new RubyRing());
            rings.Add(new ThornedRing());
        }

        private void AddBelts()
        {
            belts.Add(new ChainBelt());
            belts.Add(new LeatherBelt());
            belts.Add(new SilkThread());
        }

        private void AddQuivers()
        {
            quivers.Add(new BluntQuiver());
            quivers.Add(new SerratedQuiver());
            quivers.Add(new TwoPointQuiver());
        }

        private void AddFoci()
        {
            foci.Add(new Censer());
            foci.Add(new Grimoire());
            foci.Add(new Tome());
        }

        private void AddChestArmours()
        {
            heavyChestArmours.Add(new ChestPlate());
            heavyChestArmours.Add(new CopperPlate());
            heavyChestArmours.Add(new PlateVest());

            mediumChestArmours.Add(new PatchleatherTunic());
            mediumChestArmours.Add(new ShabbyJerkin());
            mediumChestArmours.Add(new StrappedLeather());

            lightChestArmours.Add(new ScholarsRobe());
            lightChestArmours.Add(new SilkVest());
            lightChestArmours.Add(new SimpleRobe());
        }

        private void AddFeetArmours()
        {
            heavyFeetArmours.Add(new IronGreaves());
            heavyFeetArmours.Add(new PlatedGreaves());
            heavyFeetArmours.Add(new SteelGreaves());

            mediumFeetArmours.Add(new DeerskinBoots());
            mediumFeetArmours.Add(new PatchleatherBoots());
            mediumFeetArmours.Add(new RawhideBoots());

            lightFeetArmours.Add(new SilkSlippers());
            lightFeetArmours.Add(new StitchedSlippers());
            lightFeetArmours.Add(new VelvetSlippers());
        }

        private void AddHandsArmours()
        {
            heavyHandsArmours.Add(new IronGauntlets());
            heavyHandsArmours.Add(new PlatedGauntlets());
            heavyHandsArmours.Add(new SteelGauntlets());

            mediumHandsArmours.Add(new DeerskinGloves());
            mediumHandsArmours.Add(new PatchleatherGloves());
            mediumHandsArmours.Add(new RawhideGloves());

            lightHandsArmours.Add(new SilkGloves());
            lightHandsArmours.Add(new StitchedGloves());
            lightHandsArmours.Add(new VelvetGloves());
        }

        private void AddHeadArmours()
        {
            heavyHeadArmours.Add(new BarbuteHelmet());
            heavyHeadArmours.Add(new ConeHelmet());
            heavyHeadArmours.Add(new IronHat());

            mediumHeadArmours.Add(new LeatherCap());
            mediumHeadArmours.Add(new LeatherHood());
            mediumHeadArmours.Add(new Tricorne());

            lightHeadArmours.Add(new IronCirclet());
            lightHeadArmours.Add(new TribalCirclet());
            lightHeadArmours.Add(new VineCirclet());
        }

        private void AddLegsArmours()
        {
            heavyLegsArmours.Add(new IronLegguards());
            heavyLegsArmours.Add(new PlatedLeggaurds());
            heavyLegsArmours.Add(new SteelLeggaurds());

            mediumLegsArmours.Add(new DeerskinLeggings());
            mediumLegsArmours.Add(new PatchleatherLeggings());
            mediumLegsArmours.Add(new RawhideLeggings());

            lightLegsArmours.Add(new SilkPants());
            lightLegsArmours.Add(new StitchedPants());
            lightLegsArmours.Add(new VelvetPants());
        }

        private void AddShields()
        {
            heavyShields.Add(new RawhideTowerShield());
            heavyShields.Add(new RustedTowerShield());
            heavyShields.Add(new SimpleTowerShield());

            mediumShields.Add(new PaintedBuckler());
            mediumShields.Add(new PineBuckler());
            mediumShields.Add(new RawhideBuckler());

            lightShields.Add(new BoneSoulShield());
            lightShields.Add(new TwigSoulShield());
            lightShields.Add(new YewSoulShield());
        }

        private void AddSwords()
        {
            oneHandedGenericSwords.Add(new CopperSword());
            oneHandedGenericSwords.Add(new Cutlass());
            oneHandedGenericSwords.Add(new RustedSword());

            oneHandedPierceSwords.Add(new BoneRapier());
            oneHandedPierceSwords.Add(new DentedFoil());
            oneHandedPierceSwords.Add(new RustedSpike());

            twoHandedSwords.Add(new BastardSword());
            twoHandedSwords.Add(new LongSword());
            twoHandedSwords.Add(new RustedBlade());
        }

        private void AddAxes()
        {
            oneHandedAxes.Add(new CopperHatchet());
            oneHandedAxes.Add(new FellingHatchet());
            oneHandedAxes.Add(new RustedHatchet());

            twoHandedAxes.Add(new CopperAxe());
            twoHandedAxes.Add(new SplittingAxe());
            twoHandedAxes.Add(new StoneAxe());
        }

        private void AddMaces()
        {
            oneHandedMaces.Add(new SpikedClub());
            oneHandedMaces.Add(new TribalClub());
            oneHandedMaces.Add(new WoodenClub());

            twoHandedMaces.Add(new Mallet());
            twoHandedMaces.Add(new TribalMaul());
            twoHandedMaces.Add(new WoodenMaul());
        }

        private void AddBows()
        {
            bows.Add(new CrudeBow());
            bows.Add(new ShortBow());
            bows.Add(new LongBow());
        }

        private void AddDaggers()
        {
            daggers.Add(new CarvingKnife());
            daggers.Add(new CrudeShank());
            daggers.Add(new SkinningKnife());
        }

        private void AddSceptres()
        {
            sceptres.Add(new BronzeSceptre());
            sceptres.Add(new TribalSceptre());
            sceptres.Add(new WoodenSceptre());
        }

        private void AddStaves()
        {
            staves.Add(new CarvedBranch());
            staves.Add(new LongStaff());
            staves.Add(new PrimitiveStaff());
        }

        private void AddWands()
        {
            wands.Add(new CarvedWand());
            wands.Add(new QuartzWand());
            wands.Add(new TwigWand());
        }

        private void AddAllItems()
        {
            allItems = new List<IItem>(
                rings.Count +
                belts.Count +
                quivers.Count +
                foci.Count +
                heavyChestArmours.Count +
                mediumChestArmours.Count +
                lightChestArmours.Count +
                heavyFeetArmours.Count +
                mediumFeetArmours.Count +
                lightFeetArmours.Count +
                heavyHandsArmours.Count +
                mediumHandsArmours.Count +
                lightHandsArmours.Count +
                heavyHeadArmours.Count +
                mediumHeadArmours.Count +
                lightHeadArmours.Count +
                heavyLegsArmours.Count +
                mediumLegsArmours.Count +
                lightLegsArmours.Count +
                heavyShields.Count +
                mediumShields.Count +
                lightShields.Count +
                oneHandedGenericSwords.Count +
                oneHandedPierceSwords.Count +
                twoHandedSwords.Count +
                oneHandedMaces.Count +
                twoHandedMaces.Count +
                bows.Count +
                daggers.Count +
                sceptres.Count +
                staves.Count +
                wands.Count);

            allItems.AddRange(rings);
            allItems.AddRange(belts);
            allItems.AddRange(quivers);
            allItems.AddRange(foci);
            allItems.AddRange(heavyChestArmours);
            allItems.AddRange(mediumChestArmours);
            allItems.AddRange(lightChestArmours);
            allItems.AddRange(heavyFeetArmours);
            allItems.AddRange(mediumFeetArmours);
            allItems.AddRange(lightFeetArmours);
            allItems.AddRange(heavyHandsArmours);
            allItems.AddRange(mediumHandsArmours);
            allItems.AddRange(lightHandsArmours);
            allItems.AddRange(heavyHeadArmours);
            allItems.AddRange(mediumHeadArmours);
            allItems.AddRange(lightHeadArmours);
            allItems.AddRange(heavyLegsArmours);
            allItems.AddRange(mediumLegsArmours);
            allItems.AddRange(lightLegsArmours);
            allItems.AddRange(heavyShields);
            allItems.AddRange(mediumShields);
            allItems.AddRange(lightShields);
            allItems.AddRange(oneHandedGenericSwords);
            allItems.AddRange(oneHandedPierceSwords);
            allItems.AddRange(twoHandedSwords);
            allItems.AddRange(oneHandedAxes);
            allItems.AddRange(twoHandedAxes);
            allItems.AddRange(oneHandedMaces);
            allItems.AddRange(twoHandedMaces);
            allItems.AddRange(bows);
            allItems.AddRange(daggers);
            allItems.AddRange(sceptres);
            allItems.AddRange(staves);
            allItems.AddRange(wands);
        }

        #endregion
    }
}