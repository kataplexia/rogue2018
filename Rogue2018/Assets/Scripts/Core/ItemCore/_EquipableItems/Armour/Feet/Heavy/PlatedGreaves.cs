﻿// <auto-generated/>
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PlatedGreaves : Armour
    {
        public PlatedGreaves()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyFeetArmour;
            Name = "Plated Greaves";
            Description = "A pair of plated greaves.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 5;
            DefenceChance = 5;
            SpeedModifier = -3;
        }
    }
}