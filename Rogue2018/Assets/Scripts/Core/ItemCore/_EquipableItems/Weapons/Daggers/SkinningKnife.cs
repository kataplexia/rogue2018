﻿// <auto-generated/>
using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SkinningKnife : Weapon
    {
        public SkinningKnife()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Dagger;
            Name = "Skinning Knife";
            Description = "A skinning knife.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Dagger;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 3;
            HitChance = 20;
            Implicit = new CriticalChanceImplicit();
        }
    }
}