﻿// <auto-generated/>
using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class CopperSword : Weapon
    {
        public CopperSword()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedSwordGeneric;
            Name = "Copper Sword";
            Description = "A copper sword.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Sword;
            DamageType = DamageTypes.Cut;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 3;
            HitChance = 20;
            Implicit = new HitChanceImplicit();
        }
    }
}