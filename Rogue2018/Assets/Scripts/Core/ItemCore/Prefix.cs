﻿namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;
    using Rogue2018.Interfaces.ItemInterfaces;

    [System.Serializable]
    public abstract class Prefix : IPrefix
    {
        #region Fields

        private string effectInfoString;
        private int effectValue;
        private int maximumItemLevel;
        private int minimumItemLevel;
        private int numberOfTiers;
        private List<EquipableItemBaseTypes> prefixBaseTypes;
        private string prefixNameString;
        private List<int> tierEffectValues;
        private string tierInfoString;
        private List<int> tierMinimumItemLevels;
        private List<string> tierNameStrings;

        #endregion

        #region Properties

        public string EffectInfoString
        {
            get { return effectInfoString; }
            set { effectInfoString = value; }
        }

        public int EffectValue
        {
            get { return effectValue; }
            set { effectValue = value; }
        }

        public int MaximumItemLevel
        {
            get { return maximumItemLevel; }
            set { maximumItemLevel = value; }
        }

        public int MinimumItemLevel
        {
            get { return minimumItemLevel; }
            set { minimumItemLevel = value; }
        }

        public int NumberOfTiers
        {
            get { return numberOfTiers; }
            set { numberOfTiers = value; }
        }

        public List<EquipableItemBaseTypes> PrefixBaseTypes
        {
            get { return prefixBaseTypes; }
            set { prefixBaseTypes = value; }
        }

        public string PrefixNameString
        {
            get { return prefixNameString; }
            set { prefixNameString = value; }
        }

        public List<int> TierEffectValues
        {
            get { return tierEffectValues; }
            set { tierEffectValues = value; }
        }

        public string TierInfoString
        {
            get { return tierInfoString; }
            set { tierInfoString = value; }
        }

        public List<int> TierMinimumItemLevels
        {
            get { return tierMinimumItemLevels; }
            set { tierMinimumItemLevels = value; }
        }

        public List<string> TierNameStrings
        {
            get { return tierNameStrings; }
            set { tierNameStrings = value; }
        }

        #endregion

        #region Methods

        public abstract void ApplyEffect();

        public abstract void Construct(int itemLevel);

        public abstract void ConstructTiers();

        public void SetupLists()
        {
            TierMinimumItemLevels = new List<int>(NumberOfTiers + 1);
            TierNameStrings = new List<string>(NumberOfTiers);
            TierEffectValues = new List<int>(NumberOfTiers);
        }

        public void SetupNameAndValue(int itemLevel)
        {
            for (int i = 0; i < NumberOfTiers; i++)
            {
                if (itemLevel >= TierMinimumItemLevels[i] && itemLevel < TierMinimumItemLevels[i + 1])
                {
                    if (i > 0)
                    {
                        int randomTierIndex = Game.Singleton.Random.Next(0, i);
                        PrefixNameString = TierNameStrings[randomTierIndex];
                        EffectValue = TierEffectValues[randomTierIndex];
                        TierInfoString = string.Format("[Tier {0}]", NumberOfTiers - randomTierIndex);
                    }
                    else
                    {
                        PrefixNameString = TierNameStrings[i];
                        EffectValue = TierEffectValues[i];
                        TierInfoString = string.Format("[Tier {0}]", NumberOfTiers - i);
                    }
                }
            }
        }

        #endregion
    }
}