﻿namespace Rogue2018.Core.ItemCore
{
    using System.IO;
    using System.Xml;
    using Rogue2018.Interfaces;
    using UnityEngine;

    public class ItemNameGenerator
    {
        #region Fields

        private string[] preNamesArray;
        private string[] soulShieldsPostNamesArray;
        private string[] otherShieldsPostNamesArray;
        private string[] chestArmoursPostNamesArray;
        private string[] headArmoursPostNamesArray;
        private string[] handsArmoursPostNamesArray;
        private string[] feetArmoursPostNamesArray;
        private string[] legsArmoursPostNameArray;
        private string[] ringsPostNameArray;
        private string[] beltsPostNameArray;
        private string[] quiversPostNameArray;
        private string[] axesPostNameArray;
        private string[] macesPostNameArray;
        private string[] sceptresPostNameArray;
        private string[] stavesPostNameArray;
        private string[] swordsPostNameArray;
        private string[] daggersPostNameArray;
        private string[] bowsPostNameArray;
        private string[] wandsPostNameArray;
        private string[] fociPostNameArray;

        #endregion

        #region Constructor

        public ItemNameGenerator()
        {
            string xmlPath = "ItemNameData";
            var xmlRawFile = Resources.Load<TextAsset>(xmlPath);
            string xmlData = xmlRawFile.text;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(new StringReader(xmlData));

            string[] seperatingChars = { ", " };

            XmlNode preNamesNode = xmlDocument.SelectSingleNode("/data/prenames");
            string preNamesString = preNamesNode.InnerXml;
            preNamesArray = preNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode soulShieldPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/soulshields");
            string soulShieldPostNamesString = soulShieldPostNamesNode.InnerXml;
            soulShieldsPostNamesArray = soulShieldPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode otherShieldPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/othershields");
            string otherShieldPostNamesString = otherShieldPostNamesNode.InnerXml;
            otherShieldsPostNamesArray = otherShieldPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode chestArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/chestarmours");
            string chestArmoursPostNamesString = chestArmoursPostNamesNode.InnerXml;
            chestArmoursPostNamesArray = chestArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode headArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/headarmours");
            string headArmoursPostNamesString = headArmoursPostNamesNode.InnerXml;
            headArmoursPostNamesArray = headArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode handsArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/handsarmours");
            string handsArmoursPostNamesString = handsArmoursPostNamesNode.InnerXml;
            handsArmoursPostNamesArray = handsArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode feetArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/feetarmours");
            string feetArmoursPostNamesString = feetArmoursPostNamesNode.InnerXml;
            feetArmoursPostNamesArray = feetArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode legsArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/legsarmours");
            string legsArmoursPostNamesString = legsArmoursPostNamesNode.InnerXml;
            legsArmoursPostNameArray = legsArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode ringsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/rings");
            string ringsPostNamesString = ringsPostNamesNode.InnerXml;
            ringsPostNameArray = ringsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode beltsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/belts");
            string beltsPostNamesString = beltsPostNamesNode.InnerXml;
            beltsPostNameArray = beltsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode quiversPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/quivers");
            string quiversPostNamesString = quiversPostNamesNode.InnerXml;
            quiversPostNameArray = quiversPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode axesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/axes");
            string axesPostNamesString = axesPostNamesNode.InnerXml;
            axesPostNameArray = axesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode macesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/maces");
            string macesPostNamesString = macesPostNamesNode.InnerXml;
            macesPostNameArray = macesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode sceptresPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/sceptres");
            string sceptresPostNamesString = sceptresPostNamesNode.InnerXml;
            sceptresPostNameArray = sceptresPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode stavesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/staves");
            string stavesPostNamesString = stavesPostNamesNode.InnerXml;
            stavesPostNameArray = stavesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode swordsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/swords");
            string swordsPostNamesString = swordsPostNamesNode.InnerXml;
            swordsPostNameArray = swordsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode daggersPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/daggers");
            string daggersPostNamesString = daggersPostNamesNode.InnerXml;
            daggersPostNameArray = daggersPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode bowsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/bows");
            string bowsPostNamesString = bowsPostNamesNode.InnerXml;
            bowsPostNameArray = bowsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode wandsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/wands");
            string wandsPostNamesString = wandsPostNamesNode.InnerXml;
            wandsPostNameArray = wandsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode fociPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/foci");
            string fociPostNamesString = fociPostNamesNode.InnerXml;
            fociPostNameArray = fociPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);
        }

        #endregion

        #region Methods

        public string CreateRandomName(EquipableItem equipableItem)
        {
            string preName = preNamesArray[Game.Singleton.Random.Next(0, preNamesArray.Length - 1)];
            string postName = string.Empty;

            if (equipableItem is IWeapon)
            {
                IWeapon equipableWeapon = equipableItem as IWeapon;
                if (equipableWeapon.WeaponType == WeaponTypes.Axe)
                {
                    postName = axesPostNameArray[Game.Singleton.Random.Next(0, axesPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Bow)
                {
                    postName = bowsPostNameArray[Game.Singleton.Random.Next(0, bowsPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Dagger)
                {
                    postName = daggersPostNameArray[Game.Singleton.Random.Next(0, daggersPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Mace)
                {
                    postName = macesPostNameArray[Game.Singleton.Random.Next(0, macesPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Sceptre)
                {
                    postName = sceptresPostNameArray[Game.Singleton.Random.Next(0, sceptresPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Staff)
                {
                    postName = stavesPostNameArray[Game.Singleton.Random.Next(0, stavesPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Sword)
                {
                    postName = swordsPostNameArray[Game.Singleton.Random.Next(0, swordsPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Wand)
                {
                    postName = wandsPostNameArray[Game.Singleton.Random.Next(0, wandsPostNameArray.Length - 1)];
                }
            }
            else if (equipableItem is IArmour)
            {
                if (equipableItem.EquipmentLocation == EquipmentLocations.Chest)
                {
                    postName = chestArmoursPostNamesArray[Game.Singleton.Random.Next(0, chestArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Feet)
                {
                    postName = feetArmoursPostNamesArray[Game.Singleton.Random.Next(0, feetArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Hands)
                {
                    postName = handsArmoursPostNamesArray[Game.Singleton.Random.Next(0, handsArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Head)
                {
                    postName = headArmoursPostNamesArray[Game.Singleton.Random.Next(0, headArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Legs)
                {
                    postName = legsArmoursPostNameArray[Game.Singleton.Random.Next(0, legsArmoursPostNameArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.OffHand)
                {
                    IArmour equipableArmor = equipableItem as IArmour;
                    if (equipableArmor.ArmourStyle == ArmourStyles.Light)
                    {
                        postName = soulShieldsPostNamesArray[Game.Singleton.Random.Next(0, soulShieldsPostNamesArray.Length - 1)];
                    }
                    else
                    {
                        postName = otherShieldsPostNamesArray[Game.Singleton.Random.Next(0, otherShieldsPostNamesArray.Length - 1)];
                    }
                }
            }
            else if (equipableItem is IAccessory)
            {
                IAccessory equipableAccessory = equipableItem as IAccessory;
                if (equipableAccessory.AccessoryType == AccessoryTypes.Belt)
                {
                    postName = beltsPostNameArray[Game.Singleton.Random.Next(0, beltsPostNameArray.Length - 1)];
                }
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Focus)
                {
                    postName = fociPostNameArray[Game.Singleton.Random.Next(0, fociPostNameArray.Length - 1)];
                }
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Quiver)
                {
                    postName = quiversPostNameArray[Game.Singleton.Random.Next(0, quiversPostNameArray.Length - 1)];
                }
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Ring)
                {
                    postName = ringsPostNameArray[Game.Singleton.Random.Next(0, ringsPostNameArray.Length - 1)];
                }
            }

            return preName + " " + postName;
        }

        #endregion
    }
}