﻿namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore.Implicits;

    public class ItemImplicits
    {
        #region Fields

        private List<Implicit> implicits;

        #endregion

        #region Constructor

        public ItemImplicits()
        {
            implicits = new List<Implicit>();

            implicits.Add(new HitChanceImplicit());
            implicits.Add(new FlatPhysicalDamageImplicit());
            implicits.Add(new AwarenessImplicit());
            implicits.Add(new CriticalChanceImplicit());
            implicits.Add(new CriticalDamageImplicit());
            implicits.Add(new DefenceChanceImplicit());
            implicits.Add(new ElementalDamageImplicit());
            implicits.Add(new MaxHealthImplicit());
            implicits.Add(new MaxManaImplicit());
            implicits.Add(new SpeedImplicit());
            implicits.Add(new SpellCriticalChanceImplicit());
            implicits.Add(new SpellCriticalDamageImplicit());
            implicits.Add(new SpellDamageImplicit());
            implicits.Add(new StunChanceImplicit());
        }

        #endregion

        #region Methods

        public Implicit GetRandomImplicit()
        {
            int randomIndex = Game.Singleton.Random.Next(0, implicits.Count - 1);
            Implicit returnImplicit = Game.Singleton.Tools.DeepCopy<Implicit>(implicits[randomIndex]);
            returnImplicit.Construct();
            return returnImplicit;
        }

        #endregion
    }
}