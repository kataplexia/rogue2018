﻿namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    public class ItemGenerator
    {
        #region Methods

        public EquipableItem CreateEquipableItem(EquipableItem itemBase, ItemQualities itemQuality, int itemLevel)
        {
            EquipableItem equipableItem = itemBase;
            equipableItem.ItemQuality = itemQuality;
            equipableItem.ItemLevel = itemLevel;

            equipableItem.Construct();
            return equipableItem;
        }

        public UsableItem CreateUsableItem(UsableItem itemBase)
        {
            UsableItem usableItem = itemBase;

            usableItem.Construct();
            return usableItem;
        }

        #endregion
    }
}