﻿namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    [System.Serializable]
    public class MaxHealthImplicit : Implicit
    {
        #region Constructor

        public MaxHealthImplicit()
        {
            Construct();
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D10");
            EffectInfoString = string.Format("+{0} to Maximum Health", EffectValue);
        }

        #endregion
    }
}
