﻿namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    [System.Serializable]
    public class DefenceChanceImplicit : Implicit
    {
        #region Constructor

        public DefenceChanceImplicit()
        {
            Construct();
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D5");
            EffectInfoString = string.Format("+{0}% to Defence Chance", EffectValue);
        }

        #endregion
    }
}
