﻿namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    [System.Serializable]
    public class AwarenessImplicit : Implicit
    {
        #region Constructor

        public AwarenessImplicit()
        {
            Construct();
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D5");
            EffectInfoString = string.Format("+{0} to Awareness", EffectValue);
        }

        #endregion
    }
}