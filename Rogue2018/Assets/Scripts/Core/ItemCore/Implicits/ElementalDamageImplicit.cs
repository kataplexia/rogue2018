﻿namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    [System.Serializable]
    public class ElementalDamageImplicit : Implicit
    {
        #region Constructor

        public ElementalDamageImplicit()
        {
            Construct();
        }

        #endregion

        #region Methods

        public override void ApplyEffect()
        {
            // TODO
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D10");
            EffectInfoString = string.Format("+{0}% to Elemental Damage", EffectValue);
        }

        #endregion
    }
}