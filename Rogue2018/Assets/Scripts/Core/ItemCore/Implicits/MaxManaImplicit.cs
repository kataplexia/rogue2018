﻿namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    [System.Serializable]
    public class MaxManaImplicit : Implicit
    {
        #region Constructor

        public MaxManaImplicit()
        {
            Construct();
        }

        #endregion

        #region Constructor

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D10");
            EffectInfoString = string.Format("+{0} to Maximum Mana", EffectValue);
        }

        #endregion
    }
}
