﻿namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    [System.Serializable]
    public class SpeedImplicit : Implicit
    {
        #region Constructor

        public SpeedImplicit()
        {
            Construct();
        }

        #endregion

        #region Constructor

        public override void ApplyEffect()
        {
            //// TODO
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D5");
            EffectInfoString = string.Format("+{0}% to Speed", EffectValue);
        }

        #endregion
    }
}