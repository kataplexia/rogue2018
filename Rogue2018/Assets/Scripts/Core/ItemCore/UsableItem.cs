﻿namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;
    using UnityEngine;

    public abstract class UsableItem : IItem, IUsable
    {
        #region Fields

        private int currentStackCount;
        private string description;
        private ItemQualities itemQuality;
        private int maxStackCount;
        private string name;
        private System.Type parentType;

        #endregion

        #region Properties

        public int CurrentStackCount
        {
            get { return currentStackCount; }
            set { currentStackCount = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public ItemQualities ItemQuality
        {
            get { return itemQuality; }
            set { itemQuality = value; }
        }

        public int MaxStackCount
        {
            get { return maxStackCount; }
            set { maxStackCount = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public System.Type ParentType
        {
            get { return parentType; }
            set { parentType = value; }
        }

        #endregion

        #region Methods

        public abstract void Construct();

        public void DrawName(int xOffset, int yOffset, Color drawColor)
        {
            Color backgroundDrawColor = Game.Singleton.Tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            Game.Singleton.Tools.DrawString(xOffset, yOffset, Name, drawColor, backgroundDrawColor, 0);
        }

        public void DrawStacks(int xOffset, int yOffset, Color drawColor)
        {
            Color backgroundDrawColor = Game.Singleton.Tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            Game.Singleton.Tools.DrawString(xOffset, yOffset, string.Format("[{0}/{1}]", CurrentStackCount, MaxStackCount), drawColor, backgroundDrawColor, 0);
        }

        public void Drop(bool dropAll = false)
        {
            if (dropAll)
            {
                Game.Singleton.Player.InventoryList.Remove(this);

                //// TODO: Place item inside list for current cell's contents

                string droppedMessage = string.Format("You dropped {0}x {1} on the ground", CurrentStackCount, Name);
                Game.Singleton.MessageSystem.Add(droppedMessage, Color.cyan, false);
                Game.Singleton.SetDrawRequired(true);
            }
            else
            {
                CurrentStackCount--;
                if (CurrentStackCount == 0)
                {
                    Game.Singleton.Player.InventoryList.Remove(this);
                }

                //// Create instance for inside list for current cell's contents
                //// System.Object instance = System.Activator.CreateInstance(ParentType);
                //// UsableItem droppedUsable = Game.Singleton.itemGenerator.CreateUsableItem((UsableItem)instance);
                //// Debug.Log("Created instance of: " + droppedUsable.Name + ", which has [" + droppedUsable.CurrentStackCount + "/" + droppedUsable.MaxStackCount + "] stack property.");

                string droppedMessage = string.Format("You dropped 1x {0} on the ground", Name);
                Game.Singleton.MessageSystem.Add(droppedMessage, Color.cyan, false);
                Game.Singleton.SetDrawRequired(true);
            }
        }

        public abstract void Use();

        #endregion
    }
}