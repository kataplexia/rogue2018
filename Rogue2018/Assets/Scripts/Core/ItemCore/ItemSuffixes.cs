﻿namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore.Suffixes;
    using Rogue2018.Interfaces;

    public class ItemSuffixes : ItemAffixes
    {
        #region Fields

        private List<Suffix> suffixesList;

        #endregion

        #region Constructor

        public ItemSuffixes()
        {
            suffixesList = new List<Suffix>();

            suffixesList.Add(new AwarenessSuffix());
            suffixesList.Add(new ColdResistanceSuffix());
            suffixesList.Add(new CriticalChanceSuffix());
            suffixesList.Add(new CriticalDamageSuffix());
            suffixesList.Add(new ElementalResistanceSuffix());
            suffixesList.Add(new FireResistanceSuffix());
            suffixesList.Add(new HitChanceSuffix());
            suffixesList.Add(new LightningResistanceSuffix());
            suffixesList.Add(new PlagueResistanceSuffix());
            suffixesList.Add(new SpellCriticalChanceSuffix());

            SetupEBTSuffixLists(suffixesList);
        }

        #endregion

        #region Methods

        public Suffix GetRandomSuffix(List<Suffix> currentSuffixList, int itemLevel, EquipableItemBaseTypes ebt)
        {
            List<Suffix> suffixListForEBT = GetSuffixForEBT(ebt);
            List<Suffix> modifiedSuffixesList = new List<Suffix>();

            for (int i = 0; i < suffixListForEBT.Count; i++)
            {
                bool currentListContainsMatch = false;
                foreach (Suffix currentsuffix in currentSuffixList)
                {
                    if (currentsuffix.GetType().Name == suffixListForEBT[i].GetType().Name)
                    {
                        currentListContainsMatch = true;
                    }
                }

                if (!currentListContainsMatch)
                {
                    modifiedSuffixesList.Add(suffixListForEBT[i]);
                }
            }

            int randomIndex = Game.Singleton.Random.Next(0, modifiedSuffixesList.Count - 1);
            Suffix selectedSuffix = modifiedSuffixesList[randomIndex];

            Suffix returnSuffix = Game.Singleton.Tools.DeepCopy<Suffix>(modifiedSuffixesList[randomIndex]);
            returnSuffix.Construct(itemLevel);
            return returnSuffix;
        }

        #endregion
    }
}