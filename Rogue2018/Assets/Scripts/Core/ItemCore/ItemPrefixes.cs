﻿namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore.Prefixes;
    using Rogue2018.Interfaces;

    public class ItemPrefixes : ItemAffixes
    {
        #region Fields

        private List<Prefix> prefixesList;

        #endregion

        #region Constructor

        public ItemPrefixes()
        {
            prefixesList = new List<Prefix>();

            prefixesList.Add(new DefenceChancePrefix());
            prefixesList.Add(new ElementalDamagePrefix());
            prefixesList.Add(new FlatDefenceValuePrefix());
            prefixesList.Add(new FlatPhysicalDamagePrefix());
            prefixesList.Add(new MaxHealthPrefix());
            prefixesList.Add(new MaxManaPrefix());
            prefixesList.Add(new PhysicalDamagePrefix());
            prefixesList.Add(new SpeedPrefix());
            prefixesList.Add(new SpellDamagePrefix());

            SetupEBTPrefixLists(prefixesList);
        }

        #endregion

        #region Methods

        public Prefix GetRandomPrefix(List<Prefix> currentPrefixList, int itemLevel, EquipableItemBaseTypes ebt)
        {
            List<Prefix> prefixListForEBT = GetPrefixForEBT(ebt);
            List<Prefix> modifiedPrefixesList = new List<Prefix>();

            for (int i = 0; i < prefixListForEBT.Count; i++)
            {
                bool currentListContainsMatch = false;
                foreach (Prefix currentPrefix in currentPrefixList)
                {
                    if (currentPrefix.GetType().Name == prefixListForEBT[i].GetType().Name)
                    {
                        currentListContainsMatch = true;
                    }
                }

                if (!currentListContainsMatch)
                {
                    modifiedPrefixesList.Add(prefixListForEBT[i]);
                }
            }

            int randomIndex = Game.Singleton.Random.Next(0, modifiedPrefixesList.Count - 1);
            Prefix selectedSuffix = modifiedPrefixesList[randomIndex];

            Prefix returnPrefix = Game.Singleton.Tools.DeepCopy<Prefix>(modifiedPrefixesList[randomIndex]);
            returnPrefix.Construct(itemLevel);
            return returnPrefix;
        }

        #endregion
    }
}