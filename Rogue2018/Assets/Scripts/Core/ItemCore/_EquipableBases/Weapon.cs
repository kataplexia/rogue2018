﻿namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class Weapon : EquipableItem, IWeapon
    {
        #region Fields

        private WeaponStyles weaponStyle;
        private WeaponTypes weaponType;
        private DamageTypes damageType;
        private string weaponStyleString;
        private string weaponTypeString;
        private string damageTypeString;
        private int physicalDamage;
        private int hitChance;

        #endregion

        #region Properties

        public WeaponStyles WeaponStyle
        {
            get { return weaponStyle; }
            set { weaponStyle = value; }
        }

        public WeaponTypes WeaponType
        {
            get { return weaponType; }
            set { weaponType = value; }
        }

        public DamageTypes DamageType
        {
            get { return damageType; }
            set { damageType = value; }
        }

        public string WeaponStyleString
        {
            get { return weaponStyleString; }
            set { weaponStyleString = value; }
        }

        public string WeaponTypeString
        {
            get { return weaponTypeString; }
            set { weaponTypeString = value; }
        }

        public string DamageTypeString
        {
            get { return damageTypeString; }
            set { damageTypeString = value; }
        }

        public int PhysicalDamage
        {
            get { return physicalDamage; }
            set { physicalDamage = value; }
        }

        public int HitChance
        {
            get { return hitChance; }
            set { hitChance = value; }
        }

        #endregion

        #region Methods

        public override void Construct()
        {
            EquipmentLocationString = SetupEquipmentLocationString(EquipmentLocation);
            WeaponStyleString = SetupWeaponStyleString(WeaponStyle);
            WeaponTypeString = SetupWeaponTypeString(WeaponType);
            DamageTypeString = SetupDamageTypeString(DamageType);
            SetupAffixes();
        }

        #endregion
    }
}