﻿namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class Armour : EquipableItem, IArmour
    {
        #region Fields

        private ArmourStyles armourStyle;
        private string armourStyleString;
        private int defenceValue;
        private int defenceChance;
        private int speedModifier;

        #endregion

        #region Properties

        public ArmourStyles ArmourStyle
        {
            get { return armourStyle; }
            set { armourStyle = value; }
        }

        public string ArmourStyleString
        {
            get { return armourStyleString; }
            set { armourStyleString = value; }
        }

        public int DefenceValue
        {
            get { return defenceValue; }
            set { defenceValue = value; }
        }

        public int DefenceChance
        {
            get { return defenceChance; }
            set { defenceChance = value; }
        }

        public int SpeedModifier
        {
            get { return speedModifier; }
            set { speedModifier = value; }
        }

        #endregion

        #region Methods

        public override void Construct()
        {
            EquipmentLocationString = SetupEquipmentLocationString(EquipmentLocation);
            ArmourStyleString = SetupArmourStyleString(ArmourStyle);
            SetupAffixes();
        }

        #endregion
    }
}
