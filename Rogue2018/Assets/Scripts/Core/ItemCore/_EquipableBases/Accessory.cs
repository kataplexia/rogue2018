﻿namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    [System.Serializable]
    public class Accessory : EquipableItem, IAccessory
    {
        #region Fields

        private AccessoryTypes accessoryType;
        private string accessoryTypeString;

        #endregion

        #region Properties

        public AccessoryTypes AccessoryType
        {
            get { return accessoryType; }
            set { accessoryType = value; }
        }

        public string AccessoryTypeString
        {
            get { return accessoryTypeString; }
            set { accessoryTypeString = value; }
        }

        #endregion

        #region Methods

        public override void Construct()
        {
            EquipmentLocationString = SetupEquipmentLocationString(EquipmentLocation);
            AccessoryTypeString = SetupAccessoryTypeString(AccessoryType);
            SetupAffixes();
        }

        #endregion
    }
}