﻿namespace Rogue2018.Core
{
    public class Symbols
    {
        #region Constructor

        public Symbols()
        {
            Player = "@";
            Kobold = "k";
            Wall = "#";
            Floor = "•";
            Dungeon_Floors = new string[] { "╬" };
            Cave_Floors = new string[] { " ", "•", "∙", "·" };
            Forest_Floors = new string[] { "`", "'", "," };
            Swamp_Floors = new string[] { "`", "'", "," };
            DoorClosed = "┼";
            DoorOpen = "─";
            StairsUp = "<";
            StairsDown = ">";
            Tree = "♠";
            Trees = new string[] { "♠", "♣", "↨" };
            Water = "≈";
        }

        #endregion

        #region Properties

        public string Player
        {
            get;
            private set;
        }

        public string Kobold
        {
            get;
            private set;
        }

        public string Wall
        {
            get;
            private set;
        }

        public string Floor
        {
            get;
            private set;
        }

        public string[] Dungeon_Floors
        {
            get;
            private set;
        }

        public string[] Cave_Floors
        {
            get;
            private set;
        }

        public string[] Forest_Floors
        {
            get;
            private set;
        }

        public string[] Swamp_Floors
        {
            get;
            private set;
        }

        public string DoorClosed
        {
            get;
            private set;
        }

        public string DoorOpen
        {
            get;
            private set;
        }

        public string StairsUp
        {
            get;
            private set;
        }

        public string StairsDown
        {
            get;
            private set;
        }

        public string Tree
        {
            get;
            private set;
        }

        public string[] Trees
        {
            get;
            private set;
        }

        public string Water
        {
            get;
            private set;
        }

        #endregion
    }
}