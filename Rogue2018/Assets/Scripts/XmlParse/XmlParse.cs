﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using PhiOS;
using UnityEditor;
using UnityEngine;

public class XmlParse : MonoBehaviour
{
    #region Methods

    public void ParseRexPaintXmlFile(string xmlPath, int xOffset, int yOffset, int layer = 0, bool removeEmpty = false)
    {
        Debug.Log(string.Format("Parsing {0} with offset ({1}, {2}) and layer {3}.", xmlPath, xOffset, yOffset, layer));

        GameObject workingPrefab = new GameObject();

        PhiCellPropertiesList cellPropertiesList = workingPrefab.AddComponent<PhiCellPropertiesList>();
        cellPropertiesList.propertiesList = new List<PhiCellProperties>();

        var xmlRawFile = Resources.Load<TextAsset>(xmlPath);
        string xmlData = xmlRawFile.text;
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(new StringReader(xmlData));

        XmlNodeList nodeListRows = xmlDocument.SelectNodes("/image/data/row");
        int rowCount = 0;
        foreach (XmlNode nodeRow in nodeListRows)
        {
            XmlNodeList nodeListCells = nodeRow.SelectNodes("cell");
            int cellCount = 0;
            foreach (XmlNode nodeCell in nodeListCells)
            {
                XmlNode asciiNode = nodeCell.FirstChild;
                XmlNode fgdNode = asciiNode.NextSibling;
                XmlNode bkdNode = fgdNode.NextSibling;

                TextAsset ibmGraph = (TextAsset)AssetDatabase.LoadAssetAtPath("Assets/Fonts/REXPaint_Fonts/mappings/IBMGRAPH.txt", typeof(TextAsset));
                Dictionary<int, int> ibmGraphMapping = BitmapFont.GetIBMGRAPHMapping(ibmGraph);

                int asciiCode = Int32.Parse(asciiNode.InnerXml);
                string ascii = BitmapFont.CP437ToUnicode(asciiCode, ibmGraphMapping);

                Color fgPhiCellColor = new Color();
                ColorUtility.TryParseHtmlString(fgdNode.InnerXml, out fgPhiCellColor);

                Color bgPhiCellColor = new Color();
                ColorUtility.TryParseHtmlString(bkdNode.InnerXml, out bgPhiCellColor);

                PhiCellProperties phiCellProperties = new PhiCellProperties();
                phiCellProperties.X = cellCount + xOffset;
                phiCellProperties.Y = rowCount + yOffset;
                phiCellProperties.layer = layer;
                phiCellProperties.symbol = ascii[0].ToString();
                phiCellProperties.drawColor = fgPhiCellColor;
                phiCellProperties.backgroundDrawColor = bgPhiCellColor;

                if (IsEmptyPhiCell(phiCellProperties))
                {
                    if (!removeEmpty)
                    {
                        cellPropertiesList.propertiesList.Add(phiCellProperties);
                    }
                }
                else
                {
                    cellPropertiesList.propertiesList.Add(phiCellProperties);
                }

                cellCount++;
            }

            rowCount++;
        }

        int charPos = xmlPath.Length - 1;
        while (charPos > 0 && xmlPath[charPos] != '/')
        {
            charPos--;
        }

        string prefabName = xmlPath.Substring(charPos + 1) + "CellProperties";
        int propertiesCount = cellPropertiesList.propertiesList.Count;

        UnityEngine.Object tempPrefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/XmlParse/CellPropertiesPrefabs/" + prefabName + ".prefab");
        PrefabUtility.ReplacePrefab(workingPrefab, tempPrefab, ReplacePrefabOptions.ConnectToPrefab);
        DestroyImmediate(workingPrefab);

        Debug.Log(string.Format("Parse succesful. {0} created with {1} CellProperties components.", prefabName, propertiesCount));
    }

    public bool IsEmptyPhiCell(PhiCellProperties phiCellProperties)
    {
        string checkSymbol  = phiCellProperties.symbol;
        Color checkFgColor  = phiCellProperties.drawColor;
        Color checkBgColor  = phiCellProperties.backgroundDrawColor;
        if (checkSymbol == " " && checkFgColor == Color.black && checkBgColor == Color.black)
        {
            return true;
        }

        return false;
    }

    #endregion
}
#endif