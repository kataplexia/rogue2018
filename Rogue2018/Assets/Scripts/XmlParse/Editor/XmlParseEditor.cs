﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(XmlParse))]
public class XmlParseSystemEditor : Editor
{
    #region Fields

    private string xmlPath = "RexPaint/...";
    private int xOffset = 0;
    private int yOffset = 0;
    private int layer = 0;
    private bool removeEmpty = false;

    #endregion

    #region Methods

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        XmlParse xmlParse = (XmlParse)target;

        xmlPath = EditorGUILayout.TextField("Xml Path: ", xmlPath);
        xOffset = EditorGUILayout.IntField("X Offset: ", xOffset);
        yOffset = EditorGUILayout.IntField("Y Offset: ", yOffset);
        layer = EditorGUILayout.IntField("Layer: ", layer);
        removeEmpty = EditorGUILayout.Toggle("Remove Empty Cells: ", removeEmpty);

        if (GUILayout.Button("Create Cell Properties List"))
        {
            if (CheckPath(xmlPath))
            {
                xmlParse.ParseRexPaintXmlFile(xmlPath, xOffset, yOffset, layer, removeEmpty);
            }
        }
    }

    private bool CheckPath(string path)
    {
        var checkAsset = Resources.Load<TextAsset>(xmlPath);
        if (checkAsset == null)
        {
            Debug.LogError("Path does not contain a TextAsset.");
            return false;
        }

        return true;
    }

    #endregion
}