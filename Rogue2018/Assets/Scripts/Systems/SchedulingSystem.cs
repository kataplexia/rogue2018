﻿namespace Rogue2018.Systems
{
    using System.Collections.Generic;
    using System.Linq;
    using Rogue2018.Interfaces;

    public class SchedulingSystem
    {
        #region Fields

        private readonly SortedDictionary<int, List<ISchedulable>> schedulables;
        private int time;

        #endregion

        #region Constructor

        public SchedulingSystem()
        {
            time = 0;
            schedulables = new SortedDictionary<int, List<ISchedulable>>();
        }

        #endregion

        #region Methods

        public void Add(ISchedulable schedulable)
        {
            int key = time + schedulable.Time;
            if (!schedulables.ContainsKey(key))
            {
                schedulables.Add(key, new List<ISchedulable>());
            }

            schedulables[key].Add(schedulable);
        }

        public void Remove(ISchedulable schedulable)
        {
            KeyValuePair<int, List<ISchedulable>> schedulableListFound = new KeyValuePair<int, List<ISchedulable>>(-1, null);

            foreach (var schedulableList in schedulables)
            {
                if (schedulableList.Value.Contains(schedulable))
                {
                    schedulableListFound = schedulableList;
                    break;
                }
            }

            if (schedulableListFound.Value != null)
            {
                schedulableListFound.Value.Remove(schedulable);
                if (schedulableListFound.Value.Count <= 0)
                {
                    schedulables.Remove(schedulableListFound.Key);
                }
            }
        }

        public ISchedulable Get()
        {
            var firstSchedulableGroup = schedulables.First();
            var firstSchedulable = firstSchedulableGroup.Value.First();
            Remove(firstSchedulable);
            time = firstSchedulableGroup.Key;
            return firstSchedulable;
        }

        public int GetTime()
        {
            return time;
        }

        public void Clear()
        {
            time = 0;
            schedulables.Clear();
        }

        #endregion
    }
}