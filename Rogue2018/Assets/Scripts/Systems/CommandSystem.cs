﻿namespace Rogue2018.Systems
{
    using System;
    using System.Text;
    using Rogue2018.Actors;
    using Rogue2018.Algorithms;
    using Rogue2018.Core;
    using Rogue2018.Core.MapCore;
    using Rogue2018.Interfaces;
    using RogueSharp;
    using RogueSharp.DiceNotation;
    using UnityEngine;

    public enum Directions
    {
        None = 0,
        DownLeft = 1,
        Down = 2,
        DownRight = 3,
        Left = 4,
        Center = 5,
        Right = 6,
        UpLeft = 7,
        Up = 8,
        UpRight = 9
    }

    public class CommandSystem
    {
        #region Fields

        private static Color attackMessageColor;
        private static Color defenceMessageColor;

        private bool isGoalAnExit = false;
        private int playerGoalExitIndex = 0;

        private Path explorePath = null;
        private PathFinder pathFinder = null;
        private BreadthFirstSearch breadthFirstSearch = null;

        #endregion

        #region Properties

        public bool IsPlayerTurn
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void UpdateCommandSystem()
        {
            if (!IsPlayerTurn)
            {
                ActivateMonsters();
                Game.Singleton.MessageSystem.ProcessTurn();
                Game.Singleton.SetDrawRequired(true);
            }
        }

        public void EndPlayerTurn()
        {
            IsPlayerTurn = false;
        }

        public void ResetAutoExplore()
        {
            explorePath = null;
        }

        public void AutoExplore()
        {
            if (Game.Singleton.CurrentMap.IsMapExplored && isGoalAnExit)
            {
                Exit exit = Game.Singleton.CurrentMap.Exits[playerGoalExitIndex];
                if (Game.Singleton.Player.X == exit.X && Game.Singleton.Player.Y == exit.Y)
                {
                    explorePath = null;
                }
            }

            if (explorePath == null)
            {
                try
                {
                    Game.Singleton.CurrentMap.SetIsWalkable(Game.Singleton.Player.X, Game.Singleton.Player.Y, true);

                    if (pathFinder == null)
                    {
                        pathFinder = new PathFinder(Game.Singleton.CurrentMap, 1.41f);
                    }

                    if (breadthFirstSearch == null)
                    {
                        breadthFirstSearch = new BreadthFirstSearch();
                    }

                    ICell sourceCell = Game.Singleton.CurrentMap.GetCell(Game.Singleton.Player.X, Game.Singleton.Player.Y);
                    ICell targetCell = null;

                    if (!isGoalAnExit)
                    {
                        Point playerPoint = new Point(Game.Singleton.Player.X, Game.Singleton.Player.Y);
                        Point unexploredPoint =
                            breadthFirstSearch.FindUnexploredCellPoint(Game.Singleton.CurrentMap, playerPoint);
                        targetCell = Game.Singleton.CurrentMap.GetCell(unexploredPoint.X, unexploredPoint.Y);
                    }
                    else
                    {
                        Exit exit = Game.Singleton.CurrentMap.Exits[playerGoalExitIndex];

                        playerGoalExitIndex = playerGoalExitIndex == 0
                            ? playerGoalExitIndex = Game.Singleton.CurrentMap.Exits.Count - 1
                            : playerGoalExitIndex - 1;
                        exit = Game.Singleton.CurrentMap.Exits[playerGoalExitIndex];

                        targetCell = Game.Singleton.CurrentMap.GetCell(exit.X, exit.Y);
                    }

                    explorePath = pathFinder.ShortestPath(sourceCell, targetCell);
                }
                catch (PathNotFoundException)
                {
                    //// Debug.Log("PathNotFoundException");
                }
                catch (NullReferenceException)
                {
                    //// Debug.Log("NullReferenceException");
                    Game.Singleton.CurrentMap.IsMapExplored = true;
                    isGoalAnExit = true;
                }
            }

            if (explorePath != null)
            {
                if (!isGoalAnExit && explorePath.End.IsExplored)
                {
                    explorePath = null;
                    return;
                }

                try
                {
                    Cell nextStep = explorePath.StepForward() as Cell;
                    Game.Singleton.CurrentMap.SetActorPosition(Game.Singleton.Player, nextStep.X, nextStep.Y);
                }
                catch (NoMoreStepsException)
                {
                    explorePath = null;
                }
            }
        }

        public bool MovePlayer(Directions direction)
        {
            int x = Game.Singleton.Player.X;
            int y = Game.Singleton.Player.Y;
            switch (direction)
            {
                case Directions.DownLeft:
                    {
                        x = Game.Singleton.Player.X - 1;
                        y = Game.Singleton.Player.Y + 1;
                        break;
                    }

                case Directions.Down:
                    {
                        y = Game.Singleton.Player.Y + 1;
                        break;
                    }

                case Directions.DownRight:
                    {
                        x = Game.Singleton.Player.X + 1;
                        y = Game.Singleton.Player.Y + 1;
                        break;
                    }

                case Directions.Left:
                    {
                        x = Game.Singleton.Player.X - 1;
                        break;
                    }

                case Directions.Center:
                    {
                        break;
                    }

                case Directions.Right:
                    {
                        x = Game.Singleton.Player.X + 1;
                        break;
                    }

                case Directions.UpLeft:
                    {
                        x = Game.Singleton.Player.X - 1;
                        y = Game.Singleton.Player.Y - 1;
                        break;
                    }

                case Directions.Up:
                    {
                        y = Game.Singleton.Player.Y - 1;
                        break;
                    }

                case Directions.UpRight:
                    {
                        x = Game.Singleton.Player.X + 1;
                        y = Game.Singleton.Player.Y - 1;
                        break;
                    }

                default:
                    return false;
            }

            if (Game.Singleton.CurrentMap.SetActorPosition(Game.Singleton.Player, x, y))
            {
                return true;
            }

            Monster monster = Game.Singleton.CurrentMap.GetMonsterAt(x, y);
            if (monster != null)
            {
                Attack(Game.Singleton.Player, monster);
                return true;
            }

            Audio.Singleton.QueueAudio(Audio.Singleton.PlayerStuck);
            return false;
        }

        public void ActivateMonsters()
        {
            ISchedulable schedulable = Game.Singleton.SchedulingSystem.Get();
            if (schedulable is Player)
            {
                IsPlayerTurn = true;
                Game.Singleton.SchedulingSystem.Add(Game.Singleton.Player);
            }
            else
            {
                Monster monster = schedulable as Monster;
                if (monster != null)
                {
                    monster.PerformAction(this);
                    Game.Singleton.SchedulingSystem.Add(monster);
                }

                ActivateMonsters();
            }
        }

        public bool MoveMonster(Monster monster, RogueSharp.ICell rogueCell)
        {
            if (!Game.Singleton.CurrentMap.SetActorPosition(monster, rogueCell.X, rogueCell.Y))
            {
                if (Game.Singleton.Player.X == rogueCell.X && Game.Singleton.Player.Y == rogueCell.Y)
                {
                    Attack(monster, Game.Singleton.Player);
                    return true;
                }

                return false;
            }

            return true;
        }

        public void Attack(Actor attacker, Actor defender)
        {
            StringBuilder attackMessage = new StringBuilder();
            StringBuilder defenceMessage = new StringBuilder();

            attackMessageColor = Color.white;
            defenceMessageColor = Color.white;

            int hits = ResolveAttack(attacker, defender, attackMessage);
            int blocks = ResolveDefence(defender, hits, attackMessage, defenceMessage);

            Game.Singleton.MessageSystem.Add(attackMessage.ToString(), attackMessageColor);
            if (!string.IsNullOrEmpty(defenceMessage.ToString()))
            {
                Game.Singleton.MessageSystem.Add(defenceMessage.ToString(), defenceMessageColor);
            }

            int damage = hits - blocks;
            ResolveDamage(defender, damage, blocks);
        }

        private static int ResolveAttack(Actor attacker, Actor defender, StringBuilder attackMessage)
        {
            int hits = 0;
            //// attackMessage.AppendFormat("{0} attacks {1} and rolls: ", attacker.Name, defender.Name);
            attackMessage.AppendFormat("{0} hits {1}", attacker.Name, defender.Name);

            DiceExpression attackDice = new DiceExpression().Dice(attacker.Attack, 100);
            DiceResult attackResult = attackDice.Roll();

            foreach (TermResult termResult in attackResult.Results)
            {
                //// attackMessage.Append(termResult.Value + ", ");
                if (termResult.Value >= 100 - attacker.HitChance)
                {
                    hits++;
                }
            }

            return hits;
        }

        private static int ResolveDefence(Actor defender, int hits, StringBuilder attackMessage, StringBuilder defenceMessage)
        {
            int blocks = 0;

            if (hits > 0)
            {
                if (!(defender is Player))
                {
                    Audio.Singleton.QueueAudio(Audio.Singleton.PlayerHit);
                }

                attackMessageColor = defender is Player ? Color.red : Color.cyan;
                attackMessage.AppendFormat(" {0} times", hits);
                //// defenceMessage.AppendFormat("{0} defends and rolls: ", defender.Name);
                defenceMessage.AppendFormat("{0}", defender.Name);

                DiceExpression defenceDice = new DiceExpression().Dice(defender.Defence, 100);
                DiceResult defenceRoll = defenceDice.Roll();

                foreach (TermResult termResult in defenceRoll.Results)
                {
                    //// defenceMessage.Append(termResult.Value + ", ");
                    if (termResult.Value >= 100 - defender.DefenceChance)
                    {
                        blocks++;
                    }
                }

                if (blocks > 0)
                {
                    defenceMessageColor = defender is Player ? Color.cyan : Color.red;
                    defenceMessage.AppendFormat(" blocks {0} times", blocks);
                }
                else
                {
                    defenceMessageColor = defender is Player ? Color.red : Color.cyan;
                    defenceMessage.AppendFormat(" fails to block", blocks);
                }
            }
            else
            {
                if (!(defender is Player))
                {
                    Audio.Singleton.QueueAudio(Audio.Singleton.PlayerMiss);
                }

                attackMessageColor = defender is Player ? Color.cyan : Color.red;
                attackMessage.Append(" and misses");
            }

            return blocks;
        }

        private static void ResolveDamage(Actor defender, int damage, int blocks)
        {
            if (damage > 0)
            {
                defender.Health -= damage;

                if (defender is Player)
                {
                    Audio.Singleton.QueueAudio(Audio.Singleton.PlayerTakeDamage);
                }

                Color messageColor = defender is Player ? Color.red : Color.cyan;
                Game.Singleton.MessageSystem.Add(string.Format("{0} was hit for {1} damage", defender.Name, damage), messageColor);

                if (defender.Health <= 0)
                {
                    ResolveDeath(defender);
                }
            }
            else if (blocks > 0)
            {
                Color messageColor = defender is Player ? Color.cyan : Color.red;
                Game.Singleton.MessageSystem.Add(string.Format("{0} blocked all damage", defender.Name), messageColor);
            }
        }

        private static void ResolveDeath(Actor defender)
        {
            if (defender is Player)
            {
                Game.Singleton.Player.PlayerDeath();
                return;
            }
            else if (defender is Monster)
            {
                Game.Singleton.CurrentMap.RemoveMonster((Monster)defender);

                Monster monster = defender as Monster;
                monster.HealthBar.DestroyHealthBar();

                Game.Singleton.Player.Gold += defender.Gold;

                Audio.Singleton.QueueAudio(Audio.Singleton.MonsterDeath);

                Game.Singleton.MessageSystem.Add(string.Format("{0} died and dropped {1} gold", defender.Name, defender.Gold), Color.yellow);
            }
        }

        #endregion
    }
}