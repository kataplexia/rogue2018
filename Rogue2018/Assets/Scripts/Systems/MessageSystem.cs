﻿namespace Rogue2018.Systems
{
    using System.Collections.Generic;
    using PhiOS;
    using Rogue2018.Core;
    using UnityEngine;

    public class MessageSystem
    {
        #region Fields

        private static int lastMessageCount;
        private static int recentMessageCount;
        private readonly int maxLines = 17;
        private readonly int maxChars = 34;
        private readonly Queue<string> lines;
        private readonly Queue<Color> lineColors;

        #endregion

        #region Constructor

        public MessageSystem()
        {
            lines = new Queue<string>();
            lineColors = new Queue<Color>();
        }

        #endregion

        #region Methods

        public void ProcessTurn()
        {
            lastMessageCount = recentMessageCount;
            recentMessageCount = 0;
        }

        public void Add(string message, Color messageColor, bool indent = false)
        {
            message = "∙" + message;

            if (indent)
            {
                message = "  " + message;
            }

            if (message.Length > maxChars)
            {
                int charPos = maxChars;
                while (charPos > 0 && message[charPos] != ' ')
                {
                    charPos--;
                }

                lines.Enqueue(message.Substring(0, charPos));
                lineColors.Enqueue(messageColor);
                Add(message.Substring(charPos + 1), messageColor, indent);
                recentMessageCount++;
            }
            else
            {
                lines.Enqueue(message);
                lineColors.Enqueue(messageColor);
                recentMessageCount++;
            }

            if (lines.Count > maxLines)
            {
                lines.Dequeue();
                lineColors.Dequeue();
            }
        }

        public void Draw(int xOffset, int yOffset)
        {
            string[] linesArray = lines.ToArray();
            Color[] colorsArray = lineColors.ToArray();
            for (int i = linesArray.Length - 1; i >= 0; i--)
            {
                string line = linesArray[(linesArray.Length - 1) - i];
                Color color = colorsArray[(colorsArray.Length - 1) - i];

                if ((linesArray.Length - 1) - i < linesArray.Length - (recentMessageCount + lastMessageCount))
                {
                    color = (Color.gray * 1.375f) * (((maxLines + 3) - i) * (float)(2f / (maxLines + 3)));
                }

                for (int x = 0; x < line.Length; x++)
                {
                    PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MessageLayer, x + xOffset + 1, (maxLines - 1) - i + yOffset + 1);
                    phiCell.SetContent(line.Substring(x, 1), Color.clear, color);
                }
            }
        }

        #endregion
    }
}