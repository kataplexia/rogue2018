﻿namespace Rogue2018.Actors
{
    using System.Collections.Generic;
    using PhiOS;
    using Rogue2018.Core;
    using Rogue2018.Core.ItemCore;
    using Rogue2018.Interfaces;
    using UnityEngine;

    public class Player : Actor
    {
        #region Fields

        private EquipableItem equipmentChest;
        private EquipableItem equipmentFeet;
        private EquipableItem equipmentHands;
        private EquipableItem equipmentHead;
        private EquipableItem equipmentLeftRing;
        private EquipableItem equipmentLegs;
        private EquipableItem equipmentMainHand;
        private EquipableItem equipmentOffHand;
        private EquipableItem equipmentRightRing;
        private EquipableItem equipmentWaist;
        private int inventoryCapacity = 26;
        private List<IItem> inventoryList;

        #endregion

        #region Constructor

        public Player()
        {
            Attack = 4;
            Awareness = 15;
            Defence = 2;
            DefenceChance = 40;
            Gold = 0;
            Health = 25;
            HitChance = 80;
            MaxHealth = 25;
            Name = "Kataplex";
            Speed = 4;

            DrawColorFOV = Game.Singleton.Swatch.Player;
            GlyphString = Game.Singleton.GlyphStrings.Player;
            Symbol = Game.Singleton.Symbols.Player;

            InitializeEquipment();
            InitializeInventory();
        }

        #endregion

        #region Properties

        public EquipableItem EquipmentChest
        {
            get { return equipmentChest; }
            set { equipmentChest = value; }
        }

        public EquipableItem EquipmentFeet
        {
            get { return equipmentFeet; }
            set { equipmentFeet = value; }
        }

        public EquipableItem EquipmentHands
        {
            get { return equipmentHands; }
            set { equipmentHands = value; }
        }

        public EquipableItem EquipmentHead
        {
            get { return equipmentHead; }
            set { equipmentHead = value; }
        }

        public EquipableItem EquipmentLeftRing
        {
            get { return equipmentLeftRing; }
            set { equipmentLeftRing = value; }
        }

        public EquipableItem EquipmentLegs
        {
            get { return equipmentLegs; }
            set { equipmentLegs = value; }
        }

        public EquipableItem EquipmentMainHand
        {
            get { return equipmentMainHand; }
            set { equipmentMainHand = value; }
        }

        public EquipableItem EquipmentOffHand
        {
            get { return equipmentOffHand; }
            set { equipmentOffHand = value; }
        }

        public EquipableItem EquipmentRightRing
        {
            get { return equipmentRightRing; }
            set { equipmentRightRing = value; }
        }

        public EquipableItem EquipmentWaist
        {
            get { return equipmentWaist; }
            set { equipmentWaist = value; }
        }

        public List<IItem> InventoryList
        {
            get { return inventoryList; }
            set { inventoryList = value; }
        }

        #endregion

        #region Methods

        public void AddItemToInventory(IItem item)
        {
            if (item is IUsable)
            {
                IUsable usableItem = item as IUsable;
                if (usableItem.MaxStackCount > 1)
                {
                    bool stackFound = false;
                    foreach (IItem checkItem in InventoryList)
                    {
                        if (checkItem.Name == item.Name)
                        {
                            IUsable usableCheckItem = checkItem as IUsable;
                            if (usableCheckItem.CurrentStackCount < usableCheckItem.MaxStackCount)
                            {
                                usableCheckItem.CurrentStackCount++;
                                stackFound = true;
                            }
                        }
                    }

                    if (!stackFound)
                    {
                        InventoryList.Add(item);
                    }
                }
            }
            else
            {
                InventoryList.Add(item);
            }
        }

        public bool CheckInventoryCapacity()
        {
            if (InventoryList.Count > 25)
            {
                Game.Singleton.MessageSystem.Add("You cannot carry anymore", Color.red, false);
                Game.Singleton.SetDrawRequired(true);
                return false;
            }

            return true;
        }

        public void DrawStats(int xOffset, int yOffset)
        {
            int layer = Game.Singleton.StatsLayer;

            string statsName = string.Format("Name:    {0}", Name);
            string statsHealth = string.Format("Health:  {0}/{1}", Health, MaxHealth);
            string statsAttack = string.Format("Attack:  {0} ({1}%)", Attack, HitChance);
            string statsDefence = string.Format("Defence: {0} ({1}%)", Defence, DefenceChance);
            string statsGold = string.Format("Gold:    {0}", Gold);

            for (int x = 0; x < statsName.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 1);
                phiCell.SetContent(statsName.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsHealth.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 3);
                phiCell.SetContent(statsHealth.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsAttack.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 5);
                phiCell.SetContent(statsAttack.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsDefence.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 7);
                phiCell.SetContent(statsDefence.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsGold.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 9);
                phiCell.SetContent(statsGold.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Gold);
            }
        }

        public void HealPlayer(int healValue)
        {
            Health += healValue;

            if (Health > MaxHealth)
            {
                Health = MaxHealth;
            }
        }

        public void PlayerDeath()
        {
            Audio.Singleton.QueueAudio(Audio.Singleton.PlayerDeath);

            Game.Singleton.MessageSystem.Add(string.Format("{0} was killed. Game over!", Name), Color.magenta, true);
        }

        public void TEST_FillInventoryWithRandomItems()
        {
            InventoryList = new List<IItem>();
            for (int i = 0; i < inventoryCapacity; i++)
            {
                ItemQualities itemQuality;
                int randomQualityValue = Game.Singleton.Random.Next(0, 1);
                itemQuality = randomQualityValue == 0
                    ? itemQuality = ItemQualities.Magic
                    : itemQuality = ItemQualities.Rare;

                int itemLevel = Game.Singleton.Random.Next(85, 90);
                IItem randomItem = Game.Singleton.ItemGenerator.CreateEquipableItem(
                    Game.Singleton.ItemCollection.GetRandomEquipableItem(),
                    itemQuality,
                    itemLevel);

                AddItemToInventory(randomItem);
            }
        }

        private void InitializeEquipment()
        {
            EquipmentHead = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronHat(), ItemQualities.Normal, 10);
            EquipmentHead.Equipped = true;

            EquipmentChest = Game.Singleton.ItemGenerator.CreateEquipableItem(new ChestPlate(), ItemQualities.Normal, 10);
            EquipmentChest.Equipped = true;

            EquipmentWaist = Game.Singleton.ItemGenerator.CreateEquipableItem(new LeatherBelt(), ItemQualities.Normal, 10);
            EquipmentWaist.Equipped = true;

            EquipmentLegs = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronLegguards(), ItemQualities.Normal, 10);
            EquipmentLegs.Equipped = true;

            EquipmentFeet = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronGreaves(), ItemQualities.Normal, 10);
            EquipmentFeet.Equipped = true;

            EquipmentHands = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronGauntlets(), ItemQualities.Normal, 10);
            EquipmentHands.Equipped = true;

            EquipmentMainHand = Game.Singleton.ItemGenerator.CreateEquipableItem(new WoodenClub(), ItemQualities.Normal, 10);
            EquipmentMainHand.Equipped = true;
            EquipmentMainHand.EquippedMainHand = true;

            EquipmentOffHand = Game.Singleton.ItemGenerator.CreateEquipableItem(new SimpleTowerShield(), ItemQualities.Normal, 10);
            EquipmentOffHand.Equipped = true;
            EquipmentOffHand.EquippedOffHand = true;

            EquipmentLeftRing = Game.Singleton.ItemGenerator.CreateEquipableItem(new RubyRing(), ItemQualities.Normal, 10);
            EquipmentLeftRing.Equipped = true;
            EquipmentLeftRing.EquippedLeftRing = true;

            EquipmentRightRing = Game.Singleton.ItemGenerator.CreateEquipableItem(new ThornedRing(), ItemQualities.Normal, 10);
            EquipmentRightRing.Equipped = true;
            EquipmentRightRing.EquippedRightRing = true;
        }

        private void InitializeInventory()
        {
            InventoryList = new List<IItem>();
            TEST_FillInventoryWithRandomItems();
        }

        #endregion
    }
}