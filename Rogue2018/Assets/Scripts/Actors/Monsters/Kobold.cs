﻿namespace Rogue2018.Actors.Monsters
{
    using Rogue2018.Core;
    using RogueSharp.DiceNotation;

    public class Kobold : Monster
    {
        #region Methods

        public static Kobold Create(int level)
        {
            int health = Dice.Roll("2D5");
            return new Kobold
            {
                Attack = Dice.Roll("1D3") + (level / 3),
                Awareness = 10,
                Defence = Dice.Roll("1D3") + (level / 3),
                DefenceChance = Dice.Roll("10D4"),
                Gold = Dice.Roll("5D5"),
                Health = health,
                HitChance = Dice.Roll("25D3"),
                MaxHealth = health,
                Name = "Kobold",
                Speed = 6,

                DrawColorFOV = Game.Singleton.Swatch.Kobold,
                GlyphString = Game.Singleton.GlyphStrings.Kobold,
                Symbol = Game.Singleton.Symbols.Kobold
            };
        }

        #endregion
    }
}