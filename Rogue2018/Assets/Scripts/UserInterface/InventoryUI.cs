﻿namespace Rogue2018.UserInterface
{
    using System.Text;
    using PhiOS;
    using Rogue2018.Actors;
    using Rogue2018.Core;
    using Rogue2018.Interfaces;
    using UnityEngine;

    public class InventoryUI : IUserInterface
    {
        #region Fields

        private InventoryUIStates inventoryState;
        private int currentSelection = -1;
        private int currentOptionSelection = -1;
        private int selectionOffset = 6;
        private int maxSelection = 38;
        private int maxOptionsSelection = 3;
        private int optionsBoxWidth = 14;
        private int infoBoxWidth = 48;
        private int optionsMinimumIndex = 0;

        #endregion

        #region Constructor

        public InventoryUI()
        {
            Handles = new string[] { "InventoryConsole" };
            PhiCellPropertiesLists = new PhiCellPropertiesList[Handles.Length];

            for (int i = 0; i < Handles.Length; i++)
            {
                foreach (Transform child in UIManager.Singleton.transform)
                {
                    if (child.gameObject.name == Handles[i] + "CellProperties")
                    {
                        PhiCellPropertiesLists[i] = child.GetComponent<PhiCellPropertiesList>();
                    }
                }
            }
        }

        #endregion

        #region Enum

        public enum InventoryUIStates
        {
            Default = 0,
            SelectionOptionsMenu = 1,
            SelectionItemInformation = 2
        }

        #endregion

        #region Properties

        public string[] Handles
        {
            get;
            set;
        }

        public InventoryUIStates InventoryState
        {
            get { return inventoryState; }
            set { inventoryState = value; }
        }

        public int OptionsMinimumIndex
        {
            get { return optionsMinimumIndex; }
            set { optionsMinimumIndex = value; }
        }

        public PhiCellPropertiesList[] PhiCellPropertiesLists
        {
            get;
            set;
        }

        public PhiCell QuitCellInventory
        {
            get;
            set;
        }

        public PhiCell QuitCellOptionsMenu
        {
            get;
            set;
        }

        public PhiCell QuitCellItemInformation
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void Draw()
        {
            Game.Singleton.Tools.ClearConsole(0, Game.Singleton.MapConsoleWidth - 4, Game.Singleton.MapConsoleHeight - 6, 2, 3);

            Game.Singleton.Tools.SetPhiCellRegionMouseProperties(0, 0, Game.Singleton.MapConsoleWidth, Game.Singleton.MapConsoleHeight, false, false);

            QuitCellInventory = PhiDisplay.PhiCellAt(0, Game.Singleton.MapConsoleWidth - 3, 1);
            QuitCellInventory.clickAction = Game.Singleton.InputSystem;

            QuitCellOptionsMenu        = null;
            QuitCellItemInformation    = null;

            foreach (PhiCellPropertiesList phiCellPropertiesList in PhiCellPropertiesLists)
            {
                Game.Singleton.Tools.DrawCellsFromPropertiesList(phiCellPropertiesList);
            }

            DrawEquipment();
            DrawInventoryList();
            DrawInventoryCapacity();

            if (InventoryState == InventoryUIStates.Default)
            {
                DrawSelection();
                SetMousePropertiesEquipment();
                SetMousePropertiesInventoryList();
            }
            else if (InventoryState == InventoryUIStates.SelectionOptionsMenu)
            {
                DrawSelectionOptionsMenu();
                SetMousePropertiesOptionsMenu();
            }
            else if (InventoryState == InventoryUIStates.SelectionItemInformation)
            {
                DrawSelectionItemInformation();
                SetMousePropertiesItemInformation();
            }
        }

        public void Reset()
        {
            SetState(InventoryUIStates.Default);
            currentSelection = -1;
            currentOptionSelection = -1;
        }

        public int ConvertYPositionToIndex(int yPosition)
        {
            int index = (yPosition / 2) - 6;
            if (index <= 6)
            {
                index += 3;
            }

            return index;
        }

        public int GetCurrentOptionsSelection()
        {
            return currentOptionSelection;
        }

        public void ModifiyOptionsSelection(int modifier)
        {
            currentOptionSelection += modifier;
            if (currentOptionSelection < 0)
            {
                currentOptionSelection = maxOptionsSelection - 1;
            }
            else if (currentOptionSelection > maxOptionsSelection - 1)
            {
                currentOptionSelection = 0;
            }

            Draw();
        }

        public void ChangeOptionsSelection(int selectionIndex)
        {
            currentOptionSelection = selectionIndex;
            Draw();
        }

        public int GetCurrentSelection()
        {
            return currentSelection;
        }

        public void ModifiySelection(int modifier)
        {
            currentSelection += modifier;
            if (currentSelection < 0)
            {
                currentSelection = maxSelection;
            }
            else if (currentSelection > maxSelection)
            {
                currentSelection = 0;
            }

            if (GetItemAtIndex(currentSelection) == null)
            {
                ModifiySelection(modifier);
            }
            else
            {
                Draw();
            }
        }

        public void ChangeSelection(int selectionIndex, bool goToOptions = false)
        {
            if (InventoryState != InventoryUIStates.Default)
            {
                SetState(InventoryUIStates.Default);
            }

            if (GetItemAtIndex(selectionIndex) != null)
            {
                currentSelection = selectionIndex;
                if (goToOptions)
                {
                    SetState(InventoryUIStates.SelectionOptionsMenu);
                }
                else
                {
                    Draw();
                }
            }
        }

        public void SetState(InventoryUIStates state)
        {
            if (state == InventoryUIStates.Default)
            {
                currentSelection = -1;
                currentOptionSelection = -1;
            }
            else if (state == InventoryUIStates.SelectionOptionsMenu)
            {
                currentOptionSelection = -1;
            }

            InventoryState = state;
            Draw();
        }

        public void ExecuteOptionsSelection()
        {
            IItem selectedItem = GetItemAtIndex(currentSelection);
            if (selectedItem is IUsable)
            {
                if (currentOptionSelection == 0)
                {
                    UseSelectedItem();
                }
                else if (currentOptionSelection == 1)
                {
                    DropAllOfSelectedItem();
                }
                else if (currentOptionSelection == 2)
                {
                    DropSelectedItem();
                }
                else if (currentOptionSelection == 3)
                {
                    SetState(InventoryUIStates.SelectionItemInformation);
                }
            }
            else
            {
                if (currentOptionSelection == 0)
                {
                    if (currentSelection < 10)
                    {
                        UnequipSelectedItem();
                    }
                    else
                    {
                        EquipSelectedItem();
                    }
                }
                else if (currentOptionSelection == 1)
                {
                    DropSelectedItem();
                }
                else if (currentOptionSelection == 2)
                {
                    SetState(InventoryUIStates.SelectionItemInformation);
                }
            }
        }

        public void UseSelectedItem()
        {
            IItem selectedItem = GetItemAtIndex(currentSelection);
            if (selectedItem is IUsable)
            {
                IUsable selectedUsable = selectedItem as IUsable;
                selectedUsable.Use();
                SetState(InventoryUIStates.Default);
            }
        }

        public void UnequipSelectedItem()
        {
            if (currentSelection >= 10)
            {
                return;
            }

            IItem selectedItem = GetItemAtIndex(currentSelection);
            if (selectedItem is IEquipable)
            {
                IEquipable selectedEquipable = selectedItem as IEquipable;
                selectedEquipable.Unequip();
                SetState(InventoryUIStates.Default);
            }
        }

        public void EquipSelectedItem()
        {
            if (currentSelection < 10)
            {
                return;
            }

            IItem selectedItem = GetItemAtIndex(currentSelection);
            if (selectedItem is IEquipable)
            {
                IEquipable selectedEquipable = selectedItem as IEquipable;
                selectedEquipable.Equip();
                SetState(InventoryUIStates.Default);
            }
        }

        public void DropSelectedItem()
        {
            IItem selectedItem = GetItemAtIndex(currentSelection);
            selectedItem.Drop();
            SetState(InventoryUIStates.Default);
        }

        public void DropAllOfSelectedItem()
        {
            IItem selectedItem = GetItemAtIndex(currentSelection);
            if (selectedItem is IUsable)
            {
                IUsable selectedUsable = selectedItem as IUsable;
                if (selectedUsable.MaxStackCount > 1)
                {
                    selectedUsable.Drop(true);
                    SetState(InventoryUIStates.Default);
                }
            }
        }

        private void DrawEquipment()
        {
            if (Game.Singleton.Player.EquipmentHead != null)
            {
                Game.Singleton.Player.EquipmentHead.DrawName(8, 6, CheckSelectedColor(6));
            }

            if (Game.Singleton.Player.EquipmentChest != null)
            {
                Game.Singleton.Player.EquipmentChest.DrawName(8, 8, CheckSelectedColor(8));
            }

            if (Game.Singleton.Player.EquipmentWaist != null)
            {
                Game.Singleton.Player.EquipmentWaist.DrawName(8, 10, CheckSelectedColor(10));
            }

            if (Game.Singleton.Player.EquipmentLegs != null)
            {
                Game.Singleton.Player.EquipmentLegs.DrawName(8, 12, CheckSelectedColor(12));
            }

            if (Game.Singleton.Player.EquipmentFeet != null)
            {
                Game.Singleton.Player.EquipmentFeet.DrawName(8, 14, CheckSelectedColor(14));
            }

            if (Game.Singleton.Player.EquipmentHands != null)
            {
                Game.Singleton.Player.EquipmentHands.DrawName(8, 16, CheckSelectedColor(16));
            }

            if (Game.Singleton.Player.EquipmentMainHand != null)
            {
                Game.Singleton.Player.EquipmentMainHand.DrawName(8, 18, CheckSelectedColor(18));
            }

            if (Game.Singleton.Player.EquipmentOffHand != null)
            {
                Game.Singleton.Player.EquipmentOffHand.DrawName(8, 20, CheckSelectedColor(20));
            }

            if (Game.Singleton.Player.EquipmentLeftRing != null)
            {
                Game.Singleton.Player.EquipmentLeftRing.DrawName(8, 22, CheckSelectedColor(22));
            }

            if (Game.Singleton.Player.EquipmentRightRing != null)
            {
                Game.Singleton.Player.EquipmentRightRing.DrawName(8, 24, CheckSelectedColor(24));
            }
        }

        private void SetMousePropertiesEquipment()
        {
            if (Game.Singleton.Player.EquipmentHead != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 6, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentChest != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 8, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentWaist != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 10, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentLegs != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 12, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentFeet != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 14, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentHands != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 16, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentMainHand != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 18, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentOffHand != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 20, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentLeftRing != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 22, 48, 1, true, true);
            }

            if (Game.Singleton.Player.EquipmentRightRing != null)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 24, 48, 1, true, true);
            }
        }

        private void DrawInventoryList()
        {
            Player player = Game.Singleton.Player;
            for (int i = 0; i < player.InventoryList.Count; i++)
            {
                string itemName = player.InventoryList[i].Name;
                if (player.InventoryList[i] is IUsable)
                {
                    IUsable usableItem = player.InventoryList[i] as IUsable;
                    int stackStringLength = usableItem.CurrentStackCount.ToString().Length + usableItem.MaxStackCount.ToString().Length + 4;
                    usableItem.DrawStacks(8, 33 + (i * 2), CheckSelectedColor(33 + (i * 2)));
                    player.InventoryList[i].DrawName(8 + stackStringLength, 33 + (i * 2), CheckSelectedColor(33 + (i * 2)));
                }
                else
                {
                    player.InventoryList[i].DrawName(8, 33 + (i * 2), CheckSelectedColor(33 + (i * 2)));
                }
            }
        }

        private void SetMousePropertiesInventoryList()
        {
            Player player = Game.Singleton.Player;
            for (int i = 0; i < player.InventoryList.Count; i++)
            {
                Game.Singleton.Tools.SetPhiCellRegionMouseProperties(7, 33 + (i * 2), 48, 1, true, true);
            }
        }

        private void DrawInventoryCapacity()
        {
            Player player = Game.Singleton.Player;
            string capacity = string.Format("Capacity [{0}/{1}]", player.InventoryList.Count, 28);
            Game.Singleton.Tools.DrawString(35 - capacity.Length, 85, capacity, Color.yellow, Color.black, 0);
        }

        private void DrawSelection()
        {
            if (InventoryIsEmpty() || currentSelection == -1)
            {
                return;
            }

            selectionOffset = currentSelection >= 10 ? 13 : 6;
            Game.Singleton.Tools.DrawUIBox(50, 3, 6, selectionOffset + (currentSelection * 2) - 1, 0, Color.black, Color.yellow, Color.clear);
        }

        private void DrawSelectionOptionsMenu()
        {
            if (InventoryIsEmpty())
            {
                return;
            }

            selectionOffset = currentSelection >= 10 ? 13 : 6;

            IItem selectedItem = GetItemAtIndex(currentSelection);
            StringBuilder options = new StringBuilder();

            int optionsCount = 0;
            if (selectedItem is IEquipable)
            {
                if (currentSelection < 10)
                {
                    options.Append("U: Unequip\n");
                }
                else
                {
                    options.Append("E: Equip\n");
                }

                optionsCount++;
            }
            else if (selectedItem is IUsable)
            {
                options.Append("A: Activate\n");
                IUsable selectedUsable = selectedItem as IUsable;
                if (selectedUsable.MaxStackCount > 1)
                {
                    options.Append("L: Drop All\n");
                    optionsCount++;
                }

                optionsCount++;
            }

            options.Append("D: Drop\n");
            optionsCount++;

            options.Append("I: Inspect");
            optionsCount++;

            maxOptionsSelection = optionsCount;

            Game.Singleton.Tools.DrawContextMenuBox(
                58,
                selectionOffset + (currentSelection * 2) + 2,
                optionsBoxWidth,
                options.ToString(),
                Color.yellow,
                Color.white,
                Color.black,
                "Options",
                Game.Singleton.Swatch.RexTextHeading,
                0,
                "quitCellOptionsMenu",
                true);

            Game.Singleton.Tools.DrawUIBox(50, 3, 6, selectionOffset + (currentSelection * 2) - 1, 0, Color.black, Color.yellow, Color.clear);

            Game.Singleton.Tools.DrawPhiCell(55, selectionOffset + (currentSelection * 2), "├", Color.yellow, Color.black, 0, 0, 0);
            Game.Singleton.Tools.DrawPhiCell(56, selectionOffset + (currentSelection * 2), "┤", Color.yellow, Color.black, 0, 0, 0);

            if (currentOptionSelection == -1)
            {
                return;
            }

            int optionsBoxYOffset = selectionOffset + (currentSelection * 2) + 2;
            if (optionsBoxYOffset + (maxOptionsSelection * 2) + 3 > 86)
            {
                optionsBoxYOffset = 86 - ((maxOptionsSelection * 2) + 3);
            }

            Game.Singleton.Tools.DrawUIBox(optionsBoxWidth - 2, 3, 61, optionsBoxYOffset + (currentOptionSelection * 2), 0, Color.black, Color.yellow, Color.clear);
        }

        private void SetMousePropertiesOptionsMenu()
        {
            int optionsSelectionBoxYOffset = selectionOffset + (currentSelection * 2) + 2;
            if (optionsSelectionBoxYOffset + (maxOptionsSelection * 2) + 3 > 86)
            {
                optionsSelectionBoxYOffset = 86 - ((maxOptionsSelection * 2) + 3);
            }

            for (int x = 5; x <= optionsBoxWidth; x++)
            {
                for (int y = 0; y < maxOptionsSelection; y++)
                {
                    if (y == 0)
                    {
                        OptionsMinimumIndex = optionsSelectionBoxYOffset + 1;
                    }

                    Game.Singleton.Tools.SetPhiCellMouseProperties(57 + x, optionsSelectionBoxYOffset + (y * 2) + 1, true, true);
                }
            }

            if (QuitCellOptionsMenu == null)
            {
                PhiCell phiCellQuit = Game.Singleton.Tools.GetPhiCellWithTag("quitCellOptionsMenu");
                if (phiCellQuit != null)
                {
                    QuitCellOptionsMenu = phiCellQuit;
                }
            }
        }

        private void DrawSelectionItemInformation()
        {
            if (InventoryIsEmpty())
            {
                return;
            }

            selectionOffset = currentSelection >= 10 ? 13 : 6;

            IItem selectedItem = GetItemAtIndex(currentSelection);
            StringBuilder itemInformation = new StringBuilder();

            int propertyLineCount   = 0;
            int affixLineCount      = 0;
            int implicitLineCount   = 0;

            if (selectedItem.ItemQuality != ItemQualities.None)
            {
                string seQuality = selectedItem.ItemQuality.ToString();
                itemInformation.Append("Quality:".PadRight(infoBoxWidth - seQuality.Length, ' '));
                itemInformation.Append(seQuality + "\n \n");

                propertyLineCount += 2;
            }

            if (selectedItem is IEquipable)
            {
                IEquipable selectedEquipable = selectedItem as IEquipable;

                string seItemLevel = selectedEquipable.ItemLevel.ToString();
                itemInformation.Append("Item Level:".PadRight(infoBoxWidth - seItemLevel.Length, ' '));
                itemInformation.Append(seItemLevel + "\n");

                string seEquipLocation = selectedEquipable.EquipmentLocationString;
                itemInformation.Append("Slot:".PadRight(infoBoxWidth - seEquipLocation.Length, ' '));
                itemInformation.Append(seEquipLocation + "\n");

                propertyLineCount += 2;

                if (selectedItem is IArmour)
                {
                    IArmour selectedArmour = selectedItem as IArmour;

                    string saStyle = selectedArmour.ArmourStyleString;
                    itemInformation.Append("Armour Style:".PadRight(infoBoxWidth - saStyle.Length, ' '));
                    itemInformation.Append(saStyle + "\n");

                    string saDefenceValue = "+" + selectedArmour.DefenceValue.ToString();
                    itemInformation.Append("Defence Value:".PadRight(infoBoxWidth - saDefenceValue.Length, ' '));
                    itemInformation.Append(saDefenceValue + "\n");

                    string saDefenceDefenceChance = string.Format("+{0}%", selectedArmour.DefenceChance.ToString());
                    itemInformation.Append("Defence Chance:".PadRight(infoBoxWidth - saDefenceDefenceChance.Length, ' '));
                    itemInformation.Append(saDefenceDefenceChance + "\n");

                    string saSpeedMod = selectedArmour.SpeedModifier.ToString();
                    itemInformation.Append("Speed Modifier:".PadRight(infoBoxWidth - saSpeedMod.Length, ' '));
                    itemInformation.Append(saSpeedMod + "\n");

                    propertyLineCount += 4;
                }
                else if (selectedItem is IWeapon)
                {
                    IWeapon selectedWeapon = selectedItem as IWeapon;

                    string swType = selectedWeapon.WeaponTypeString;
                    itemInformation.Append("Weapon Type:".PadRight(infoBoxWidth - swType.Length, ' '));
                    itemInformation.Append(swType + "\n");

                    string swStyle = selectedWeapon.WeaponStyleString;
                    itemInformation.Append("Weapon Style:".PadRight(infoBoxWidth - swStyle.Length, ' '));
                    itemInformation.Append(swStyle + "\n");

                    string swDamageType = selectedWeapon.DamageTypeString;
                    itemInformation.Append("Damage Type:".PadRight(infoBoxWidth - swDamageType.Length, ' '));
                    itemInformation.Append(swDamageType + "\n");

                    string swPhysicalDamage = "+" + selectedWeapon.PhysicalDamage.ToString();
                    itemInformation.Append("Physical Damage:".PadRight(infoBoxWidth - swPhysicalDamage.Length, ' '));
                    itemInformation.Append(swPhysicalDamage + "\n");

                    string swHitChance = string.Format("+{0}%", selectedWeapon.HitChance.ToString());
                    itemInformation.Append("Hit Chance:".PadRight(infoBoxWidth - swHitChance.Length, ' '));
                    itemInformation.Append(swHitChance + "\n");

                    propertyLineCount += 5;
                }

                if (selectedEquipable.Implicit != null)
                {
                    itemInformation.Append(" \n");
                    string implicitEffect = selectedEquipable.Implicit.EffectInfoString;
                    itemInformation.Append(implicitEffect + "\n");
                    implicitLineCount += 2;
                }

                if (selectedItem.ItemQuality != ItemQualities.None && selectedItem.ItemQuality != ItemQualities.Normal)
                {
                    itemInformation.Append(" \n");

                    for (int i = 0; i < selectedEquipable.Prefixes.Count; i++)
                    {
                        string prefixEffect = selectedEquipable.Prefixes[i].EffectInfoString;
                        itemInformation.Append(prefixEffect + "\n");
                        affixLineCount++;
                    }

                    for (int i = 0; i < selectedEquipable.Suffixes.Count; i++)
                    {
                        string suffixEffect = selectedEquipable.Suffixes[i].EffectInfoString;
                        itemInformation.Append(suffixEffect + "\n");
                        affixLineCount++;
                    }

                    itemInformation.Append(" \n");
                    affixLineCount++;
                }
            }

            if (selectedItem.ItemQuality == ItemQualities.Normal && propertyLineCount > 2)
            {
                itemInformation.Append(" \n");
            }

            if (propertyLineCount == 2 && affixLineCount == 0)
            {
                itemInformation.Append("\n");
            }

            itemInformation.AppendFormat("{0}", selectedItem.Description);

            string itemName = selectedItem.Name;
            Game.Singleton.Tools.DrawInfoBox(
                58,
                selectionOffset + (currentSelection * 2) + 2,
                infoBoxWidth,
                itemInformation.ToString(),
                propertyLineCount,
                affixLineCount,
                implicitLineCount,
                Color.yellow,
                Color.white,
                Color.black,
                itemName,
                0,
                "quitCellItemInformation",
                true);

            Game.Singleton.Tools.DrawUIBox(50, 3, 6, selectionOffset + (currentSelection * 2) - 1, 0, Color.black, Color.yellow, Color.clear);

            Game.Singleton.Tools.DrawPhiCell(55, selectionOffset + (currentSelection * 2), "├", Color.yellow, Color.black, 0, 0, 0);
            Game.Singleton.Tools.DrawPhiCell(56, selectionOffset + (currentSelection * 2), "┤", Color.yellow, Color.black, 0, 0, 0);
        }

        private void SetMousePropertiesItemInformation()
        {
            if (QuitCellItemInformation == null)
            {
                PhiCell phiCellQuit = Game.Singleton.Tools.GetPhiCellWithTag("quitCellItemInformation");
                if (phiCellQuit != null)
                {
                    QuitCellItemInformation = phiCellQuit;
                }
            }
        }

        private bool InventoryIsEmpty()
        {
            Player player = Game.Singleton.Player;
            if (player.EquipmentHead == null &&
                player.EquipmentChest == null &&
                player.EquipmentWaist == null &&
                player.EquipmentLegs == null &&
                player.EquipmentFeet == null &&
                player.EquipmentHands == null &&
                player.EquipmentMainHand == null &&
                player.EquipmentOffHand == null &&
                player.EquipmentLeftRing == null &&
                player.EquipmentRightRing == null &&
                player.InventoryList.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Color CheckSelectedColor(int yPosition)
        {
            int index = ConvertYPositionToIndex(yPosition);
            if (index == currentSelection)
            {
                return Color.yellow;
            }
            else
            {
                return GetQualityColor(index);
            }
        }

        private Color GetQualityColor(int index)
        {
            IItem itemAtIndex = GetItemAtIndex(index);
            if (itemAtIndex.ItemQuality != ItemQualities.None)
            {
                return Game.Singleton.Tools.GetQualityColor(itemAtIndex.ItemQuality.ToString());
            }
            else
            {
                return Color.white;
            }
        }

        private IItem GetItemAtIndex(int selectionIndex)
        {
            Player player = Game.Singleton.Player;
            if (selectionIndex == 0)
            {
                return player.EquipmentHead;
            }
            else if (selectionIndex == 1)
            {
                return player.EquipmentChest;
            }
            else if (selectionIndex == 2)
            {
                return player.EquipmentWaist;
            }
            else if (selectionIndex == 3)
            {
                return player.EquipmentLegs;
            }
            else if (selectionIndex == 4)
            {
                return player.EquipmentFeet;
            }
            else if (selectionIndex == 5)
            {
                return player.EquipmentHands;
            }
            else if (selectionIndex == 6)
            {
                return player.EquipmentMainHand;
            }
            else if (selectionIndex == 7)
            {
                return player.EquipmentOffHand;
            }
            else if (selectionIndex == 8)
            {
                return player.EquipmentLeftRing;
            }
            else if (selectionIndex == 9)
            {
                return player.EquipmentRightRing;
            }
            else if (selectionIndex >= 10)
            {
                if (player.InventoryList.Count > (selectionIndex - 10))
                {
                    return player.InventoryList[selectionIndex - 10];
                }
                else
                {
                    return null;
                }
            }

            return null;
        }

        #endregion
    }
}