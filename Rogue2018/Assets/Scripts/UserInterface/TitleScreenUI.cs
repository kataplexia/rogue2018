﻿namespace Rogue2018.UserInterface
{
    using PhiOS;
    using Rogue2018.Core;
    using Rogue2018.Interfaces;
    using UnityEngine;

    public class TitleScreenUI : IUserInterface
    {
        #region Fields

        private static int currentSelection = 0;
        private static int selectionOffset = 69;
        private static int maxSelection = 3;

        #endregion

        #region Constructor

        public TitleScreenUI()
        {
            Handles = new string[] { "TitleScreen", "TitleScreenMenu", "Logo" };
            PhiCellPropertiesLists = new PhiCellPropertiesList[Handles.Length];

            for (int i = 0; i < Handles.Length; i++)
            {
                foreach (Transform child in UIManager.Singleton.transform)
                {
                    if (child.gameObject.name == Handles[i] + "CellProperties")
                    {
                        PhiCellPropertiesLists[i] = child.GetComponent<PhiCellPropertiesList>();
                    }
                }
            }
        }

        #endregion

        #region Properties

        public string[] Handles
        {
            get;
            set;
        }

        public PhiCellPropertiesList[] PhiCellPropertiesLists
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void Draw()
        {
            Game.Singleton.Tools.ClearConsole(0, 20, 13, 130, 67, true);

            //// Game.Singleton.Tools.SetPhiCellRegionMouseProperties(0, 129, , game.MapConsoleHeight, false, false);

            Game.Singleton.Tools.DrawCellsFromPropertiesList(PhiCellPropertiesLists[0]);
            Game.Singleton.Tools.DrawCellsFromPropertiesList(PhiCellPropertiesLists[1], true, 0.25f);
            Game.Singleton.Tools.DrawCellsFromPropertiesList(PhiCellPropertiesLists[2], true, 0.25f);

            Game.Singleton.Tools.DrawString(138, 70, "New Game", Color.white, Color.clear, 0, true, 0.25f);
            Game.Singleton.Tools.DrawString(138, 72, "Continue", Color.white, Color.clear, 0, true, 0.25f);
            Game.Singleton.Tools.DrawString(138, 74, "Options", Color.white, Color.clear, 0, true, 0.25f);
            Game.Singleton.Tools.DrawString(138, 76, "Quit", Color.white, Color.clear, 0, true, 0.25f);

            DrawSelection();
        }

        public int GetCurrentSelection()
        {
            return currentSelection;
        }

        public void ModifiySelection(int modifier)
        {
            currentSelection += modifier;
            if (currentSelection < 0)
            {
                currentSelection = maxSelection;
            }
            else if (currentSelection > maxSelection)
            {
                currentSelection = 0;
            }

            Draw();
        }

        public void ChangeSelection(int selectionIndex, bool goToOptions = false)
        {
            currentSelection = selectionIndex;
            Draw();
        }

        public void ExecuteSelection(int selectionToExecute = -1)
        {
            if (selectionToExecute != -1)
            {
                if (selectionToExecute == 0)
                {
                    Game.Singleton.NewGame();
                }
                //// TODO: OTHER SELECTIONS
            }
            else
            {
                if (currentSelection == 0)
                {
                    Game.Singleton.NewGame();
                }
                //// TODO: OTHER SELECTIONS
            }
        }

        private void DrawSelection()
        {
            Game.Singleton.Tools.DrawUIBox(12, 3, 136, selectionOffset + (currentSelection * 2), 0, Color.clear, Color.yellow, Color.clear, string.Empty, string.Empty, false, true, 0.25f);
        }

        #endregion
    }
}