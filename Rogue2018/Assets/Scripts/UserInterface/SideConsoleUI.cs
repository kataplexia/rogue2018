﻿namespace Rogue2018.UserInterface
{
    using PhiOS;
    using Rogue2018.Core;
    using Rogue2018.Interfaces;
    using UnityEngine;

    public class SideConsoleUI : IUserInterface
    {
        #region Constructor

        public SideConsoleUI()
        {
            Handles = new string[] { "SideConsole" };
            PhiCellPropertiesLists = new PhiCellPropertiesList[Handles.Length];

            for (int i = 0; i < Handles.Length; i++)
            {
                foreach (Transform child in UIManager.Singleton.transform)
                {
                    if (child.gameObject.name == Handles[i] + "CellProperties")
                    {
                        PhiCellPropertiesLists[i] = child.GetComponent<PhiCellPropertiesList>();
                    }
                }
            }
        }

        #endregion

        #region Properties

        public string[] Handles
        {
            get;
            set;
        }

        public PhiCellPropertiesList[] PhiCellPropertiesLists
        {
            get;
            set;
        }

        #endregion

        #region Properties

        public void Draw()
        {
            foreach (PhiCellPropertiesList phiCellPropertiesList in PhiCellPropertiesLists)
            {
                Game.Singleton.Tools.DrawCellsFromPropertiesList(phiCellPropertiesList);
            }
        }

        #endregion
    }
}