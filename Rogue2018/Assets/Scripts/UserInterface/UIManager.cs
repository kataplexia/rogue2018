﻿namespace Rogue2018.UserInterface
{
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using PhiOS;
    using Rogue2018.Core;
    using Rogue2018.Systems;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    public class UIManager : MonoBehaviour
    {
        #region Fields

        public static UIManager Singleton;

        public SpriteRenderer TransitionSprite;
        public GameObject[] CellPropertiesPrefabs;
        public Vector2 QuickBarInventoryPos = new Vector2(123, 85);
        public bool IsInventoryDisplaying = false;

        private static int previousZoomLevel = 0;

        #endregion

        #region Properties

        public TitleScreenUI TitleScreenUI
        {
            get;
            private set;
        }

        public InventoryUI InventoryUI
        {
            get;
            private set;
        }

        public SideConsoleUI SideConsoleUI
        {
            get;
            private set;
        }

        public bool IsInitialized
        {
            get;
            private set;
        }

        public bool IsHandlingAllInput
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        public IEnumerator Start()
        {
            while (!PhiDisplay.IsInitialized() || !Game.Singleton.IsInitialized)
            {
                yield return null;
            }

            for (int i = 0; i < CellPropertiesPrefabs.Length; i++)
            {
                GameObject cellPropertiesObject = GameObject.Instantiate(CellPropertiesPrefabs[i]);
                cellPropertiesObject.name = CellPropertiesPrefabs[i].name;
                cellPropertiesObject.transform.parent = transform;
            }

            TitleScreenUI = new TitleScreenUI();
            InventoryUI = new InventoryUI();
            SideConsoleUI = new SideConsoleUI();

            TitleScreenUI.Draw();

            Game.Singleton.Tools.SetPhiCellMouseProperties((int)QuickBarInventoryPos.x, (int)QuickBarInventoryPos.y, true, true);

            yield return new WaitForFixedUpdate();
            IsInitialized = true;
            TransitionSprite.enabled = false;
        }

        public IEnumerator InventoryDisplay()
        {
            if (!IsInventoryDisplaying)
            {
                IsInventoryDisplaying = true;
                IsHandlingAllInput = true;
                Game.Singleton.CurrentMap.HideHealthBars();
                Game.Singleton.SetDrawMapConsole(false);
                Game.Singleton.ClearMapConsole();
                yield return new WaitForFixedUpdate();
                previousZoomLevel = PhiDisplay.Singleton.currentZoomLevel;
                PhiDisplay.Singleton.SetZoom(0);
                InventoryUI.Draw();
            }
            else
            {
                IsInventoryDisplaying = false;
                IsHandlingAllInput = false;
                Game.Singleton.ClearMapConsole();
                Game.Singleton.SetDrawMapConsole(true);
                yield return new WaitForFixedUpdate();
                PhiDisplay.Singleton.SetZoom(previousZoomLevel);
                Game.Singleton.SetDrawRequired(true);
                InventoryUI.Reset();
            }
        }

        private void Awake()
        {
            TransitionSprite.enabled = true;
            IsInitialized = false;
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }

        private void OnEnable()
        {
            InputSystem.OnMouseDownEvent += OnMouseDown;
            InputSystem.OnHoverEnterEvent += OnHoverEnter;
            InputSystem.OnHoverExitEvent += OnHoverExit;
        }

        private void OnDisable()
        {
            InputSystem.OnMouseDownEvent -= OnMouseDown;
            InputSystem.OnHoverEnterEvent += OnHoverEnter;
            InputSystem.OnHoverExitEvent += OnHoverExit;
        }

        private void Update()
        {
            if (!IsInitialized)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.F9))
            {
                Game.Singleton.NewGame();
            }

            if (!Game.Singleton.IsGameStarted)
            {
                UpdateTitleScreenKeyboardInput();
                //// TODO: MOUSE INPUT FOR TITLE SCREEN MENU
            }
            else
            {
                UpdateInventoryUIKeyBoardInput();
                UpdateInventoryMouseInput();
            }
        }

        private void UpdateTitleScreenKeyboardInput()
        {
            if (Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.Keypad8) ||
                Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.UpArrow) ||
                Input.GetKeyDown(KeyCode.Keypad8) ||
                Input.GetKeyDown(KeyCode.UpArrow))
            {
                TitleScreenUI.ModifiySelection(-1);
            }
            else if (Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.Keypad2) ||
                     Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.DownArrow) ||
                     Input.GetKeyDown(KeyCode.Keypad2) ||
                     Input.GetKeyDown(KeyCode.DownArrow))
            {
                TitleScreenUI.ModifiySelection(1);
            }

            string alphabeticInput = Game.Singleton.InputSystem.GetAlphabeticInput();
            if (alphabeticInput != string.Empty)
            {
                if (alphabeticInput.ToLower() == "n")
                {
                    TitleScreenUI.ExecuteSelection(0);
                }
                //// TODO: OTHER ALPHA INPUT
            }

            if (Input.GetKeyDown(KeyCode.KeypadEnter) ||
                Input.GetKeyDown(KeyCode.Return) ||
                Input.GetKeyDown(KeyCode.Space))
            {
                TitleScreenUI.ExecuteSelection();
            }
        }

        private void UpdateInventoryUIKeyBoardInput()
        {
            if (!IsInventoryDisplaying)
            {
                if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.C))
                {
                    StartCoroutine(InventoryDisplay());
                    return;
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
                    {
                        StartCoroutine(InventoryDisplay());
                        return;
                    }
                    else
                    {
                        InventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                        return;
                    }
                }

                if (Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.Keypad8) ||
                    Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.UpArrow) ||
                    Input.GetKeyDown(KeyCode.Keypad8) ||
                    Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                    {
                        InventoryUI.ModifiyOptionsSelection(-1);
                    }
                    else if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
                    {
                        InventoryUI.ModifiySelection(-1);
                    }
                }
                else if (Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.Keypad2) ||
                    Game.Singleton.InputSystem.CheckKeyHeld(KeyCode.DownArrow) ||
                    Input.GetKeyDown(KeyCode.Keypad2) ||
                    Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                    {
                        InventoryUI.ModifiyOptionsSelection(1);
                    }
                    else if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
                    {
                        InventoryUI.ModifiySelection(1);
                    }
                }

                string alphabeticInput = Game.Singleton.InputSystem.GetAlphabeticInput();
                if (alphabeticInput != string.Empty)
                {
                    if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                    {
                        if (alphabeticInput.ToLower() == "a")
                        {
                            InventoryUI.UseSelectedItem();
                        }
                        else if (alphabeticInput.ToLower() == "u")
                        {
                            InventoryUI.UnequipSelectedItem();
                        }
                        else if (alphabeticInput.ToLower() == "e")
                        {
                            InventoryUI.EquipSelectedItem();
                        }
                        else if (alphabeticInput.ToLower() == "d")
                        {
                            InventoryUI.DropSelectedItem();
                        }
                        else if (alphabeticInput.ToLower() == "l")
                        {
                            InventoryUI.DropAllOfSelectedItem();
                        }
                        else if (alphabeticInput.ToLower() == "i")
                        {
                            InventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionItemInformation);
                        }
                    }
                    else if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
                    {
                        InventoryUI.ChangeSelection(Game.Singleton.Tools.GetAlphabeticIndex(alphabeticInput) + 10, true);
                    }
                }

                if (Input.GetKeyDown(KeyCode.KeypadEnter) ||
                    Input.GetKeyDown(KeyCode.Return) ||
                    Input.GetKeyDown(KeyCode.Space))
                {
                    if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                    {
                        InventoryUI.ExecuteOptionsSelection();
                    }
                    else if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
                    {
                        InventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionOptionsMenu);
                    }
                }
            }
        }

        private void UpdateInventoryMouseInput()
        {
            if (!IsHandlingAllInput)
            {
                return;
            }

            if (Game.Singleton.InputSystem.OverHoverableCell)
            {
                return;
            }

            if (Input.GetMouseButtonDown(1))
            {
                InventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                return;
            }
        }

        private void OnMouseDown(int mouseX, int mouseY, int mouseButtonIndex)
        {
            if (mouseX == (int)QuickBarInventoryPos.x &&
                mouseY == (int)QuickBarInventoryPos.y)
            {
                InventoryDisplay();
                return;
            }

            if (InventoryUI.QuitCellInventory != null)
            {
                if (mouseX == (int)InventoryUI.QuitCellInventory.position.x &&
                    mouseY == (int)InventoryUI.QuitCellInventory.position.y)
                {
                    InventoryDisplay();
                    return;
                }
            }

            if (InventoryUI.QuitCellOptionsMenu != null)
            {
                if (mouseX == (int)InventoryUI.QuitCellOptionsMenu.position.x &&
                    mouseY == (int)InventoryUI.QuitCellOptionsMenu.position.y)
                {
                    InventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                    return;
                }
            }

            if (InventoryUI.QuitCellItemInformation != null)
            {
                if (mouseX == (int)InventoryUI.QuitCellItemInformation.position.x &&
                    mouseY == (int)InventoryUI.QuitCellItemInformation.position.y)
                {
                    InventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                    return;
                }
            }

            if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
            {
                if (mouseButtonIndex == 0)
                {
                    InventoryUI.ChangeSelection(InventoryUI.ConvertYPositionToIndex(mouseY));
                    InventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionOptionsMenu);
                }
                else if (mouseButtonIndex == 1)
                {
                    InventoryUI.ChangeSelection(InventoryUI.ConvertYPositionToIndex(mouseY));
                    InventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionItemInformation);
                }
            }
            else if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
            {
                if (mouseButtonIndex == 0)
                {
                    InventoryUI.ExecuteOptionsSelection();
                }
            }
        }

        private void OnHoverEnter(int mouseX, int mouseY, int cellX, int cellY)
        {
            if (!IsHandlingAllInput)
            {
                return;
            }

            if (mouseX > Game.Singleton.MapConsoleWidth)
            {
                return;
            }

            if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.Default)
            {
                InventoryUI.ChangeSelection(InventoryUI.ConvertYPositionToIndex(cellY));
            }
            else if (InventoryUI.InventoryState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
            {
                InventoryUI.ChangeOptionsSelection((cellY - InventoryUI.OptionsMinimumIndex) / 2);
            }
        }

        private void OnHoverExit(int mouseX, int mouseY)
        {
            if (!IsHandlingAllInput)
            {
                return;
            }

            if (mouseX > Game.Singleton.MapConsoleWidth)
            {
                return;
            }
        }

        #endregion
    }
}