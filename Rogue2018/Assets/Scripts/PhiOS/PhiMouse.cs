﻿namespace PhiOS
{
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using PhiOS.Interfaces;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1307:AccessibleFieldsMustBeginWithUpperCaseLetter", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1407:ArithmeticExpressionsMustDeclarePrecedence", Justification = "Unnecessary")]
    public class PhiMouse : MonoBehaviour
    {
        public static PhiMouse Singleton;

        public bool debugMouseCells;
        public bool debugMousePosition;
        public bool hideNativeCursor = true;
        public int reservedLayer = -1;
        public string cursorUp = "┼";
        public string cursorDown = "█";
        public Color cursorColor;
        public float cursorFadeTime = 0.25f;
        public bool fadeToClear = true;
        public float cursorHideTime = 3f;
        [HideInInspector]
        public Vector3 mousePosition;
        [HideInInspector]
        public Vector2 cellPosition;

        private bool initialized = false;
        private Vector2 lastCellPosition;
        private PhiCell currentCell;
        private PhiCell currentCellHover;
        private IHoverAction hoverAction;
        private bool dragging = false;
        private Vector2 dragStart;
        private IDragAction dragAction;
        private float currentHideTimer;

        public IEnumerator Start()
        {
            PhiDisplay display = PhiDisplay.Singleton;
            while (!display.initialized)
            {
                yield return null;
            }

            yield return null;

            initialized = true;
        }

        public void Update()
        {
            if (initialized)
            {
                PhiDisplay display = PhiDisplay.Singleton;

                int ommittedCellCountX = (int)(display.zoomDisplayRects[0].width - display.zoomDisplayRects[display.currentZoomLevel].width);
                int ommittedCellCountY = (int)(display.zoomDisplayRects[0].height - display.zoomDisplayRects[display.currentZoomLevel].height);

                mousePosition = new Vector3(
                    Mathf.Clamp((int)(Input.mousePosition.x / 10), 0, display.displayWidth - 1),
                    Mathf.Clamp((int)(display.displayHeight - (Input.mousePosition.y / 10)), 0, display.displayHeight - 1));

                if (mousePosition.x < display.zoomDisplayRects[display.currentZoomLevel].width + ommittedCellCountX)
                {
                    mousePosition *= display.zoomDisplayRects[display.currentZoomLevel].width / display.zoomDisplayRects[0].width;
                    mousePosition = new Vector3(
                        (int)(mousePosition.x + (ommittedCellCountX / 2)),
                        (int)(mousePosition.y + (ommittedCellCountY / 2)));
                }

                cellPosition = new Vector2(
                    Mathf.Clamp(mousePosition.x, 0f, display.displayWidth - 1),
                    Mathf.Clamp(mousePosition.y, 0f, display.displayHeight - 1));

                if (cellPosition == lastCellPosition)
                {
                    currentHideTimer += Time.deltaTime;
                    currentHideTimer = Mathf.Clamp(currentHideTimer, 0f, cursorHideTime);
                }
                else
                {
                    currentHideTimer = 0;
                }

                lastCellPosition = cellPosition;

                if (currentCell != null)
                {
                    Color clearColor = display.GetBackgroundColorForCell(
                                           (int)currentCell.position.x,
                                           (int)currentCell.position.y,
                                           reservedLayer);

                    currentCell.SetContent(
                        string.Empty,
                        clearColor,
                        fadeToClear ? display.clearColor : cursorColor,
                        cursorFadeTime,
                        cursorColor,
                        PhiCellFades.MOUSE_DEFAULT);

                    currentCell = null;
                }

                if (currentHideTimer == cursorHideTime)
                {
                    return;
                }

                currentCellHover = null;

                currentCell = display.GetPhiCell(reservedLayer, cellPosition.x, cellPosition.y);

                Color currentCellBackgroundColor = display.GetBackgroundColorForCell(
                    (int)currentCell.position.x,
                    (int)currentCell.position.y,
                    reservedLayer);

                currentCell.SetContent(
                    Input.GetMouseButton(0) || Input.GetMouseButton(1) ? cursorDown : cursorUp,
                    currentCellBackgroundColor,
                    cursorColor,
                    0f,
                    cursorColor,
                    string.Empty);

                if (!dragging)
                {
                    for (int i = display.GetNumLayers() - 1; i >= 0; i--)
                    {
                        PhiCell cellHover = display.GetPhiCell(i, currentCell.position.x, currentCell.position.y);

                        if (cellHover.content != string.Empty)
                        {
                            currentCellHover = cellHover;

                            if (currentCellHover.hoverAction != null)
                            {
                                if (hoverAction != currentCellHover.hoverAction)
                                {
                                    if (hoverAction != null)
                                    {
                                        hoverAction.OnHoverExit();
                                    }

                                    hoverAction = currentCellHover.hoverAction;
                                    hoverAction.OnHoverEnter((int)cellHover.position.x, (int)cellHover.position.y);
                                }
                            }
                            else if (hoverAction != null)
                            {
                                hoverAction.OnHoverExit();
                                hoverAction = null;
                            }

                            break;
                        }
                    }
                }

                if (!dragging)
                {
                    if (Input.GetMouseButtonDown(0) &&
                        currentCellHover != null &&
                        currentCellHover.clickAction != null)
                    {
                        currentCellHover.clickAction.OnMouseDown(0);
                    }
                    else if (Input.GetMouseButtonDown(1) &&
                        currentCellHover != null &&
                        currentCellHover.clickAction != null)
                    {
                        currentCellHover.clickAction.OnMouseDown(1);
                    }
                }

                if (!dragging &&
                    Input.GetMouseButtonDown(0) &&
                    currentCellHover != null &&
                    currentCellHover.dragAction != null)
                {
                    dragging = true;
                    dragStart = currentCell.position;
                    dragAction = currentCellHover.dragAction;
                    dragAction.OnDragStart();
                } 
                else if (dragging &&
                         Input.GetMouseButtonUp(0) &&
                         dragAction != null)
                {
                    dragging = false;
                    Vector2 dragDelta = currentCell.position - dragStart;
                    dragAction.OnDragDelta(dragDelta);
                    dragAction = null;
                }
                else if (dragging &&
                         dragAction != null)
                {
                    Vector2 dragDelta = currentCell.position - dragStart;
                    dragAction.OnDragDelta(dragDelta);
                }

                if (!dragging)
                {
                    if (Input.mouseScrollDelta.y != 0f &&
                        currentCellHover != null &&
                        currentCellHover.scrollAction != null)
                    {
                        currentCellHover.scrollAction.OnScrollDelta(Mathf.RoundToInt(Input.mouseScrollDelta.y));
                    }
                }

                if (debugMousePosition)
                {
                    Debug.Log(string.Format("MousePos: [{0}, {1}]", mousePosition.x, mousePosition.y));
                }

                if (debugMouseCells && Input.GetKeyDown(KeyCode.F4))
                {
                    Debug.Log("TEST: SHOWING CELLS WHICH ARE INTERACTABLE WITH THE MOUSE");
                    for (int x = 0; x < PhiDisplay.Singleton.displayWidth; x++)
                    {
                        for (int y = 0; y < PhiDisplay.Singleton.displayHeight; y++)
                        {
                            PhiCell phiCell = PhiDisplay.PhiCellAt(0, x, y);
                            if (phiCell.clickAction != null || phiCell.hoverAction != null)
                            {
                                phiCell.SetContent(phiCell.content, Color.magenta, phiCell.color);
                            }
                        }
                    }
                }
            }
        }

        private void Awake()
        {
            if (hideNativeCursor)
            {
                Cursor.visible = false;
            }

            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }
    }
}