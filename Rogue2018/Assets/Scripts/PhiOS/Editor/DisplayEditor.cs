﻿namespace PhiOS
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Display))]
    public class DisplayEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                PhiDisplay display = (PhiDisplay)target;

                if (display.autoDisplayHeight && display.displayHeight != 0)
                {
                    display.displayHeight = 0;
                }
            }
        }
    }
}