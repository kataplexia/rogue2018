﻿namespace PhiOS
{
    using System.Diagnostics.CodeAnalysis;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1307:AccessibleFieldsMustBeginWithUpperCaseLetter", Justification = "Unnecessary")]
    [System.Serializable]
    public class PhiCellProperties
    {
        public int X;
        public int Y;
        public int layer;
        public string symbol;
        public Color drawColor;
        public Color backgroundDrawColor;
    }
}