﻿namespace PhiOS.Interfaces
{
    using UnityEngine;

    public interface IDragAction
    {
        void OnDragStart();

        void OnDragDelta(Vector2 delta);
    }
}