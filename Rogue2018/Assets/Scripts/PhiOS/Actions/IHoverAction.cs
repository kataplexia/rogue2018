﻿namespace PhiOS.Interfaces
{
    public interface IHoverAction
    {
        void OnHoverEnter(int cellX, int cellY);

        void OnHoverExit();
    }
}