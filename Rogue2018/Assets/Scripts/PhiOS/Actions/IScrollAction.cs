﻿namespace PhiOS.Interfaces
{
    public interface IScrollAction
    {
        void OnScrollDelta(int delta);
    }
}