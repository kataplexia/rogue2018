﻿namespace PhiOS
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1307:AccessibleFieldsMustBeginWithUpperCaseLetter", Justification = "Unnecessary")]
    [System.Serializable]
    public class PhiCellPropertiesList : MonoBehaviour
    {
        public List<PhiCellProperties> propertiesList;
    }
}