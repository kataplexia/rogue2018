﻿namespace PhiOS
{
    using System.Diagnostics.CodeAnalysis;
    using PhiOS.Interfaces;
    using UnityEngine;

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Unnecessary")]
    public class PhiCellFades
    {
        public static string DEFAULT_REVERSE = "░▒▓█";
        public static string DEFAULT = "█▓▒░";
        public static string MOUSE_DEFAULT = "+";
    }

    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1307:AccessibleFieldsMustBeginWithUpperCaseLetter", Justification = "Unnecessary")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Unnecessary")]
    public class PhiCell
    {
        public int layer;
        public Vector2 position;
        public MonoBehaviour owner;
        public string content = string.Empty;
        public Color backgroundColor;
        public Color color;
        public string tag;
        public IHoverAction hoverAction;
        public IClickAction clickAction;
        public IDragAction dragAction;
        public IScrollAction scrollAction;

        private string targetContent = string.Empty;
        private Color targetColor;
        private float fadeLeft = 0f;
        private float fadeMax = 0f;
        private Color fadeColor;
        private string fades = string.Empty;
        private bool fadeFinished = true;

        public void Clear()
        {
            SetContent(string.Empty, Color.clear, Color.clear);
            tag = string.Empty;
        }

        public void Clear(float fadeTime, Color fadeColor)
        {
            SetContent(string.Empty, Color.clear, Color.clear, fadeTime, fadeColor, PhiCellFades.DEFAULT_REVERSE);
            tag = string.Empty;
        }

        public void SetContent(string content, Color backgroundColor, Color color)
        {
            SetContent(content, backgroundColor, color, 0f, color, string.Empty);
        }

        public void SetContent(string content, Color backgroundColor, Color color, float fadeTime, Color fadeColor)
        {
            SetContent(content, backgroundColor, color, fadeTime, fadeColor, PhiCellFades.DEFAULT);
        }

        public void SetContent(
            string content,
            Color backgroundColor,
            Color color,
            float fadeMax,
            Color fadeColor,
            string fades)
        {
            targetContent = content;
            this.backgroundColor = backgroundColor;
            targetColor = color;

            if (fadeMax > 0f)
            {
                this.fadeLeft = this.fadeMax = Random.Range(0f, fadeMax);
                this.color = this.fadeColor = fadeColor;
                this.fades = fades;
                fadeFinished = false;
            }
            else
            {
                this.fadeLeft = 0f;
                this.fadeMax = 0f;
                fadeFinished = false;
            }

            if (targetContent != string.Empty)
            {
                PhiDisplay.Singleton.AddCellAsTopLayer(this);
            }
        }

        public void SetTag(string setTag)
        {
            tag = setTag;
        }

        public string GetTag()
        {
            return tag;
        }

        public void Update()
        {
            if (PhiDisplay.Singleton.initialized)
            {
                if (fadeLeft > 0f)
                {
                    content = targetContent.Trim().Length > 0 || content.Trim().Length > 0 ?
                        fades.Substring(Mathf.RoundToInt((fadeLeft / fadeMax) * (fades.Length - 1)), 1) :
                        targetContent;
                    color = Color.Lerp(
                        targetColor,
                        fadeColor,
                        PhiDisplay.Singleton.colorLerpCurve.Evaluate(fadeLeft / fadeMax));
                    fadeLeft -= Time.deltaTime;
                }
                else
                {
                    if (!fadeFinished && targetContent == string.Empty)
                    {
                        PhiDisplay.Singleton.RemoveCellAsTopLayer(this);
                    }

                    fadeFinished = true;
                    fadeLeft = 0f;
                    content = targetContent;
                    color = targetColor;
                }
            }
        }
    }
}