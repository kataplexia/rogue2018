﻿namespace Rogue2018.Behaviours
{
    using System.Collections.Generic;
    using Rogue2018.Core;
    using Rogue2018.Interfaces;
    using Rogue2018.Systems;
    using RogueSharp;

    public class GoalMapMoveAttack : IBehaviour
    {
        #region Fields

        private FieldOfView monsterFOV;

        #endregion

        #region Methods

        public bool Act(Monster monster, CommandSystem commandSystem)
        {
            if (monsterFOV == null)
            {
                monsterFOV = new FieldOfView(Game.Singleton.CurrentMap);
            }

            if (!monster.TurnsAlerted.HasValue)
            {
                monsterFOV.ComputeFOV(monster.X, monster.Y, monster.Awareness, true);
                if (monsterFOV.IsInFOV(Game.Singleton.Player.X, Game.Singleton.Player.Y))
                {
                    monster.TurnsAlerted = 1;
                }
            }
            else
            {
                List<GoalMap.WeightedPoint> neighbours = Game.Singleton.CurrentMap.MonsterGoalMap.GetNeighbors(monster.X, monster.Y);
                int bestWeight = int.MaxValue;
                ICell bestNeighbourCell = null;

                foreach (GoalMap.WeightedPoint neighbour in neighbours)
                {
                    ICell weightedPointCell = Game.Singleton.CurrentMap.GetCell(neighbour.X, neighbour.Y);
                    bool isPlayerCell = weightedPointCell.X == Game.Singleton.Player.X && weightedPointCell.Y == Game.Singleton.Player.Y;

                    if (!weightedPointCell.IsWalkable && !isPlayerCell)
                    {
                        continue;
                    }
                    else if (neighbour.Weight < bestWeight)
                    {
                        bestWeight = neighbour.Weight;
                        bestNeighbourCell = weightedPointCell;
                    }
                }

                if (bestNeighbourCell != null)
                {
                    commandSystem.MoveMonster(monster, bestNeighbourCell);
                }

                monster.TurnsAlerted++;

                if (monster.TurnsAlerted > 10)
                {
                    monster.TurnsAlerted = null;
                }
            }

            return true;
        }

        #endregion
    }
}