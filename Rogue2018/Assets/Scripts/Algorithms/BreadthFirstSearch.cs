﻿namespace Rogue2018.Algorithms
{
    using System.Collections.Generic;
    using RogueSharp;

    public class BreadthFirstSearch
    {
        #region Fields

        private bool[][] visited;

        #endregion

        #region Methods

        public Point FindUnexploredCellPoint(Map map, Point origin)
        {
            Stack<Point> points = new Stack<Point>();
            points.Push(origin);

            visited = new bool[map.Height][];
            for (int i = 0; i < visited.Length; i++)
            {
                visited[i] = new bool[map.Width];
            }

            while (points.Count != 0)
            {
                Point point = points.Pop();

                if (point != origin && (!map.IsWalkable(point.X, point.Y) || visited[point.Y][point.X]))
                {
                    continue;
                }

                if (point.X < map.Width && point.X > 0 && point.Y < map.Height && point.Y > 0)
                {
                    visited[point.Y][point.X] = true;

                    if (map.IsExplored(point.X, point.Y))
                    {
                        if (map.IsWalkable(point.X - 1, point.Y))
                        {
                            points.Push(new Point(point.X - 1, point.Y));
                        }

                        if (map.IsWalkable(point.X + 1, point.Y))
                        {
                            points.Push(new Point(point.X + 1, point.Y));
                        }

                        if (map.IsWalkable(point.X, point.Y - 1))
                        {
                            points.Push(new Point(point.X, point.Y - 1));
                        }

                        if (map.IsWalkable(point.X, point.Y + 1))
                        {
                            points.Push(new Point(point.X, point.Y + 1));
                        }
                    }
                    else
                    {
                        return point;
                    }
                }
            }

            return null;
        }

        #endregion
    }
}
