﻿#if UNITY_EDITOR
using System.Collections.Generic;
using PhiOS;
using UnityEditor;
using UnityEngine;

public class TextureParse : MonoBehaviour
{
    #region Methods

    public void ParseTextureFile(Texture2D texture, int xOffset, int yOffset, int layer = 0)
    {
        Debug.Log(string.Format("Parsing {0} with offset ({1}, {2}) and layer {3}.", texture.name, xOffset, yOffset, layer));

        GameObject workingPrefab = new GameObject();

        PhiCellPropertiesList cellPropertiesList = workingPrefab.AddComponent<PhiCellPropertiesList>();
        cellPropertiesList.propertiesList = new List<PhiCellProperties>();

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                Color textureColor = texture.GetPixel(x, y);

                PhiCellProperties phiCellProperties = new PhiCellProperties();
                phiCellProperties.X = x + xOffset;
                phiCellProperties.Y = (texture.height - y - 1) + yOffset;
                phiCellProperties.layer = layer;
                phiCellProperties.symbol = " ";
                phiCellProperties.drawColor = Color.clear;
                phiCellProperties.backgroundDrawColor = textureColor;

                cellPropertiesList.propertiesList.Add(phiCellProperties);
            }
        }

        string prefabName = texture.name + "CellProperties";
        int propertiesCount = cellPropertiesList.propertiesList.Count;

        Object tempPrefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/TextureParse/CellPropertiesPrefabs/" + prefabName + ".prefab");
        PrefabUtility.ReplacePrefab(workingPrefab, tempPrefab, ReplacePrefabOptions.ConnectToPrefab);
        DestroyImmediate(workingPrefab);

        Debug.Log(string.Format("Parse succesful. {0} created with {1} CellProperties components.", prefabName, propertiesCount));
    }

    #endregion
}
#endif