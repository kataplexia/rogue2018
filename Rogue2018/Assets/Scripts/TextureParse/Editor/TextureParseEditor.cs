﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TextureParse))]
public class TextureParseSystemEditor : Editor
{
    #region Fields

    private Texture2D texture = null;
    private int xOffset = 0;
    private int yOffset = 0;
    private int layer = 0;

    #endregion

    #region Methods

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        TextureParse textureParse = (TextureParse)target;

        texture = (Texture2D)EditorGUILayout.ObjectField("Texture", texture, typeof(Texture2D), false);
        xOffset = EditorGUILayout.IntField("X Offset: ", xOffset);
        yOffset = EditorGUILayout.IntField("Y Offset: ", yOffset);
        layer = EditorGUILayout.IntField("Layer: ", layer);

        if (GUILayout.Button("Create Cell Properties List"))
        {
            if (texture != null)
            {
                textureParse.ParseTextureFile(texture, xOffset, yOffset, layer);
            }
        }
    }

    #endregion
}
