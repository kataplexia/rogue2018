﻿using UnityEngine;

using System.Collections.Generic;
using RogueSharp;

using UnityEngine.SceneManagement;
using Rogue2018.Core;
using Rogue2018.UserInterface;

namespace Rogue2018.Systems
{
    public class InputSystem : IClickAction, IHoverAction
    {
        // IClickAction and IHoverAction properties
        public delegate void OnMouseDownAction(int mouseX, int mouseY, int mouseButtonIndex);
        public delegate void OnHoverEnterAction(int mouseX, int mouseY, int cellX, int cellY);
        public delegate void OnHoverExitAction(int mouseX, int mouseY);
        public static event OnMouseDownAction OnMouseDownEvent;
        public static event OnHoverEnterAction OnHoverEnterEvent;
        public static event OnHoverExitAction OnHoverExitEvent;

        private static bool _overHoverableCell;
        public bool OverHoverableCell { get { return _overHoverableCell; } }

        public void OnMouseDown(int mouseButtonIndex)
        {
            if (OnMouseDownEvent != null)
                OnMouseDownEvent(GetMouseCellPositionX(), GetMouseCellPositionY(), mouseButtonIndex);
        }

        public void OnHoverEnter(int cellX, int cellY)
        {
            _overHoverableCell = true;
            if (OnHoverEnterEvent != null)
                OnHoverEnterEvent(GetMouseCellPositionX(), GetMouseCellPositionY(), cellX, cellY);
        }

        public void OnHoverExit()
        {
            _overHoverableCell = false;
            if (OnHoverExitEvent != null)
                OnHoverExitEvent(GetMouseCellPositionX(), GetMouseCellPositionY());
        }

        private int GetMouseCellPositionX()
        {
            return (int)Input.mousePosition.x / 10;
        }

        private int GetMouseCellPositionY()
        {
            return game._mapConsoleHeight - ((int)Input.mousePosition.y / 10) - 1;
        }

        // Constructor
        private static Game game;
        private static Display display;
        private static UIManager uiManager;
        private static CommandSystem commandSystem;
        private static MessageSystem messageSystem;
        public void ConstructInputSystem()
        {
            game = Game.GET;
            display = Display.GET;
            uiManager = UIManager.GET;
            commandSystem = game.commandSystem;
            messageSystem = game.messageSystem;
        }

        // Method for updating input
        private static bool _didPlayerAct;
        public void UpdateInput()
        {
            if (uiManager.isHandlingAllInput)
                return;

            // Check for miniMap zoom input
            if (CheckInputMiniMapZoom())
                return;

            // Check for zoom input
            if (CheckInputZoom())
                return;

            // TEST: Add 20 random equipable items to player inventory with random quality
            if (Input.GetKeyDown(KeyCode.F1))
            {
                Debug.Log("TEST: FILLED INVENTORY WITH RANDOM ITEMS");
                game.player.TEST_FillInventoryWithRandomItems();
                return;
            }

            // TEST: Reveals the whole map
            if (Input.GetKeyDown(KeyCode.F2))
            {
                Debug.Log("TEST: REVEALING THE MAP");
                game.tools.RevealMap();
                game.SetDrawRequired(true);
                return;
            }

            // TEST: Toggles draw tiles
            if (Input.GetKeyDown(KeyCode.F3))
            {
                Debug.Log("TEST: TOGGLING TILE DRAWING / ASCII DRAWING");
                Options.GET.DrawTiles = Options.GET.DrawTiles ? false : true;
                game.SetDrawRequired(true);
                return;
            }

            // TEST: resets the game
            if (Input.GetKeyDown(KeyCode.F5))
            {
                Debug.Log("TEST: RESETTING THE GAME");
                SceneManager.LoadScene(0);
                return;
            }

            if (Input.GetKeyDown(KeyCode.F6))
            {
                Debug.Log("TEST: DEBUG WEIGHTED POINTS AROUND PLAYER AND MONSTERS");
                ICell cellToCompute = game.rogueMap.GetCell(game.player.X, game.player.Y);

                int size = 20;

                int xMin = cellToCompute.X - (size / 2);
                xMin = Mathf.Clamp(xMin, 0, xMin);
                int xMax = cellToCompute.X + (size / 2);
                xMax = Mathf.Clamp(xMax, xMax, game.rogueMap.Width);

                int yMin = cellToCompute.Y - (size / 2);
                yMin = Mathf.Clamp(yMin, 0, yMin);
                int yMax = cellToCompute.Y + (size / 2);
                yMax = Mathf.Clamp(yMax, yMax, game.rogueMap.Height);

                for (int y = yMin; y < yMax; y++)
                {
                    for (int x = xMin; x < xMax; x++)
                    {
                        int cellWeight = game.rogueMap.monsterGoalMap._cellWeights[x, y];

                        if (cellWeight == int.MinValue)
                            continue;

                        string cellWeightString = "";

                        if (cellWeight > 9)
                        {
                            if (cellWeight == 10)
                                cellWeightString = "A";
                            else if (cellWeight == 11)
                                cellWeightString = "B";
                            else if (cellWeight == 12)
                                cellWeightString = "C";
                            else if (cellWeight == 13)
                                cellWeightString = "D";
                            else if (cellWeight == 14)
                                cellWeightString = "E";
                            else if (cellWeight == 15)
                                cellWeightString = "F";
                            else if (cellWeight == 16)
                                cellWeightString = "G";
                            else if (cellWeight == 17)
                                cellWeightString = "H";
                            else if (cellWeight == 18)
                                cellWeightString = "I";
                            else if (cellWeight == 19)
                                cellWeightString = "J";
                            else if (cellWeight == 20)
                                cellWeightString = "K";
                        }

                        if (cellWeightString == "")
                            cellWeightString = cellWeight.ToString();

                        game.rogueMap.SetSymbol(x, y, cellWeightString);

                        Color drawColor = Color.white * ((10 - cellWeight) * 0.1f);
                        game.rogueMap.SetDrawColorFOV(x, y, drawColor);
                        game.rogueMap.SetDrawColor(x, y, drawColor);
                    }
                }

                foreach (Monster monster in game.rogueMap._monsters)
                {
                    List<GoalMap.WeightedPoint> weightedPoints = game.rogueMap.monsterGoalMap.GetNeighbors(monster.X, monster.Y);
                    foreach (GoalMap.WeightedPoint weightedPoint in weightedPoints)
                    {
                        game.rogueMap.SetSymbol(weightedPoint.X, weightedPoint.Y, weightedPoint.Weight.ToString());

                        Color drawColor = Color.red * ((10 - weightedPoint.Weight) * 0.1f);
                        game.rogueMap.SetDrawColorFOV(weightedPoint.X, weightedPoint.Y, drawColor);
                        game.rogueMap.SetDrawColor(weightedPoint.X, weightedPoint.Y, drawColor);
                    }
                }

                game.SetDrawRequired(true);
            }

            // Check for player directional input
            _didPlayerAct = false;
            if (commandSystem.IsPlayerTurn)
            {
                // Check for auto explore input
                CheckInputAutoExploreKeyDown();

                CheckInputAutoExploreKeyHeld();

                // Check for direction input KeyDown
                CheckInputDirectionalKeyDown();

                // Check for direction input KeyHeld
                CheckInputDirectionalKeyHeld();

                // Check for stairs interaction input
                CheckInputStairsInteraction();

                // TODO: Replace with menu
                // Check for escape key input
                if (Input.GetKeyDown(KeyCode.Escape))
                    Application.Quit();

                // Check for action and end turn then draw
                if (_didPlayerAct)
                    commandSystem.EndPlayerTurn();
            }
            else
            {
                // It is not the players turn so activate monsters then draw
                commandSystem.ActivateMonsters();
                messageSystem.ProcessTurn();
                game.SetDrawRequired(true);

            }
        }

        // Returns input for zoom command
        private bool CheckInputZoom()
        {
            // Keyboard input for zoom
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                if (Input.GetKeyDown(KeyCode.Z) && display.currentZoomLevel > 0)
                {
                    game.StartZoomRoutine(--display.currentZoomLevel);
                    return true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Z) && display.currentZoomLevel < display.zoomOrthographicSizes.Length - 1)
            {
                game.StartZoomRoutine(++display.currentZoomLevel);
                return true;
            }

            // Check if mouse is over map console
            if (game.mouse.mousePosition.x <= 119)
            {
                // Mouse scroll input for zoom
                if (Input.mouseScrollDelta.y < 0 && display.currentZoomLevel > 0)
                {
                    game.StartZoomRoutine(--display.currentZoomLevel);
                    return true;
                }
                else if (Input.mouseScrollDelta.y > 0 && display.currentZoomLevel < display.zoomOrthographicSizes.Length - 1)
                {
                    game.StartZoomRoutine(++display.currentZoomLevel);
                    return true;
                }
            }
            return false;
        }

        private bool CheckInputMiniMapZoom()
        {
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            { 
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    game.miniMap.SetScale(--game.miniMap.currentScaleFactor);
                    return true;
                }
            }
            else if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            {
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    game.miniMap.SetScale(++game.miniMap.currentScaleFactor);
                    return true;
                }
            }

            // Check if mouse is over minimap
            if (game.mouse.mousePosition.x >= 126 &&
                game.mouse.mousePosition.x <= 153 &&
                game.mouse.mousePosition.y >= 59 &&
                game.mouse.mousePosition.y <= 86)
            {
                // Mouse scroll input for minimap zoom
                if (Input.mouseScrollDelta.y < 0)
                {
                    game.miniMap.SetScale(--game.miniMap.currentScaleFactor);
                    return true;
                }
                else if (Input.mouseScrollDelta.y > 0)
                {
                    game.miniMap.SetScale(++game.miniMap.currentScaleFactor);
                    return true;
                }
            }
            return false;
        }

        // Method for checking input for auto explore
        private void CheckInputAutoExploreKeyDown()
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                bool monstersAlerted = game.rogueMap.AreMonstersAlerted();
                bool monstersInFOV = game.rogueMap.AreMonstersInFOV();
                if (!monstersAlerted && !monstersInFOV)
                {
                    commandSystem.AutoExplore();
                    _didPlayerAct = true;
                    return;
                }
                else if (monstersAlerted)
                {
                    string monsterAlertedMessage = "Cannot explore while enemies are alerted";
                    messageSystem.Add(monsterAlertedMessage, Color.red, false);
                    game.SetDrawRequired(true);
                    return;
                }
                else if (monstersInFOV)
                {
                    string monsterAlertedMessage = "Cannot explore while enemies are in sight";
                    messageSystem.Add(monsterAlertedMessage, Color.red, false);
                    game.SetDrawRequired(true);
                    return;
                }
            }
            else if (Input.GetKeyUp(KeyCode.X))
            {
                commandSystem.ResetAutoExplore();
            }
        }

        private void CheckInputAutoExploreKeyHeld()
        {
            if (CheckKeyHeld(KeyCode.X))
            {
                bool monstersAlerted = game.rogueMap.AreMonstersAlerted();
                bool monstersInFOV = game.rogueMap.AreMonstersInFOV();
                if (!monstersAlerted && !monstersInFOV)
                {
                    commandSystem.AutoExplore();
                    _didPlayerAct = true;
                    return;
                }
            }
        }

        // Method for checking directional input using KeyDown
        private void CheckInputDirectionalKeyDown()
        {
            if (Input.GetKeyDown(KeyCode.Keypad1))
                _didPlayerAct = commandSystem.MovePlayer(Directions.DownLeft);
            else if (Input.GetKeyDown(KeyCode.Keypad2))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Down);
            else if (Input.GetKeyDown(KeyCode.Keypad3))
                _didPlayerAct = commandSystem.MovePlayer(Directions.DownRight);
            else if (Input.GetKeyDown(KeyCode.Keypad4))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Left);
            else if (Input.GetKeyDown(KeyCode.Keypad5))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Center);
            else if (Input.GetKeyDown(KeyCode.Keypad6))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Right);
            else if (Input.GetKeyDown(KeyCode.Keypad7))
                _didPlayerAct = commandSystem.MovePlayer(Directions.UpLeft);
            else if (Input.GetKeyDown(KeyCode.Keypad8))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Up);
            else if (Input.GetKeyDown(KeyCode.Keypad9))
                _didPlayerAct = commandSystem.MovePlayer(Directions.UpRight);
        }

        // Returns true for directional input using KeyHeld
        private void CheckInputDirectionalKeyHeld()
        {
            if (CheckKeyHeld(KeyCode.Keypad1))
                _didPlayerAct = commandSystem.MovePlayer(Directions.DownLeft);
            else if (CheckKeyHeld(KeyCode.Keypad2))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Down);
            else if (CheckKeyHeld(KeyCode.Keypad3))
                _didPlayerAct = commandSystem.MovePlayer(Directions.DownRight);
            else if (CheckKeyHeld(KeyCode.Keypad4))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Left);
            else if (CheckKeyHeld(KeyCode.Keypad5))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Center);
            else if (CheckKeyHeld(KeyCode.Keypad6))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Right);
            else if (CheckKeyHeld(KeyCode.Keypad7))
                _didPlayerAct = commandSystem.MovePlayer(Directions.UpLeft);
            else if (CheckKeyHeld(KeyCode.Keypad8))
                _didPlayerAct = commandSystem.MovePlayer(Directions.Up);
            else if (CheckKeyHeld(KeyCode.Keypad9))
                _didPlayerAct = commandSystem.MovePlayer(Directions.UpRight);
        }

        // Returns true if key is held
        private static float keyHoldStart = 0f;
        private static float keyHoldTimer = 0f;
        private static float keyHoldTimeRequired = 0.75f;
        public bool CheckKeyHeld(KeyCode key)
        {
            if (Input.GetKeyDown(key))
            {
                keyHoldStart = Time.time;
                keyHoldTimer = keyHoldStart;
                return false;
            }
            else if (Input.GetKeyUp(key))
                return false;

            if (Input.GetKey(key))
            {
                keyHoldTimer += Time.deltaTime;
                if (keyHoldTimer > (keyHoldStart + keyHoldTimeRequired))
                    return true;
            }
            return false;
        }

        // Returns true for stairs interaction input
        private void CheckInputStairsInteraction()
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                // Check input for moving down stairs
                if (Input.GetKeyDown(KeyCode.Period) && game.rogueMap.CanMoveDownStairs())
                {
                    game.ChangeMap(game.rogueMap.GetExit(game.player.X, game.player.Y).ExitToName);
                    game.SetDrawRequired(true);
                }
                // Check input for moving up stairs
                else if (Input.GetKeyDown(KeyCode.Comma) && game.rogueMap.CanMoveUpStairs())
                {
                    game.ChangeMap(game.rogueMap.GetExit(game.player.X, game.player.Y).ExitToName);
                    game.SetDrawRequired(true);
                }
            }
        }

        public string GetAlphabeticInput()
        {
            bool shiftHeld = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            if (Input.GetKeyDown(KeyCode.A))
                if (shiftHeld) return "A"; else return "a";
            else if (Input.GetKeyDown(KeyCode.B))
                if (shiftHeld) return "B"; else return "b";
            else if (Input.GetKeyDown(KeyCode.C))
                if (shiftHeld) return "C"; else return "c";
            else if (Input.GetKeyDown(KeyCode.D))
                if (shiftHeld) return "D"; else return "d";
            else if (Input.GetKeyDown(KeyCode.E))
                if (shiftHeld) return "E"; else return "e";
            else if (Input.GetKeyDown(KeyCode.F))
                if (shiftHeld) return "F"; else return "f";
            else if (Input.GetKeyDown(KeyCode.G))
                if (shiftHeld) return "G"; else return "g";
            else if (Input.GetKeyDown(KeyCode.H))
                if (shiftHeld) return "H"; else return "h";
            else if (Input.GetKeyDown(KeyCode.I))
                if (shiftHeld) return "I"; else return "i";
            else if (Input.GetKeyDown(KeyCode.J))
                if (shiftHeld) return "J"; else return "j";
            else if (Input.GetKeyDown(KeyCode.K))
                if (shiftHeld) return "K"; else return "k";
            else if (Input.GetKeyDown(KeyCode.L))
                if (shiftHeld) return "L"; else return "l";
            else if (Input.GetKeyDown(KeyCode.M))
                if (shiftHeld) return "M"; else return "m";
            else if (Input.GetKeyDown(KeyCode.N))
                if (shiftHeld) return "N"; else return "n";
            else if (Input.GetKeyDown(KeyCode.O))
                if (shiftHeld) return "O"; else return "o";
            else if (Input.GetKeyDown(KeyCode.P))
                if (shiftHeld) return "P"; else return "p";
            else if (Input.GetKeyDown(KeyCode.Q))
                if (shiftHeld) return "Q"; else return "q";
            else if (Input.GetKeyDown(KeyCode.R))
                if (shiftHeld) return "R"; else return "r";
            else if (Input.GetKeyDown(KeyCode.S))
                if (shiftHeld) return "S"; else return "s";
            else if (Input.GetKeyDown(KeyCode.T))
                if (shiftHeld) return "T"; else return "t";
            else if (Input.GetKeyDown(KeyCode.U))
                if (shiftHeld) return "U"; else return "u";
            else if (Input.GetKeyDown(KeyCode.V))
                if (shiftHeld) return "V"; else return "v";
            else if (Input.GetKeyDown(KeyCode.W))
                if (shiftHeld) return "W"; else return "w";
            else if (Input.GetKeyDown(KeyCode.X))
                if (shiftHeld) return "X"; else return "x";
            else if (Input.GetKeyDown(KeyCode.Y))
                if (shiftHeld) return "Y"; else return "y";
            else if (Input.GetKeyDown(KeyCode.Z))
                if (shiftHeld) return "Z"; else return "z";
            else
                return "";
        }
    }
}