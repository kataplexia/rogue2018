﻿using System.Collections.Generic;
using UnityEngine;
using Rogue2018.Core;

namespace Rogue2018.Systems
{
    public class MessageSystem
    {
        // Constructor
        private static readonly int _maxLines = 17;
        private static readonly int _maxChars = 34;
        private readonly Queue<string> _lines;
        private readonly Queue<Color> _lineColors;
        
        public MessageSystem()
        {
            _lines = new Queue<string>();
            _lineColors = new Queue<Color>();
        }

        // Constructor
        private static Game game;
        public void ConstructMessageSystem()
        {
            game = Game.GET;
        }

        private static int lastMessageCount;
        private static int recentMessageCount;
        public void ProcessTurn()
        {
            lastMessageCount = recentMessageCount;
            recentMessageCount = 0;
        }

        // Method for adding a message to the queue
        public void Add(string message, Color messageColor, bool indent = false)
        {
            message = "∙" + message;

            if (indent)
                message = "  " + message;

            if (message.Length > _maxChars)
            {
                int charPos = _maxChars;
                while (charPos > 0 && message[charPos] != ' ')
                    charPos--;
                _lines.Enqueue(message.Substring(0, charPos));
                _lineColors.Enqueue(messageColor);
                Add(message.Substring(charPos + 1), messageColor, indent);
                recentMessageCount++;
            }
            else
            {
                _lines.Enqueue(message);
                _lineColors.Enqueue(messageColor);
                recentMessageCount++;
            }

            if (_lines.Count > _maxLines)
            {
                _lines.Dequeue();
                _lineColors.Dequeue();
            }
        }

        // Method for drawing message console
        public void Draw(int xOffset, int yOffset)
        {
            string[] lines = _lines.ToArray();
            Color[] colors = _lineColors.ToArray();
            for (int i = lines.Length - 1; i >= 0; i--)
            {
                string line = lines[(lines.Length - 1) - i];
                Color color = colors[(colors.Length - 1) - i];

                if ((lines.Length - 1) - i < lines.Length - (recentMessageCount + lastMessageCount))
                    color = (Color.gray * 1.375f) * (((_maxLines + 3) - i) * (float)(2f / (_maxLines + 3)));

                for (int x = 0; x < line.Length; x++)
                {
                    Cell cell = Display.CellAt(game._messageLayer, x + xOffset + 1, (_maxLines - 1) - i + yOffset + 1);
                    cell.SetContent(line.Substring(x, 1), Color.clear, color);
                }
            }
        }
    }
}