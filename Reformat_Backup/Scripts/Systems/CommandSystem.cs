﻿using System.Text;
using System.Collections.Generic;
using UnityEngine;
using RogueSharp;
using RogueSharp.DiceNotation;
using Rogue2018.Actors;
using Rogue2018.Core;
using Rogue2018.Core.MapCore;
using Rogue2018.Interfaces;

namespace Rogue2018.Systems
{
    public class CommandSystem
    {
        // Constructor
        private static Game                 game;
        private static Player               player;
        private static RogueMap             rogueMap;
        private static MessageSystem        messageSystem;
        private static SchedulingSystem     schedulingSystem;
        public void ConstructCommandSystem()
        {
            game = Game.GET;
            player = game.player;
            rogueMap = game.rogueMap;
            messageSystem = game.messageSystem;
            schedulingSystem = game.schedulingSystem;
        }

        // Method for ending player's turn
        public bool IsPlayerTurn { get; set; }
        public void EndPlayerTurn()
        {
            IsPlayerTurn = false;
        }

        private bool isGoalAnExit = false;
        public void ResetAutoExplore()
        {
            if (rogueMap.playerGoalCell != null)
            {
                if (!isGoalAnExit && rogueMap.IsExplored(rogueMap.playerGoalCell.X, rogueMap.playerGoalCell.Y))
                    rogueMap.playerGoalCell = null;
            }
        }

        public void AutoExplore()
        {
            if (rogueMap.playerGoalCell != null)
            {
                if (player.X == rogueMap.playerGoalCell.X && player.Y == rogueMap.playerGoalCell.Y)
                {
                    isGoalAnExit = !isGoalAnExit;
                    rogueMap.playerGoalCell = null;
                }    
            }

            if (rogueMap.playerGoalCell == null)
            {
                if (rogueMap.IsMapExplored())
                {
                    rogueMap.FindPlayerGoalExit();
                    isGoalAnExit = true;
                }
                else
                {
                    rogueMap.FindPlayerGoal();
                }
            }

            List<GoalMap.WeightedPoint> neighbours = Game.GET.rogueMap.playerGoalMap.GetNeighbors(player.X, player.Y);

            int bestWeight = int.MaxValue;
            ICell bestNeighbourCell = null;
            foreach (GoalMap.WeightedPoint neighbour in neighbours)
            {
                ICell weightedPointCell = rogueMap.GetCell(neighbour.X, neighbour.Y);

                if (neighbour.Weight < bestWeight)
                {
                    bestWeight = neighbour.Weight;
                    bestNeighbourCell = weightedPointCell;
                }
            }

            if (bestNeighbourCell != null)
                rogueMap.SetActorPosition(player, bestNeighbourCell.X, bestNeighbourCell.Y);

            /*

            rogueMap.SetIsWalkable(player.X, player.Y, true);

            RogueSharp.Path explorePath = null;
            try
            {
                explorePath = game.rogueMap.playerGoalMap.FindPath(player.X, player.Y);
            }
            catch (RogueSharp.PathNotFoundException) { }

            if (explorePath != null)
            {
                try
                {
                    RogueSharp.ICell nextStep = explorePath.StepForward();
                    rogueMap.SetActorPosition(player, nextStep.X, nextStep.Y);

                }
                catch (RogueSharp.NoMoreStepsException) { }
            }

            */
        }

        // Return true if Player is able to move
        public bool MovePlayer(Directions direction)
        {
            // Setup position for movement
            int x = player.X;
            int y = player.Y;
            switch (direction)
            {
                    case Directions.DownLeft:
                    {
                        x = player.X - 1;
                        y = player.Y + 1;
                        break;
                    }
                    case Directions.Down:
                    {
                        y = player.Y + 1;
                        break;
                    }
                    case Directions.DownRight:
                    {
                        x = player.X + 1;
                        y = player.Y + 1;
                        break;
                    }
                    case Directions.Left:
                    {
                        x = player.X - 1;
                        break;
                    }
                    case Directions.Center:
                    {
                        break;
                    }
                    case Directions.Right:
                    {
                        x = player.X + 1;
                        break;
                    }
                    case Directions.UpLeft:
                    {
                        x = player.X - 1;
                        y = player.Y - 1;
                        break;
                    }
                    case Directions.Up:
                    {
                        y = player.Y - 1;
                        break;
                    }
                    case Directions.UpRight:
                    {
                        x = player.X + 1;
                        y = player.Y - 1;
                        break;
                    }
                    default:
                        return false;
            }

            // Check if able to move to position
            if (rogueMap.SetActorPosition(player, x, y))
                return true;

            // Check for monster at position
            Monster monster = rogueMap.GetMonsterAt(x, y);
            if (monster != null)
            {
                Attack(player, monster);
                return true;
            }

            Audio.GET.QueueAudio(Audio.GET.playerStuck);
            return false;
        }
        
        // Gets next scheduleable and performs actions until player's turn
        public void ActivateMonsters()
        {
            ISchedulable schedulable = schedulingSystem.Get();
            if (schedulable is Player)
            {
                // turnTimer.Stop();
                IsPlayerTurn = true;
                schedulingSystem.Add(player);
            }
            else
            {
                Monster monster = schedulable as Monster;
                if (monster != null)
                {
                    monster.PerformAction(this);
                    schedulingSystem.Add(monster);
                }
                ActivateMonsters();
            }
        }

        // Attempts to move monster and attack player
        public bool MoveMonster(Monster monster, RogueSharp.ICell rogueCell)
        {
            if (!rogueMap.SetActorPosition(monster, rogueCell.X, rogueCell.Y))
            {
                if (player.X == rogueCell.X && player.Y == rogueCell.Y)
                {
                    Attack(monster, player);
                    return true;
                }
                return false;
            }
            return true;
        }

        // Method for attacking
        private static Color attackMessageColor;
        private static Color defenceMessageColor;
        public void Attack(Actor attacker, Actor defender)
        {
            StringBuilder attackMessage = new StringBuilder();
            StringBuilder defenceMessage = new StringBuilder();

            attackMessageColor = Color.white;
            defenceMessageColor = Color.white;

            int hits = ResolveAttack(attacker, defender, attackMessage);
            int blocks = ResolveDefence(defender, hits, attackMessage, defenceMessage);

            messageSystem.Add(attackMessage.ToString(), attackMessageColor);
            if (!string.IsNullOrEmpty(defenceMessage.ToString()))
                messageSystem.Add(defenceMessage.ToString(), defenceMessageColor);

            int damage = hits - blocks;
            ResolveDamage(defender, damage, blocks);
        }

        // Returns number of hits by resolving attack
        private static int ResolveAttack(Actor attacker, Actor defender, StringBuilder attackMessage)
        {
            int hits = 0;
            // attackMessage.AppendFormat("{0} attacks {1} and rolls: ", attacker.Name, defender.Name);
            attackMessage.AppendFormat("{0} hits {1}", attacker.Name, defender.Name);

            DiceExpression attackDice = new DiceExpression().Dice(attacker.Attack, 100);
            DiceResult attackResult = attackDice.Roll();

            foreach (TermResult termResult in attackResult.Results)
            {
                // attackMessage.Append(termResult.Value + ", ");
                if (termResult.Value >= 100 - attacker.HitChance)
                    hits++;
            }
            return hits;
        }

        // Returns number of blocks by resolving defence
        private static int ResolveDefence(Actor defender, int hits, StringBuilder attackMessage, StringBuilder defenceMessage)
        {
            int blocks = 0;

            if (hits > 0)
            {
                if (!(defender is Player))
                    Audio.GET.QueueAudio(Audio.GET.playerHit);

                attackMessageColor = defender is Player ? Color.red : Color.cyan;
                attackMessage.AppendFormat(" {0} times", hits);
                // defenceMessage.AppendFormat("{0} defends and rolls: ", defender.Name);
                defenceMessage.AppendFormat("{0}", defender.Name);

                DiceExpression defenceDice = new DiceExpression().Dice(defender.Defence, 100);
                DiceResult defenceRoll = defenceDice.Roll();

                foreach (TermResult termResult in defenceRoll.Results)
                {
                    // defenceMessage.Append(termResult.Value + ", ");
                    if (termResult.Value >= 100 - defender.DefenceChance)
                        blocks++;
                }
                if (blocks > 0)
                {
                    defenceMessageColor = defender is Player ? Color.cyan : Color.red;
                    defenceMessage.AppendFormat(" blocks {0} times", blocks);
                }   
                else
                {
                    defenceMessageColor = defender is Player ? Color.red : Color.cyan;
                    defenceMessage.AppendFormat(" fails to block", blocks);
                }

            }
            else
            {
                if (!(defender is Player))
                    Audio.GET.QueueAudio(Audio.GET.playerMiss);

                attackMessageColor = defender is Player ? Color.cyan : Color.red;
                attackMessage.Append(" and misses");
            }
                
            return blocks;
        }

        // Method for resolving damage
        private static void ResolveDamage(Actor defender, int damage, int blocks)
        {
            if (damage > 0)
            {
                defender.Health -= damage;

                if (defender is Player)
                    Audio.GET.QueueAudio(Audio.GET.playerTakeDamage);

                Color messageColor = defender is Player ? Color.red : Color.cyan;
                messageSystem.Add(string.Format("{0} was hit for {1} damage", defender.Name, damage), messageColor);

                if (defender.Health <= 0)
                    ResolveDeath(defender);
            }
            else if (blocks > 0)
            {
                Color messageColor = defender is Player ? Color.cyan : Color.red;
                messageSystem.Add(string.Format("{0} blocked all damage", defender.Name), messageColor);
            }   
        }

        // Method for resolving actor's death
        private static void ResolveDeath(Actor defender)
        {
            if (defender is Player)
            {
                game.PlayerDeath();
                return;
            }
            else if (defender is Monster)
            {
                rogueMap.RemoveMonster((Monster)defender);

                Monster monster = defender as Monster;
                monster.healthBar.DestroyHealthBar();

                player.Gold += defender.Gold;

                Audio.GET.QueueAudio(Audio.GET.monsterDeath);

                messageSystem.Add(string.Format("{0} died and dropped {1} gold", defender.Name, defender.Gold), Color.yellow);
            }
        }
    }
}