﻿using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Core.MapCore;
using Rogue2018.Actors;

public class MiniMap : MonoBehaviour
{
    // Members
    public SpriteRenderer miniMapSpriteRenderer;
    private Texture2D texture;
    private Sprite sprite;

    // Properties
    [HideInInspector]
    public int currentScaleFactor = 2;
    private int minimumScaleFactor = 1;
    private int maxiumumScaleFactor = 5;

    // Method for starting the minimap
    private void Start()
    {
        texture = new Texture2D(100, 100);

        texture.wrapMode = TextureWrapMode.Clamp;
        texture.filterMode = FilterMode.Point;

        miniMapSpriteRenderer.enabled = true;
    }

    // Method for drawing the minimap
    public void Draw()
    {
        if (!Game.GET.isInitialized)
            return;

        ClearMiniMap();

        SetMapCellPixels();

        DrawMiniMap();
    }

    // Method for clearing the minimap
    public void ClearMiniMap()
    {
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
                texture.SetPixel(x, y, new Color(0, 0, 0, 1));
        }
    }

    // Method for setting up pixels to draw to the minimap according to the map's roguecells
    private void SetMapCellPixels()
    {
        Color drawColor = Color.white;
        RogueMap rogueMap = Game.GET.rogueMap;
        foreach (RogueSharp.Cell rogueCell in rogueMap.GetAllCells())
        {
            if (rogueCell.IsExplored)
            {
                bool isInFOV = Game.GET.rogueMap.IsInFOV(rogueCell.X, rogueCell.Y);
                drawColor = isInFOV ?
                    rogueMap.MiniMapDrawColorFOV(rogueCell.X, rogueCell.Y) : rogueMap.MiniMapDrawColor(rogueCell.X, rogueCell.Y);

                foreach (Exit exit in rogueMap.exits)
                {
                    if (exit.X == rogueCell.X && exit.Y == rogueCell.Y)
                        drawColor = isInFOV ? Color.magenta : Color.magenta * 0.75f;
                }

                foreach (Door door in rogueMap.doors)
                {
                    if (door.X == rogueCell.X && door.Y == rogueCell.Y && !door.IsOpen)
                        drawColor = isInFOV ? Game.GET.swatch.DbWood : Game.GET.swatch.ComplimentDarker;
                }

                if (Game.GET.player.X == rogueCell.X && Game.GET.player.Y == rogueCell.Y)
                    drawColor = Game.GET.swatch.Player;

                Monster monster = rogueMap.GetMonsterAt(rogueCell.X, rogueCell.Y);
                if (monster != null)
                    drawColor = isInFOV ? monster.DrawColorFOV : Game.GET.swatch.MiniMapFloor;

                /*

                if (rogueCell.IsWalkable)
                {
                    drawColor = isInFOV ? Game.GET.swatch.MiniMapFloorFOV : Game.GET.swatch.MiniMapFloor;
                    foreach (Exit exit in rogueMap.exits)
                    {
                        if(exit.X == rogueCell.X && exit.Y == rogueCell.Y)
                            drawColor = isInFOV ? Color.magenta : Game.GET.swatch.DbOldBlood;
                    }
                }
                else
                {
                    drawColor = isInFOV ? Game.GET.swatch.MiniMapWallFOV : Game.GET.swatch.MiniMapWall;
                    if (Game.GET.player.X == rogueCell.X && Game.GET.player.Y == rogueCell.Y)
                        drawColor = Game.GET.swatch.Player;
                    Monster monster = rogueMap.GetMonsterAt(rogueCell.X, rogueCell.Y);
                    if (monster != null)
                        drawColor = isInFOV ? monster.DrawColorFOV : Game.GET.swatch.MiniMapFloor;
                }

                */

                int miniMapDrawXOffset = GetMiniMapDrawXOffset(Game.GET.player, texture.width);
                int miniMapDrawYOffset = GetMiniMapDrawYOffset(Game.GET.player, texture.height);

                int xPosOrigin = rogueCell.X * currentScaleFactor;
                int yPosOrigin = rogueCell.Y * currentScaleFactor;
                Vector2 blockOrigin = new Vector2(xPosOrigin + miniMapDrawXOffset, yPosOrigin + miniMapDrawYOffset);

                int xPosFinal = rogueCell.X * currentScaleFactor + (currentScaleFactor - 1);
                int yPosFinal = rogueCell.Y * currentScaleFactor + (currentScaleFactor - 1);
                Vector2 blockFinal = new Vector2(xPosFinal + miniMapDrawXOffset, yPosFinal + miniMapDrawYOffset);

                Color[] drawColors = new Color[currentScaleFactor * currentScaleFactor];
                for (int i = 0; i < drawColors.Length; i++)
                    drawColors[i] = drawColor;

                if (CheckDrawable(blockOrigin) && CheckDrawable(blockFinal))
                    texture.SetPixels(
                        (int)blockOrigin.x,
                        (int)blockOrigin.y,
                        currentScaleFactor,
                        currentScaleFactor,
                        drawColors);
            }
        }  
    }

    // Returns true if the pixelPosition is drawable
    private bool CheckDrawable(Vector2 pixelPosition)
    {
        if (pixelPosition.x >= 0 &&
            pixelPosition.x < texture.width &&
            pixelPosition.y >= 0 &&
            pixelPosition.y < texture.height)
            return true;
        else return false;
    }

    // Returns an xOffset for drawing the minimap centered on the player
    private int GetMiniMapDrawXOffset(Player player, int miniMapWidth)
    {
        int scaledPlayerX = player.X * currentScaleFactor;
        if (scaledPlayerX > (miniMapWidth / 2))
            return -1 * (scaledPlayerX - (miniMapWidth / 2));
        else if (scaledPlayerX < (miniMapWidth / 2))
            return ((miniMapWidth / 2) - scaledPlayerX);
        return 0;
    }

    // Returns a yOffset for drawing the minimap centered on the player
    private int GetMiniMapDrawYOffset(Player player, int miniMapHeight)
    {
        int scaledPlayerY = player.Y * currentScaleFactor;
        if (scaledPlayerY > (miniMapHeight / 2))
            return -1 * (scaledPlayerY - (miniMapHeight / 2));
        else if (scaledPlayerY < (miniMapHeight / 2))
            return ((miniMapHeight / 2) - scaledPlayerY);
        return 0;
    }

    // Method for applying the constructed texture to the minimap sprite
    private void DrawMiniMap()
    {
        texture.Apply();
        sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0), 100);
        miniMapSpriteRenderer.sprite = sprite;
    }

    // Method for setting the scale of the minimap
    public void SetScale(int scaleFactor)
    {
        if (currentScaleFactor != scaleFactor)
            currentScaleFactor = scaleFactor;

        currentScaleFactor = Mathf.Clamp(currentScaleFactor, minimumScaleFactor, maxiumumScaleFactor);

        Draw();
    }
}