﻿using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Interfaces;

namespace Rogue2018.UserInterface
{
    public class StatsUI : IUserInterface
    {
        // IUserInterface properties
        public string handle { get; set; }
        public CellPropertiesList cellPropertiesList { get; set; }
        public void Draw()
        {
            // Draw Console
            game.tools.DrawCellsFromPropertiesList(cellPropertiesList);
        }

        // Constructor
        private Game        game;
        private UIManager   uiManager;
        public StatsUI()
        {
            game        = Game.GET;
            uiManager   = UIManager.GET;
            handle = "StatsConsole";
            
            foreach (Transform child in uiManager.transform)
            {
                if (child.gameObject.name.Contains(handle))
                    cellPropertiesList = child.GetComponent<CellPropertiesList>();
            }
        }
    }
}