﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RogueSharp;
using Rogue2018.Actors;
using Rogue2018.Systems;

namespace Rogue2018.Core.MapCore
{
    public class RogueMap : Map
    {
        // Constructor
        private static Game game;
        private static SchedulingSystem schedulingSystem;

        private List<RogueSharp.Cell> unexploredWalkableCells;

        public List<Monster> _monsters;
        public List<Rectangle> rooms;

        public List<Door> doors { get; set; }
        public List<Exit> exits { get; set; }

        public GoalMap playerGoalMap;
        public GoalMap monsterGoalMap;

        public RogueMap()
        {
            game = Game.GET;
            schedulingSystem = game.schedulingSystem;

            _monsters = new List<Monster>();
            rooms = new List<Rectangle>();

            doors = new List<Door>();
            exits = new List<Exit>();

            schedulingSystem.Clear();
        }

        // Method for updating map's contents
        public void UpdateMap(IMap source)
        {
            foreach (ICell sourceCell in source.GetAllCells())
                SetCellProperties(
                    sourceCell.X,
                    sourceCell.Y,
                    sourceCell.IsTransparent,
                    sourceCell.IsWalkable,
                    sourceCell.IsExplored);
        }

        public RogueSharp.Cell playerGoalCell;
        
        public void FindPlayerGoal()
        {
            playerGoalMap.ClearGoals();

            Point playerPoint = new Point(game.player.X, game.player.Y);
            int bestDistance = int.MaxValue;

            int distanceCheckCount = 20 > unexploredWalkableCells.Count ? unexploredWalkableCells.Count : 20;
            for (int i = 0; i < distanceCheckCount; i++)
            {
                RogueSharp.Cell cell = unexploredWalkableCells[i];
                Point cellPoint = new Point(cell.X, cell.Y);
                int cellDistance = (int)Point.Distance(cellPoint, playerPoint);
                if (cellDistance < bestDistance)
                {
                    bestDistance = cellDistance;
                    playerGoalCell = cell;
                }
            }

            /*

            // foreach (RogueSharp.Cell cell in GetAllUnexploredWalkableCells())
            foreach (RogueSharp.Cell cell in unexploredWalkableCells)
            {
                Point cellPoint = new Point(cell.X, cell.Y);
                int cellDistance = (int)Point.Distance(cellPoint, playerPoint);
                if (cellDistance < bestDistance)
                {
                    bestDistance = cellDistance;
                    playerGoalCell = cell;
                }
            }

            */

            if (playerGoalCell != null)
            {
                playerGoalMap.AddGoal(playerGoalCell.X, playerGoalCell.Y, 1);
                playerGoalMap.ComputeCellWeights();
            }
        }

        private int playerGoalExitIndex = 0;
        public void FindPlayerGoalExit()
        {
            playerGoalMap.ClearGoals();
            if (playerGoalExitIndex == 0)
                playerGoalExitIndex = exits.Count - 1;
            else if (playerGoalExitIndex == exits.Count - 1)
                playerGoalExitIndex--;
            else
                playerGoalExitIndex--;

            playerGoalCell = GetCell(exits[playerGoalExitIndex].X, exits[playerGoalExitIndex].Y) as RogueSharp.Cell;

            if (playerGoalCell != null)
            {
                playerGoalMap.AddGoal(playerGoalCell.X, playerGoalCell.Y, 1);
                playerGoalMap.ComputeCellWeights();
            }
        }

        public bool IsMapExplored()
        {
            foreach (RogueSharp.Cell cell in GetAllCells())
            {
                if (cell.IsWalkable && !cell.IsExplored)
                    return false;
            }
            return true;
        }

        public bool AreMonstersInFOV()
        {
            foreach (Monster monster in _monsters)
            {
                if (IsInFOV(monster.X, monster.Y))
                    return true;
            }
            return false;
        }

        public bool AreMonstersAlerted()
        {
            foreach (Monster monster in _monsters)
            {
                if (monster.TurnsAlerted.HasValue)
                    return true;
            }
            return false;
        }

        public void UpdateMonsterGoalMap()
        {
            monsterGoalMap.ClearGoals();
            monsterGoalMap.AddGoal(game.player.X, game.player.Y, 1);

            monsterGoalMap.ClearObstacles();
            foreach (Monster monster in _monsters)
                monsterGoalMap.AddObstacle(monster.X, monster.Y);

            monsterGoalMap.ComputeCellWeightsLimited(GetCell(game.player.X, game.player.Y), 20);
        }

        // Method for drawing
        public void Draw(int xOffset, int yOffset)
        {
            // Draw map PhiCells
            // int drawDistance = (int)Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].width;
            // IEnumerable<ICell> cellsToDraw = GetCellsInSquare(game.player.X, game.player.Y, drawDistance);

            int drawWidth = (int)Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].width;
            int drawHeight = (int)Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].height;
            IEnumerable<ICell> cellsToDraw = GetCellsInRect(game.player.X, game.player.Y, drawWidth, drawHeight, true);

            foreach (ICell cell in cellsToDraw)
                DrawMapCell(cell as RogueSharp.Cell, xOffset, yOffset);

            // Draw door PhiCells
            foreach (Door door in doors)
                door.Draw(xOffset, yOffset, this);

            // Draw stairs PhiCells
            foreach (Exit exit in exits)
                exit.Draw(xOffset, yOffset, this);

            // Draw monster PhiCells
            foreach (Monster monster in _monsters)
                monster.Draw(xOffset, yOffset, this);
        }

        public void DrawHealthBars(int xOffset, int yOffset)
        {
            foreach (Monster monster in _monsters)
            {
                if (IsInFOV(monster.X, monster.Y))
                {
                    monster.DrawHealthBar(xOffset, yOffset);
                }
                else if (monster.healthBar != null)
                {
                    if (monster.healthBar.IsEnabled)
                        monster.healthBar.IsEnabled = false;
                }
            }
        }

        public void DestroyHealthBars()
        {
            foreach (Monster monster in _monsters)
            {
                if (monster.healthBar != null)
                {
                    monster.healthBar.DestroyHealthBar();
                    monster.healthBar = null;
                }
            }
        }

        public void HideHealthBars()
        {
            foreach (Monster monster in _monsters)
            {
                if (monster.healthBar != null)
                {
                    if (monster.healthBar.IsEnabled)
                        monster.healthBar.IsEnabled = false;
                }
            }
        }

        // Method for drawing dungeon map stats
        public void DrawStats(int xOffset, int yOffset)
        {
            // Draw monster stats
            int m = 0;
            foreach (Monster monster in _monsters)
            {
                if (IsInFOV(monster.X, monster.Y))
                {
                    // monster.DrawStats(xOffset, yOffset, m);
                    m++;
                }
            }
        }

        // Method for drawing a map PhiCell
        private void DrawMapCell(RogueSharp.Cell rogueCell, int xOffset, int yOffset)
        {
            // Check if drawable
            // if (!game.tools.CheckDrawable(rogueCell, xOffset, yOffset))
            //    return;

            // Check if unexplored
            if (!rogueCell.IsExplored)
                return;

            if (IsInFOV(rogueCell.X, rogueCell.Y))
            {
                Cell phiCell = Display.CellAt(0, rogueCell.X + xOffset, rogueCell.Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString(rogueCell.X, rogueCell.Y) : Symbol(rogueCell.X, rogueCell.Y);
                    Color drawColor = DrawColorFOV(rogueCell.X, rogueCell.Y);
                    Color backgroundDrawColor = BackgroundDrawColorFOV(rogueCell.X, rogueCell.Y);
                    if (!rogueCell.IsWalkable && !Options.GET.DrawTiles)
                    {
                        string symbol = Symbol(rogueCell.X, rogueCell.Y);
                        if (symbol == "#" || symbol == "≈")
                            backgroundDrawColor = drawColor * 0.5f;
                    }
                    phiCell.SetContent(phiCellContent, backgroundDrawColor, drawColor);
                }
            }
            else
            {
                Cell phiCell = Display.CellAt(0, rogueCell.X + xOffset, rogueCell.Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString(rogueCell.X, rogueCell.Y) : Symbol(rogueCell.X, rogueCell.Y);
                    Color drawColor = DrawColor(rogueCell.X, rogueCell.Y);
                    Color backgroundDrawColor = BackgroundDrawColor(rogueCell.X, rogueCell.Y);
                    if (!rogueCell.IsWalkable && !Options.GET.DrawTiles)
                    {
                        string symbol = Symbol(rogueCell.X, rogueCell.Y);
                        if (symbol == "#" || symbol == "≈")
                            backgroundDrawColor = drawColor * 0.5f;
                    }
                    phiCell.SetContent(phiCellContent, backgroundDrawColor, drawColor);
                }
            }
        }

        // Method for updating player FOV
        public void UpdatePlayerFOV()
        {
            if (unexploredWalkableCells == null)
            {
                unexploredWalkableCells = new List<RogueSharp.Cell>();
                foreach (RogueSharp.Cell cell in GetAllUnexploredWalkableCells())
                    unexploredWalkableCells.Add(cell);
            }

            Player player = game.player;

            // Compute FOV using position and awareness
            ComputeFOV(player.X, player.Y, player.Awareness, true);

            // Mark cells in FOV as explored
            IEnumerable<ICell> awarenessCells = GetCellsInDiamond(player.X, player.Y, player.Awareness);
            foreach (ICell cell in awarenessCells)
            {
                if (IsInFOV(cell.X, cell.Y))
                {
                    SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
                    if (cell.IsWalkable)
                        unexploredWalkableCells.Remove(unexploredWalkableCells.FirstOrDefault(c => c.X == cell.X && c.Y == cell.Y));
                }
            }
        }

        // Returns true if able to place actor on the rogueCell
        public bool SetActorPosition(Actor actor, int x, int y)
        {
            // Check for movement to center ie. wait a turn
            if (actor is Player && x == actor.X && y == actor.Y)
            {
                UpdateMonsterGoalMap();
                return true;
            }
                
            // Only allow placement if walkable
            if (GetCell(x, y).IsWalkable)
            {
                // Actor's previous cell is now walkable
                SetIsWalkable(actor.X, actor.Y, true);

                // Update the actor's position
                actor.X = x;
                actor.Y = y;

                if (actor is Player)
                    UpdateMonsterGoalMap();

                // Actor's new cell is now not walkable
                SetIsWalkable(actor.X, actor.Y, false);

                // Check for doors to open
                OpenDoor(actor, x, y);

                // UpdatePlayerFOV
                if (actor is Player)
                {
                    Audio.GET.QueueAudio(Audio.GET.playerMove);

                    UpdatePlayerFOV();
                }
                    
                return true;
            }
            return false;
        }

        // Method for setting rogueCell.IsWalkable
        public void SetIsWalkable(int x, int y, bool isWalkable)
        {
            RogueSharp.Cell rogueCell = GetCell(x, y) as RogueSharp.Cell;
            SetCellProperties(rogueCell.X, rogueCell.Y, rogueCell.IsTransparent, isWalkable, rogueCell.IsExplored);
        }

        // Method for getting door
        public Door GetDoor(int x, int y)
        {
            return doors.SingleOrDefault(d => d.X == x && d.Y == y);
        }

        // Method for opening door
        public void OpenDoor(Actor actor, int x, int y)
        {
            Door door = GetDoor(x, y);
            if (door != null && !door.IsOpen)
            {
                door.IsOpen = true;
                var rogueCell = GetCell(x, y);
                
                // Mark door rogueCell as transparent
                SetCellProperties(x, y, true, rogueCell.IsWalkable, rogueCell.IsExplored);

                Audio.GET.QueueAudio(Audio.GET.playerOpenDoor);

                // Send message for door opening
                //string doorOpenMessage = string.Format("{0} opened a door.", actor.Name);
                //game.messageSystem.Add(doorOpenMessage, Color.white);
            }
        }

        // Method for adding player after map generation
        public void AddPlayer(Player player)
        {
            game.player = player;
            SetIsWalkable(player.X, player.Y, false);
            UpdatePlayerFOV();
            schedulingSystem.Add(player);
        }

        // Method for adding a monster
        public void AddMonster(Monster monster)
        {
            _monsters.Add(monster);
            // Monster's rogueCell set to not walkable
            SetIsWalkable(monster.X, monster.Y, false);
            schedulingSystem.Add(monster);
        }

        // Method for removing a monster
        public void RemoveMonster(Monster monster)
        {
            _monsters.Remove(monster);
            // Monster's rogueCell set to walkable
            SetIsWalkable(monster.X, monster.Y, true);
            schedulingSystem.Remove(monster);
        }

        // Method for adding all monsters to the scheduling system
        public void AddMonstersToSchedule()
        {
            foreach (Monster monster in _monsters)
                schedulingSystem.Add(monster);
        }

        // Method for removing all monsters from the scheduling system
        public void RemoveMonstersFromSchedule()
        {
            foreach(Monster monster in _monsters)
                schedulingSystem.Remove(monster);
        }

        // Method for getting a monster
        public Monster GetMonsterAt(int x, int y)
        {
            return _monsters.FirstOrDefault(m => m.X == x && m.Y == y);
        }

        // Get random walkable location in map
        public Point GetRandomWalkableLocationInMap()
        {
            for (int i = 0; i < 100; i++)
            {
                int x = game.random.Next(1, Width - 2);
                int y = game.random.Next(1, Height - 2);
                if (IsWalkable(x, y))
                    return new Point(x, y);
            }
            return null;
        }

        // Get random walkable location in a room
        public Point GetRandomWalkableLocationInRoom(Rectangle room)
        {
            if (DoesRoomHaveWalkableSpace(room))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = game.random.Next(1, room.Width - 2) + room.X;
                    int y = game.random.Next(1, room.Height - 2) + room.Y;
                    if (IsWalkable(x, y))
                        return new Point(x, y);
                }
            }
            return null;
        }

        public Point GetRandomWalkableLocationInMapSection(MapSection mapSection)
        {
            if (DoesMapSectionHaveWalkableSpace(mapSection))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = game.random.Next(mapSection.Bounds.Left, mapSection.Bounds.Right - 1);
                    int y = game.random.Next(mapSection.Bounds.Top, mapSection.Bounds.Bottom - 1);

                    if (IsWalkable(x, y))
                        return new Point(x, y);
                }
            }
            return null;
        }

        // Method for getting exit
        public Exit GetExit(int x, int y)
        {
            return exits.SingleOrDefault(e => e.X == x && e.Y == y);
        }

        // Check if player is standing on an exit
        public Exit OnExit()
        {
            Player player = game.player;
            foreach (Exit exit in exits)
            {
                if (exit.X == player.X && exit.Y == player.Y)
                    return exit;
            }
            return null;
        }

        // Check if player is standing on stairsDown
        public bool CanMoveDownStairs()
        {
            if (OnExit() == null)
                return false;

            if (OnExit().IsUp)
                return false;
            else
                return true;
        }

        // Check if player is standing on stairsUp
        public bool CanMoveUpStairs()
        {
            if (OnExit() == null)
                return false;

            if (OnExit().IsUp)
                return true;
            else
                return false;
        }

        // Returns true if a room has walkable space
        public bool DoesRoomHaveWalkableSpace(Rectangle room)
        {
            for (int x = 1; x <= room.Width - 2; x++)
            {
                for (int y = 1; y <= room.Height - 2; y++)
                {
                    if (IsWalkable(x + room.X, y + room.Y))
                        return true;
                }
            }
            return false;
        }

        // Returns true if a map section has walkable space
        public bool DoesMapSectionHaveWalkableSpace(MapSection mapSection)
        {
            for (int x = mapSection.Bounds.Left; x < mapSection.Bounds.Right - 1; x++)
            {
                for (int y = mapSection.Bounds.Top; y < mapSection.Bounds.Bottom - 1; y++)
                {
                    if (IsWalkable(x, y))
                        return true;
                }
            }
            return false;
        }

        // Returns true if a cell is a border cell (takes into account padding)
        public bool IsBorderCell(ICell cell, int padding)
        {
            return cell.X <= (int)(padding / 2) || cell.X >= Width - 1 - (int)(padding / 2)
                   || cell.Y <= (int)(padding / 2) || cell.Y >= Height - 1 - (int)(padding / 2);
        }

        // Returns a count of neighbouring unwalkable cells
        public int CountWallsNear(ICell cell, int distance)
        {
            int count = 0;
            foreach (ICell nearbyCell in GetCellsInSquare(cell.X, cell.Y, distance))
            {
                if (nearbyCell.X == cell.X && nearbyCell.Y == cell.Y)
                    continue;
                if (!nearbyCell.IsWalkable)
                    count++;
            }
            return count;
        }

        // Recursively returns a random suitable map section to place an exit
        public MapSection GetRandomSuitableMapSection(List<MapSection> mapSections)
        {
            int randomIndex = game.random.Next(0, mapSections.Count - 1);

            if (mapSections[randomIndex].HasExit)
                return GetRandomSuitableMapSection(mapSections);

            foreach (ICell cell in mapSections[randomIndex].Cells)
            {
                if (cell.IsWalkable && CountWallsNear(cell, 1) == 0)
                    return mapSections[randomIndex];
            }
            return GetRandomSuitableMapSection(mapSections);
        }
    }
}