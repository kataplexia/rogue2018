﻿using UnityEngine;
using RogueSharp;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.MapCore
{
    public class Door : IDrawable
    {
        // Constructor
        private static Game game;
        public Door()
        {
            game = Game.GET;
            Symbol = game.symbols.DoorClosed;
            DrawColor = game.swatch.Door;
            BackgroundDrawColor = game.swatch.DoorBackground;
        }

        // Properties
        public bool IsOpen                  { get; set; }

        // IDrawable properties
        public Color DrawColor              { get; set; }
        public Color BackgroundDrawColor    { get; set; }
        public Color DrawColorFOV           { get; set; }
        public Color BackgroundDrawColorFOV { get; set; }
        public string Symbol                { get; set; }
        public string GlyphString           { get; set; }
        public int X                        { get; set; }
        public int Y                        { get; set; }

        // Method for drawing
        public void Draw(int xOffset, int yOffset, IMap map)
        {
            // Check if drawable
            if (!game.tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
                return;

            // Check if unexplored
            if (!map.GetCell(X, Y).IsExplored)
                return;
            
            // Set symbol and glyph string
            Symbol = IsOpen ? game.symbols.DoorOpen : game.symbols.DoorClosed;
            GlyphString = IsOpen ? game.glyphStrings.DoorOpen : game.glyphStrings.DoorClosed;

            // Check FOV
            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = game.swatch.FloorBackgroundFOV * 0.8f;
                DrawColorFOV = game.swatch.AlternateDarker;
                Cell phiCell = Display.CellAt(game._mapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }
            }
            else
            {
                BackgroundDrawColor = game.swatch.FloorBackground * 0.8f;
                DrawColor = game.swatch.AlternateDarkest;
                Cell phiCell = Display.CellAt(game._mapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }
        }
    }
}