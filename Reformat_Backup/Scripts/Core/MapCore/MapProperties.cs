﻿using RogueSharp;

namespace Rogue2018.Core.MapCore
{
    public class MapProperties : IMapProperties
    {
        //IMapGenerationProperties properties
        private GenerationStyles _generationStyle;
        private Biomes _biome;

        public GenerationStyles GenerationStyle     { get { return _generationStyle; } set { _generationStyle = value; } }
        public Biomes Biome                         { get { return _biome; } set { _biome = value; } }

        // Shared properties
        public string   MapName                     { get; set; }
        public string[] ExitStrings                 { get; set; }
        public int      MapWidth                    { get; set; }
        public int      MapHeight                   { get; set; }

        // Random room generation style properties
        public int      MaxRoomCount                { get; set; }
        public int      MaxRoomSize                 { get; set; }
        public int      MinRoomSize                 { get; set; }

        // Cellular generation style properties
        public int      FillProbability             { get; set; }
        public int      IterationCount              { get; set; }
        public int      BigAreaFillCutoff           { get; set; }
        public bool     TransparentWalls            { get; set; }

        // From string style properties
        public string       MapRepresentationPath   { get; set; }
        public string[,]    ExitStringsAdvanced     { get; set; }
        public Point        StartingPoint           { get; set; }

        // Stored Map properties
        public bool     MapGenerated                { get; set; }
        public RogueMap StoredMap                   { get; set; }
        public Point    StoredPlayerPosition        { get; set; }

        // Constructor for border only generation style
        public void Construct_BorderOnlyStlyeProperties(
            string mapName,
            string[] exitStrings,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight)
        {
            MapName = mapName;
            ExitStrings = exitStrings;
            GenerationStyle = generationStyle;
            Biome = biome;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
        }

        // Constructor for random rooms generation style
        public void Construct_RandomRoomsStyleProperties(
            string mapName,
            string[] exitStrings,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight,
            int maxRoomCount,
            int maxRoomSize,
            int minRoomSize)
        {
            MapName         = mapName;
            ExitStrings     = exitStrings;
            GenerationStyle = generationStyle;
            Biome           = biome;
            MapWidth        = mapWidth;
            MapHeight       = mapHeight;
            MaxRoomCount    = maxRoomCount;
            MaxRoomSize     = maxRoomSize;
            MinRoomSize     = minRoomSize;
        }

        // Constructor for cellular generation style
        public void Construct_CellularStyleProperties(
            string mapName,
            string[] exitStrings,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight,
            int fillProbability,
            int iterationCount,
            int bigAreaFillCutoff,
            bool transparentWalls)
        {
            MapName             = mapName;
            ExitStrings         = exitStrings;
            GenerationStyle     = generationStyle;
            Biome               = biome;
            MapWidth            = mapWidth;
            MapHeight           = mapHeight;
            FillProbability     = fillProbability;
            IterationCount      = iterationCount;
            BigAreaFillCutoff   = bigAreaFillCutoff;
            TransparentWalls    = transparentWalls;
        }

        public void Construct_FromStringStyleProperties(
            string mapName,
            string[,] exitStringsAdvanced,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight,
            string mapRepresentationPath,
            Point startingPoint)
        {
            MapName = mapName;
            ExitStringsAdvanced = exitStringsAdvanced;
            GenerationStyle = generationStyle;
            Biome = biome;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            MapRepresentationPath = mapRepresentationPath;
            StartingPoint = startingPoint;
        }
    }
}