﻿using System.Collections.Generic;
using RogueSharp;

namespace Rogue2018.Core.MapCore
{
    public class MapSection
    {
        private int     _top;
        private int     _bottom;
        private int     _right;
        private int     _left;
        private bool    _hasExit;
        // private Point   _exitPoint;
        private int _exitPointX;
        private int _exitPointY;

        public bool HasExit     { get { return _hasExit; } set { _hasExit = value; } }
        // public Point ExitPoint  { get { return _exitPoint; } set { _exitPoint = value; } }
        public int ExitPointX   { get { return _exitPointX; } set { _exitPointX = value; } }
        public int ExitPointY   { get { return _exitPointY; } set { _exitPointY = value; } }

        public Rectangle Bounds
        {
            get { return new Rectangle(_left, _top, _right - _left + 1, _bottom - _top + 1); }
        }

        public HashSet<ICell> Cells { get; private set; }

        public MapSection()
        {
            Cells = new HashSet<ICell>();
            _top = int.MaxValue;
            _left = int.MaxValue;
        }

        public void AddCell(ICell cell)
        {
            Cells.Add(cell);
            UpdateBounds(cell);
        }

        private void UpdateBounds(ICell cell)
        {
            if (cell.X > _right)
                _right = cell.X;
            if (cell.X < _left)
                _left = cell.X;
            if (cell.Y > _bottom)
                _bottom = cell.Y;
            if (cell.Y < _top)
                _top = cell.Y;
        }

        public override string ToString()
        {
            return string.Format("Bounds: {0}", Bounds);
        }
    }
}