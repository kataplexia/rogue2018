﻿using System.Collections.Generic;
using RogueSharp;

namespace Rogue2018.Core.MapCore
{
    public class DivisionAnalyzer
    {
        private readonly IMap _map;
        private readonly List<MapSection> _mapSections;
        private readonly int _padding;
        private readonly int _divisionsX;
        private readonly int _divisionsY;
        private readonly int _divisionSizeX;
        private readonly int _divisionSizeY;

        public DivisionAnalyzer(IMap map, int padding, int divisionsX, int divisionsY)
        {
            _map = map;
            _mapSections = new List<MapSection>();
            _padding = padding;
            _divisionsX = divisionsX;
            _divisionsY = divisionsY;
            _divisionSizeX = (_map.Width - _padding) / _divisionsX;
            _divisionSizeY = (_map.Height - _padding) / _divisionsY;
        }

        public List<MapSection> GetMapSections()
        {
            for (int x = 0; x < _divisionsX; x++)
            {
                for (int y = 0; y < _divisionsY; y++)
                {
                    MapSection mapSection = new MapSection();
                    for (int row = 0; row < _divisionSizeY; row++)
                    {
                        int xOrigin         = (x * _divisionSizeX) + (_padding / 2);
                        int yOrigin         = (y * _divisionSizeY) + (_padding / 2) + row;
                        int xDestination    = (x * _divisionSizeX) + (_padding / 2) + (_divisionSizeX - 1);
                        int yDestination    = yOrigin;

                        IEnumerable<ICell> cells = _map.GetCellsAlongLine(xOrigin, yOrigin, xDestination, yDestination);
                        foreach (ICell cell in cells)
                            mapSection.AddCell(cell);
                    }
                    _mapSections.Add(mapSection);
                }
            }
            return _mapSections;
        }
    }
}