﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{
    public bool DrawTiles = true;

    // Awake with Game as singleton and do not destroy
    public static Options GET;
    private void Awake()
    {
        if (GET != null)
            GameObject.Destroy(GET);
        else
            GET = this;
    }
}
