﻿using UnityEngine;
using Rogue2018.Core.MapCore;

namespace Rogue2018.Core
{
    public class Swatch : MonoBehaviour
    {
        // http://paletton.com/#uid=73d0u0k5qgb2NnT41jT74c8bJ8X

        [Header("PRIMARY")]
        public Color PrimaryLightest            = new Color(141, 164, 160);
        public Color PrimaryLighter             = new Color(110, 121, 119);
        public Color PrimaryLight               = new Color(88, 100, 98);
        public Color Primary                    = new Color(68, 82, 79);
        public Color PrimaryDark                = new Color(48, 61, 59);
        public Color PrimaryDarker              = new Color(29, 45, 42);
        public Color PrimaryDarkest             = new Color(16, 27, 25);

        [Header("SECONDARY")]
        public Color SecondaryLightest          = new Color(145, 148, 152);
        public Color SecondaryLighter           = new Color(116, 120, 126);
        public Color SecondaryLight             = new Color(93, 97, 105);
        public Color Secondary                  = new Color(72, 77, 85);
        public Color SecondaryDark              = new Color(51, 56, 64);
        public Color SecondaryDarker            = new Color(31, 38, 47);
        public Color SecondaryDarkest           = new Color(19, 23, 29);

        [Header("ALTERNATE")]
        public Color AlternateLightest          = new Color(255, 220, 206);
        public Color AlternateLighter           = new Color(200, 171, 161);
        public Color AlternateLight             = new Color(166, 134, 121);
        public Color Alternate                  = new Color(134, 106, 96);
        public Color AlternateDark              = new Color(115, 80, 67);
        public Color AlternateDarker            = new Color(82, 53, 38);
        public Color AlternateDarkest           = new Color(46, 29, 20);

        [Header("COMPLIMENT")]
        public Color ComplimentLightest         = new Color(229, 217, 210);
        public Color ComplimentLighter          = new Color(190, 180, 174);
        public Color ComplimentLight            = new Color(158, 147, 138);
        public Color Compliment                 = new Color(129, 116, 107);
        public Color ComplimentDark             = new Color(97, 84, 75);
        public Color ComplimentDarker           = new Color(71, 56, 45);
        public Color ComplimentDarkest          = new Color(34, 26, 21);

        [Header("VERDANT PRIMARY")]
        public Color VerdantPrimaryLightest;
        public Color VerdantPrimaryLighter;
        public Color VerdantPrimaryLight;
        public Color VerdantPrimary;
        public Color VerdantPrimaryDark;
        public Color VerdantPrimaryDarker;
        public Color VerdantPrimaryDarkest;

        [Header("VERDANT SECONDARY")]
        public Color VerdantSecondaryLightest;
        public Color VerdantSecondaryLighter;
        public Color VerdantSecondaryLight;
        public Color VerdantSecondary;
        public Color VerdantSecondaryDark;
        public Color VerdantSecondaryDarker;
        public Color VerdantSecondaryDarkest;
        
        // http://pixeljoint.com/forum/forum_posts.asp?TID=12795

        [Header("DB COLORS")]
        public Color DbDark                     = new Color(20, 12, 28);
        public Color DbDarker                   = new Color(20, 12, 28);
        public Color DbOldBlood                 = new Color(68, 36, 52);
        public Color DbDeepWater                = new Color(48, 52, 109);
        public Color DbOldStone                 = new Color(78, 74, 78);
        public Color DbWood                     = new Color(133, 76, 48);
        public Color DbVegetation               = new Color(52, 101, 36);
        public Color DbBlood                    = new Color(208, 70, 72);
        public Color DbStone                    = new Color(117, 113, 97);
        public Color DbWater                    = new Color(89, 125, 206);
        public Color DbBrightWood               = new Color(210, 125, 44);
        public Color DbMetal                    = new Color(133, 149, 161);
        public Color DbGrass                    = new Color(109, 170, 44);
        public Color DbSkin                     = new Color(210, 170, 153);
        public Color DbSky                      = new Color(109, 194, 202);
        public Color DbSun                      = new Color(218, 212, 94);
        public Color DbLight                    = new Color(222, 238, 21);

        [Header("REX COLOURS")]
        public Color RexTextHeading             = new Color(178, 134, 0);
        public Color RexTextOption              = new Color(140, 105, 0);
        public Color RexOptionBorder            = new Color(35, 35, 35);
        public Color RexOptionBackDark          = new Color(17, 17, 17);
        public Color RexOptionBackDarkest       = new Color(13, 13, 13);

        // Minimap variables
        public Color MiniMapFloor               { get; private set; }
        public Color MiniMapFloorFOV            { get; private set; }
        public Color MiniMapWall                { get; private set; }
        public Color MiniMapWallFOV             { get; private set; }

        // Environment variables
        public Color FloorBackground            { get; private set; }
        public Color Floor                      { get; private set; }
        public Color FloorBackgroundFOV         { get; private set; }
        public Color FloorFOV                   { get; private set; }
        public Color WallBackground             { get; private set; }
        public Color Wall                       { get; private set; }
        public Color WallBackgroundFOV          { get; private set; }
        public Color WallFOV                    { get; private set; }

        // Cave biome presets
        public Color MiniMapFloor_Cave          { get; private set; }
        public Color MiniMapFloorFOV_Cave       { get; private set; }
        public Color MiniMapWall_Cave           { get; private set; }
        public Color MiniMapWallFOV_Cave        { get; private set; }

        public Color FloorBackground_Cave       { get; private set; }
        public Color Floor_Cave                 { get; private set; }
        public Color FloorBackgroundFOV_Cave    { get; private set; }
        public Color FloorFOV_Cave              { get; private set; }
        public Color WallBackground_Cave        { get; private set; }
        public Color Wall_Cave                  { get; private set; }
        public Color WallBackgroundFOV_Cave     { get; private set; }
        public Color WallFOV_Cave               { get; private set; }

        // Dungeon biome presets
        public Color MiniMapFloor_Dungeon       { get; private set; }
        public Color MiniMapFloorFOV_Dungeon    { get; private set; }
        public Color MiniMapWall_Dungeon        { get; private set; }
        public Color MiniMapWallFOV_Dungeon     { get; private set; }

        public Color FloorBackground_Dungeon    { get; private set; }
        public Color Floor_Dungeon              { get; private set; }
        public Color FloorBackgroundFOV_Dungeon { get; private set; }
        public Color FloorFOV_Dungeon           { get; private set; }
        public Color WallBackground_Dungeon     { get; private set; }
        public Color Wall_Dungeon               { get; private set; }
        public Color WallBackgroundFOV_Dungeon  { get; private set; }
        public Color WallFOV_Dungeon            { get; private set; }

        // Forest biome presets
        public Color MiniMapFloor_Forest        { get; private set; }
        public Color MiniMapFloorFOV_Forest     { get; private set; }
        public Color MiniMapWall_Forest         { get; private set; }
        public Color MiniMapWallFOV_Forest      { get; private set; }

        public Color FloorBackground_Forest     { get; private set; }
        public Color Floor_Forest               { get; private set; }
        public Color FloorBackgroundFOV_Forest  { get; private set; }
        public Color FloorFOV_Forest            { get; private set; }
        public Color WallBackground_Forest      { get; private set; }
        public Color Wall_Forest                { get; private set; }
        public Color WallBackgroundFOV_Forest   { get; private set; }
        public Color WallFOV_Forest             { get; private set; }

        // Swamp biome presets
        public Color MiniMapFloor_Swamp         { get; private set; }
        public Color MiniMapFloorFOV_Swamp      { get; private set; }
        public Color MiniMapWall_Swamp          { get; private set; }
        public Color MiniMapWallFOV_Swamp       { get; private set; }

        public Color FloorBackground_Swamp      { get; private set; }
        public Color Floor_Swamp                { get; private set; }
        public Color FloorBackgroundFOV_Swamp   { get; private set; }
        public Color FloorFOV_Swamp             { get; private set; }
        public Color WallBackground_Swamp       { get; private set; }
        public Color Wall_Swamp                 { get; private set; }
        public Color WallBackgroundFOV_Swamp    { get; private set; }
        public Color WallFOV_Swamp              { get; private set; }

        // Other environment presets
        public Color DoorBackground             { get; private set; }
        public Color Door                       { get; private set; }
        public Color DoorBackgroundFOV          { get; private set; }
        public Color DoorFOV                    { get; private set; }
        public Color Stairs                     { get; private set; }
        public Color StairsFOV                  { get; private set; }

        // Item quality presets
        public Color NormalQuality              { get; private set; }
        public Color MagicQuality               { get; private set; }
        public Color RareQuality                { get; private set; }
        public Color UniqueQuality              { get; private set; }

        // UI presets
        public Color TextHeading                { get; private set; }
        public Color Text                       { get; private set; }
        public Color Gold                       { get; private set; }

        // Actor presets
        public Color Player                     { get; private set; }
        public Color Kobold                     { get; private set; }

        // Awake setup color presets
        private void Awake()
        {
            InitializeSwatch();
        }

        // Setup presets
        private void InitializeSwatch()
        {
            // Cave biome presets
            MiniMapFloor_Cave           = ComplimentDarkest;
            MiniMapFloorFOV_Cave        = ComplimentDarker;
            MiniMapWall_Cave            = AlternateDarkest;
            MiniMapWallFOV_Cave         = AlternateDarker;

            FloorBackground_Cave        = ComplimentDarkest * 0.25f;
            Floor_Cave                  = ComplimentDarkest * 0.5f;
            FloorBackgroundFOV_Cave     = ComplimentDarkest * 0.75f;
            FloorFOV_Cave               = ComplimentDarker;
            WallBackground_Cave         = FloorBackground;
            Wall_Cave                   = AlternateDarkest;
            WallBackgroundFOV_Cave      = FloorBackgroundFOV;
            WallFOV_Cave                = AlternateDarker;

            // Dungeon biome presets
            MiniMapFloor_Dungeon        = SecondaryDarkest * 0.5f;
            MiniMapFloorFOV_Dungeon     = SecondaryDarker * 0.5f;
            MiniMapWall_Dungeon         = SecondaryDark;
            MiniMapWallFOV_Dungeon      = SecondaryLightest;

            FloorBackground_Dungeon     = SecondaryDarkest * 0.25f;
            Floor_Dungeon               = SecondaryDarkest;
            FloorBackgroundFOV_Dungeon  = SecondaryDarkest;
            FloorFOV_Dungeon            = SecondaryDarker;
            WallBackground_Dungeon      = FloorBackground;
            Wall_Dungeon                = SecondaryDark;
            WallBackgroundFOV_Dungeon   = FloorBackgroundFOV;
            WallFOV_Dungeon             = SecondaryLightest;

            // Forest biome presets
            MiniMapFloor_Forest         = VerdantPrimaryDarkest;
            MiniMapFloorFOV_Forest      = VerdantPrimaryDarker;
            MiniMapWall_Forest          = VerdantPrimary;
            MiniMapWallFOV_Forest       = VerdantPrimaryLight;

            FloorBackground_Forest      = VerdantPrimaryDarkest * 0.25f;
            Floor_Forest                = VerdantPrimaryDarker * 0.25f;
            FloorBackgroundFOV_Forest   = VerdantPrimaryDarkest * 0.5f;
            FloorFOV_Forest             = VerdantPrimaryDarker * 0.5f;
            WallBackground_Forest       = VerdantPrimaryDarkest * 0.25f;
            Wall_Forest                 = VerdantPrimaryDarkest * 0.75f;
            WallBackgroundFOV_Forest    = VerdantPrimaryDarkest * 0.5f;
            WallFOV_Forest              = VerdantPrimaryDarker;

            // Swamp biome presets
            MiniMapFloor_Swamp          = VerdantSecondaryDarkest;
            MiniMapFloorFOV_Swamp       = VerdantSecondaryDarker;
            MiniMapWall_Swamp           = DbDeepWater;
            MiniMapWallFOV_Swamp        = DbWater;

            FloorBackground_Swamp       = VerdantSecondaryDarkest * 0.25f;
            Floor_Swamp                 = VerdantSecondaryDarkest;
            FloorBackgroundFOV_Swamp    = VerdantSecondaryDarkest;
            FloorFOV_Swamp              = VerdantSecondaryDarker;
            WallBackground_Swamp        = FloorBackground_Swamp;
            Wall_Swamp                  = DbDeepWater;
            WallBackgroundFOV_Swamp     = FloorBackgroundFOV_Swamp;
            WallFOV_Swamp               = DbWater;

            // Other environment presets
            DoorBackground              = ComplimentDarkest;
            Door                        = ComplimentLighter;
            DoorBackgroundFOV           = ComplimentDarker;
            DoorFOV                     = ComplimentLightest;
            Stairs                      = Secondary;
            StairsFOV                   = SecondaryLighter;

            // UI presets
            TextHeading                 = Color.white;
            Text                        = DbLight;
            Gold                        = DbSun;

            // Item quality presets
            NormalQuality               = SecondaryLightest;
            MagicQuality                = DbGrass;
            RareQuality                 = DbSky;
            UniqueQuality               = DbWood;

            // Actor presets
            Player                      = DbBlood;
            Kobold                      = DbBrightWood;
        }

        // Set variables to presets according to biome
        public void SetSwatchForBiome(Biomes biome)
        {
            if (biome == Biomes.Cave)
            {
                MiniMapFloor            = MiniMapFloor_Cave;
                MiniMapFloorFOV         = MiniMapFloorFOV_Cave;
                MiniMapWall             = MiniMapWall_Cave;
                MiniMapWallFOV          = MiniMapWallFOV_Cave;

                FloorBackground         = FloorBackground_Cave;
                Floor                   = Floor_Cave;
                FloorBackgroundFOV      = FloorBackgroundFOV_Cave;
                FloorFOV                = FloorFOV_Cave;
                WallBackground          = WallBackground_Cave;
                Wall                    = Wall_Cave;
                WallBackgroundFOV       = WallBackgroundFOV_Cave;
                WallFOV                 = WallFOV_Cave;
            }
            else if (biome == Biomes.Dungeon)
            {
                MiniMapFloor            = MiniMapFloor_Dungeon;
                MiniMapFloorFOV         = MiniMapFloorFOV_Dungeon;
                MiniMapWall             = MiniMapWall_Dungeon;
                MiniMapWallFOV          = MiniMapWallFOV_Dungeon;

                FloorBackground         = FloorBackground_Dungeon;
                Floor                   = Floor_Dungeon;
                FloorBackgroundFOV      = FloorBackgroundFOV_Dungeon;
                FloorFOV                = FloorFOV_Dungeon;
                WallBackground          = WallBackground_Dungeon;
                Wall                    = Wall_Dungeon;
                WallBackgroundFOV       = WallBackgroundFOV_Dungeon;
                WallFOV                 = WallFOV_Dungeon;
            }
            else if (biome == Biomes.Forest)
            {
                MiniMapFloor            = MiniMapFloor_Forest;
                MiniMapFloorFOV         = MiniMapFloorFOV_Forest;
                MiniMapWall             = MiniMapWall_Forest;
                MiniMapWallFOV          = MiniMapWallFOV_Forest;

                FloorBackground         = FloorBackground_Forest;
                Floor                   = Floor_Forest;
                FloorBackgroundFOV      = FloorBackgroundFOV_Forest;
                FloorFOV                = FloorFOV_Forest;
                WallBackground          = WallBackground_Forest;
                Wall                    = Wall_Forest;
                WallBackgroundFOV       = WallBackgroundFOV_Forest;
                WallFOV                 = WallFOV_Forest;
            }
            else if (biome == Biomes.Swamp)
            {
                MiniMapFloor            = MiniMapFloor_Swamp;
                MiniMapFloorFOV         = MiniMapFloorFOV_Swamp;
                MiniMapWall             = MiniMapWall_Swamp;
                MiniMapWallFOV          = MiniMapWallFOV_Swamp;

                FloorBackground         = FloorBackground_Swamp;
                Floor                   = Floor_Swamp;
                FloorBackgroundFOV      = FloorBackgroundFOV_Swamp;
                FloorFOV                = FloorFOV_Swamp;
                WallBackground          = WallBackground_Swamp;
                Wall                    = Wall_Swamp;
                WallBackgroundFOV       = WallBackgroundFOV_Swamp;
                WallFOV                 = WallFOV_Swamp;
            }
        }
    }
}