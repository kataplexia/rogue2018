﻿using UnityEngine;
using RogueSharp;
using Rogue2018.Interfaces;

namespace Rogue2018.Core
{
    public abstract class Actor : IActor, IDrawable, ISchedulable
    {
        // Constructor
        public static Game game;
        public Actor()
        {
            game = Game.GET;
        }

        // IActor properties
        private int     _attack;
        private int     _attackChance;
        private int     _awareness;
        private int     _defence;
        private int     _defenceChance;
        private int     _gold;
        private int     _health;
        private int     _maxHealth;
        private string  _name;
        private int     _speed;

        public int Attack           { get { return _attack; } set { _attack = value; } }
        public int HitChance        { get { return _attackChance; } set { _attackChance = value; } }
        public int Awareness        { get { return _awareness; } set { _awareness = value; } }
        public int Defence          { get { return _defence; } set { _defence = value; } }
        public int DefenceChance    { get { return _defenceChance; } set { _defenceChance = value; } }
        public int Gold             { get { return _gold; } set { _gold = value; } }
        public int Health           { get { return _health; } set { _health = value; } }
        public int MaxHealth        { get { return _maxHealth; } set { _maxHealth = value; } }
        public string Name          { get { return _name; } set { _name = value; } }
        public int Speed            { get { return _speed; } set { _speed = value; } }

        // IDrawable properties
        public Color DrawColor              { get; set; }
        public Color BackgroundDrawColor    { get; set; }
        public Color DrawColorFOV           { get; set; }
        public Color BackgroundDrawColorFOV { get; set; }
        public string Symbol                { get; set; }
        public string GlyphString           { get; set; }
        public int X                        { get; set; }
        public int Y                        { get; set; }

        // Method for drawing
        public void Draw(int xOffset, int yOffset, IMap map)
        {
            // Check if drawable
            if (!game.tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
                return;

            // Check if unexplored
            if (!map.GetCell(X, Y).IsExplored)
                return;

            // Check FOV
            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = game.rogueMap.BackgroundDrawColorFOV(X, Y);
                Cell phiCell = Display.CellAt(game._actorLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }  
            }
            else
            {
                BackgroundDrawColor = game.rogueMap.BackgroundDrawColor(X, Y);
                DrawColor = game.rogueMap.DrawColor(X, Y); ;
                Cell phiCell = Display.CellAt(game._actorLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? game.glyphStrings.Floor : game.rogueMap.Symbol(X, Y);
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }
        }

        // ISchedulable properties
        public int Time { get { return Speed; } }
    }
}
