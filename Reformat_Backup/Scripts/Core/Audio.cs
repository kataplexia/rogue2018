﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    private AudioSource audioSource;
    private List<AudioClip> clipSchedule;
    private int maxClips = 1;

    public AudioClip playerMove;
    public AudioClip playerStuck;
    public AudioClip playerOpenDoor;
    public AudioClip playerStairs;
    public AudioClip playerDeath;

    public AudioClip playerHit;
    public AudioClip playerMiss;
    public AudioClip playerTakeDamage;
    
    public AudioClip monsterDeath;

    // Awake with Audio as singleton and do not destroy
    public static Audio GET;
    private void Awake()
    {
        if (GET != null)
            GameObject.Destroy(GET);
        else
            GET = this;
    }

    private void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        clipSchedule = new List<AudioClip>();
    }

    public void QueueAudio(AudioClip clip)
    {
        if (clipSchedule.Count < maxClips)
            clipSchedule.Add(clip);
        else
        {
            clipSchedule.Clear();
            clipSchedule.Add(clip);
        }
    }

    private void FixedUpdate()
    {
        if (clipSchedule.Count == 0)
            return;

        if (!audioSource.isPlaying)
            PlayAudio(clipSchedule[0]);
    }

    private void PlayAudio(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
        clipSchedule.Remove(clip);
    }
}
