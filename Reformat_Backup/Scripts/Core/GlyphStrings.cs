﻿using UnityEngine;
using Rogue2018.Core.MapCore;

namespace Rogue2018.Core
{
    public class GlyphStrings
    {
        // Actor glyph strings
        public string Player        { get; private set; }
        public string Kobold        { get; private set; }

        // Environment glyph strings
        public string DoorClosed    { get; private set; }
        public string DoorOpen      { get; private set; }
        public string StairsUp      { get; private set; }
        public string StairsDown    { get; private set; }
        public string Tree          { get; private set; }
        public string Water         { get; private set; }

        public string Wall_Dungeon  { get; private set; }
        public string Floor_Dungeon { get; private set; }
        public string Wall_Cave     { get; private set; }
        public string Floor_Cave    { get; private set; }
        public string Wall_Forest   { get; private set; }
        public string Floor_Forest  { get; private set; }
        public string Wall_Swamp    { get; private set; }
        public string Floor_Swamp   { get; private set; }

        // Glyph string variables
        public string Wall          { get; private set; }
        public string Floor         { get; private set; }

        public GlyphStrings()
        {
            // create actor glyph strings
            Player          = "PLAYER";
            Kobold          = "KOBOLD";

            // Create environment glyph strings
            DoorClosed      = "DOOR_CLOSED";
            DoorOpen        = "DOOR_OPEN";
            StairsUp        = "STAIRS_UP";
            StairsDown      = "STAIRS_DOWN";
            Tree            = "TREE";
            Water           = "WATER";

            Wall_Dungeon    = "DUNGEON_WALL_BITMAP_";
            Floor_Dungeon   = "DUNGEON_FLOOR";
            Wall_Cave       = "CAVE_WALL_BITMAP_";
            Floor_Cave      = "CAVE_FLOOR";
            Wall_Forest     = "TREE";
            Floor_Forest    = "FOREST_FLOOR";
            Wall_Swamp      = "SWAMP_WALL_BITMAP_";
            Floor_Swamp     = "SWAMP_FLOOR";
        }

        public void SetGlyphStringsForBiome(Biomes biome)
        {
            if (biome == Biomes.Cave)
            {
                Wall = Wall_Cave;
                Floor = Floor_Cave;
            }
            else if (biome == Biomes.Dungeon)
            {
                Wall = Wall_Dungeon;
                Floor = Floor_Dungeon;
            }
            else if (biome == Biomes.Forest)
            {
                Wall = Wall_Forest;
                Floor = Floor_Forest;
            }
            else if (biome == Biomes.Swamp)
            {
                Wall = Wall_Swamp;
                Floor = Floor_Swamp;
            }
        }
    }
}