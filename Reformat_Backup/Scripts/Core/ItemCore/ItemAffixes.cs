﻿using System.Collections.Generic;
using Rogue2018.Core.ItemCore.Suffixes;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    public class ItemAffixes
    {
        // Equipable base type suffix lists
        private static List<Suffix> ebtBeltSuffixes                     = new List<Suffix>();
        private static List<Suffix> ebtQuiverSuffixes                   = new List<Suffix>();
        private static List<Suffix> ebtRingSuffixes                     = new List<Suffix>();
        private static List<Suffix> ebtHeavyChestarmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtMediumChestArmourSuffixes        = new List<Suffix>();
        private static List<Suffix> ebtLightChestArmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtHeavyFeetArmourSuffixes          = new List<Suffix>();
        private static List<Suffix> ebtMediumFeetArmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtLightFeetArmourSuffixes          = new List<Suffix>();
        private static List<Suffix> ebtHeavyHandsArmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtMediumHandsArmourSuffixes        = new List<Suffix>();
        private static List<Suffix> ebtLightHandsArmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtHeavyHeadArmourSuffixes          = new List<Suffix>();
        private static List<Suffix> ebtMediumHeadArmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtLightHeadArmourSuffixes          = new List<Suffix>();
        private static List<Suffix> ebtHeavyLegsArmourSuffixes          = new List<Suffix>();
        private static List<Suffix> ebtMediumLegsArmourSuffixes         = new List<Suffix>();
        private static List<Suffix> ebtLightLegsArmourSuffixes          = new List<Suffix>();
        private static List<Suffix> ebtHeavyShieldSuffixes              = new List<Suffix>();
        private static List<Suffix> ebtMediumShieldSuffixes             = new List<Suffix>();
        private static List<Suffix> ebtLightShieldSuffixes              = new List<Suffix>();
        private static List<Suffix> ebtOneHandedAxeSuffixes             = new List<Suffix>();
        private static List<Suffix> ebtTwoHandedAxeSuffixes             = new List<Suffix>();
        private static List<Suffix> ebtBowSuffixes                      = new List<Suffix>();
        private static List<Suffix> ebtDaggerSuffixes                   = new List<Suffix>();
        private static List<Suffix> ebtOneHandedMaceSuffixes            = new List<Suffix>();
        private static List<Suffix> ebtTwoHandedMaceSuffixes            = new List<Suffix>();
        private static List<Suffix> ebtSceptreSuffixes                  = new List<Suffix>();
        private static List<Suffix> ebtStaffSuffixes                    = new List<Suffix>();
        private static List<Suffix> ebtOneHandedSwordGenericSuffixes    = new List<Suffix>();
        private static List<Suffix> ebtOneHandedSwordPierceSuffixes     = new List<Suffix>();
        private static List<Suffix> ebtTwoHandedSwordSuffixes           = new List<Suffix>();
        private static List<Suffix> ebtWandSuffixes                     = new List<Suffix>();
        private static List<Suffix> ebtFocusSuffixes                    = new List<Suffix>();

        // Equipable base type prefix lists
        private static List<Prefix> ebtBeltPrefixes                     = new List<Prefix>();
        private static List<Prefix> ebtQuiverPrefixes                   = new List<Prefix>();
        private static List<Prefix> ebtRingPrefixes                     = new List<Prefix>();
        private static List<Prefix> ebtHeavyChestarmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtMediumChestArmourPrefixes        = new List<Prefix>();
        private static List<Prefix> ebtLightChestArmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtHeavyFeetArmourPrefixes          = new List<Prefix>();
        private static List<Prefix> ebtMediumFeetArmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtLightFeetArmourPrefixes          = new List<Prefix>();
        private static List<Prefix> ebtHeavyHandsArmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtMediumHandsArmourPrefixes        = new List<Prefix>();
        private static List<Prefix> ebtLightHandsArmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtHeavyHeadArmourPrefixes          = new List<Prefix>();
        private static List<Prefix> ebtMediumHeadArmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtLightHeadArmourPrefixes          = new List<Prefix>();
        private static List<Prefix> ebtHeavyLegsArmourPrefixes          = new List<Prefix>();
        private static List<Prefix> ebtMediumLegsArmourPrefixes         = new List<Prefix>();
        private static List<Prefix> ebtLightLegsArmourPrefixes          = new List<Prefix>();
        private static List<Prefix> ebtHeavyShieldPrefixes              = new List<Prefix>();
        private static List<Prefix> ebtMediumShieldPrefixes             = new List<Prefix>();
        private static List<Prefix> ebtLightShieldPrefixes              = new List<Prefix>();
        private static List<Prefix> ebtOneHandedAxePrefixes             = new List<Prefix>();
        private static List<Prefix> ebtTwoHandedAxePrefixes             = new List<Prefix>();
        private static List<Prefix> ebtBowPrefixes                      = new List<Prefix>();
        private static List<Prefix> ebtDaggerPrefixes                   = new List<Prefix>();
        private static List<Prefix> ebtOneHandedMacePrefixes            = new List<Prefix>();
        private static List<Prefix> ebtTwoHandedMacePrefixes            = new List<Prefix>();
        private static List<Prefix> ebtSceptrePrefixes                  = new List<Prefix>();
        private static List<Prefix> ebtStaffPrefixes                    = new List<Prefix>();
        private static List<Prefix> ebtOneHandedSwordGenericPrefixes    = new List<Prefix>();
        private static List<Prefix> ebtOneHandedSwordPiercePrefixes     = new List<Prefix>();
        private static List<Prefix> ebtTwoHandedSwordPrefixes           = new List<Prefix>();
        private static List<Prefix> ebtWandPrefixes                     = new List<Prefix>();
        private static List<Prefix> ebtFocusPrefixes                    = new List<Prefix>();

        // Setup equipable base type suffix lists
        public void SetupEBTSuffixLists(List<Suffix> suffixes)
        {
            foreach (Suffix suffix in suffixes)
            {
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Belt))
                    ebtBeltSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Quiver))
                    ebtQuiverSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Ring))
                    ebtRingSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.HeavyChestArmour))
                    ebtHeavyChestarmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.MediumChestArmour))
                    ebtMediumChestArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.LightChestArmour))
                    ebtLightChestArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.HeavyFeetArmour))
                    ebtHeavyFeetArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.MediumFeetArmour))
                    ebtMediumFeetArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.LightFeetArmour))
                    ebtLightFeetArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.HeavyHandsArmour))
                    ebtHeavyHandsArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.MediumHandsArmour))
                    ebtMediumHandsArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.LightHandsArmour))
                    ebtLightHandsArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.HeavyHeadArmour))
                    ebtHeavyHeadArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.MediumHeadArmour))
                    ebtMediumHeadArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.LightHeadArmour))
                    ebtLightHeadArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.HeavyLegsArmour))
                    ebtHeavyLegsArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.MediumLegsArmour))
                    ebtMediumLegsArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.LightLegsArmour))
                    ebtLightLegsArmourSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.HeavyShield))
                    ebtHeavyShieldSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.MediumShield))
                    ebtMediumShieldSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.LightShield))
                    ebtLightShieldSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedAxe))
                    ebtOneHandedAxeSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.TwoHandedAxe))
                    ebtTwoHandedAxeSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Bow))
                    ebtBowSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Dagger))
                    ebtDaggerSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedMace))
                    ebtOneHandedMaceSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.TwoHandedMace))
                    ebtTwoHandedMaceSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Sceptre))
                    ebtSceptreSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Staff))
                    ebtStaffSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedSwordGeneric))
                    ebtOneHandedSwordGenericSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedSwordPierce))
                    ebtOneHandedSwordPierceSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.TwoHandedSword))
                    ebtTwoHandedSwordSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Wand))
                    ebtWandSuffixes.Add(suffix);
                if (suffix.SuffixBaseTypes.Contains(EquipableItemBaseTypes.Focus))
                    ebtFocusSuffixes.Add(suffix);
            }
        }

        // Setup equipable base type prefix lists
        public void SetupEBTPrefixLists(List<Prefix> prefixes)
        {
            foreach (Prefix prefix in prefixes)
            {
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Belt))
                    ebtBeltPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Quiver))
                    ebtQuiverPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Ring))
                    ebtRingPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.HeavyChestArmour))
                    ebtHeavyChestarmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.MediumChestArmour))
                    ebtMediumChestArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.LightChestArmour))
                    ebtLightChestArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.HeavyFeetArmour))
                    ebtHeavyFeetArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.MediumFeetArmour))
                    ebtMediumFeetArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.LightFeetArmour))
                    ebtLightFeetArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.HeavyHandsArmour))
                    ebtHeavyHandsArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.MediumHandsArmour))
                    ebtMediumHandsArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.LightHandsArmour))
                    ebtLightHandsArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.HeavyHeadArmour))
                    ebtHeavyHeadArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.MediumHeadArmour))
                    ebtMediumHeadArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.LightHeadArmour))
                    ebtLightHeadArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.HeavyLegsArmour))
                    ebtHeavyLegsArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.MediumLegsArmour))
                    ebtMediumLegsArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.LightLegsArmour))
                    ebtLightLegsArmourPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.HeavyShield))
                    ebtHeavyShieldPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.MediumShield))
                    ebtMediumShieldPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.LightShield))
                    ebtLightShieldPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedAxe))
                    ebtOneHandedAxePrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.TwoHandedAxe))
                    ebtTwoHandedAxePrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Bow))
                    ebtBowPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Dagger))
                    ebtDaggerPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedMace))
                    ebtOneHandedMacePrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.TwoHandedMace))
                    ebtTwoHandedMacePrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Sceptre))
                    ebtSceptrePrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Staff))
                    ebtStaffPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedSwordGeneric))
                    ebtOneHandedSwordGenericPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.OneHandedSwordPierce))
                    ebtOneHandedSwordPiercePrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.TwoHandedSword))
                    ebtTwoHandedSwordPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Wand))
                    ebtWandPrefixes.Add(prefix);
                if (prefix.PrefixBaseTypes.Contains(EquipableItemBaseTypes.Focus))
                    ebtFocusPrefixes.Add(prefix);
            }
        }

        public List<Suffix> GetSuffixForEBT(EquipableItemBaseTypes ebt)
        {
            if (ebt == EquipableItemBaseTypes.Belt)
                return ebtBeltSuffixes;
            else if (ebt == EquipableItemBaseTypes.Quiver)
                return ebtQuiverSuffixes;
            else if (ebt == EquipableItemBaseTypes.Ring)
                return ebtRingSuffixes;
            else if (ebt == EquipableItemBaseTypes.HeavyChestArmour)
                return ebtHeavyChestarmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.MediumChestArmour)
                return ebtMediumChestArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.LightChestArmour)
                return ebtLightChestArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.HeavyFeetArmour)
                return ebtHeavyFeetArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.MediumFeetArmour)
                return ebtMediumFeetArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.LightFeetArmour)
                return ebtLightFeetArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.HeavyHandsArmour)
                return ebtHeavyHandsArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.MediumHandsArmour)
                return ebtMediumHandsArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.LightHandsArmour)
                return ebtLightHandsArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.HeavyHeadArmour)
                return ebtHeavyHeadArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.MediumHeadArmour)
                return ebtMediumHeadArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.LightHeadArmour)
                return ebtLightHeadArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.HeavyLegsArmour)
                return ebtHeavyLegsArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.MediumLegsArmour)
                return ebtMediumLegsArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.LightLegsArmour)
                return ebtLightLegsArmourSuffixes;
            else if (ebt == EquipableItemBaseTypes.HeavyShield)
                return ebtHeavyShieldSuffixes;
            else if (ebt == EquipableItemBaseTypes.MediumShield)
                return ebtMediumShieldSuffixes;
            else if (ebt == EquipableItemBaseTypes.LightShield)
                return ebtLightShieldSuffixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedAxe)
                return ebtOneHandedAxeSuffixes;
            else if (ebt == EquipableItemBaseTypes.TwoHandedAxe)
                return ebtTwoHandedAxeSuffixes;
            else if (ebt == EquipableItemBaseTypes.Bow)
                return ebtBowSuffixes;
            else if (ebt == EquipableItemBaseTypes.Dagger)
                return ebtDaggerSuffixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedMace)
                return ebtOneHandedMaceSuffixes;
            else if (ebt == EquipableItemBaseTypes.TwoHandedMace)
                return ebtTwoHandedMaceSuffixes;
            else if (ebt == EquipableItemBaseTypes.Sceptre)
                return ebtSceptreSuffixes;
            else if (ebt == EquipableItemBaseTypes.Staff)
                return ebtStaffSuffixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedSwordGeneric)
                return ebtOneHandedSwordGenericSuffixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedSwordPierce)
                return ebtOneHandedSwordPierceSuffixes;
            else if (ebt == EquipableItemBaseTypes.TwoHandedSword)
                return ebtTwoHandedSwordSuffixes;
            else if (ebt == EquipableItemBaseTypes.Wand)
                return ebtWandSuffixes;
            else if (ebt == EquipableItemBaseTypes.Focus)
                return ebtFocusSuffixes;
            else
                return null;
        }

        public List<Prefix> GetPrefixForEBT(EquipableItemBaseTypes ebt)
        {
            if (ebt == EquipableItemBaseTypes.Belt)
                return ebtBeltPrefixes;
            else if (ebt == EquipableItemBaseTypes.Quiver)
                return ebtQuiverPrefixes;
            else if (ebt == EquipableItemBaseTypes.Ring)
                return ebtRingPrefixes;
            else if (ebt == EquipableItemBaseTypes.HeavyChestArmour)
                return ebtHeavyChestarmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.MediumChestArmour)
                return ebtMediumChestArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.LightChestArmour)
                return ebtLightChestArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.HeavyFeetArmour)
                return ebtHeavyFeetArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.MediumFeetArmour)
                return ebtMediumFeetArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.LightFeetArmour)
                return ebtLightFeetArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.HeavyHandsArmour)
                return ebtHeavyHandsArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.MediumHandsArmour)
                return ebtMediumHandsArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.LightHandsArmour)
                return ebtLightHandsArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.HeavyHeadArmour)
                return ebtHeavyHeadArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.MediumHeadArmour)
                return ebtMediumHeadArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.LightHeadArmour)
                return ebtLightHeadArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.HeavyLegsArmour)
                return ebtHeavyLegsArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.MediumLegsArmour)
                return ebtMediumLegsArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.LightLegsArmour)
                return ebtLightLegsArmourPrefixes;
            else if (ebt == EquipableItemBaseTypes.HeavyShield)
                return ebtHeavyShieldPrefixes;
            else if (ebt == EquipableItemBaseTypes.MediumShield)
                return ebtMediumShieldPrefixes;
            else if (ebt == EquipableItemBaseTypes.LightShield)
                return ebtLightShieldPrefixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedAxe)
                return ebtOneHandedAxePrefixes;
            else if (ebt == EquipableItemBaseTypes.TwoHandedAxe)
                return ebtTwoHandedAxePrefixes;
            else if (ebt == EquipableItemBaseTypes.Bow)
                return ebtBowPrefixes;
            else if (ebt == EquipableItemBaseTypes.Dagger)
                return ebtDaggerPrefixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedMace)
                return ebtOneHandedMacePrefixes;
            else if (ebt == EquipableItemBaseTypes.TwoHandedMace)
                return ebtTwoHandedMacePrefixes;
            else if (ebt == EquipableItemBaseTypes.Sceptre)
                return ebtSceptrePrefixes;
            else if (ebt == EquipableItemBaseTypes.Staff)
                return ebtStaffPrefixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedSwordGeneric)
                return ebtOneHandedSwordGenericPrefixes;
            else if (ebt == EquipableItemBaseTypes.OneHandedSwordPierce)
                return ebtOneHandedSwordPiercePrefixes;
            else if (ebt == EquipableItemBaseTypes.TwoHandedSword)
                return ebtTwoHandedSwordPrefixes;
            else if (ebt == EquipableItemBaseTypes.Wand)
                return ebtWandPrefixes;
            else if (ebt == EquipableItemBaseTypes.Focus)
                return ebtFocusPrefixes;
            else
                return null;
        }
    }
}