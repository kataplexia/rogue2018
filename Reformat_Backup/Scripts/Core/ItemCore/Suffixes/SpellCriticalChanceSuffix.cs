﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Suffixes
{
    [System.Serializable]
    public class SpellCriticalChanceSuffix : Suffix
    {
        public SpellCriticalChanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 11;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Critical Chance for Spells", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(21);
            TierMinimumItemLevels.Add(28);
            TierMinimumItemLevels.Add(41);
            TierMinimumItemLevels.Add(59);
            TierMinimumItemLevels.Add(76);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("of Menace");
            TierNameStrings.Add("of Havoc");
            TierNameStrings.Add("of Disaster");
            TierNameStrings.Add("of Calamity");
            TierNameStrings.Add("of Ruin");
            TierNameStrings.Add("of Unmaking");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(10, 19));
            TierEffectValues.Add(Game.GET.random.Next(20, 39));
            TierEffectValues.Add(Game.GET.random.Next(40, 59));
            TierEffectValues.Add(Game.GET.random.Next(60, 79));
            TierEffectValues.Add(Game.GET.random.Next(80, 99));
            TierEffectValues.Add(Game.GET.random.Next(100, 109));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}