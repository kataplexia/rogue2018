﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Suffixes
{
    [System.Serializable]
    public class FireResistanceSuffix : Suffix
    {
        public FireResistanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 8;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Fire Resistance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(12);
            TierMinimumItemLevels.Add(24);
            TierMinimumItemLevels.Add(36);
            TierMinimumItemLevels.Add(48);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(72);
            TierMinimumItemLevels.Add(84);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("of the Whelpling");
            TierNameStrings.Add("of the Salamander");
            TierNameStrings.Add("of the Drake");
            TierNameStrings.Add("of the Kiln");
            TierNameStrings.Add("of the Furnace");
            TierNameStrings.Add("of the Volcano");
            TierNameStrings.Add("of the Magma");
            TierNameStrings.Add("of Tzteosh");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(6, 11));
            TierEffectValues.Add(Game.GET.random.Next(12, 17));
            TierEffectValues.Add(Game.GET.random.Next(18, 23));
            TierEffectValues.Add(Game.GET.random.Next(24, 29));
            TierEffectValues.Add(Game.GET.random.Next(30, 35));
            TierEffectValues.Add(Game.GET.random.Next(36, 41));
            TierEffectValues.Add(Game.GET.random.Next(42, 45));
            TierEffectValues.Add(Game.GET.random.Next(46, 48));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}