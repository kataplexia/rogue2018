﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Suffixes
{
    [System.Serializable]
    public class CriticalChanceSuffix : Suffix
    {
        public CriticalChanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 6;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Critical Chance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(20);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(44);
            TierMinimumItemLevels.Add(59);
            TierMinimumItemLevels.Add(73);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("of Needling");
            TierNameStrings.Add("of Stinging");
            TierNameStrings.Add("of Piercing");
            TierNameStrings.Add("of Rupturing");
            TierNameStrings.Add("of Penetrating");
            TierNameStrings.Add("of Incision");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(10, 14));
            TierEffectValues.Add(Game.GET.random.Next(15, 19));
            TierEffectValues.Add(Game.GET.random.Next(20, 24));
            TierEffectValues.Add(Game.GET.random.Next(25, 29));
            TierEffectValues.Add(Game.GET.random.Next(30, 34));
            TierEffectValues.Add(Game.GET.random.Next(35, 38));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}