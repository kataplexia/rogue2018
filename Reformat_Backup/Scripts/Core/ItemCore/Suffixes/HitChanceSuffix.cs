﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Suffixes
{
    [System.Serializable]
    public class HitChanceSuffix : Suffix
    {
        public HitChanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            MinimumItemLevel = 1;
            MaximumItemLevel = 100;
            NumberOfTiers = 9;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Hit Chance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(12);
            TierMinimumItemLevels.Add(20);
            TierMinimumItemLevels.Add(26);
            TierMinimumItemLevels.Add(33);
            TierMinimumItemLevels.Add(41);
            TierMinimumItemLevels.Add(50);
            TierMinimumItemLevels.Add(63);
            TierMinimumItemLevels.Add(76);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("of Calm");
            TierNameStrings.Add("of Steadiness");
            TierNameStrings.Add("of Accuracy");
            TierNameStrings.Add("of Precision");
            TierNameStrings.Add("of the Sniper");
            TierNameStrings.Add("of the Marksman");
            TierNameStrings.Add("of the Deadeye");
            TierNameStrings.Add("of the Ranger");
            TierNameStrings.Add("of the Assassin");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(1, 4));
            TierEffectValues.Add(Game.GET.random.Next(5, 8));
            TierEffectValues.Add(Game.GET.random.Next(9, 12));
            TierEffectValues.Add(Game.GET.random.Next(13, 16));
            TierEffectValues.Add(Game.GET.random.Next(17, 20));
            TierEffectValues.Add(Game.GET.random.Next(21, 23));
            TierEffectValues.Add(Game.GET.random.Next(24, 26));
            TierEffectValues.Add(Game.GET.random.Next(27, 29));
            TierEffectValues.Add(Game.GET.random.Next(30, 32));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}