﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Suffixes
{
    [System.Serializable]
    public class ElementalResistanceSuffix : Suffix
    {
        public ElementalResistanceSuffix()
        {
            SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);

            MinimumItemLevel = 12;
            MaximumItemLevel = 100;
            NumberOfTiers = 5;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Elemental Resistances", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(24);
            TierMinimumItemLevels.Add(36);
            TierMinimumItemLevels.Add(48);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("of the Crystal");
            TierNameStrings.Add("of the Prism");
            TierNameStrings.Add("of the Kaleidoscope");
            TierNameStrings.Add("of Variegation");
            TierNameStrings.Add("of the Rainbow");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(3, 5));
            TierEffectValues.Add(Game.GET.random.Next(6, 8));
            TierEffectValues.Add(Game.GET.random.Next(9, 11));
            TierEffectValues.Add(Game.GET.random.Next(12, 14));
            TierEffectValues.Add(Game.GET.random.Next(15, 16));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}