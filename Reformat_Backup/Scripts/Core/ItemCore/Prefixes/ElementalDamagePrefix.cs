﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Prefixes
{
    [System.Serializable]
    public class ElementalDamagePrefix : Prefix
    {
        public ElementalDamagePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel    = 4;
            MaximumItemLevel    = 100;
            NumberOfTiers       = 5;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Elemental Damage", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(15);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(81);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("Catalyzing");
            TierNameStrings.Add("Infusing");
            TierNameStrings.Add("Empowering");
            TierNameStrings.Add("Unleashed");
            TierNameStrings.Add("Overpowering");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(5, 10));
            TierEffectValues.Add(Game.GET.random.Next(11, 20));
            TierEffectValues.Add(Game.GET.random.Next(21, 30));
            TierEffectValues.Add(Game.GET.random.Next(31, 36));
            TierEffectValues.Add(Game.GET.random.Next(37, 42));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}
