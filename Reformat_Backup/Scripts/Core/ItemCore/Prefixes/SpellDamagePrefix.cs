﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Prefixes
{
    [System.Serializable]
    public class SpellDamagePrefix : Prefix
    {
        public SpellDamagePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            MinimumItemLevel    = 2;
            MaximumItemLevel    = 100;
            NumberOfTiers       = 8;

            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Spell Damage", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(11);
            TierMinimumItemLevels.Add(23);
            TierMinimumItemLevels.Add(35);
            TierMinimumItemLevels.Add(46);
            TierMinimumItemLevels.Add(58);
            TierMinimumItemLevels.Add(64);
            TierMinimumItemLevels.Add(84);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("Apprentice's");
            TierNameStrings.Add("Adept's");
            TierNameStrings.Add("Scholar's");
            TierNameStrings.Add("Professor's");
            TierNameStrings.Add("Occultist's");
            TierNameStrings.Add("Incanter's");
            TierNameStrings.Add("Glyphic");
            TierNameStrings.Add("Runic");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(10, 19));
            TierEffectValues.Add(Game.GET.random.Next(20, 29));
            TierEffectValues.Add(Game.GET.random.Next(30, 39));
            TierEffectValues.Add(Game.GET.random.Next(40, 49));
            TierEffectValues.Add(Game.GET.random.Next(50, 59));
            TierEffectValues.Add(Game.GET.random.Next(60, 69));
            TierEffectValues.Add(Game.GET.random.Next(70, 74));
            TierEffectValues.Add(Game.GET.random.Next(75, 79));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}
