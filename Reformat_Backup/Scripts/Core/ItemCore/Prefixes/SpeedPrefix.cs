﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Prefixes
{
    [System.Serializable]
    public class SpeedPrefix : Prefix
    {
        public SpeedPrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);

            MinimumItemLevel    = 1;
            MaximumItemLevel    = 100;
            NumberOfTiers       = 6;
            
            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Speed", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(15);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(40);
            TierMinimumItemLevels.Add(55);
            TierMinimumItemLevels.Add(86);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("Runner's");
            TierNameStrings.Add("Sprinter's");
            TierNameStrings.Add("Stallion's");
            TierNameStrings.Add("Gazelle's");
            TierNameStrings.Add("Cheetah's");
            TierNameStrings.Add("Hellion's");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(10);
            TierEffectValues.Add(15);
            TierEffectValues.Add(20);
            TierEffectValues.Add(25);
            TierEffectValues.Add(30);
            TierEffectValues.Add(35);
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}