﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Prefixes
{
    [System.Serializable]
    public class FlatDefenceValuePrefix : Prefix
    {
        public FlatDefenceValuePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.Belt);

            MinimumItemLevel    = 1;
            MaximumItemLevel    = 100;
            NumberOfTiers       = 6;
            
            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1} to Defence Value", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(18);
            TierMinimumItemLevels.Add(30);
            TierMinimumItemLevels.Add(46);
            TierMinimumItemLevels.Add(59);
            TierMinimumItemLevels.Add(73);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("Lacquered");
            TierNameStrings.Add("Studded");
            TierNameStrings.Add("Ribbed");
            TierNameStrings.Add("Fortified");
            TierNameStrings.Add("Plated");
            TierNameStrings.Add("Carapaced");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(3, 10));
            TierEffectValues.Add(Game.GET.random.Next(11, 35));
            TierEffectValues.Add(Game.GET.random.Next(36, 60));
            TierEffectValues.Add(Game.GET.random.Next(61, 138));
            TierEffectValues.Add(Game.GET.random.Next(139, 322));
            TierEffectValues.Add(Game.GET.random.Next(323, 400));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}