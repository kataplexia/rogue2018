﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore.Prefixes
{
    [System.Serializable]
    public class DefenceChancePrefix : Prefix
    {
        public DefenceChancePrefix()
        {
            PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);

            MinimumItemLevel    = 3;
            MaximumItemLevel    = 100;
            NumberOfTiers       = 8;
            
            Construct(MinimumItemLevel);
        }

        public override void Construct(int itemLevel)
        {
            // Setup lists
            SetupLists();

            // Construct tier properties lists
            ConstructTiers();

            // Setup PrefixNameString and EffectValue according to item level 
            SetupNameAndValue(itemLevel);

            // Setup EffectInfoString
            EffectInfoString = string.Format("{0} +{1}% to Defence Chance", TierInfoString, EffectValue);
        }

        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            TierMinimumItemLevels.Add(MinimumItemLevel);
            TierMinimumItemLevels.Add(17);
            TierMinimumItemLevels.Add(29);
            TierMinimumItemLevels.Add(42);
            TierMinimumItemLevels.Add(60);
            TierMinimumItemLevels.Add(72);
            TierMinimumItemLevels.Add(84);
            TierMinimumItemLevels.Add(86);
            TierMinimumItemLevels.Add(MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            TierNameStrings.Add("Reinforced");
            TierNameStrings.Add("Layered");
            TierNameStrings.Add("Lobstered");
            TierNameStrings.Add("Buttressed");
            TierNameStrings.Add("Thickened");
            TierNameStrings.Add("Girded");
            TierNameStrings.Add("Impregnable");
            TierNameStrings.Add("Impenetrable");

            // Setup tierEffectValueProperties list
            TierEffectValues.Add(Game.GET.random.Next(15, 26));
            TierEffectValues.Add(Game.GET.random.Next(27, 42));
            TierEffectValues.Add(Game.GET.random.Next(43, 55));
            TierEffectValues.Add(Game.GET.random.Next(56, 67));
            TierEffectValues.Add(Game.GET.random.Next(68, 79));
            TierEffectValues.Add(Game.GET.random.Next(80, 91));
            TierEffectValues.Add(Game.GET.random.Next(92, 100));
            TierEffectValues.Add(Game.GET.random.Next(101, 110));
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}