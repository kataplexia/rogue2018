﻿using UnityEngine;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    public class HealingPotion : UsableItem
    {
        // Constructor
        public override void Construct()
        {
            // TODO: If healing potion becomes a base for differnt types of healing potion, put this there
            ParentType = this.GetType();

            Name = "Healing Potion";
            ItemQuality = ItemQualities.None;
            Description = "A foul smelling tincture that can help to mend your wounds.";
            MaxStackCount = 5;
            CurrentStackCount = 1;
        }

        // Method for using the healing potion
        public override void Use()
        {
            // TODO:  healing potion becomes a base for differnt types of healing potion, put this there
            int healValue = 20;

            game.player.HealPlayer(healValue);

            CurrentStackCount--;
            if (CurrentStackCount == 0)
                game.player.inventoryList.Remove(this);

            // Draw dropped message
            string usedMessage = string.Format("You used {0}, and restored {1} health.", Name, healValue);
            game.messageSystem.Add(usedMessage, Color.cyan, false);
            game.SetDrawRequired(true);
            Debug.Log(usedMessage);
        }
    }
}