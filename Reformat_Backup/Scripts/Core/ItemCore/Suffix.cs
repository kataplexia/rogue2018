﻿using System.Collections.Generic;
using Rogue2018.Interfaces.ItemInterfaces;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public abstract class Suffix : ISuffix
    {
        // IPrefix properties
        private List<EquipableItemBaseTypes>    _suffixBaseTypes;
        private string                          _suffixNameString;
        private string                          _tierInfoString;
        private string                          _effectInfoString;
        private int                             _effectValue;
        private int                             _numberOfTiers;
        private int                             _minimumItemLevel;
        private int                             _maximumItemLevel;
        private List<int>                       _tierMinimumItemLevels;
        private List<string>                    _tierNameStrings;
        private List<int>                       _tierEffectValues;

        public List<EquipableItemBaseTypes>     SuffixBaseTypes         { get { return _suffixBaseTypes; } set { _suffixBaseTypes = value; } }
        public string                           SuffixNameString        { get { return _suffixNameString; } set { _suffixNameString = value; } }
        public string                           TierInfoString          { get { return _tierInfoString; } set { _tierInfoString = value; } }
        public string                           EffectInfoString        { get { return _effectInfoString; } set { _effectInfoString = value; } }
        public int                              EffectValue             { get { return _effectValue; } set { _effectValue = value; } }
        public int                              NumberOfTiers           { get { return _numberOfTiers; } set { _numberOfTiers = value; } }
        public int                              MinimumItemLevel        { get { return _minimumItemLevel; } set { _minimumItemLevel = value; } }
        public int                              MaximumItemLevel        { get { return _maximumItemLevel; } set { _maximumItemLevel = value; } }
        public List<int>                        TierMinimumItemLevels   { get { return _tierMinimumItemLevels; } set { _tierMinimumItemLevels = value; } }
        public List<string>                     TierNameStrings         { get { return _tierNameStrings; } set { _tierNameStrings = value; } }
        public List<int>                        TierEffectValues        { get { return _tierEffectValues; } set { _tierEffectValues = value; } }

        public abstract void Construct(int itemLevel);
        public abstract void ConstructTiers();
        public abstract void ApplyEffect();

        public void SetupLists()
        {
            // Create lists
            TierMinimumItemLevels = new List<int>(NumberOfTiers + 1);
            TierNameStrings = new List<string>(NumberOfTiers);
            TierEffectValues = new List<int>(NumberOfTiers);
        }

        public void SetupNameAndValue(int itemLevel)
        {
            for (int i = 0; i < NumberOfTiers; i++)
            {
                if (itemLevel >= TierMinimumItemLevels[i] &&
                    itemLevel < TierMinimumItemLevels[i + 1]
                    )
                {
                    if (i > 0)
                    {
                        int randomTierIndex = Game.GET.random.Next(0, i);
                        SuffixNameString = TierNameStrings[randomTierIndex];
                        EffectValue = TierEffectValues[randomTierIndex];
                        TierInfoString = string.Format("[Tier {0}]", NumberOfTiers - randomTierIndex);
                    }
                    else
                    {
                        SuffixNameString = TierNameStrings[i];
                        EffectValue = TierEffectValues[i];
                        TierInfoString = string.Format("[Tier {0}]", NumberOfTiers - i);
                    }
                }
            }
        }
    }
}