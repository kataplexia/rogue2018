﻿using UnityEngine;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    public abstract class UsableItem : IItem, IUsable
    {
        // Constructor
        public static Game game;
        public UsableItem()
        {
            game = Game.GET;
        }

        // IItem properties
        private ItemQualities _itemQuality;
        private string _name;
        private string _description;

        public ItemQualities ItemQuality    { get { return _itemQuality; } set { _itemQuality = value; } }
        public string Name                  { get { return _name; } set { _name = value; } }
        public string Description           { get { return _description; } set { _description = value; } }
        public void DrawName(int xOffset, int yOffset, Color fgColor)
        {
            Color bgColor = game.tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            game.tools.DrawString(xOffset, yOffset, Name, fgColor, bgColor, 0);
        }

        // IUsable properties
        public abstract void Construct();

        private System.Type _parentType;
        private int         _maxStackCount;
        private int         _currentStackCount;

        public System.Type ParentType       { get { return _parentType; } set { _parentType = value; } }
        public int MaxStackCount            { get { return _maxStackCount; } set { _maxStackCount = value; } }
        public int CurrentStackCount        { get { return _currentStackCount; } set { _currentStackCount = value; } }
        public void DrawStacks(int xOffset, int yOffset, Color fgColor)
        {
            Color bgColor = game.tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            game.tools.DrawString(xOffset, yOffset, string.Format("[{0}/{1}]", CurrentStackCount, MaxStackCount), fgColor, bgColor, 0);
        }

        public abstract void Use();
        
        public void Drop(bool dropAll = false)
        {
            if (dropAll)
            {
                // Remove the item from the player's inventory list
                game.player.inventoryList.Remove(this);

                // TODO: Place item inside list for current cell's contents

                // Draw dropped message
                string droppedMessage = string.Format("You dropped {0}x {1} on the ground", CurrentStackCount, Name);
                game.messageSystem.Add(droppedMessage, Color.cyan, false);
                game.SetDrawRequired(true);
                Debug.Log(droppedMessage);
            }
            else
            {
                this.CurrentStackCount--;
                if (CurrentStackCount == 0)
                    game.player.inventoryList.Remove(this);

                // Create instance for inside list for current cell's contents
                // System.Object instance = System.Activator.CreateInstance(ParentType);
                // UsableItem droppedUsable = game.itemGenerator.CreateUsableItem((UsableItem)instance);
                // Debug.Log("Created instance of: " + droppedUsable.Name + ", which has [" + droppedUsable.CurrentStackCount + "/" + droppedUsable.MaxStackCount + "] stack property.");

                // Draw dropped message
                string droppedMessage = string.Format("You dropped 1x {0} on the ground", Name);
                game.messageSystem.Add(droppedMessage, Color.cyan, false);
                game.SetDrawRequired(true);
                Debug.Log(droppedMessage);
            }
        }
    }
}