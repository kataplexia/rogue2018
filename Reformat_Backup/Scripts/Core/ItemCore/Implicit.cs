﻿using Rogue2018.Interfaces.ItemInterfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public abstract class Implicit : IImplicit
    {
        // IImplicit properties
        private string  _effectInfoString;
        private int     _effectValue;

        public string EffectInfoString  { get { return _effectInfoString; } set { _effectInfoString = value; } }
        public int EffectValue          { get { return _effectValue; } set { _effectValue = value; } }

        public abstract void Construct();
        public abstract void ApplyEffect();
    }
}