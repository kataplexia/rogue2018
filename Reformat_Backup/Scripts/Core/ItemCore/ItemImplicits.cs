﻿using System.Collections.Generic;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    public class ItemImplicits
    {
        // Implicits list
        public List<Implicit> implicits;

        // Constructor
        private static Game game;
        public ItemImplicits()
        {
            game = Game.GET;
            implicits = new List<Implicit>();

            // Add Implicits to the list
            implicits.Add(new HitChanceImplicit());
            implicits.Add(new FlatPhysicalDamageImplicit());
            implicits.Add(new AwarenessImplicit());
            implicits.Add(new CriticalChanceImplicit());
            implicits.Add(new CriticalDamageImplicit());
            implicits.Add(new DefenceChanceImplicit());
            implicits.Add(new ElementalDamageImplicit());
            implicits.Add(new MaxHealthImplicit());
            implicits.Add(new MaxManaImplicit());
            implicits.Add(new SpeedImplicit());
            implicits.Add(new SpellCriticalChanceImplicit());
            implicits.Add(new SpellCriticalDamageImplicit());
            implicits.Add(new SpellDamageImplicit());
            implicits.Add(new StunChanceImplicit());
        }

        // Method for getting a random implicit from the list
        public Implicit GetRandomImplicit()
        {
            int randomIndex = game.random.Next(0, implicits.Count - 1);
            Implicit returnImplicit = game.tools.DeepCopy<Implicit>(implicits[randomIndex]);
            returnImplicit.Construct();
            return returnImplicit;
        }
    }
}