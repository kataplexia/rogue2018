﻿using RogueSharp.DiceNotation;

namespace Rogue2018.Core.ItemCore.Implicits
{
    [System.Serializable]
    public class CriticalChanceImplicit : Implicit
    {
        public CriticalChanceImplicit()
        {
            Construct();
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D5");
            EffectInfoString = string.Format("+{0}% to Critical Chance", EffectValue);
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}