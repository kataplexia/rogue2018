﻿using RogueSharp.DiceNotation;

namespace Rogue2018.Core.ItemCore.Implicits
{
    [System.Serializable]
    public class SpeedImplicit : Implicit
    {
        public SpeedImplicit()
        {
            Construct();
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D5");
            EffectInfoString = string.Format("+{0}% to Speed", EffectValue);
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}