﻿using RogueSharp.DiceNotation;

namespace Rogue2018.Core.ItemCore.Implicits
{
    [System.Serializable]
    public class DefenceChanceImplicit : Implicit
    {
        public DefenceChanceImplicit()
        {
            Construct();
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D5");
            EffectInfoString = string.Format("+{0}% to Defence Chance", EffectValue);
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}
