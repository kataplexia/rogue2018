﻿using RogueSharp.DiceNotation;

namespace Rogue2018.Core.ItemCore.Implicits
{
    [System.Serializable]
    public class MaxManaImplicit : Implicit
    {
        public MaxManaImplicit()
        {
            Construct();
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D10");
            EffectInfoString = string.Format("+{0} to Maximum Mana", EffectValue);
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}
