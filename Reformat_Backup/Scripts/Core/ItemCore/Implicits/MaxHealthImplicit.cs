﻿using RogueSharp.DiceNotation;

namespace Rogue2018.Core.ItemCore.Implicits
{
    [System.Serializable]
    public class MaxHealthImplicit : Implicit
    {
        public MaxHealthImplicit()
        {
            EffectValue = Dice.Roll("1D10");
            EffectInfoString = string.Format("+{0} to Maximum Health", EffectValue);
        }

        public override void Construct()
        {
            EffectValue = Dice.Roll("1D10");
            EffectInfoString = string.Format("+{0} to Maximum Health", EffectValue);
        }

        public override void ApplyEffect()
        {
            // TODO
        }
    }
}
