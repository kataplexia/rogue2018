﻿using System.Collections.Generic;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    public class ItemGenerator
    {
        public EquipableItem CreateEquipableItem(
            EquipableItem   itemBase,
            ItemQualities   itemQuality,
            int             itemLevel
            )
        {
            EquipableItem equipableItem     = itemBase;
            equipableItem.ItemQuality       = itemQuality;
            equipableItem.ItemLevel         = itemLevel;
            
            // Construct and return item
            equipableItem.Construct();
            return equipableItem;
        }

        public UsableItem CreateUsableItem(
            UsableItem itemBase
            )
        {
            UsableItem usableItem = itemBase;

            // Construct and return item
            usableItem.Construct();
            return usableItem;
        }
    }
}


