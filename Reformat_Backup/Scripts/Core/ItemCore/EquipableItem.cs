﻿using System.Collections.Generic;
using UnityEngine;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public abstract class EquipableItem : IItem, IEquipable
    {
        // Constructor
        public static Game game;
        public EquipableItem()
        {
            game                = Game.GET;
            Equipped            = false;
            EquippedMainHand    = false;
            EquippedOffHand     = false;
            EquippedLeftRing    = false;
            EquippedRightRing   = false;
        }

        // IItem properties
        private ItemQualities           _itemQuality;
        private string                  _name;
        private string                  _description;

        public ItemQualities ItemQuality                        { get { return _itemQuality; } set { _itemQuality = value; } }
        public string Name                                      { get { return _name; } set { _name = value; } }
        public string Description                               { get { return _description; } set { _description = value; } }
        public void DrawName(int xOffset, int yOffset, Color fgColor)
        {
            Color bgColor = game.tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            game.tools.DrawString(xOffset, yOffset, Name, fgColor, bgColor, 0);
        }

        // IEquipable properties
        private EquipableItemBaseTypes  _equipableItemBaseType;
        private int                     _itemLevel;
        private bool                    _equipped;
        private bool                    _equippedMainHand;
        private bool                    _equippedOffHand;
        private bool                    _equippedLeftRing;
        private bool                    _equippedRightRing;
        private EquipmentLocations      _equipmentLocation;
        private string                  _equipmentLocationString;
        private List<Prefix>            _prefixes;
        private List<Suffix>            _suffixes;
        private Implicit                _implicit;

        public EquipableItemBaseTypes EquipableItemBaseType     { get { return _equipableItemBaseType; } set { _equipableItemBaseType = value; } }
        public int ItemLevel                                    { get { return _itemLevel; } set { _itemLevel = value; } }
        public bool Equipped                                    { get { return _equipped; } set { _equipped = value; } }
        public bool EquippedMainHand                            { get { return _equippedMainHand; } set { _equippedMainHand = value; } }
        public bool EquippedOffHand                             { get { return _equippedOffHand; } set { _equippedOffHand = value; } }
        public bool EquippedLeftRing                            { get { return _equippedLeftRing; } set { _equippedLeftRing = value; } }
        public bool EquippedRightRing                           { get { return _equippedRightRing; } set { _equippedRightRing = value; } }
        public EquipmentLocations EquipmentLocation             { get { return _equipmentLocation; } set { _equipmentLocation = value; } }
        public string EquipmentLocationString                   { get { return _equipmentLocationString; } set { _equipmentLocationString = value; } }
        public List<Prefix> Prefixes                            { get { return _prefixes; } set { _prefixes = value; } }
        public List<Suffix> Suffixes                            { get { return _suffixes; } set { _suffixes = value; } }
        public Implicit Implicit                                { get { return _implicit; } set { _implicit = value; } }
        public abstract void Construct();
        public void Equip()
        {
            // Check if player's equipment slot is empty
            string checkString = CheckFreeSlotToEquipString(EquipmentLocation);
            if (checkString[0] == 'N')
            {
                string cannotEquipMessage = checkString.Substring(2);
                game.messageSystem.Add(cannotEquipMessage, Color.cyan, false);
                game.SetDrawRequired(true);
                return;
            }

            // Set item to player's qeuipment slot
            if (EquipmentLocation == EquipmentLocations.Chest)
                game.player.equipmentChest = this;
            else if (EquipmentLocation == EquipmentLocations.EitherHand)
            {
                if (game.player.equipmentMainHand == null)
                {
                    game.player.equipmentMainHand = this;
                    EquippedMainHand = true;
                }
                else if (game.player.equipmentOffHand == null)
                {
                    game.player.equipmentOffHand = this;
                    EquippedOffHand = true;
                }
            }
            else if (EquipmentLocation == EquipmentLocations.Feet)
                game.player.equipmentFeet = this;
            else if (EquipmentLocation == EquipmentLocations.Hands)
                game.player.equipmentHands = this;
            else if (EquipmentLocation == EquipmentLocations.Head)
                game.player.equipmentHead = this;
            else if (EquipmentLocation == EquipmentLocations.Legs)
                game.player.equipmentLegs = this;
            else if (EquipmentLocation == EquipmentLocations.MainHand)
                game.player.equipmentMainHand = this;
            else if (EquipmentLocation == EquipmentLocations.OffHand)
                game.player.equipmentOffHand = this;
            else if (EquipmentLocation == EquipmentLocations.Ring)
            {
                if (game.player.equipmentLeftRing == null)
                {
                    game.player.equipmentLeftRing = this;
                    EquippedLeftRing = true;
                }
                else if (game.player.equipmentRightRing == null)
                {
                    game.player.equipmentRightRing = this;
                    EquippedRightRing = true;
                }
            }
            else if (EquipmentLocation == EquipmentLocations.Waist)
                game.player.equipmentWaist = this;

            // Set equipped property
            Equipped = true;

            // Remove item from player's inventory list
            game.player.inventoryList.Remove(this);

            // Draw equipped message
            string equippedMessage = string.Format("You equipped {0}", Name);
            game.messageSystem.Add(equippedMessage, Color.cyan, false);
            game.SetDrawRequired(true);
        }

        public void Unequip()
        {
            // Check for inventory space before unequipping
            if (!game.player.CheckInventoryCapacity())
                return;

            // Remove item from the player's equipment slot and add to inventory list
            RemoveItemAtEquipmentLocation(EquipmentLocation);
            game.player.AddItemToInventory(this as IItem);

            // Draw unequipped message
            string unequippedMessage = string.Format("You unequipped {0}", Name);
            game.messageSystem.Add(unequippedMessage, Color.cyan, false);
            game.SetDrawRequired(true);
        }

        public void Drop(bool dropAll = false)
        {
            // Remove the item from the player's equipment slot or inventory list
            if (Equipped)
                RemoveItemAtEquipmentLocation(EquipmentLocation);
            else
                game.player.inventoryList.Remove(this);

            // TODO: Place item inside list for current cell's contents

            // Draw dropped message
            string droppedMessage = string.Format("You dropped {0} on the ground", Name);
            game.messageSystem.Add(droppedMessage, Color.cyan, false);
            game.SetDrawRequired(true);
        }

        // Returns a string denoting whether or not an equipment slot was found. Y for true, and N/Message for false
        private string CheckFreeSlotToEquipString(EquipmentLocations equipmentLocation)
        {
            // Equipment is for the chest, check if chest slot is free
            if (equipmentLocation == EquipmentLocations.Chest && game.player.equipmentChest == null)
                return "Y";

            // Equipment is for either hand
            else if (equipmentLocation == EquipmentLocations.EitherHand)
            {
                // Check if MainHand is free but OffHand is not
                if (game.player.equipmentMainHand == null && game.player.equipmentOffHand != null)
                {
                    IEquipable offHandEquipable = game.player.equipmentOffHand as IEquipable;

                    // Check if OffHand is an accessory
                    if (offHandEquipable is IAccessory)
                    {
                        IAccessory offHandAccessory = offHandEquipable as IAccessory;

                        // Check if OffHand is a quiver, otherwise it is a focus
                        if (offHandAccessory.AccessoryType == AccessoryTypes.Quiver)
                            return string.Format("N/Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                        else return "Y";
                    }
                    else return "Y";
                }

                // Check if OffHand is free but MainHand is not
                else if (game.player.equipmentMainHand != null && game.player.equipmentOffHand == null)
                {
                    IWeapon mainHandWeapon = game.player.equipmentMainHand as IWeapon;
                    
                    // Check if MainHand is two handed
                    if (mainHandWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                        return string.Format("N/Cannot equip {0}. Main Hand equipment is Two Handed", Name);
                   
                    // MainHand is not two handed
                    else
                    {
                        // Check if MainHand is ranged
                        if (mainHandWeapon.WeaponStyle == WeaponStyles.Ranged)
                            return string.Format("N/Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);
                        else return "Y";
                    }
                }
                else if (game.player.equipmentMainHand != null && game.player.equipmentOffHand != null)
                    return string.Format("N/Cannot equip {0}. No equipment slot available", Name);
                else return "Y";
            }
            
            // Equipment is for the feet, check if feet slot is free
            else if (equipmentLocation == EquipmentLocations.Feet && game.player.equipmentFeet == null)
                return "Y";

            // Equipment is for the hands, check if hands slot is free
            else if (equipmentLocation == EquipmentLocations.Hands && game.player.equipmentHands == null)
                return "Y";

            // Equipment is for the head, check if head slot is free
            else if (equipmentLocation == EquipmentLocations.Head && game.player.equipmentHead == null)
                return "Y";

            // Equipment is for the legs, check if legs slot is free
            else if (equipmentLocation == EquipmentLocations.Legs && game.player.equipmentLegs == null)
                return "Y";

            // Equipment is for the MainHand, check if MainHand slot is free
            else if (equipmentLocation == EquipmentLocations.MainHand &&
                game.player.equipmentMainHand == null)
            {
                IWeapon thisWeapon = this as IWeapon;

                // Equipment is two handed, check if OffHand slot is free
                if (thisWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                {
                     if (game.player.equipmentOffHand == null)
                        return "Y";
                     else return string.Format("N/Cannot equip {0}. No equipment slot available", Name);
                }

                // Equipment is ranged
                else if (thisWeapon.WeaponStyle == WeaponStyles.Ranged)
                {
                    // Check if OffHand is slot not free
                    if (game.player.equipmentOffHand != null)
                    {
                        IEquipable offHandEquipable = game.player.equipmentOffHand as IEquipable;

                        // Equipment is a bow
                        if (thisWeapon.WeaponType == WeaponTypes.Bow)
                        {
                            // Check if OffHand is an accessory, else it is a shield
                            if (offHandEquipable is IAccessory)
                            {
                                IAccessory offHandAccessory = offHandEquipable as IAccessory;

                                // Check if OffHand is a focus, else it is a quiver
                                if (offHandAccessory.AccessoryType == AccessoryTypes.Focus)
                                    return string.Format("N/Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                                else return "Y";
                            }
                            else return string.Format("N/Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                        }

                        // Equipment is a wand
                        else if (thisWeapon.WeaponType == WeaponTypes.Wand)
                        {
                            // Check if OffHand is an accessory, else it is a shield
                            if (offHandEquipable is IAccessory)
                            {
                                IAccessory offHandAccessory = offHandEquipable as IAccessory;

                                // Check if OffHand is a quiver, else it is a focus
                                if (offHandAccessory.AccessoryType == AccessoryTypes.Quiver)
                                    return string.Format("N/Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                                else return "Y";
                            }
                            else return "Y"; 
                        }

                        // Check if OffHand is a weapon
                        else if (offHandEquipable is IWeapon)
                            return string.Format("N/Cannot equip {0}. Off Hand equipment is of an incompatible type", Name);
                        else return "Y";
                    }
                    else return "Y";
                }
                else return "Y";
            }

            // Equipment is for the OffHand, check if OffHand slot is free
            else if (equipmentLocation == EquipmentLocations.OffHand && game.player.equipmentOffHand == null)
            {
                // Check if MainHand slot is not free
                if (game.player.equipmentMainHand != null)
                {
                    IWeapon mainHandWeapon = game.player.equipmentMainHand as IWeapon;

                    // Check if MainHand is two handed
                    if (mainHandWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                        return string.Format("N/Cannot equip {0}. Main Hand equipment is Two Handed", Name);

                    // MainHand is not two handed
                    else
                    {
                        // MainHand is a bow, check if this is a shield
                        if (mainHandWeapon.WeaponType == WeaponTypes.Bow && this is IArmour)
                            return string.Format("N/Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);

                        // MainHand is not bow, check if this is an accessory
                        else if (mainHandWeapon.WeaponType != WeaponTypes.Bow && this is IAccessory)
                        {
                            IAccessory equipableAccessory = this as IAccessory;

                            // Check if this is a quiver, else it is a focus
                            if (equipableAccessory.AccessoryType == AccessoryTypes.Quiver)
                                return string.Format("N/Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);
                            else return "Y";
                        }
                        else return "Y";
                    }
                }
                else return "Y";
            }

            // Equipment is a ring, check if a ring slot is free
            else if (equipmentLocation == EquipmentLocations.Ring)
            {
                // Check if a ring slot is free
                if (game.player.equipmentLeftRing == null || game.player.equipmentRightRing == null)
                    return "Y";
                else return string.Format("N/Cannot equip {0}. No equipment slot available", Name);
            }
            // Equipment is for the waist, check if waist slot is free
            else if (equipmentLocation == EquipmentLocations.Waist && game.player.equipmentWaist == null)
                return "Y";
            else return string.Format("N/Cannot equip {0}. No equipment slot available", Name);
        }

        // Method for removing an item from the player's equipment slots given an equipment location
        private void RemoveItemAtEquipmentLocation(EquipmentLocations equipmentLocation)
        {
            if (equipmentLocation == EquipmentLocations.Chest)
            {
                game.player.equipmentChest = null;
                Equipped = false;
            }
            else if (equipmentLocation == EquipmentLocations.EitherHand)
            {
                if (EquippedMainHand)
                {
                    game.player.equipmentMainHand = null;
                    Equipped = false;
                    EquippedMainHand = false;
                }
                else if (EquippedOffHand)
                {
                    game.player.equipmentOffHand = null;
                    Equipped = false;
                    EquippedOffHand = false;
                }
            }
            else if (equipmentLocation == EquipmentLocations.Feet)
            {
                game.player.equipmentFeet = null;
                Equipped = false;
            }
            else if (equipmentLocation == EquipmentLocations.Hands)
            {
                game.player.equipmentHands = null;
                Equipped = false;
            }
            else if (equipmentLocation == EquipmentLocations.Head)
            {
                game.player.equipmentHead = null;
                Equipped = false;
            }
            else if (equipmentLocation == EquipmentLocations.Legs)
            {
                game.player.equipmentLegs = null;
                Equipped = false;
            }
            else if (equipmentLocation == EquipmentLocations.MainHand)
            {
                game.player.equipmentMainHand = null;
                Equipped = false;
                EquippedMainHand = false;
            }
            else if (equipmentLocation == EquipmentLocations.OffHand)
            {
                game.player.equipmentOffHand = null;
                Equipped = false;
                EquippedOffHand = false;
            }
            else if (equipmentLocation == EquipmentLocations.Ring)
            {
                if (EquippedLeftRing)
                {
                    game.player.equipmentLeftRing = null;
                    Equipped = false;
                    EquippedLeftRing = false;
                }
                else if (EquippedRightRing)
                {
                    game.player.equipmentRightRing = null;
                    Equipped = false;
                    EquippedRightRing = false;
                }
            }
            else if (equipmentLocation == EquipmentLocations.Waist)
            {
                game.player.equipmentWaist = null;
                Equipped = false;
            }
        }

        // Method for setting affixes
        public int prefixCount { get; private set; }
        public int suffixCount { get; private set; }
        public void SetupAffixes()
        {
            prefixCount = 0;
            suffixCount = 0;
            if (ItemQuality == ItemQualities.Magic)
            {
                int affixCount = game.random.Next(1, 2);
                if (affixCount == 1)
                {
                    int prefixOrSuffix = game.random.Next(0, 1);
                    if (prefixOrSuffix == 0)
                        prefixCount = 1;
                    else
                        suffixCount = 1;
                }
                else if (affixCount == 2)
                {
                    prefixCount = 1;
                    suffixCount = 1;
                }
            }
            else if (ItemQuality == ItemQualities.Rare)
            {
                int prefixOrSuffix = game.random.Next(0, 1);
                if (prefixOrSuffix == 0)
                {
                    prefixCount = 2 + game.random.Next(0, 1);
                    suffixCount = 1 + game.random.Next(0, 2);
                }
                else
                {
                    prefixCount = 1 + game.random.Next(0, 2);
                    suffixCount = 2 + game.random.Next(0, 1);
                } 
            }

            Prefixes = new List<Prefix>(prefixCount);
            Suffixes = new List<Suffix>(suffixCount);

            AddPrefixes(prefixCount);
            AddSuffixes(suffixCount);

            RenameItem(prefixCount, suffixCount);
        }

        // Method for adding prefixes
        public void AddPrefixes(int prefixCount)
        {
            if (prefixCount == 0)
                return;

            for (int i = 0; i < prefixCount; i++)
                Prefixes.Add(game.itemPrefixes.GetRandomPrefix(Prefixes, ItemLevel, EquipableItemBaseType));
        }

        // Method for adding suffixes 
        public void AddSuffixes(int suffixCount)
        {
            if (suffixCount == 0)
                return;

            for (int i = 0; i < suffixCount; i++)
                Suffixes.Add(game.itemSuffixes.GetRandomSuffix(Suffixes, ItemLevel, EquipableItemBaseType));
        }

        // Method for renaming
        public void RenameItem(int prefixCount, int suffixCount)
        {
            if (ItemQuality == ItemQualities.Magic)
            {
                if (prefixCount > 0 && suffixCount == 0)
                {
                    string preName = Prefixes[0].PrefixNameString;
                    Name = string.Format("{0} {1}", preName, Name);
                }
                else if (suffixCount > 0 && prefixCount == 0)
                {
                    string postName = Suffixes[0].SuffixNameString;
                    Name = string.Format("{0} {1}", Name, postName);
                }
                else if (suffixCount > 0 && prefixCount > 0)
                {
                    string preName = Prefixes[0].PrefixNameString;
                    string postName = Suffixes[0].SuffixNameString;
                    Name = string.Format("{0} {1} {2}", preName, Name, postName);
                }
            }
            else if (ItemQuality == ItemQualities.Rare)
            {
                string randomName = game.itemNameGenerator.CreateRandomName(this);
                Name = string.Format("{0} ({1})", randomName, Name);
            }
            else if (ItemQuality == ItemQualities.Unique)
            {
                // TODO: Name item from random strings
            }
        }

        // Returns a string according to passed equipment location
        public string SetupEquipmentLocationString(EquipmentLocations equipmentLocation)
        {
            if (equipmentLocation == EquipmentLocations.Head)
                return "Head";
            else if (equipmentLocation == EquipmentLocations.Chest)
                return "Chest";
            else if (equipmentLocation == EquipmentLocations.Waist)
                return "Waist";
            else if (equipmentLocation == EquipmentLocations.Legs)
                return "Legs";
            else if (equipmentLocation == EquipmentLocations.Feet)
                return "Feet";
            else if (equipmentLocation == EquipmentLocations.Hands)
                return "Hands";
            else if (equipmentLocation == EquipmentLocations.MainHand)
                return "Main Hand";
            else if (equipmentLocation == EquipmentLocations.OffHand)
                return "Off Hand";
            else if (equipmentLocation == EquipmentLocations.EitherHand)
                return "Either Hand";
            else if (equipmentLocation == EquipmentLocations.Ring)
                return "Ring";
            else return "";
        }

        // Returns a string according to passed weapon style
        public string SetupWeaponStyleString(WeaponStyles weaponStyle)
        {
            if (weaponStyle == WeaponStyles.OneHanded)
                return "One Handed";
            else if (weaponStyle == WeaponStyles.TwoHanded)
                return "Two Handed";
            else if (weaponStyle == WeaponStyles.Ranged)
                return "Ranged";
            else
                return "";
        }

        // Returns a string according to passed weapon type
        public string SetupWeaponTypeString(WeaponTypes weaponType)
        {
            if (weaponType == WeaponTypes.Axe)
                return "Axe";
            else if (weaponType == WeaponTypes.Bow)
                return "Bow";
            else if (weaponType == WeaponTypes.Mace)
                return "Mace";
            else if (weaponType == WeaponTypes.Staff)
                return "Staff";
            else if (weaponType == WeaponTypes.Sword)
                return "Sword";
            else if (weaponType == WeaponTypes.Dagger)
                return "Dagger";
            else if (weaponType == WeaponTypes.Sceptre)
                return "Sceptre";
            else if (weaponType == WeaponTypes.Wand)
                return "Wand";
            else
                return "";
        }

        // Returns a string according to passed damage type
        public string SetupDamageTypeString(DamageTypes damageType)
        {
            if (damageType == DamageTypes.Blunt)
                return "Blunt";
            else if (damageType == DamageTypes.Cut)
                return "Cut";
            else if (damageType == DamageTypes.Pierce)
                return "Pierce";
            else
                return "";
        }

        // Returns a string according to passed armour style
        public string SetupArmourStyleString(ArmourStyles armourStyle)
        {
            if (armourStyle == ArmourStyles.Heavy)
                return "Heavy";
            else if (armourStyle == ArmourStyles.Light)
                return "Light";
            else if (armourStyle == ArmourStyles.Medium)
                return "Medium";
            else
                return "";
        }

        // Returns a string according to passed accessory type
        public string SetupAccessoryTypeString(AccessoryTypes accessoryType)
        {
            if (accessoryType == AccessoryTypes.Belt)
                return "Belt";
            else if (accessoryType == AccessoryTypes.Ring)
                return "Ring";
            else if (accessoryType == AccessoryTypes.Quiver)
                return "Quiver";
            else
                return "";
        }
    }
}