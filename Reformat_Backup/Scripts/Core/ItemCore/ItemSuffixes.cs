﻿using System.Collections.Generic;
using Rogue2018.Core.ItemCore.Suffixes;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    public class ItemSuffixes : ItemAffixes
    {
        // Suffixes list
        private static List<Suffix> suffixesList;

        // Constructor
        private static Game game;
        public ItemSuffixes()
        {
            game = Game.GET;
            suffixesList = new List<Suffix>();

            // Add Suffixes to the list
            suffixesList.Add(new AwarenessSuffix());
            suffixesList.Add(new ColdResistanceSuffix());
            suffixesList.Add(new CriticalChanceSuffix());
            suffixesList.Add(new CriticalDamageSuffix());
            suffixesList.Add(new ElementalResistanceSuffix());
            suffixesList.Add(new FireResistanceSuffix());
            suffixesList.Add(new HitChanceSuffix());
            suffixesList.Add(new LightningResistanceSuffix());
            suffixesList.Add(new PlagueResistanceSuffix());
            suffixesList.Add(new SpellCriticalChanceSuffix());

            SetupEBTSuffixLists(suffixesList);
        }

        // Method for getting a random suffix from the list given the item's current prefixes, item level and ebt
        public Suffix GetRandomSuffix(
            List<Suffix> currentSuffixList,
            int itemLevel,
            EquipableItemBaseTypes ebt
            )
        {
            // Get suffix list for equipable base type
            List<Suffix> suffixListForEBT = GetSuffixForEBT(ebt);

            // Create empty modified suffix list
            List<Suffix> modifiedSuffixesList = new List<Suffix>();

            // Check entries in suffix list for matches in the item's current suffix list, add to modified list if there is no match
            for (int i = 0; i < suffixListForEBT.Count; i++)
            {
                bool currentListContainsMatch = false;
                foreach (Suffix currentsuffix in currentSuffixList)
                {
                    if (currentsuffix.GetType().Name == suffixListForEBT[i].GetType().Name)
                        currentListContainsMatch = true;
                }
                if (!currentListContainsMatch)
                    modifiedSuffixesList.Add(suffixListForEBT[i]);
            }

            // Select suffix at random index from modified suffix list
            int randomIndex = game.random.Next(0, modifiedSuffixesList.Count - 1);
            Suffix selectedSuffix = modifiedSuffixesList[randomIndex];

            // Deep copy, construct and return the selected suffix
            Suffix returnSuffix = game.tools.DeepCopy<Suffix>(modifiedSuffixesList[randomIndex]);
            returnSuffix.Construct(itemLevel);
            return returnSuffix;
        }
    }
}