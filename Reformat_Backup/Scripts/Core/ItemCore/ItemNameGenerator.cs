﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Rogue2018.Interfaces;
using System.IO;
using System.Xml;

namespace Rogue2018.Core.ItemCore
{
    public class ItemNameGenerator
    {
        private Game game;

        public string[] preNamesArray;
        public string[] soulShieldsPostNamesArray;
        public string[] otherShieldsPostNamesArray;
        public string[] chestArmoursPostNamesArray;
        public string[] headArmoursPostNamesArray;
        public string[] handsArmoursPostNamesArray;
        public string[] feetArmoursPostNamesArray;
        public string[] legsArmoursPostNameArray;
        public string[] ringsPostNameArray;
        public string[] beltsPostNameArray;
        public string[] quiversPostNameArray;
        public string[] axesPostNameArray;
        public string[] macesPostNameArray;
        public string[] sceptresPostNameArray;
        public string[] stavesPostNameArray;
        public string[] swordsPostNameArray;
        public string[] daggersPostNameArray;
        public string[] bowsPostNameArray;
        public string[] wandsPostNameArray;
        public string[] fociPostNameArray;

        public ItemNameGenerator()
        {
            game = Game.GET;

            // Setup filepath
            string xmlPath = "ItemNameData";

            // Load Xml data
            var xmlRawFile = Resources.Load<TextAsset>(xmlPath);
            string xmlData = xmlRawFile.text;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(new StringReader(xmlData));

            // Setup seperating chars
            string[] seperatingChars = { ", " };

            // Get prenames, split and store as array
            XmlNode preNamesNode = xmlDocument.SelectSingleNode("/data/prenames");
            string preNamesString = preNamesNode.InnerXml;
            preNamesArray = preNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get soul shield postnames, split and store as array
            XmlNode soulShieldPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/soulshields");
            string soulShieldPostNamesString= soulShieldPostNamesNode.InnerXml;
            soulShieldsPostNamesArray = soulShieldPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get other shield postnames, split and store as array
            XmlNode otherShieldPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/othershields");
            string otherShieldPostNamesString = otherShieldPostNamesNode.InnerXml;
            otherShieldsPostNamesArray = otherShieldPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get chest armours postnames, split and store as array
            XmlNode chestArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/chestarmours");
            string chestArmoursPostNamesString = chestArmoursPostNamesNode.InnerXml;
            chestArmoursPostNamesArray = chestArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get head armours postnames, split and store as array
            XmlNode headArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/headarmours");
            string headArmoursPostNamesString = headArmoursPostNamesNode.InnerXml;
            headArmoursPostNamesArray = headArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get hands armours postnames, split and store as array
            XmlNode handsArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/handsarmours");
            string handsArmoursPostNamesString = handsArmoursPostNamesNode.InnerXml;
            handsArmoursPostNamesArray = handsArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get feet armours postnames, split and store as array
            XmlNode feetArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/feetarmours");
            string feetArmoursPostNamesString = feetArmoursPostNamesNode.InnerXml;
            feetArmoursPostNamesArray = feetArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get legs armours postnames, split and store as array
            XmlNode legsArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/legsarmours");
            string legsArmoursPostNamesString = legsArmoursPostNamesNode.InnerXml;
            legsArmoursPostNameArray = legsArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get rings postnames, split and store as array
            XmlNode ringsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/rings");
            string ringsPostNamesString = ringsPostNamesNode.InnerXml;
            ringsPostNameArray = ringsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get belts postnames, split and store as array
            XmlNode beltsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/belts");
            string beltsPostNamesString = beltsPostNamesNode.InnerXml;
            beltsPostNameArray = beltsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get quivers postnames, split and store as array
            XmlNode quiversPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/quivers");
            string quiversPostNamesString = quiversPostNamesNode.InnerXml;
            quiversPostNameArray = quiversPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get axes postnames, split and store as array
            XmlNode axesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/axes");
            string axesPostNamesString = axesPostNamesNode.InnerXml;
            axesPostNameArray = axesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get maces postnames, split and store as array
            XmlNode macesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/maces");
            string macesPostNamesString = macesPostNamesNode.InnerXml;
            macesPostNameArray = macesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get sceptres postnames, split and store as array
            XmlNode sceptresPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/sceptres");
            string sceptresPostNamesString = sceptresPostNamesNode.InnerXml;
            sceptresPostNameArray = sceptresPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get staves postnames, split and store as array
            XmlNode stavesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/staves");
            string stavesPostNamesString = stavesPostNamesNode.InnerXml;
            stavesPostNameArray = stavesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get swords postnames, split and store as array
            XmlNode swordsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/swords");
            string swordsPostNamesString = swordsPostNamesNode.InnerXml;
            swordsPostNameArray = swordsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get daggers postnames, split and store as array
            XmlNode daggersPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/daggers");
            string daggersPostNamesString = daggersPostNamesNode.InnerXml;
            daggersPostNameArray = daggersPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get bows postnames, split and store as array
            XmlNode bowsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/bows");
            string bowsPostNamesString = bowsPostNamesNode.InnerXml;
            bowsPostNameArray = bowsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get wands postnames, split and store as array
            XmlNode wandsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/wands");
            string wandsPostNamesString = wandsPostNamesNode.InnerXml;
            wandsPostNameArray = wandsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            // Get foci postnames, split and store as array
            XmlNode fociPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/foci");
            string fociPostNamesString = fociPostNamesNode.InnerXml;
            fociPostNameArray = fociPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);
        }

        // Returns a random name depending on item type
        public string CreateRandomName(EquipableItem equipableItem)
        {
            // Get random preName
            string preName = preNamesArray[game.random.Next(0, preNamesArray.Length - 1)];

            // Get random postName according to item type
            string postName = "";
            if (equipableItem is IWeapon)
            {
                IWeapon equipableWeapon = equipableItem as IWeapon;
                if (equipableWeapon.WeaponType == WeaponTypes.Axe)
                    postName = axesPostNameArray[game.random.Next(0, axesPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Bow)
                    postName = bowsPostNameArray[game.random.Next(0, bowsPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Dagger)
                    postName = daggersPostNameArray[game.random.Next(0, daggersPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Mace)
                    postName = macesPostNameArray[game.random.Next(0, macesPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Sceptre)
                    postName = sceptresPostNameArray[game.random.Next(0, sceptresPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Staff)
                    postName = stavesPostNameArray[game.random.Next(0, stavesPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Sword)
                    postName = swordsPostNameArray[game.random.Next(0, swordsPostNameArray.Length - 1)];
                else if (equipableWeapon.WeaponType == WeaponTypes.Wand)
                    postName = wandsPostNameArray[game.random.Next(0, wandsPostNameArray.Length - 1)];
            }
            else if (equipableItem is IArmour)
            {
                if (equipableItem.EquipmentLocation == EquipmentLocations.Chest)
                    postName = chestArmoursPostNamesArray[game.random.Next(0, chestArmoursPostNamesArray.Length - 1)];
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Feet)
                    postName = feetArmoursPostNamesArray[game.random.Next(0, feetArmoursPostNamesArray.Length - 1)];
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Hands)
                    postName = handsArmoursPostNamesArray[game.random.Next(0, handsArmoursPostNamesArray.Length - 1)];
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Head)
                    postName = headArmoursPostNamesArray[game.random.Next(0, headArmoursPostNamesArray.Length - 1)];
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Legs)
                    postName = legsArmoursPostNameArray[game.random.Next(0, legsArmoursPostNameArray.Length - 1)];
                else if (equipableItem.EquipmentLocation == EquipmentLocations.OffHand)
                {
                    IArmour equipableArmor = equipableItem as IArmour;
                    if (equipableArmor.ArmourStyle == ArmourStyles.Light)
                        postName = soulShieldsPostNamesArray[game.random.Next(0, soulShieldsPostNamesArray.Length - 1)];
                    else
                        postName = otherShieldsPostNamesArray[game.random.Next(0, otherShieldsPostNamesArray.Length - 1)];
                }
            }
            else if (equipableItem is IAccessory)
            {
                IAccessory equipableAccessory = equipableItem as IAccessory;
                if (equipableAccessory.AccessoryType == AccessoryTypes.Belt)
                    postName = beltsPostNameArray[game.random.Next(0, beltsPostNameArray.Length - 1)];
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Focus)
                    postName = fociPostNameArray[game.random.Next(0, fociPostNameArray.Length - 1)];
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Quiver)
                    postName = quiversPostNameArray[game.random.Next(0, quiversPostNameArray.Length - 1)];
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Ring)
                    postName = ringsPostNameArray[game.random.Next(0, ringsPostNameArray.Length - 1)]; 
            }

            // Return combined preName and PostName
            return preName + " " + postName;
        }
    }
}
