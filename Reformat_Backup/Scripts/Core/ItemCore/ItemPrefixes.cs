﻿using UnityEngine;

using System.Collections.Generic;
using Rogue2018.Core.ItemCore.Prefixes;
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    public class ItemPrefixes : ItemAffixes
    {
        // Prefixes list
        public List<Prefix> prefixesList;

        // Constructor
        private static Game game;
        public ItemPrefixes()
        {
            game = Game.GET;
            prefixesList = new List<Prefix>();

            // Add Prefixes to the list
            prefixesList.Add(new DefenceChancePrefix());
            prefixesList.Add(new ElementalDamagePrefix());
            prefixesList.Add(new FlatDefenceValuePrefix());
            prefixesList.Add(new FlatPhysicalDamagePrefix());
            prefixesList.Add(new MaxHealthPrefix());
            prefixesList.Add(new MaxManaPrefix());
            prefixesList.Add(new PhysicalDamagePrefix());
            prefixesList.Add(new SpeedPrefix());
            prefixesList.Add(new SpellDamagePrefix());

            SetupEBTPrefixLists(prefixesList);
        }

        // Method for getting a random prefix from the list given the item's current prefixes, item level and ebt
        public Prefix GetRandomPrefix(
            List<Prefix> currentPrefixList,
            int itemLevel,
            EquipableItemBaseTypes ebt
            )
        {
            // Get prefix list for equipable base type
            List<Prefix> prefixListForEBT = GetPrefixForEBT(ebt);

            // Create empty modified prefix list
            List <Prefix> modifiedPrefixesList = new List<Prefix>();

            // Check entries in prefix list for matches in the item's current prefix list, add to modified list if there is no match
            for (int i = 0; i < prefixListForEBT.Count; i++)
            {
                bool currentListContainsMatch = false;
                foreach (Prefix currentPrefix in currentPrefixList)
                {
                    if (currentPrefix.GetType().Name == prefixListForEBT[i].GetType().Name)
                        currentListContainsMatch = true;
                }
                if (!currentListContainsMatch)
                    modifiedPrefixesList.Add(prefixListForEBT[i]);
            }

            // Select suffix at random index from modified suffix list
            int randomIndex = game.random.Next(0, modifiedPrefixesList.Count - 1);
            Prefix selectedSuffix = modifiedPrefixesList[randomIndex];

            // Deep copy, construct and return the selected suffix
            Prefix returnPrefix = game.tools.DeepCopy<Prefix>(modifiedPrefixesList[randomIndex]);
            returnPrefix.Construct(itemLevel);
            return returnPrefix;
        }
    }
}