﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class CrudeShank : Weapon
    {
        public CrudeShank()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Dagger;
            Name = "Crude Shank";
            Description = "A crude shank.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Dagger;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 2;
            HitChance = 20;
            Implicit = new CriticalChanceImplicit();
        }
    }
}