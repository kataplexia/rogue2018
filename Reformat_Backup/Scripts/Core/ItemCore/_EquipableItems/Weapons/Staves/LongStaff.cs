﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class LongStaff : Weapon
    {
        public LongStaff()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Staff;
            Name = "Long Staff";
            Description = "A long staff.";
            WeaponStyle = WeaponStyles.TwoHanded;
            WeaponType = WeaponTypes.Staff;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 6;
            HitChance = 15;
            Implicit = new SpellCriticalChanceImplicit();
        }
    }
}