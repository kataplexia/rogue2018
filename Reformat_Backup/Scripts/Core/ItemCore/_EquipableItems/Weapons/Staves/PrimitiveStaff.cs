﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PrimitiveStaff : Weapon
    {
        public PrimitiveStaff()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Staff;
            Name = "Primitive Staff";
            Description = "A primitive staff.";
            WeaponStyle = WeaponStyles.TwoHanded;
            WeaponType = WeaponTypes.Staff;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 5;
            HitChance = 15;
            Implicit = new SpellCriticalChanceImplicit();
        }
    }
}