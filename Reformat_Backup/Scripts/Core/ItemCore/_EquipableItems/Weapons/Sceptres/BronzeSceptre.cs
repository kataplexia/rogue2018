﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class BronzeSceptre : Weapon
    {
        public BronzeSceptre()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Sceptre;
            Name = "Bronze Sceptre";
            Description = "A bronze sceptre.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Sceptre;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 4;
            HitChance = 20;
            Implicit = new ElementalDamageImplicit();
        }
    }
}