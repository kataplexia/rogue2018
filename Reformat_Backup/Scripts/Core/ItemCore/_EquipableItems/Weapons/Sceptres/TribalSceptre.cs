﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class TribalSceptre : Weapon
    {
        public TribalSceptre()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Sceptre;
            Name = "Tribal Sceptre";
            Description = "A tribal sceptre.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Sceptre;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 3;
            HitChance = 20;
            Implicit = new ElementalDamageImplicit();
        }
    }
}