﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class WoodenMaul : Weapon
    {
        public WoodenMaul()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.TwoHandedMace;
            Name = "Wooden Maul";
            Description = "A wooden maul.";
            WeaponStyle = WeaponStyles.TwoHanded;
            WeaponType = WeaponTypes.Mace;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 6;
            HitChance = 15;
            Implicit = new StunChanceImplicit();
        }
    }
}