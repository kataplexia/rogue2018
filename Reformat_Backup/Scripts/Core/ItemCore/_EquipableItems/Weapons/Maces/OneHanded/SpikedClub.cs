﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SpikedClub : Weapon
    {
        public SpikedClub()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedMace;
            Name = "Spiked Club";
            Description = "A spiked club.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Mace;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 4;
            HitChance = 20;
            Implicit = new StunChanceImplicit();
        }
    }
}