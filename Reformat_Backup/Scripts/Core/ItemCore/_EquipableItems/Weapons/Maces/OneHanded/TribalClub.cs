﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class TribalClub : Weapon
    {
        public TribalClub()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedMace;
            Name = "Tribal Club";
            Description = "A tribal club.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Mace;
            DamageType = DamageTypes.Blunt;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 3;
            HitChance = 20;
            Implicit = new StunChanceImplicit();
        }
    }
}