﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class QuartzWand : Weapon
    {
        public QuartzWand()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Wand;
            Name = "Quartz Wand";
            Description = "A quartz wand.";
            WeaponStyle = WeaponStyles.Ranged;
            WeaponType = WeaponTypes.Wand;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 3;
            HitChance = 10;
            Implicit = new SpellCriticalDamageImplicit();
        }
    }
}