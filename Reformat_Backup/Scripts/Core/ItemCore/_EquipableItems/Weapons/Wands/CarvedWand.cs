﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class CarvedWand : Weapon
    {
        public CarvedWand()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Wand;
            Name = "Carved Wand";
            Description = "A carved wand.";
            WeaponStyle = WeaponStyles.Ranged;
            WeaponType = WeaponTypes.Wand;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 2;
            HitChance = 10;
            Implicit = new SpellCriticalDamageImplicit();
        }
    }
}