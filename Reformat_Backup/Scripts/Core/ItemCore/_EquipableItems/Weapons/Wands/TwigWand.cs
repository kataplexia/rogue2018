﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class TwigWand : Weapon
    {
        public TwigWand()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Wand;
            Name = "Twig Wand";
            Description = "A twig wand.";
            WeaponStyle = WeaponStyles.Ranged;
            WeaponType = WeaponTypes.Wand;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 1;
            HitChance = 10;
            Implicit = new SpellCriticalDamageImplicit();
        }
    }
}