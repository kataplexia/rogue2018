﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class LongBow : Weapon
    {
        public LongBow()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Bow;
            Name = "Long Bow";
            Description = "A long bow.";
            WeaponStyle = WeaponStyles.Ranged;
            WeaponType = WeaponTypes.Bow;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 4;
            HitChance = 15;
        }
    }
}