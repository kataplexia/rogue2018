﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class ShortBow : Weapon
    {
        public ShortBow()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Bow;
            Name = "Short Bow";
            Description = "A short bow.";
            WeaponStyle = WeaponStyles.Ranged;
            WeaponType = WeaponTypes.Bow;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 3;
            HitChance = 15;
        }
    }
}