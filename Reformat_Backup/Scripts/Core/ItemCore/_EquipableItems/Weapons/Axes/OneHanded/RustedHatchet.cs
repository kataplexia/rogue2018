﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class RustedHatchet : Weapon
    {
        public RustedHatchet()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedAxe;
            Name = "Rusted Hatchet";
            Description = "A rusted hatchet.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Axe;
            DamageType = DamageTypes.Cut;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 4;
            HitChance = 25;
        }
    }
}