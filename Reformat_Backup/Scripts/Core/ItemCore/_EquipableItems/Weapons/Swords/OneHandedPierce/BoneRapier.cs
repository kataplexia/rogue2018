﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class BoneRapier : Weapon
    {
        public BoneRapier()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedSwordPierce;
            Name = "Bone Rapier";
            Description = "A bone rapier.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Sword;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 2;
            HitChance = 25;
            Implicit = new CriticalDamageImplicit();
        }
    }
}