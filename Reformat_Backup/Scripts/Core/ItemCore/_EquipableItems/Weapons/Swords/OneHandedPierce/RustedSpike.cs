﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class RustedSpike : Weapon
    {
        public RustedSpike()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedSwordPierce;
            Name = "Rusted Spike";
            Description = "A rusted spike.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Sword;
            DamageType = DamageTypes.Pierce;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 1;
            HitChance = 25;
            Implicit = new CriticalDamageImplicit();
        }
    }
}