﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class RustedSword : Weapon
    {
        public RustedSword()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedSwordGeneric;
            Name = "Rusted Sword";
            Description = "A rusted sword.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Sword;
            DamageType = DamageTypes.Cut;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 2;
            HitChance = 20;
            Implicit = new HitChanceImplicit();
        }
    }
}