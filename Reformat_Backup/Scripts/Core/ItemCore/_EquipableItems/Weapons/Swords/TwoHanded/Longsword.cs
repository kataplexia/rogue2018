﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class LongSword : Weapon
    {
        public LongSword()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.TwoHandedSword;
            Name = "Longsword";
            Description = "A longsword.";
            WeaponStyle = WeaponStyles.TwoHanded;
            WeaponType = WeaponTypes.Sword;
            DamageType = DamageTypes.Cut;
            EquipmentLocation = EquipmentLocations.MainHand;
            PhysicalDamage = 5;
            HitChance = 15;
            Implicit = new HitChanceImplicit();
        }
    }
}