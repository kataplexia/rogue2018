﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SerratedQuiver : Accessory
    {
        public SerratedQuiver()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Quiver;
            Name = "Serrated Arrow Quiver";
            Description = "A quiver of serrated arrows.";
            EquipmentLocation = EquipmentLocations.OffHand;
            AccessoryType = AccessoryTypes.Quiver;
            Implicit = new FlatPhysicalDamageImplicit();
        }
    }
}