﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class Tome : Accessory
    {
        public Tome()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Focus;
            Name = "Tome";
            Description = "A weathered tome.";
            EquipmentLocation = EquipmentLocations.OffHand;
            AccessoryType = AccessoryTypes.Focus;
            Implicit = new SpellCriticalDamageImplicit();
        }
    }
}