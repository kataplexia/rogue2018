﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class ThornedRing : Accessory
    {
        public ThornedRing()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Ring;
            Name = "Thorned Ring";
            Description = "A wooden ring, covered in thorns. Wearing it may be slightly uncomfortable.";
            EquipmentLocation = EquipmentLocations.Ring;
            AccessoryType = AccessoryTypes.Ring;
            Implicit = new FlatPhysicalDamageImplicit();
        }
    }
}