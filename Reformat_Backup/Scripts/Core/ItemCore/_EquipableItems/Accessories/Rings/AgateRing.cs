﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class AgateRing : Accessory
    {
        public AgateRing()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Ring;
            Name = "Agate Ring";
            Description = "An ornate agate ring.";
            EquipmentLocation = EquipmentLocations.Ring;
            AccessoryType = AccessoryTypes.Ring;
            Implicit = new MaxManaImplicit();
        }
    }
}