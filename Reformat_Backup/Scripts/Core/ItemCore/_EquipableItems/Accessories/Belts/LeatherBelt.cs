﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class LeatherBelt : Accessory
    {
        public LeatherBelt()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Belt;
            Name = "Leather Belt";
            Description = "A belt made of leather.";
            EquipmentLocation = EquipmentLocations.Waist;
            AccessoryType = AccessoryTypes.Belt;
            Implicit = new MaxHealthImplicit();
        }
    }
}