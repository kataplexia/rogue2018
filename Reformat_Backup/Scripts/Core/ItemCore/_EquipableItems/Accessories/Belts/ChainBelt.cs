﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class ChainBelt : Accessory
    {
        public ChainBelt()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Belt;
            Name = "Chain Belt";
            Description = "A belt made of woven chain.";
            EquipmentLocation = EquipmentLocations.Waist;
            AccessoryType = AccessoryTypes.Belt;
            Implicit = new DefenceChanceImplicit();
        }
    }
}