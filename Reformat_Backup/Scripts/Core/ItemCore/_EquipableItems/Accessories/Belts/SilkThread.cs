﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SilkThread : Accessory
    {
        public SilkThread()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.Belt;
            Name = "Silk Thread";
            Description = "A fine belt made of silk.";
            EquipmentLocation = EquipmentLocations.Waist;
            AccessoryType = AccessoryTypes.Belt;
            Implicit = new SpellDamageImplicit();
        }
    }
}