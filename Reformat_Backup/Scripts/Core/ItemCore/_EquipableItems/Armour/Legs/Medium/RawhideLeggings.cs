﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class RawhideLeggings : Armour
    {
        public RawhideLeggings()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumLegsArmour;
            Name = "Rawhide Leggings";
            Description = "A pair of rawhide leggings.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Legs;
            DefenceValue = 3;
            DefenceChance = 6;
            SpeedModifier = -2;
        }
    }
}