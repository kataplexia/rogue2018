﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class DeerskinLeggings : Armour
    {
        public DeerskinLeggings()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumLegsArmour;
            Name = "Deerskin Leggings";
            Description = "A pair of deerskin leggings.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Legs;
            DefenceValue = 4;
            DefenceChance = 6;
            SpeedModifier = -2;
        }
    }
}