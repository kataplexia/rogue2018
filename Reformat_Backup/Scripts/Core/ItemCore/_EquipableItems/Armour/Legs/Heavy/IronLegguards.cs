﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class IronLegguards : Armour
    {
        public IronLegguards()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyLegsArmour;
            Name = "Iron Legguards";
            Description = "A pair of iron legguards.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Legs;
            DefenceValue = 3;
            DefenceChance = 8;
            SpeedModifier = -3;
        }
    }
}