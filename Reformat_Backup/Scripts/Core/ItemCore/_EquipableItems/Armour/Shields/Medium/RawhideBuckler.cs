﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class RawhideBuckler : Armour
    {
        public RawhideBuckler()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumShield;
            Name = "Rawhide Buckler";
            Description = "A rawhide buckler.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.OffHand;
            DefenceValue = 8;
            DefenceChance = 10;
            SpeedModifier = -5;
            Implicit = new SpeedImplicit();
        }
    }
}