﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class TwigSoulShield : Armour
    {
        public TwigSoulShield()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightShield;
            Name = "Twig Soul Shield";
            Description = "A twig soul shield.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.OffHand;
            DefenceValue = 6;
            DefenceChance = 8;
            SpeedModifier = -4;
            Implicit = new SpellDamageImplicit();
        }
    }
}