﻿using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class BoneSoulShield : Armour
    {
        public BoneSoulShield()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightShield;
            Name = "Bone Soul Shield";
            Description = "A bone soul shield.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.OffHand;
            DefenceValue = 10;
            DefenceChance = 8;
            SpeedModifier = -4;
            Implicit = new SpellDamageImplicit();
        }
    }
}