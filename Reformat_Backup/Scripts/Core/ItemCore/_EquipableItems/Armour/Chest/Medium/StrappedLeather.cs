﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class StrappedLeather : Armour
    {
        public StrappedLeather()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumChestArmour;
            Name = "Strapped Leather";
            Description = "Chestware comprised of strapped leather pieces.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 3;
            DefenceChance = 6;
            SpeedModifier = -2;
        }
    }
}