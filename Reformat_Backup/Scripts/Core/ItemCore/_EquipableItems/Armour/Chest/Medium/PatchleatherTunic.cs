﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PatchleatherTunic : Armour
    {
        public PatchleatherTunic()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumChestArmour;
            Name = "Patchleather Tunic";
            Description = "A patchleather tunic.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 4;
            DefenceChance = 6;
            SpeedModifier = -2;
        }
    }
}