﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class CopperPlate : Armour
    {
        public CopperPlate()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyChestArmour;
            Name = "Copper Plate";
            Description = "A copper plate.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 5;
            DefenceChance = 8;
            SpeedModifier = -3;
        }
    }
}