﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class ChestPlate : Armour
    {   
        public ChestPlate()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyChestArmour;
            Name = "Chest Plate";
            Description = "A chest plate.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 4;
            DefenceChance = 8;
            SpeedModifier = -3;
        }
    }
}