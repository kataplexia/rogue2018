﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PlateVest : Armour
    {
        public PlateVest()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyChestArmour;
            Name = "Plate Vest";
            Description = "A plate vest.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 3;
            DefenceChance = 8;
            SpeedModifier = -3;
        }
    }
}