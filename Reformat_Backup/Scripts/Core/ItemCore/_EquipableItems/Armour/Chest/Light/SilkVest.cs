﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SilkVest : Armour
    {
        public SilkVest()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightChestArmour;
            Name = "Silk Vest";
            Description = "A silk vest.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 2;
            DefenceChance = 4;
            SpeedModifier = -1;
        }
    }
}