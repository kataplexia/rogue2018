﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class ScholarsRobe : Armour
    {
        public ScholarsRobe()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightChestArmour;
            Name = "Scholar's Robe";
            Description = "A scholar's robe.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Chest;
            DefenceValue = 3;
            DefenceChance = 4;
            SpeedModifier = -1;
        }
    }
}