﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PatchleatherGloves : Armour
    {
        public PatchleatherGloves()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumHandsArmour;
            Name = "Patchleather Gloves";
            Description = "A pair of patchleather gloves.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Hands;
            DefenceValue = 2;
            DefenceChance = 3;
            SpeedModifier = -2;
        }
    }
}