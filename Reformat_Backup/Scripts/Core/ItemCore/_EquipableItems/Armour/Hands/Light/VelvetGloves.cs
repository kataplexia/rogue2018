﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class VelvetGloves : Armour
    {
        public VelvetGloves()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightHandsArmour;
            Name = "Velvet Gloves";
            Description = "A pair of velvet gloves.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Hands;
            DefenceValue = 3;
            DefenceChance = 1;
            SpeedModifier = -1;
        }
    }
}