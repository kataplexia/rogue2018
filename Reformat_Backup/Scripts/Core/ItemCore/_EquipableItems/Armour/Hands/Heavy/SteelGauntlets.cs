﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SteelGauntlets : Armour
    {
        public SteelGauntlets()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyHandsArmour;
            Name = "Steel Gauntlets";
            Description = "A pair of steel gauntlets.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Hands;
            DefenceValue = 4;
            DefenceChance = 5;
            SpeedModifier = -3;
        }
    }
}