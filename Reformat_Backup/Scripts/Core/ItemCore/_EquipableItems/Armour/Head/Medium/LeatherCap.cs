﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class LeatherCap : Armour
    {
        public LeatherCap()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumHeadArmour;
            Name = "Leather Cap";
            Description = "A leather cap.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Head;
            DefenceValue = 2;
            DefenceChance = 3;
            SpeedModifier = -2;
        }
    }
}