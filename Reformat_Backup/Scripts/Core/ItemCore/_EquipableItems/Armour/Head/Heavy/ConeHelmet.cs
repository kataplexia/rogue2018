﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class ConeHelmet : Armour
    {
        public ConeHelmet()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyHeadArmour;
            Name = "Cone Helmet";
            Description = "A cone helmet.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Head;
            DefenceValue = 4;
            DefenceChance = 5;
            SpeedModifier = -3;
        }
    }
}