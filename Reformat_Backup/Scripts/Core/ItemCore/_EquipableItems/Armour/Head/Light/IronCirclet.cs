﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class IronCirclet : Armour
    {
        public IronCirclet()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightHeadArmour;
            Name = "Iron Circlet";
            Description = "An iron circlet.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Head;
            DefenceValue = 2;
            DefenceChance = 1;
            SpeedModifier = -1;
        }
    }
}