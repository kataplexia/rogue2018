﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SteelGreaves : Armour
    {
        public SteelGreaves()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyFeetArmour;
            Name = "Steel Greaves";
            Description = "A pair of steel greaves.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 4;
            DefenceChance = 5;
            SpeedModifier = -3;
        }
    }
}