﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class IronGreaves : Armour
    {
        public IronGreaves()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyFeetArmour;
            Name = "Iron Greaves";
            Description = "A pair of iron greaves.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 3;
            DefenceChance = 5;
            SpeedModifier = -3;
        }
    }
}