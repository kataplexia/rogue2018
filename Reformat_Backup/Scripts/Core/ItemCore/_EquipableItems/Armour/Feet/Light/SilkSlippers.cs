﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class SilkSlippers : Armour
    {
        public SilkSlippers()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightFeetArmour;
            Name = "Silk Slippers";
            Description = "A pair of silk slippers.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 2;
            DefenceChance = 1;
            SpeedModifier = -1;
        }
    }
}