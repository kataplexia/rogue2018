﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class StitchedSlippers : Armour
    {
        public StitchedSlippers()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightFeetArmour;
            Name = "Stitched Slippers";
            Description = "A pair of stitched slippers.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 1;
            DefenceChance = 1;
            SpeedModifier = -1;
        }
    }
}