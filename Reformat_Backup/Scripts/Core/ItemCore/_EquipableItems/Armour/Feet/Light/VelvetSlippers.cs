﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class VelvetSlippers : Armour
    {
        public VelvetSlippers()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.LightFeetArmour;
            Name = "Velvet Slippers";
            Description = "A pair of velvet slippers.";
            ArmourStyle = ArmourStyles.Light;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 3;
            DefenceChance = 1;
            SpeedModifier = -1;
        }
    }
}