﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class RawhideBoots : Armour
    {
        public RawhideBoots()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumFeetArmour;
            Name = "Rawhide Boots";
            Description = "A pair of rawhide boots.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Feet;
            DefenceValue = 3;
            DefenceChance = 3;
            SpeedModifier = -2;
        }
    }
}