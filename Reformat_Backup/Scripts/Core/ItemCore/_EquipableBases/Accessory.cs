﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class Accessory : EquipableItem, IAccessory
    {
        private AccessoryTypes _accessoryType;
        private string _accessoryTypeString;

        public AccessoryTypes AccessoryType { get { return _accessoryType; } set { _accessoryType = value; } }
        public string AccessoryTypeString { get { return _accessoryTypeString; } set { _accessoryTypeString = value; } }

        // Constructor
        public override void Construct()
        {
            // IEquipable
            EquipmentLocationString = SetupEquipmentLocationString(EquipmentLocation);

            // IAccessory
            AccessoryTypeString = SetupAccessoryTypeString(AccessoryType);

            // Setup item's affixes
            SetupAffixes();
        }
    }
}