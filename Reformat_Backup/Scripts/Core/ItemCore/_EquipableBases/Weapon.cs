﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class Weapon : EquipableItem, IWeapon
    {
        // IWeapon properties
        private WeaponStyles    _weaponStyle;
        private WeaponTypes     _weaponType;
        private DamageTypes     _damageType;
        private string          _weaponStyleString;
        private string          _weaponTypeString;
        private string          _damageTypeString;
        private int             _physicalDamage;
        private int             _hitChance;

        public WeaponStyles WeaponStyle { get { return _weaponStyle; } set { _weaponStyle = value; } }
        public WeaponTypes WeaponType   { get { return _weaponType; } set { _weaponType = value; } }
        public DamageTypes DamageType   { get { return _damageType; } set { _damageType = value; } }
        public string WeaponStyleString { get { return _weaponStyleString; } set { _weaponStyleString = value; } }
        public string WeaponTypeString  { get { return _weaponTypeString; } set { _weaponTypeString = value; } }
        public string DamageTypeString  { get { return _damageTypeString; } set { _damageTypeString = value; } }
        public int PhysicalDamage       { get { return _physicalDamage; } set { _physicalDamage = value; } }
        public int HitChance            { get { return _hitChance; } set { _hitChance = value; } }

        // Constructor
        public override void Construct()
        {
            // IEquipable
            EquipmentLocationString = SetupEquipmentLocationString(EquipmentLocation);

            // IWeapon
            WeaponStyleString   = SetupWeaponStyleString(WeaponStyle);
            WeaponTypeString    = SetupWeaponTypeString(WeaponType);
            DamageTypeString    = SetupDamageTypeString(DamageType);

            // Setup item's affixes
            SetupAffixes();
        }
    }
}