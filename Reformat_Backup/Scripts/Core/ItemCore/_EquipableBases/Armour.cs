﻿using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class Armour : EquipableItem, IArmour
    {
        // IArmour properties
        private ArmourStyles    _armourStyle;
        private string          _armourStyleString;
        private int             _defenceValue;
        private int             _defenceChance;
        private int             _speedModifier;

        public ArmourStyles ArmourStyle { get { return _armourStyle; } set { _armourStyle = value; } }
        public string ArmourStyleString { get { return _armourStyleString; } set { _armourStyleString = value; } }
        public int DefenceValue         { get { return _defenceValue; } set { _defenceValue = value; } }
        public int DefenceChance        { get { return _defenceChance; } set { _defenceChance = value; } }
        public int SpeedModifier        { get { return _speedModifier; } set { _speedModifier = value; } }

        // Constructor
        public override void Construct()
        {
            // IEquipable
            EquipmentLocationString = SetupEquipmentLocationString(EquipmentLocation);

            // IArmour
            ArmourStyleString = SetupArmourStyleString(ArmourStyle);

            // Setup item's affixes
            SetupAffixes();
        }
    }
}
