﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Rogue2018.Core
{
    public class Boot : MonoBehaviour
    {
        // Method for awake boot
        private void Awake()
        {
            Screen.SetResolution(1600, 900, false);
            SceneManager.LoadScene("Game");
        }
    }
}