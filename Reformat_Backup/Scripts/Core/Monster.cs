﻿using System;
using UnityEngine;
using RogueSharp;
using Rogue2018.Systems;
using Rogue2018.Behaviours;
using Rogue2018.Interfaces;

namespace Rogue2018.Core
{
    public class Monster : Actor
    {
        // Properties
        public int? TurnsAlerted { get; set; }
        public GoalMapMoveAttack moveAndAttackBehaviour;
        public HealthBar healthBar;

        public Monster()
        {
            moveAndAttackBehaviour = new GoalMapMoveAttack();
        }

        // Method for performing an action
        public void PerformAction(CommandSystem commandSystem)
        {
            // Decide on type of action here
            IBehaviour behaviour = moveAndAttackBehaviour;
            behaviour.Act(this, commandSystem);
        }

        // Method for drawing monster stats
        public void DrawHealthBar(int xOffset, int yOffset)
        {
            if (healthBar == null)
                healthBar = new HealthBar(Name);

            if (!healthBar.IsEnabled)
                healthBar.IsEnabled = true;

            healthBar.UpdateHealthBarPosition(X, Y, xOffset, yOffset);
            healthBar.UpdateHealthBarValues(Health, MaxHealth);
            healthBar.Draw();
        }

        private static int yStart           = 15;
        private static int yMax             = 31;
        private static int healthBarWidth   = 14;
        public void DrawStats(int xOffset, int yOffset, int pos)
        {
            // Setup properties
            int layer = game._statsLayer;
            int yPos = yStart + (pos * 2);

            // Check if able to draw stats
            if (yPos > yMax)
                return;

            // Draw monster's symbol
            string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
            Cell phiCellSymbol = Display.CellAt(layer, 1 + xOffset, yPos);
            phiCellSymbol.SetContent(phiCellContent, Color.clear, DrawColorFOV);

            // Draw colon after monster's symbol
            Cell phiCellColon = Display.CellAt(layer, 2 + xOffset, yPos);
            phiCellColon.SetContent(":", Color.clear, Color.white);

            // Setup bar width
            int healthWidth = Convert.ToInt32(((double)Health / (double)MaxHealth) * healthBarWidth);

            // Setup colors
            Color fgHealthColor = game.swatch.PrimaryLighter;
            Color bgHealthColor = game.swatch.PrimaryDarkest;

            // Draw stats
            for (int x = 0; x <= healthBarWidth; x++)
            {
                if (x >= 1 && x <= Name.Length)
                {
                    // Printing Name with bar background
                    if (x <= healthWidth)
                    {
                        Cell phiCell = Display.CellAt(layer, x + 3 + xOffset, yPos);
                        phiCell.SetContent(Name.Substring(x - 1, 1), fgHealthColor, DrawColorFOV * 0.5f);
                    }
                    else
                    {
                        Cell phiCell = Display.CellAt(layer, x + 3 + xOffset, yPos);
                        phiCell.SetContent(Name.Substring(x - 1, 1), bgHealthColor, DrawColorFOV * 0.5f);
                    }
                }
                else
                {
                    // Printing Bar
                    if (x <= healthWidth)
                    {
                        Cell phiCell = Display.CellAt(layer, x + 3 + xOffset, yPos);
                        phiCell.SetContent(" ", fgHealthColor, Color.clear);
                    }
                    else
                    {
                        Cell phiCell = Display.CellAt(layer, x + 3 + xOffset, yPos);
                        phiCell.SetContent(" ", bgHealthColor, Color.clear);
                    }
                }
            }
        }
    }
}