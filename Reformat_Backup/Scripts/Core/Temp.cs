﻿using UnityEngine;

namespace Rogue2018.Core
{
    public class Temp : MonoBehaviour
    {
        public static Temp GET;
        private void Awake()
        {
            if (GET != null)
                GameObject.Destroy(GET);
            else
                GET = this;
            // DontDestroyOnLoad(this);
        }

        public void ClearTemp()
        {
            foreach (Transform child in transform)
                Destroy(child.gameObject);
        }
    }
}