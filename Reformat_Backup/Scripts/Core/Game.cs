﻿using System;
using System.Text;
using System.Collections;

using UnityEngine;

using RogueSharp;
using RogueSharp.Random;

using Rogue2018.Actors;
using Rogue2018.Systems;
using Rogue2018.UserInterface;
using Rogue2018.Core.ItemCore;
using Rogue2018.Core.MapCore;

namespace Rogue2018.Core
{
    public class Game : MonoBehaviour
    {
        // Mono Members
        public Swatch               swatch;
        public MiniMap              miniMap;

        // Members
        public Display              display                 { get; private set; }
        public Mouse                mouse                   { get; private set; }
        public UIManager            uiManager               { get; private set; }
        public Symbols              symbols                 { get; private set; }
        public GlyphStrings         glyphStrings            { get; private set; }
        public Tools                tools                   { get; private set; }
        public InputSystem          inputSystem             { get; private set; }
        public CommandSystem        commandSystem           { get; private set; }
        public MessageSystem        messageSystem           { get; private set; }
        public SchedulingSystem     schedulingSystem        { get; private set; }
        public ItemPrefixes         itemPrefixes            { get; private set; }
        public ItemSuffixes         itemSuffixes            { get; private set; }
        public ItemGenerator        itemGenerator           { get; private set; }
        public ItemNameGenerator    itemNameGenerator       { get; private set; }
        public ItemCollection       itemCollection          { get; private set; }
        public Player               player                  { get; set; }
        public MapCollection        mapCollection           { get; private set; }
        public RogueMap             rogueMap                { get; private set; }

        // Random Properties
        public int                  seed                    { get; private set; }
        public IRandom              random                  { get; private set; }

        // Properties
        public bool                 isInitialized           { get; private set; }
        public int                  actNumber               { get; private set; }
        public string               mapName                 { get; private set; }

        private static bool         _drawMapConsole         = true;
        private static bool         _drawRequired           = false;

        // Map Console properties
        public readonly int         _mapConsoleWidth        = 120;
        public readonly int         _mapConsoleHeight       = 90;
        public static int           _mapConsoleDrawXOffset;
        public static int           _mapConsoleDrawYOffset;

        // Stats Console properties
        public readonly int         _statsConsoleWidth      = 36;
        public readonly int         _statsConsoleHeight     = 30;

        // Message Space
        public readonly int         _messageConsolWidth     = 36;
        public readonly int         _messageConsolHeight    = 19;

        // Layer Values
        public readonly int         _mapLayer               = 0;
        public readonly int         _actorLayer             = 0;
        public readonly int         _messageLayer           = 0;
        public readonly int         _statsLayer             = 0;

        // Awake with Game as singleton and do not destroy
        public static Game GET;
        private void Awake()
        {
            isInitialized = false;
            if (GET != null)
                GameObject.Destroy(GET);
            else
                GET = this;
            // DontDestroyOnLoad(this);
        }

        // Routine for starting the game
        public IEnumerator Start()
        {
            // Wait until display has initialized
            while (!Display.IsInitialized())
                yield return null;

            // Generate seed
            seed                = (int)DateTime.UtcNow.Ticks;
            random              = new DotNetRandom(seed);

            // Get members
            display             = Display.GET;
            mouse               = Mouse.GET;
            uiManager           = UIManager.GET;

            // Instantiate members
            symbols             = new Symbols();
            glyphStrings        = new GlyphStrings();
            tools               = new Tools();
            inputSystem         = new InputSystem();
            commandSystem       = new CommandSystem();
            messageSystem       = new MessageSystem();
            schedulingSystem    = new SchedulingSystem();
            itemPrefixes        = new ItemPrefixes();
            itemSuffixes        = new ItemSuffixes();
            itemGenerator       = new ItemGenerator();
            itemNameGenerator   = new ItemNameGenerator();
            itemCollection      = new ItemCollection();
            mapCollection       = new MapCollection();

            // Initialize act and map level values
            actNumber = 1;
            mapName = mapCollection.GetFirstMapNameOfAct(actNumber);

            // Generate map
            GenerateMap();
            messageSystem.Add(ArrivedMessage(), Color.magenta);

            // Construct static members
            inputSystem.ConstructInputSystem();
            commandSystem.ConstructCommandSystem();
            messageSystem.ConstructMessageSystem();

            yield return new WaitForFixedUpdate();
            isInitialized = true;
            _drawRequired = true;
        }

        // TODO: add the ability to create maps using the different styles available
        // Method for generating the map
        private void GenerateMap()
        {
            Biomes currentBiome = mapCollection.GetBiome(actNumber, mapName);
            swatch.SetSwatchForBiome(currentBiome);
            glyphStrings.SetGlyphStringsForBiome(currentBiome);

            MapGenerator mapGenerator = new MapGenerator(actNumber, mapName);
            rogueMap = mapGenerator.CreateMap();

            rogueMap.UpdatePlayerFOV();
        }

        // Update is called once per frame
        private void Update()
        {
            // Check that PhiOS display and game is initialized
            if (!Display.IsInitialized() || !isInitialized)
                return;
            // Update drawable phiCells if required
            inputSystem.UpdateInput();
            if (_drawRequired)
            {
                UpdateDrawable();
                _drawRequired = false;
            }
        }

        public void ClearMapConsole()
        {
            tools.ClearConsole(_mapLayer, _mapConsoleWidth, _mapConsoleHeight, 0, 0);
        }

        // Method for starting the zoom routine
        public void StartZoomRoutine(int zoomLevel)
        {
            StartCoroutine(ZoomRoutine(zoomLevel));
        }

        // Routine for drawing before zooming in or out
        private IEnumerator ZoomRoutine(int zoomLevel)
        {
            SetDrawRequired(true);
            yield return new WaitForFixedUpdate();
            display.SetZoom(display.currentZoomLevel);
        }

        private void UpdateDrawable()
        {
            // TODO: Add flag for each of these consoles so they are only cleared when changes are made
            if (_drawMapConsole)
            {
                // Clear Map Console
                tools.ClearConsole(_mapLayer, _mapConsoleWidth, _mapConsoleHeight, 0, 0);

                // Calculate map console draw offsets
                _mapConsoleDrawXOffset = tools.GetMapConsoleDrawXOffset(player, _mapConsoleWidth);
                _mapConsoleDrawYOffset = tools.GetMapConsoleDrawYOffset(player, _mapConsoleHeight);

                // Draw map console
                rogueMap.Draw(_mapConsoleDrawXOffset, _mapConsoleDrawYOffset);

                // Draw player
                player.Draw(_mapConsoleDrawXOffset, _mapConsoleDrawYOffset, rogueMap);
            }

            // Clear Stats console
            tools.ClearConsole(_statsLayer, _statsConsoleWidth, _statsConsoleHeight, _mapConsoleWidth + 2, 3);

            // Draw stats console
            player.DrawStats(_mapConsoleWidth + 2, 3);
            // rogueMap.DrawStats(_mapConsoleWidth + 2, 0);
            rogueMap.DrawHealthBars(_mapConsoleDrawXOffset, _mapConsoleDrawYOffset);

            // Clear Message Console
            tools.ClearConsole(_messageLayer, _messageConsolWidth, _messageConsolHeight, _mapConsoleWidth + 2, _statsConsoleHeight + 9);

            // Draw message console
            messageSystem.Draw(_mapConsoleWidth + 2, _statsConsoleHeight + 9);

            // Draw mini map
            miniMap.Draw();
        }

        // Method for setting draw required
        public void SetDrawRequired(bool value)
        {
            _drawRequired = value;
        }

        public void SetDrawMapConsole(bool value)
        {
            _drawMapConsole = value;
        }

        // Method for moving to next map
        public void ChangeMap(string newMapName)
        {
            rogueMap.DestroyHealthBars();

            // Update the current map stored in the MapCollection
            mapCollection.StoreMap(actNumber, mapName, rogueMap, player);
            rogueMap.RemoveMonstersFromSchedule();

            mapName = newMapName;

            // Check for stored map
            if (mapCollection.CheckForStoredMap(actNumber, mapName))
            {
                Biomes currentBiome = mapCollection.GetBiome(actNumber, mapName);
                swatch.SetSwatchForBiome(currentBiome);
                glyphStrings.SetGlyphStringsForBiome(currentBiome);

                rogueMap = mapCollection.GetStoredMap(actNumber, mapName);
                rogueMap.AddMonstersToSchedule();

                Point storedPlayerPosition = mapCollection.GetStoredPlayerPosition(actNumber, mapName);
                player.X = storedPlayerPosition.X;
                player.Y = storedPlayerPosition.Y;

                rogueMap.UpdatePlayerFOV();
            }
            else
                GenerateMap();

            messageSystem = new MessageSystem();
            commandSystem = new CommandSystem();
            messageSystem.ConstructMessageSystem();
            commandSystem.ConstructCommandSystem();

            messageSystem.Add(ArrivedMessage(), Color.magenta);

            Audio.GET.QueueAudio(Audio.GET.playerStairs);
        }

        public string ArrivedMessage()
        {
            StringBuilder arrivedMessage = new StringBuilder();
            arrivedMessage.AppendFormat("{0} arrives at ", player.Name);

            string customMapName = mapCollection.GetMapProperties(actNumber, mapName).MapName;
            if (customMapName.Length > 0)
                arrivedMessage.Append(customMapName);
            else
                arrivedMessage.AppendFormat("Level {0}", mapName);

            return arrivedMessage.ToString();
        }

        // TODO: add more functionality
        // Method for handling player's death
        public void PlayerDeath()
        {
            Audio.GET.QueueAudio(Audio.GET.playerDeath);

            messageSystem.Add(string.Format("{0} was killed. Game over!", player.Name), Color.magenta, true);
        }
    }
}