﻿namespace Rogue2018.Interfaces
{
    public interface IUserInterface
    {
        string handle { get; set; }
        CellPropertiesList cellPropertiesList { get; set; }

        void Draw();
    }
}


