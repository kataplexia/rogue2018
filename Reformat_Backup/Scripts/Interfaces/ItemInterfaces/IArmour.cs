﻿namespace Rogue2018.Interfaces
{
    public enum ArmourStyles
    {
        None    = 0,
        Light   = 1,
        Medium  = 2,
        Heavy   = 3
    }

    public interface IArmour
    {
        ArmourStyles ArmourStyle    { get; set; }
        string ArmourStyleString    { get; set; }
        int DefenceValue            { get; set; }
        int DefenceChance           { get; set; }
        int SpeedModifier           { get; set; }
    }
}
