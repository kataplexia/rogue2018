﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rogue2018.Interfaces
{
    public interface IUsable
    {
        System.Type ParentType  { get; set; }
        int MaxStackCount       { get; set; }
        int CurrentStackCount   { get; set; }

        void Construct();
        void DrawStacks(int xOffset, int yOffset, Color fgColor);
        void Use();
        void Drop(bool dropAll = false);
    }
}