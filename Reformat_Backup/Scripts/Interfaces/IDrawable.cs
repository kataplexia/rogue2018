﻿using UnityEngine;
using RogueSharp;

namespace Rogue2018.Interfaces
{
	public interface IDrawable
	{
        // Properties
		Color DrawColor                 { get; set; }
        Color BackgroundDrawColor       { get; set; }
        Color DrawColorFOV              { get; set; }
        Color BackgroundDrawColorFOV    { get; set; }
        string Symbol                   { get; set; }
        string GlyphString              { get; set; }
		int X                           { get; set; }
		int Y                           { get; set; }

        // Method for drawing
		void Draw(int xOffset, int yOffset, IMap map);
	}
}
