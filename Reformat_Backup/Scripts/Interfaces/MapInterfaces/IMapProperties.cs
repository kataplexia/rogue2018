﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rogue2018.Core.MapCore
{
    public enum GenerationStyles
    {
        None        = 0,
        BorderOnly  = 1,
        Cellular    = 2,
        RandomRooms = 3,
        FromString  = 4
    }

    public enum Biomes
    {
        None    = 0,
        Dungeon = 1,
        Cave    = 2,
        Forest  = 3,
        Swamp   = 4
    }

    public interface IMapProperties
    {
        GenerationStyles GenerationStyle    { get; set; }
        Biomes Biome                        { get; set; }
    }
}