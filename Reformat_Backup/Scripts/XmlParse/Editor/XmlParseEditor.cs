﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Rogue2018.Systems;

[CustomEditor(typeof(XmlParse))]
public class XmlParseSystemEditor : Editor
{
    private string xmlPath      = "RexPaint/...";
    private int xOffset         = 0;
    private int yOffset         = 0;
    private int layer           = 0;
    private bool removeEmpty    = false;

    // InspectorGUI
    public override void OnInspectorGUI()
    {
        // Setup GUI
        DrawDefaultInspector();
        XmlParse xmlParse = (XmlParse)target;

        // GUI fields
        xmlPath = EditorGUILayout.TextField("Xml Path: ", xmlPath);
        xOffset = EditorGUILayout.IntField("X Offset: ", xOffset);
        yOffset = EditorGUILayout.IntField("Y Offset: ", yOffset);
        layer = EditorGUILayout.IntField("Layer: ", layer);
        removeEmpty = EditorGUILayout.Toggle("Remove Empty Cells: ", removeEmpty);

        // GUI button
        if (GUILayout.Button("Create Cell Properties List"))
        {
            if (CheckPath(xmlPath))
                xmlParse.ParseRexPaintXmlFile(xmlPath, xOffset, yOffset, layer, removeEmpty);
        }
    }

    // Returns true if a path is valid
    private bool CheckPath(string path)
    {
        var checkAsset = Resources.Load<TextAsset>(xmlPath);
        if (checkAsset == null)
        {
            Debug.LogError("Path does not contain a TextAsset.");
            return false;
        }
        return true;
    }
}
