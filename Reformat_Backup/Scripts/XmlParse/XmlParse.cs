﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEditor;

public class XmlParse : MonoBehaviour
{
    // Method for parsing a RexPaintXmlFile and drawing PhiCells using the data
    public void ParseRexPaintXmlFile(string xmlPath, int xOffset, int yOffset, int layer = 0, bool removeEmpty = false)
    {
        // Editor debug
        Debug.Log(string.Format("Parsing {0} with offset ({1}, {2}) and layer {3}.", xmlPath, xOffset, yOffset, layer));

        // Create prefab object
        GameObject workingPrefab = new GameObject();

        // Initialize cell properties list
        CellPropertiesList cellPropertiesList = workingPrefab.AddComponent<CellPropertiesList>();
        cellPropertiesList.propertiesList = new List<CellProperties>();

        // Load Xml data
        var xmlRawFile = Resources.Load<TextAsset>(xmlPath);
        string xmlData = xmlRawFile.text;
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(new StringReader(xmlData));

        // List of rows
        XmlNodeList nodeListRows = xmlDocument.SelectNodes("/image/data/row");
        // Debug.Log(string.Format("Number of rows: {0}", nodeListRows.Count));

        int rowCount = 0;
        foreach (XmlNode nodeRow in nodeListRows)
        {
            // List of cells
            XmlNodeList nodeListCells = nodeRow.SelectNodes("cell");
            // Debug.Log(string.Format("Row {0} has {1} cells.", rowCount, nodeListCells.Count));

            int cellCount = 0;
            foreach (XmlNode nodeCell in nodeListCells)
            {
                // Get node references
                XmlNode asciiNode = nodeCell.FirstChild;
                XmlNode fgdNode = asciiNode.NextSibling;
                XmlNode bkdNode = fgdNode.NextSibling;
                // Debug.Log("Ascii: " + ascii.InnerXml + ", fgd: " + fgd.InnerXml + ", bkd: " + bkd.InnerXml);

                // Create mapping
                TextAsset IBMGRAPH = (TextAsset)AssetDatabase.LoadAssetAtPath("Assets/Fonts/REXPaint_Fonts/mappings/IBMGRAPH.txt", typeof(TextAsset));
                Dictionary<int, int> ibmGraphMapping = BitmapFont.GetIBMGRAPHMapping(IBMGRAPH);

                // Convert asciiNode contents from string to CP437 int and convert to Unicode
                int asciiCode = Int32.Parse(asciiNode.InnerXml);
                string ascii = BitmapFont.CP437ToUnicode(asciiCode, ibmGraphMapping);

                // Convert fgdNode contents from hex to Color
                Color fgPhiCellColor = new Color();
                ColorUtility.TryParseHtmlString(fgdNode.InnerXml, out fgPhiCellColor);

                // Convert fgdNode contents from hex to Color
                Color bgPhiCellColor = new Color();
                ColorUtility.TryParseHtmlString(bkdNode.InnerXml, out bgPhiCellColor);

                // Setup cell properties
                CellProperties cellProperties = new CellProperties();
                cellProperties.X        = cellCount + xOffset;
                cellProperties.Y        = rowCount + yOffset;
                cellProperties.layer    = layer;
                cellProperties.symbol   = ascii[0];
                cellProperties.fgColor  = fgPhiCellColor;
                cellProperties.bgColor  = bgPhiCellColor;

                // Do not add the cell properties to the list if it is empty and removing empty cells
                if (IsEmptyCell(cellProperties))
                {
                    if (!removeEmpty)
                        cellPropertiesList.propertiesList.Add(cellProperties);
                }
                else
                    cellPropertiesList.propertiesList.Add(cellProperties);

                cellCount++;
            }
            rowCount++;
        }

        // Create prefab name and properties count
        int charPos = xmlPath.Length - 1;
        while (charPos > 0 && xmlPath[charPos] != '/')
            charPos--;
        string prefabName = xmlPath.Substring(charPos + 1) + "CellProperties";
        int propertiesCount = cellPropertiesList.propertiesList.Count;

        // Create prefab
        UnityEngine.Object tempPrefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/XmlParse/CellPropertiesPrefabs/" + prefabName + ".prefab");
        PrefabUtility.ReplacePrefab(workingPrefab, tempPrefab, ReplacePrefabOptions.ConnectToPrefab);
        GameObject.DestroyImmediate(workingPrefab);

        // Editor debug
        Debug.Log(String.Format("Parse succesful. {0} created with {1} CellProperties components.", prefabName, propertiesCount));
    }

    // Checks for empty cells
    public bool IsEmptyCell(CellProperties cellProperties)
    {
        char checkSymbol = cellProperties.symbol;
        Color checkFgColor = cellProperties.fgColor;
        Color checkBgColor = cellProperties.bgColor;
        if (checkSymbol == ' ' && checkFgColor == Color.black && checkBgColor == Color.black)
            return true;
        return false;
    }
}
#endif