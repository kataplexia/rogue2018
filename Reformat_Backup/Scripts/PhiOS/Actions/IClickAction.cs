﻿using UnityEngine;
using System.Collections;

public interface IClickAction {
    //void OnMouseDown();
    void OnMouseDown(int mouseButtonIndex);
}
