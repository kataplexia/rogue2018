﻿using UnityEngine;
using System.Collections;

public interface IHoverAction {
	void OnHoverEnter(int cellX, int cellY);
	void OnHoverExit();
}
