﻿using UnityEngine;
using System.Collections;

public class Example_Extended : MonoBehaviour
{
    public TextAsset xml;

    public IEnumerator Start()
    {
		// wait until display has initialized
		while (!Display.IsInitialized ())
		{
			yield return null;
		}

        StartCoroutine (RandomGridDisplay());
	}

	public IEnumerator RandomGridDisplay()
    {	
		// fill random cells every frame on layer 1
		while (Application.isPlaying)
        {
			for (int i = 0; i < 50; i++)
            {
                int xMin = (int) Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].xMin;
                int xMax = (int) Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].xMax;
                int yMin = (int) Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].yMin;
                int yMax = (int) Display.GET.zoomDisplayRects[Display.GET.currentZoomLevel].yMax;

				Cell cell = Display.CellAt(
					1,
					Random.Range(xMin, xMax),
					Random.Range(yMin, yMax));

				// random color and alpha
				Color color = Color.Lerp(Color.yellow, Color.green, Random.Range(0f, 1f));
				color = new Color(color.r, color.g, color.b, Random.Range(0f, 1f));

				cell.SetContent(
					Random.Range(0, 10) + "",
					Color.clear,
					color);
			}

            /*
            for (int i = 0; i < 50; i++)
            {
                Cell cell = Display.CellAt(
                    1,
                    Random.Range(Display.GetDisplayWidth() - 40, Display.GetDisplayWidth()),
                    Random.Range(0, Display.GetDisplayHeight()));

                // random color and alpha
                Color color = Color.Lerp(Color.red, Color.blue, Random.Range(0f, 1f));
                color = new Color(color.r, color.g, color.b, Random.Range(0f, 1f));

                cell.SetContent(
                    Random.Range(0, 10) + "",
                    Color.clear,
                    color);
            }
            */

            yield return null;
		}
	}
}
