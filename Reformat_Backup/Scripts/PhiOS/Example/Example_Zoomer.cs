﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example_Zoomer : MonoBehaviour {
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            if (Input.GetKeyDown(KeyCode.Z) && Display.GET.currentZoomLevel > 0)
            {
                Display.GET.SetZoom(--Display.GET.currentZoomLevel);
                // _drawRequired = true;
                return;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Z) && Display.GET.currentZoomLevel < Display.GET.zoomOrthographicSizes.Length - 1)
        {
            Display.GET.SetZoom(++Display.GET.currentZoomLevel);
            // _drawRequired = true;
            return;
        }
    }
}
