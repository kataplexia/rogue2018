﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class CellPropertiesList : MonoBehaviour
{
    public List<CellProperties> propertiesList;
}
