﻿using UnityEngine;

[System.Serializable]
public class CellProperties
{
    public int X;
    public int Y;
    public int layer;
    public char symbol;
    public Color fgColor;
    public Color bgColor;
}
