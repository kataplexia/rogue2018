﻿using RogueSharp.DiceNotation;
using Rogue2018.Core;

namespace Rogue2018.Actors.Monsters
{
    public class Kobold : Monster
    {
        // Constructor
        public static Kobold Create(int level)
        {
            int health = Dice.Roll("2D5");
            return new Kobold
            {
                Attack = Dice.Roll("1D3") + level / 3,
                HitChance = Dice.Roll("25D3"),
                Awareness = 10,
                Defence = Dice.Roll("1D3") + level / 3,
                DefenceChance = Dice.Roll("10D4"),
                Gold = Dice.Roll("5D5"),
                Health = health,
                MaxHealth = health,
                Name = "Kobold",
                Speed = 6,

                DrawColorFOV = Game.GET.swatch.Kobold,
                Symbol = game.symbols.Kobold,
                GlyphString = game.glyphStrings.Kobold
            };
        }
    }
}