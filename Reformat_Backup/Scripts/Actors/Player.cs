﻿using System.Collections.Generic;
using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore;

namespace Rogue2018.Actors
{
    public class Player : Actor
    {
        // Equipment properties
        public EquipableItem equipmentHead;
        public EquipableItem equipmentChest;
        public EquipableItem equipmentWaist;
        public EquipableItem equipmentLegs;
        public EquipableItem equipmentFeet;
        public EquipableItem equipmentHands;
        public EquipableItem equipmentMainHand;
        public EquipableItem equipmentOffHand;
        public EquipableItem equipmentLeftRing;
        public EquipableItem equipmentRightRing;

        // Inventory
        public List<IItem> inventoryList;

        // Constructor
        public Player()
        {
            Attack = 4;
            HitChance = 80;
            Awareness = 15;
            Defence = 2;
            DefenceChance = 40;
            Gold = 0;
            Health = 25;
            MaxHealth = 25;
            Name = "Kataplex";
            Speed = 4;

            DrawColorFOV = game.swatch.Player;
            Symbol = game.symbols.Player;
            GlyphString = game.glyphStrings.Player;

            // Initialize equipment
            InitializeEquipment();

            // Initialize invnetory
            inventoryList = new List<IItem>();
            InitializeInventory();
        }

        // Creates inital set of equipment
        private void InitializeEquipment()
        {
            // TODO: Equip initial items
            equipmentHead = game.itemGenerator.CreateEquipableItem(new IronHat(), ItemQualities.Normal, 10);
            equipmentHead.Equipped = true;

            equipmentChest = game.itemGenerator.CreateEquipableItem(new ChestPlate(), ItemQualities.Normal, 10);
            equipmentChest.Equipped = true;

            equipmentWaist = game.itemGenerator.CreateEquipableItem(new LeatherBelt(), ItemQualities.Normal, 10);
            equipmentWaist.Equipped = true;

            equipmentLegs = game.itemGenerator.CreateEquipableItem(new IronLegguards(), ItemQualities.Normal, 10);
            equipmentLegs.Equipped = true;

            equipmentFeet = game.itemGenerator.CreateEquipableItem(new IronGreaves(), ItemQualities.Normal, 10);
            equipmentFeet.Equipped = true;

            equipmentHands = game.itemGenerator.CreateEquipableItem(new IronGauntlets(), ItemQualities.Normal, 10);
            equipmentHands.Equipped = true;

            equipmentMainHand = game.itemGenerator.CreateEquipableItem(new WoodenClub(), ItemQualities.Normal, 10);
            equipmentMainHand.Equipped = true;
            equipmentMainHand.EquippedMainHand = true;

            equipmentOffHand = game.itemGenerator.CreateEquipableItem(new SimpleTowerShield(), ItemQualities.Normal, 10);
            equipmentOffHand.Equipped = true;
            equipmentOffHand.EquippedOffHand = true;

            equipmentLeftRing = game.itemGenerator.CreateEquipableItem(new RubyRing(), ItemQualities.Normal, 10);
            equipmentLeftRing.Equipped = true;
            equipmentLeftRing.EquippedLeftRing = true;

            equipmentRightRing = game.itemGenerator.CreateEquipableItem(new ThornedRing(), ItemQualities.Normal, 10);
            equipmentRightRing.Equipped = true;
            equipmentRightRing.EquippedRightRing = true;
        }

        // Creates initial inventory list
        private void InitializeInventory()
        {
            TEST_FillInventoryWithRandomItems();
        }

        // TEST: Add 20 random equipable items to player inventory with random quality
        public void TEST_FillInventoryWithRandomItems()
        {
            inventoryList = new List<IItem>();
            for (int i = 0; i < 25; i++)
            {
                // Check capacity
                if (CheckInventoryCapacity())
                {
                    // Setup item quality
                    ItemQualities itemQuality;
                    int randomQualityValue = game.random.Next(0, 1);
                    if (randomQualityValue == 0)
                        itemQuality = ItemQualities.Magic;
                    else
                        itemQuality = ItemQualities.Rare;

                    // Setup random item level
                    int itemLevel = game.random.Next(85, 90);

                    // Create random item
                    IItem randomItem = game.itemGenerator.CreateEquipableItem(
                        game.itemCollection.GetRandomEquipableItem(),
                        itemQuality,
                        itemLevel
                        );

                    // Add random item to inventory
                    AddItemToInventory(randomItem);
                }
            }
            /*
            for (int i = 0; i < 30; i++)
            {
                AddItemToInventory(game.itemGenerator.CreateUsableItem(new HealingPotion()) as IItem);
            }
            */
        }

        public bool CheckInventoryCapacity()
        {
            if (inventoryList.Count < 26)
                return true;
            else
            {
                game.messageSystem.Add("You cannot carry anymore", Color.red, false);
                game.SetDrawRequired(true);
                return false;
            }
        }

        // Adds an item to the player's inventory
        public void AddItemToInventory(IItem item)
        {
            if (item is IUsable)
            {
                IUsable usableItem = item as IUsable;
                if (usableItem.MaxStackCount > 1)
                {
                    bool stackFound = false;
                    foreach (IItem checkItem in inventoryList)
                    {
                        if (checkItem.Name == item.Name)
                        {
                            IUsable usableCheckItem = checkItem as IUsable;
                            if (usableCheckItem.CurrentStackCount < usableCheckItem.MaxStackCount)
                            {
                                usableCheckItem.CurrentStackCount++;                                
                                stackFound = true;
                            }
                        }
                    }
                    if (!stackFound)
                        inventoryList.Add(item);
                }
            }
            else
                inventoryList.Add(item);
        }

        // Method for drawing player stats
        public void DrawStats(int xOffset, int yOffset)
        {
            // Setup stats layer
            int layer = game._statsLayer;

            // Setup stats strings
            string statsName = string.Format("Name:    {0}", Name);
            string statsHealth = string.Format("Health:  {0}/{1}", Health, MaxHealth);
            string statsAttack = string.Format("Attack:  {0} ({1}%)", Attack, HitChance);
            string statsDefence = string.Format("Defence: {0} ({1}%)", Defence, DefenceChance);
            string statsGold = string.Format("Gold:    {0}", Gold);

            // Draw statsName
            for (int x = 0; x < statsName.Length; x++)
            {
                Cell cell = Display.CellAt(layer, x + xOffset + 1, yOffset + 1);
                cell.SetContent(statsName.Substring(x, 1), Color.clear, game.swatch.Text);
            }

            // Draw statsHealth
            for (int x = 0; x < statsHealth.Length; x++)
            {
                Cell cell = Display.CellAt(layer, x + xOffset + 1, yOffset + 3);
                cell.SetContent(statsHealth.Substring(x, 1), Color.clear, game.swatch.Text);
            }

            // Draw statsAttack
            for (int x = 0; x < statsAttack.Length; x++)
            {
                Cell cell = Display.CellAt(layer, x + xOffset + 1, yOffset + 5);
                cell.SetContent(statsAttack.Substring(x, 1), Color.clear, game.swatch.Text);
            }

            // Draw statsDefence
            for (int x = 0; x < statsDefence.Length; x++)
            {
                Cell cell = Display.CellAt(layer, x + xOffset + 1, yOffset + 7);
                cell.SetContent(statsDefence.Substring(x, 1), Color.clear, game.swatch.Text);
            }

            // Draw statsGold
            for (int x = 0; x < statsGold.Length; x++)
            {
                Cell cell = Display.CellAt(layer, x + xOffset + 1, yOffset + 9);
                cell.SetContent(statsGold.Substring(x, 1), Color.clear, game.swatch.Gold);
            }
        }

        // Method for restoring health to the player
        public void HealPlayer(int healValue)
        {
            Health += healValue;
            if (Health > MaxHealth)
                Health = MaxHealth;
        }
    }
}