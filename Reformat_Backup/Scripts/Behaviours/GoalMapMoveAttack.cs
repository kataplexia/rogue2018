﻿using UnityEngine;
using System.Collections.Generic;
using RogueSharp;
using Rogue2018.Actors;
using Rogue2018.Core;
using Rogue2018.Core.MapCore;
using Rogue2018.Systems;
using Rogue2018.Interfaces;

namespace Rogue2018.Behaviours
{
    public class GoalMapMoveAttack : IBehaviour
    {
        private Game        game;
        private Player      player;
        private RogueMap    rogueMap;
        private FieldOfView monsterFOV;

        public bool Act(Monster monster, CommandSystem commandSystem)
        {
            if (game == null)
                game = Game.GET;

            if (player == null)
                player = Game.GET.player;

            if (rogueMap == null)
                rogueMap = Game.GET.rogueMap;

            if (monsterFOV == null)
                monsterFOV = new FieldOfView(rogueMap);

            // If monster not alerted check FOV using awareness and alert if necessary
            if (!monster.TurnsAlerted.HasValue)
            {
                monsterFOV.ComputeFOV(monster.X, monster.Y, monster.Awareness, true);
                if (monsterFOV.IsInFOV(player.X, player.Y))
                {
                    // string alertMessage = string.Format("{0} is eager to fight {1}", monster.Name, player.Name);
                    // Game.GET.messageSystem.Add(alertMessage, Color.white);
                    monster.TurnsAlerted = 1;
                }
            }
            else
            {
                List<GoalMap.WeightedPoint> neighbours = Game.GET.rogueMap.monsterGoalMap.GetNeighbors(monster.X, monster.Y);

                int bestWeight = int.MaxValue;
                ICell bestNeighbourCell = null;
                foreach (GoalMap.WeightedPoint neighbour in neighbours)
                {
                    ICell weightedPointCell = rogueMap.GetCell(neighbour.X, neighbour.Y);

                    bool isPlayerCell = weightedPointCell.X == Game.GET.player.X && weightedPointCell.Y == Game.GET.player.Y;

                    if (!weightedPointCell.IsWalkable && !isPlayerCell)
                        continue;

                    else if (neighbour.Weight < bestWeight)
                    {
                        bestWeight = neighbour.Weight;
                        bestNeighbourCell = weightedPointCell;
                    }
                }
                
                if (bestNeighbourCell != null)
                    commandSystem.MoveMonster(monster, bestNeighbourCell);

                // Add alerted turns
                monster.TurnsAlerted++;

                // Lose alerted status after 10 turns leaving playerFOV
                if (monster.TurnsAlerted > 10)
                    monster.TurnsAlerted = null;
            }
            return true;
        }
    }
}