﻿using UnityEngine;

using RogueSharp;
using Rogue2018.Actors;
using Rogue2018.Core;
using Rogue2018.Core.MapCore;
using Rogue2018.Systems;
using Rogue2018.Interfaces;

namespace Rogue2018.Behaviours
{
    public class MoveAndAttack_beh : IBehaviour
    {
        private FieldOfView monsterFOV;
        private PathFinder pathFinder;

        public bool Act(Monster monster, CommandSystem commandSystem)
        {
            RogueMap rogueMap = Game.GET.rogueMap;
            Player player = Game.GET.player;

            if (monsterFOV == null)
                monsterFOV = new FieldOfView(rogueMap);

            // RogueMap rogueMap = Game.GET.rogueMap;
            // Player player = Game.GET.player;
            // FieldOfView monsterFOV = new FieldOfView(rogueMap);

            // If monster not alerted check FOV using awareness and alert if necessary
            if (!monster.TurnsAlerted.HasValue)
            {
                monsterFOV.ComputeFOV(monster.X, monster.Y, monster.Awareness, true);
                if (monsterFOV.IsInFOV(player.X, player.Y))
                {
                    // string alertMessage = string.Format("{0} is eager to fight {1}", monster.Name, player.Name);
                    // Game.GET.messageSystem.Add(alertMessage, Color.white);
                    monster.TurnsAlerted = 1;
                }
            }
            else
            {
                // Before we pathfind, make sure the monster and player rogueCells are walkable
                rogueMap.SetIsWalkable(monster.X, monster.Y, true);
                rogueMap.SetIsWalkable(player.X, player.Y, true);

                if (pathFinder == null)
                    pathFinder = new PathFinder(rogueMap, 1.0f);

                Path path = null;

                try
                {
                    path = pathFinder.ShortestPath(rogueMap.GetCell(monster.X, monster.Y), rogueMap.GetCell(player.X, player.Y));
                }
                catch (PathNotFoundException) { }

                // Reset not walkable
                rogueMap.SetIsWalkable(monster.X, monster.Y, false);
                rogueMap.SetIsWalkable(player.X, player.Y, false);

                // Path found execute commandSystem
                if (path != null)
                {
                    try
                    {
                        if (!commandSystem.MoveMonster(monster, path.StepForward()))
                            pathFinder = null;

                    }
                    catch (NoMoreStepsException) { }
                }

                // Add alerted turns
                monster.TurnsAlerted++;

                // Lose alerted status after 10 turns leaving playerFOV
                if (monster.TurnsAlerted > 10)
                    monster.TurnsAlerted = null;
            }
            return true;
        }
    }
}