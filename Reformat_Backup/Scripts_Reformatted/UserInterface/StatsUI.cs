﻿using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Interfaces;

using PhiOS;

namespace Rogue2018.UserInterface
{
    public class StatsUI : IUserInterface
    {
        // IUserInterface properties
        public string[] handles { get; set; }
        public PhiCellPropertiesList[] phiCellPropertiesLists { get; set; }
        public void Draw()
        {
            foreach (PhiCellPropertiesList phiCellPropertiesList in phiCellPropertiesLists)
            {
                game.Tools.DrawCellsFromPropertiesList(phiCellPropertiesList);
            }
        }

        // Constructor
        private Game        game;
        private UIManager   uiManager;
        public StatsUI()
        {
            game = Game.Singleton;
            uiManager = UIManager.Singleton;

            handles = new string[] { "StatsConsole" };
            phiCellPropertiesLists = new PhiCellPropertiesList[handles.Length];

            for (int i = 0; i < handles.Length; i++)
            {
                foreach (Transform child in uiManager.transform)
                {
                    if (child.gameObject.name == handles[i] + "CellProperties")
                        phiCellPropertiesLists[i] = child.GetComponent<PhiCellPropertiesList>();
                }
            }
        }
    }
}