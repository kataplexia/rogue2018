﻿using System.Text;
using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Actors;
using Rogue2018.Interfaces;

using PhiOS;

namespace Rogue2018.UserInterface
{
    public class InventoryUI : IUserInterface
    {
        // Method for resetting InventoryUI
        public void Reset()
        {
            SetState(InventoryUIStates.Default);
            _currentSelection = -1;
            _currentOptionSelection = -1;
        }

        public enum InventoryUIStates
        {
            Default                     = 0,
            SelectionOptionsMenu        = 1,
            SelectionItemInformation    = 2
        }

        // Properties
        public InventoryUIStates    inventoryUIState        = InventoryUIStates.Default;
        private static int          _currentSelection       = -1;
        private static int          _currentOptionSelection = -1;
        private static int          _selectionOffset        = 6;
        private static int          _maxSelection           = 38;
        private static int          _maxOptionsSelection    = 3;
        private static int          _optionsBoxWidth        = 14;
        private static int          _infoBoxWidth           = 48;
        public int                  _optionsMinimumIndex    = 0;

        public PhiCell quitCellInventory;
        public PhiCell quitCellOptionsMenu;
        public PhiCell quitCellItemInformation;

        // IUserInterface properties
        public string[] handles { get; set; }
        public PhiCellPropertiesList[] phiCellPropertiesLists { get; set; }

        // Method for drawing
        public void Draw()
        {
            // Clear console
            game.Tools.ClearConsole(0, game.MapConsoleWidth - 4, game.MapConsoleHeight - 6, 2, 3);

            // Clear mouse properties
            game.Tools.SetPhiCellRegionMouseProperties(0, 0, game.MapConsoleWidth, game.MapConsoleHeight, false, false);

            // Setup inventory quit cell
            quitCellInventory = PhiDisplay.PhiCellAt(0, game.MapConsoleWidth - 3, 1);
            quitCellInventory.clickAction = game.InputSystem;

            // Clear other quit cells
            quitCellOptionsMenu        = null;
            quitCellItemInformation    = null;

            // Draw Console
            foreach (PhiCellPropertiesList phiCellPropertiesList in phiCellPropertiesLists)
            {
                game.Tools.DrawCellsFromPropertiesList(phiCellPropertiesList);
            }

            // Draw Equipment
            DrawEquipment();

            // Draw inventory list
            DrawInventoryList();

            // Draw inventory capacity
            DrawInventoryCapacity();

            // Draw default state and selection
            if (inventoryUIState == InventoryUIStates.Default)
            {
                DrawSelection();
                SetMousePropertiesEquipment();
                SetMousePropertiesInventoryList();
            }

            // Draw selection options menu
            else if (inventoryUIState == InventoryUIStates.SelectionOptionsMenu)
            {
                DrawSelectionOptionsMenu();
                SetMousePropertiesOptionsMenu();
            }

            // Draw selection item information
            else if (inventoryUIState == InventoryUIStates.SelectionItemInformation)
            {
                DrawSelectionItemInformation();
                SetMousePropertiesItemInformation();
            }
        }

        // Method for drawing equipment
        private void DrawEquipment()
        {
            Player player = game.Player;
            if (player.EquipmentHead != null)
                player.EquipmentHead.DrawName       (8, 6, CheckSelectedColor(6));
            if (player.EquipmentChest != null)
                player.EquipmentChest.DrawName      (8, 8, CheckSelectedColor(8));
            if (player.EquipmentWaist != null)
                player.EquipmentWaist.DrawName      (8, 10, CheckSelectedColor(10));
            if (player.EquipmentLegs != null)
                player.EquipmentLegs.DrawName       (8, 12, CheckSelectedColor(12));
            if (player.EquipmentFeet != null)
                player.EquipmentFeet.DrawName       (8, 14, CheckSelectedColor(14));
            if (player.EquipmentHands != null)
                player.EquipmentHands.DrawName      (8, 16, CheckSelectedColor(16));
            if (player.EquipmentMainHand != null)
                player.EquipmentMainHand.DrawName   (8, 18, CheckSelectedColor(18));
            if (player.EquipmentOffHand != null)
                player.EquipmentOffHand.DrawName    (8, 20, CheckSelectedColor(20));
            if (player.EquipmentLeftRing != null)
                player.EquipmentLeftRing.DrawName   (8, 22, CheckSelectedColor(22));
            if (player.EquipmentRightRing != null)
                player.EquipmentRightRing.DrawName  (8, 24, CheckSelectedColor(24));
        }

        // Method for setting mouse properties for equipment
        private void SetMousePropertiesEquipment()
        {
            Player player = game.Player;
            if (player.EquipmentHead != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 6, 48, 1, true, true);
            if (player.EquipmentChest != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 8, 48, 1, true, true);
            if (player.EquipmentWaist != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 10, 48, 1, true, true);
            if (player.EquipmentLegs != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 12, 48, 1, true, true);
            if (player.EquipmentFeet != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 14, 48, 1, true, true);
            if (player.EquipmentHands != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 16, 48, 1, true, true);
            if (player.EquipmentMainHand != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 18, 48, 1, true, true);
            if (player.EquipmentOffHand != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 20, 48, 1, true, true);
            if (player.EquipmentLeftRing != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 22, 48, 1, true, true);
            if (player.EquipmentRightRing != null)
                game.Tools.SetPhiCellRegionMouseProperties(7, 24, 48, 1, true, true);
        }

        // Method for drawing inventory list
        private void DrawInventoryList()
        {
            Player player = game.Player;
            for (int i = 0; i < player.InventoryList.Count; i++)
            {
                string itemName = player.InventoryList[i].Name;
                if (player.InventoryList[i] is IUsable)
                {
                    IUsable usableItem = player.InventoryList[i] as IUsable;
                    int stackStringLength = usableItem.CurrentStackCount.ToString().Length + usableItem.MaxStackCount.ToString().Length + 4;
                    usableItem.DrawStacks(8, 33 + (i * 2), CheckSelectedColor(33 + (i * 2)));
                    player.InventoryList[i].DrawName(8 + stackStringLength, 33 + (i * 2), CheckSelectedColor(33 + (i * 2)));
                }
                else
                    player.InventoryList[i].DrawName(8, 33 + (i * 2), CheckSelectedColor(33 + (i * 2)));
            }
        }

        // Method for setting mouse properties for equipment
        private void SetMousePropertiesInventoryList()
        {
            Player player = game.Player;
            for (int i = 0; i < player.InventoryList.Count; i++)
                game.Tools.SetPhiCellRegionMouseProperties(7, 33 + (i * 2), 48, 1, true, true);
        }

        // Method for drawing inventory capacity
        private void DrawInventoryCapacity()
        {
            Player player = game.Player;
            string capacity = string.Format("Capacity [{0}/{1}]", player.InventoryList.Count, 28);
            game.Tools.DrawString(35 - capacity.Length , 85, capacity, Color.yellow, Color.black, 0);
        }

        // Method for drawing current selection
        private void DrawSelection()
        {
            // Check empty inventory
            if (InventoryIsEmpty() || _currentSelection == -1)
                return;

            // Set the selection offset
            if (_currentSelection >= 10)
                _selectionOffset = 13;
            else
                _selectionOffset = 6;

            // Draw the selection box
            game.Tools.DrawUIBox(50, 3, 6, _selectionOffset + (_currentSelection * 2) - 1, 0, Color.black, Color.yellow, Color.clear);
        }

        // Method for drawing current selection's options menu
        private void DrawSelectionOptionsMenu()
        {
            // Check empty inventory
            if (InventoryIsEmpty())
                return;

            // Set the selection offset
            if (_currentSelection >= 10)
                _selectionOffset = 13;
            else
                _selectionOffset = 6;

            // Get the selected item and create a string builder for storing it's information
            IItem selectedItem = GetItemAtIndex(_currentSelection);
            StringBuilder options = new StringBuilder();

            // Setup empty options count
            int optionsCount = 0;

            // Check if selected item is equipable or usable and add respective options
            if (selectedItem is IEquipable)
            {
                if (_currentSelection < 10)
                    options.Append("U: Unequip\n");
                else
                    options.Append("E: Equip\n");
                optionsCount++;
            }
            else if (selectedItem is IUsable)
            {
                options.Append("A: Activate\n");
                IUsable selectedUsable = selectedItem as IUsable;
                if (selectedUsable.MaxStackCount > 1)
                {
                    options.Append("L: Drop All\n");
                    optionsCount++;
                }
                optionsCount++;
            }

            // Add the drop option
            options.Append("D: Drop\n");
            optionsCount++;

            // Add the inspect option
            options.Append("I: Inspect");
            optionsCount++;

            // Set the max options selection value
            _maxOptionsSelection = optionsCount;

            // Draw the options menu
            game.Tools.DrawContextMenuBox(
                58,
                _selectionOffset + (_currentSelection * 2) + 2,
                _optionsBoxWidth,
                options.ToString(),
                Color.yellow,
                Color.white,
                Color.black,
                "Options",
                game.Swatch.RexTextHeading,
                0,
                "quitCellOptionsMenu",
                true);

            // Draw the inventory selection box
            game.Tools.DrawUIBox(50, 3, 6, _selectionOffset + (_currentSelection * 2) - 1, 0, Color.black, Color.yellow, Color.clear);

            // Draw the inventory selection symbols
            game.Tools.DrawPhiCell(55, _selectionOffset + (_currentSelection * 2), "├", Color.yellow, Color.black, 0, 0, 0);
            game.Tools.DrawPhiCell(56, _selectionOffset + (_currentSelection * 2), "┤", Color.yellow, Color.black, 0, 0, 0);

            if (_currentOptionSelection == -1)
                return;

            // Check if final box height is off screen and modify offset for drawing selection box
            int optionsBoxYOffset = _selectionOffset + (_currentSelection * 2) + 2;
            if (optionsBoxYOffset + (_maxOptionsSelection * 2) + 3 > 86)
                optionsBoxYOffset = 86 - ((_maxOptionsSelection * 2) + 3);

            // Draw the options selection box
            game.Tools.DrawUIBox(_optionsBoxWidth - 2, 3, 61, optionsBoxYOffset + (_currentOptionSelection * 2), 0, Color.black, Color.yellow, Color.clear);
        }

        // Method for setting mouse properties for options menu
        private void SetMousePropertiesOptionsMenu()
        {
            // Check if final box height is off screen and modify offset for drawing selection box
            int optionsSelectionBoxYOffset = _selectionOffset + (_currentSelection * 2) + 2;
            if (optionsSelectionBoxYOffset + (_maxOptionsSelection * 2) + 3 > 86)
                optionsSelectionBoxYOffset = 86 - ((_maxOptionsSelection * 2) + 3);

            // Set hoverable and clickable regions
            for (int x = 5; x <= _optionsBoxWidth; x++)
            {
                for (int y = 0; y < _maxOptionsSelection; y++)
                {
                    if (y == 0)
                        _optionsMinimumIndex = optionsSelectionBoxYOffset + 1;
                    game.Tools.SetPhiCellMouseProperties(57 + x, optionsSelectionBoxYOffset + (y * 2) + 1, true, true);
                }
            }

            if (quitCellOptionsMenu == null)
            {
                PhiCell phiCellQuit = game.Tools.GetPhiCellWithTag("quitCellOptionsMenu");
                if (phiCellQuit != null)
                    quitCellOptionsMenu = phiCellQuit;
            }
        }

        // Method for drawing current selection's item information
        private void DrawSelectionItemInformation()
        {
            // Check empty inventory
            if (InventoryIsEmpty())
                return;

            // Set the selection offset
            if (_currentSelection >= 10)
                _selectionOffset = 13;
            else
                _selectionOffset = 6;

            // Get the selected item and create a string builder for storing it's information
            IItem selectedItem = GetItemAtIndex(_currentSelection);
            StringBuilder itemInformation = new StringBuilder();

            // Initialize property and affix count
            int propertyLineCount   = 0;
            int affixLineCount      = 0;
            int implicitLineCount   = 0;

            // Set the item's quality
            if (selectedItem.ItemQuality != ItemQualities.None)
            {
                string seQuality = selectedItem.ItemQuality.ToString();
                itemInformation.Append("Quality:".PadRight(_infoBoxWidth - seQuality.Length, ' '));
                itemInformation.Append(seQuality + "\n \n");

                // Modify property count
                propertyLineCount+=2;
            }

            if (selectedItem is IEquipable)
            {
                // Get selected item as equipable
                IEquipable selectedEquipable = selectedItem as IEquipable;

                // Setup the equipable's item level
                string seItemLevel = selectedEquipable.ItemLevel.ToString();
                itemInformation.Append("Item Level:".PadRight(_infoBoxWidth - seItemLevel.Length, ' '));
                itemInformation.Append(seItemLevel + "\n");

                // Setup the equipable's equip location
                string seEquipLocation = selectedEquipable.EquipmentLocationString;
                itemInformation.Append("Slot:".PadRight(_infoBoxWidth - seEquipLocation.Length, ' '));
                itemInformation.Append(seEquipLocation + "\n");

                // Modify property count
                propertyLineCount+=2;

                // Setup the armour's information
                if (selectedItem is IArmour)
                {
                    // Get selected item as armour
                    IArmour selectedArmour = selectedItem as IArmour;

                    // Setup the armour's style
                    string saStyle = selectedArmour.ArmourStyleString;
                    itemInformation.Append("Armour Style:".PadRight(_infoBoxWidth - saStyle.Length, ' '));
                    itemInformation.Append(saStyle + "\n");

                    // Setup the armour's defence value
                    string saDefenceValue = "+" + selectedArmour.DefenceValue.ToString();
                    itemInformation.Append("Defence Value:".PadRight(_infoBoxWidth - saDefenceValue.Length, ' '));
                    itemInformation.Append(saDefenceValue + "\n");

                    // Setup the armour's defence chance
                    string saDefenceDefenceChance = string.Format("+{0}%", selectedArmour.DefenceChance.ToString());
                    itemInformation.Append("Defence Chance:".PadRight(_infoBoxWidth - saDefenceDefenceChance.Length, ' '));
                    itemInformation.Append(saDefenceDefenceChance + "\n");

                    // Setup the arour's speed modifier
                    string saSpeedMod = selectedArmour.SpeedModifier.ToString();
                    itemInformation.Append("Speed Modifier:".PadRight(_infoBoxWidth - saSpeedMod.Length, ' '));
                    itemInformation.Append(saSpeedMod + "\n");

                    // Modify property count
                    propertyLineCount += 4;
                }
                // Setup the weapon's information
                else if (selectedItem is IWeapon)
                {
                    // Get selected item as weapon
                    IWeapon selectedWeapon = selectedItem as IWeapon;

                    // Setup the weapon's type
                    string swType = selectedWeapon.WeaponTypeString;
                    itemInformation.Append("Weapon Type:".PadRight(_infoBoxWidth - swType.Length, ' '));
                    itemInformation.Append(swType + "\n");

                    // Setup the weapon's style
                    string swStyle = selectedWeapon.WeaponStyleString;
                    itemInformation.Append("Weapon Style:".PadRight(_infoBoxWidth - swStyle.Length, ' '));
                    itemInformation.Append(swStyle + "\n");

                    // Setup the weapon's damage type
                    string swDamageType = selectedWeapon.DamageTypeString;
                    itemInformation.Append("Damage Type:".PadRight(_infoBoxWidth - swDamageType.Length, ' '));
                    itemInformation.Append(swDamageType + "\n");

                    // Setup the weapon's physical damage
                    string swPhysicalDamage = "+" + selectedWeapon.PhysicalDamage.ToString();
                    itemInformation.Append("Physical Damage:".PadRight(_infoBoxWidth - swPhysicalDamage.Length, ' '));
                    itemInformation.Append(swPhysicalDamage + "\n");

                    // Setup the weapon's hit chance
                    string swHitChance = string.Format("+{0}%", selectedWeapon.HitChance.ToString());
                    itemInformation.Append("Hit Chance:".PadRight(_infoBoxWidth - swHitChance.Length, ' '));
                    itemInformation.Append(swHitChance + "\n");

                    // Modify property count
                    propertyLineCount += 5;
                }

                // Check for implicit
                if (selectedEquipable.Implicit != null)
                {
                    // Add line break
                    itemInformation.Append(" \n");
                    string implicitEffect = selectedEquipable.Implicit.EffectInfoString;
                    itemInformation.Append(implicitEffect + "\n");
                    implicitLineCount += 2;
                }

                // Check for magic quality or greater
                if (selectedItem.ItemQuality != ItemQualities.None &&
                    selectedItem.ItemQuality != ItemQualities.Normal)
                {
                    // Add line break
                    itemInformation.Append(" \n");

                    // Setup the equipable's prefixes
                    for (int i = 0; i < selectedEquipable.Prefixes.Count; i++)
                    {
                        string prefixEffect = selectedEquipable.Prefixes[i].EffectInfoString;
                        itemInformation.Append(prefixEffect + "\n");
                        affixLineCount++;
                    }

                    // Setup the equipable's suffixes
                    for (int i = 0; i < selectedEquipable.Suffixes.Count; i++)
                    {
                        string suffixEffect = selectedEquipable.Suffixes[i].EffectInfoString;
                        itemInformation.Append(suffixEffect + "\n");
                        affixLineCount++;
                    }

                    // Add line break
                    itemInformation.Append(" \n");
                    affixLineCount++;
                }
            }

            // Add line breaks for formatting
            if (selectedItem.ItemQuality == ItemQualities.Normal && propertyLineCount > 2)
                itemInformation.Append(" \n");
            if (propertyLineCount == 2 && affixLineCount == 0)
                itemInformation.Append("\n");

            // Setup the item's description
            itemInformation.AppendFormat("{0}", selectedItem.Description);

            // Draw the item's information box
            string itemName = selectedItem.Name;
            game.Tools.DrawInfoBox(
                58,
                _selectionOffset + (_currentSelection * 2) + 2,
                _infoBoxWidth,
                itemInformation.ToString(),
                propertyLineCount,
                affixLineCount,
                implicitLineCount,
                Color.yellow,
                Color.white,
                Color.black,
                itemName,
                0,
                "quitCellItemInformation",
                true);

            // Draw the selection box
            game.Tools.DrawUIBox(50, 3, 6, _selectionOffset + (_currentSelection * 2) - 1, 0, Color.black, Color.yellow, Color.clear);

            // Draw the selection symbols
            game.Tools.DrawPhiCell(55, _selectionOffset + (_currentSelection * 2), "├", Color.yellow, Color.black, 0, 0, 0);
            game.Tools.DrawPhiCell(56, _selectionOffset + (_currentSelection * 2), "┤", Color.yellow, Color.black, 0, 0, 0);
        }

        // Method for setting mouse properties for item information box
        private void SetMousePropertiesItemInformation()
        {
            if (quitCellItemInformation == null)
            {
                PhiCell phiCellQuit = game.Tools.GetPhiCellWithTag("quitCellItemInformation");
                if (phiCellQuit != null)
                    quitCellItemInformation = phiCellQuit;
            }
        }

        // Returns true if the inventory is empty
        private bool InventoryIsEmpty()
        {
            Player player = game.Player;
            if (player.EquipmentHead == null &&
                player.EquipmentChest == null &&
                player.EquipmentWaist == null &&
                player.EquipmentLegs == null &&
                player.EquipmentFeet == null &&
                player.EquipmentHands == null &&
                player.EquipmentMainHand == null &&
                player.EquipmentOffHand == null &&
                player.EquipmentLeftRing == null &&
                player.EquipmentRightRing == null &&
                player.InventoryList.Count == 0
                )
                return true;
            else
                return false;
        }

        // Constructor
        private Game game;
        private UIManager uiManager;
        public InventoryUI()
        {
            game = Game.Singleton;
            uiManager = UIManager.Singleton;

            handles = new string[] { "InventoryConsole" };
            phiCellPropertiesLists = new PhiCellPropertiesList[handles.Length];

            for (int i = 0; i < handles.Length; i ++)
            {
                foreach (Transform child in uiManager.transform)
                {
                    if (child.gameObject.name == handles[i] + "CellProperties")
                        phiCellPropertiesLists[i] = child.GetComponent<PhiCellPropertiesList>();
                }
            }
        }

        // Returns index given yPosition
        public int ConvertYPositionToIndex(int yPosition)
        {
            int index = (yPosition / 2) - 6;
            if (index <= 6)
                index += 3;
            return index;
        }

        // Returns color for selected item
        private Color CheckSelectedColor(int yPosition)
        {
            int index = ConvertYPositionToIndex(yPosition);
            if (index == _currentSelection)
                return Color.yellow;
            else
                return GetQualityColor(index);
        }

        // Returns color fo unselected item based on item quality
        private Color GetQualityColor(int index)
        {
            IItem itemAtIndex = GetItemAtIndex(index);
            if (itemAtIndex.ItemQuality != ItemQualities.None)
                return game.Tools.GetQualityColor(itemAtIndex.ItemQuality.ToString());
            else
                return Color.white;
        }

        // Returns item at current selection
        private IItem GetItemAtIndex(int selectionIndex)
        {
            Player player = game.Player;
            if (selectionIndex == 0)
                return player.EquipmentHead;
            else if (selectionIndex == 1)
                return player.EquipmentChest;
            else if (selectionIndex == 2)
                return player.EquipmentWaist;
            else if (selectionIndex == 3)
                return player.EquipmentLegs;
            else if (selectionIndex == 4)
                return player.EquipmentFeet;
            else if (selectionIndex == 5)
                return player.EquipmentHands;
            else if (selectionIndex == 6)
                return player.EquipmentMainHand;
            else if (selectionIndex == 7)
                return player.EquipmentOffHand;
            else if (selectionIndex == 8)
                return player.EquipmentLeftRing;
            else if (selectionIndex == 9)
                return player.EquipmentRightRing;

            else if (selectionIndex >= 10)
            {
                if (player.InventoryList.Count > (selectionIndex - 10))
                    return player.InventoryList[selectionIndex - 10];
                else
                    return null;
            }
            return null;
        }

        // Returns the current selection
        public int GetCurrentOptionsSelection()
        {
            return _currentOptionSelection;
        }

        // Method for changing current selection using a modifier
        public void ModifiyOptionsSelection(int modifier)
        {
            _currentOptionSelection += modifier;
            if (_currentOptionSelection < 0)
                _currentOptionSelection = _maxOptionsSelection - 1;
            else if (_currentOptionSelection > _maxOptionsSelection - 1)
                _currentOptionSelection = 0;
            Draw();
        }

        // Method for changing current selection using an index
        public void ChangeOptionsSelection(int selectionIndex)
        {
            _currentOptionSelection = selectionIndex;
            Draw();
        }

        // Returns the current selection
        public int GetCurrentSelection()
        {
            return _currentSelection;
        }

        // Method for changing current selection using a modifier
        public void ModifiySelection(int modifier)
        {
            _currentSelection += modifier;
            if (_currentSelection < 0)
                _currentSelection = _maxSelection;
            else if (_currentSelection > _maxSelection)
                _currentSelection = 0;
            if (GetItemAtIndex(_currentSelection) == null)
                ModifiySelection(modifier);
            else
                Draw();
        }

        // Method for changing current selection using an index
        public void ChangeSelection(int selectionIndex, bool goToOptions = false)
        {
            if (inventoryUIState != InventoryUIStates.Default)
                SetState(InventoryUIStates.Default);

            if (GetItemAtIndex(selectionIndex) != null)
            {
                _currentSelection = selectionIndex;
                if (goToOptions)
                    SetState(InventoryUIStates.SelectionOptionsMenu);
                else
                    Draw();
            }
        }

        // Method for setting the inventory UI's state and drawing
        public void SetState(InventoryUIStates state)
        {
            if (state == InventoryUIStates.Default)
            {
                _currentSelection = -1;
                _currentOptionSelection = -1;
            }
            else if (state == InventoryUIStates.SelectionOptionsMenu)
                _currentOptionSelection = -1;
            inventoryUIState = state;
            Draw();
        }

        // Method for executing a command within the options menu
        public void ExecuteOptionsSelection()
        {
            IItem selectedItem = GetItemAtIndex(_currentSelection);
            if (selectedItem is IUsable)
            {
                if (_currentOptionSelection == 0)
                    UseSelectedItem();
                else if (_currentOptionSelection == 1)
                    DropAllOfSelectedItem();
                else if (_currentOptionSelection == 2)
                    DropSelectedItem();
                else if (_currentOptionSelection == 3)
                    SetState(InventoryUIStates.SelectionItemInformation);
            }
            else
            {
                if (_currentOptionSelection == 0)
                    if (_currentSelection < 10) UnequipSelectedItem(); else EquipSelectedItem();
                else if (_currentOptionSelection == 1)
                    DropSelectedItem();
                else if (_currentOptionSelection == 2)
                    SetState(InventoryUIStates.SelectionItemInformation);
            }
        }

        // Method for using selected item
        public void UseSelectedItem()
        {
            IItem selectedItem = GetItemAtIndex(_currentSelection);
            if (selectedItem is IUsable)
            {
                IUsable selectedUsable = selectedItem as IUsable;
                selectedUsable.Use();
                SetState(InventoryUIStates.Default);
            }
        }

        // Method for unequipping selected item
        public void UnequipSelectedItem()
        {
            if (_currentSelection >= 10)
                return;

            IItem selectedItem = GetItemAtIndex(_currentSelection);
            if (selectedItem is IEquipable)
            {
                IEquipable selectedEquipable = selectedItem as IEquipable;
                selectedEquipable.Unequip();
                SetState(InventoryUIStates.Default);
            }
        }

        // Method for equipping selected item
        public void EquipSelectedItem()
        {
            if (_currentSelection < 10)
                return;

            IItem selectedItem = GetItemAtIndex(_currentSelection);
            if (selectedItem is IEquipable)
            {
                IEquipable selectedEquipable = selectedItem as IEquipable;
                selectedEquipable.Equip();
                SetState(InventoryUIStates.Default);
            }
        }

        // Method for dropping selected item
        public void DropSelectedItem()
        {
            IItem selectedItem = GetItemAtIndex(_currentSelection);
            selectedItem.Drop();
            SetState(InventoryUIStates.Default);
        }

        // Method for dropping all of a selected item
        public void DropAllOfSelectedItem()
        {
            IItem selectedItem = GetItemAtIndex(_currentSelection);
            if (selectedItem is IUsable)
            {
                IUsable selectedUsable = selectedItem as IUsable;
                if (selectedUsable.MaxStackCount > 1)
                {
                    selectedUsable.Drop(true);
                    SetState(InventoryUIStates.Default);
                }
            }
        }
    }
}