﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Systems;

using PhiOS;

namespace Rogue2018.UserInterface
{
    public class UIManager : MonoBehaviour
    {
        // Mono Members
        public TextMesh buildDebug;
        public SpriteRenderer transitionSprite;
        public GameObject[] cellPropertiesPrefabs;

        // Members
        private static Game game;
        private static PhiDisplay display;

        // User Interface members
        public TitleScreenUI titleScreenUI { get; private set; }
        public InventoryUI inventoryUI { get; private set; }
        public StatsUI statsUI { get; private set; }

        // Properties
        public bool isInitialized { get; private set; }
        public bool isHandlingAllInput { get; private set; }
        private static int _previousZoomLevel = 0;
        public Vector2 quickBarInventoryPos = new Vector2(123, 85);

        // Awake with UIManager as singleton and do not destroy
        public static UIManager Singleton;
        private void Awake()
        {
            transitionSprite.enabled = true;
            isInitialized = false;
            if (Singleton != null)
                GameObject.Destroy(Singleton);
            else
                Singleton = this;
            // DontDestroyOnLoad(this);
        }

        private void OnEnable()
        {
            InputSystem.OnMouseDownEvent += OnMouseDown;
            InputSystem.OnHoverEnterEvent += OnHoverEnter;
            InputSystem.OnHoverExitEvent += OnHoverExit;
        }

        private void OnDisable()
        {
            InputSystem.OnMouseDownEvent -= OnMouseDown;
            InputSystem.OnHoverEnterEvent += OnHoverEnter;
            InputSystem.OnHoverExitEvent += OnHoverExit;
        }

        // Routine for starting the UIManager
        public IEnumerator Start()
        {
            // Wait until display has initialized
            while (!PhiDisplay.IsInitialized() || !Game.Singleton.IsInitialized)
                yield return null;

            // Get members
            game = Game.Singleton;
            display = PhiDisplay.GET;

            // Instantiate cell properties prefabs
            for (int i = 0; i < cellPropertiesPrefabs.Length; i++)
            {
                GameObject cellPropertiesObject = GameObject.Instantiate(cellPropertiesPrefabs[i]);
                cellPropertiesObject.name = cellPropertiesPrefabs[i].name;
                cellPropertiesObject.transform.parent = this.transform;
            }

            // Instantiate members
            titleScreenUI = new TitleScreenUI();
            inventoryUI = new InventoryUI();
            statsUI = new StatsUI();

            titleScreenUI.Draw();

            // Set mouse properties for stats console
            game.Tools.SetPhiCellMouseProperties((int)quickBarInventoryPos.x, (int)quickBarInventoryPos.y, true, true);

            yield return new WaitForFixedUpdate();
            isInitialized = true;
            transitionSprite.enabled = false;
        }

        // Method for displaying inventory UI
        public static bool _inventoryDisplay = false;
        public IEnumerator InventoryDisplay()
        {
            if (!_inventoryDisplay)
            {
                _inventoryDisplay = true;
                isHandlingAllInput = true;
                game.CurrentMap.HideHealthBars();
                game.SetDrawMapConsole(false);
                game.ClearMapConsole();
                yield return new WaitForFixedUpdate();
                _previousZoomLevel = display.currentZoomLevel;
                display.SetZoom(0);
                inventoryUI.Draw();
            }
            else
            {
                _inventoryDisplay = false;
                isHandlingAllInput = false;
                game.ClearMapConsole();
                game.SetDrawMapConsole(true);
                yield return new WaitForFixedUpdate();
                display.SetZoom(_previousZoomLevel);
                game.SetDrawRequired(true);
                inventoryUI.Reset();
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (!isInitialized)
                return;

            if (Input.GetKeyDown(KeyCode.F9))
                Game.Singleton.NewGame();

            if (!Game.Singleton.IsGameStarted)
            {
                UpdateTitleScreenKeyboardInput();
                // TODO: MOUSE INPUT FOR TITLE SCREEN MENU
            }
            else
            {
                UpdateInventoryUIKeyBoardInput();
                UpdateInventoryMouseInput();
            }
        }

        private void UpdateTitleScreenKeyboardInput()
        {
            // Check input for changing your current selection within the inventory
            if (game.InputSystem.CheckKeyHeld(KeyCode.Keypad8) ||
                game.InputSystem.CheckKeyHeld(KeyCode.UpArrow) ||
                Input.GetKeyDown(KeyCode.Keypad8) ||
                Input.GetKeyDown(KeyCode.UpArrow))
            {
                titleScreenUI.ModifiySelection(-1);
            }

            else if (game.InputSystem.CheckKeyHeld(KeyCode.Keypad2) ||
                     game.InputSystem.CheckKeyHeld(KeyCode.DownArrow) ||
                     Input.GetKeyDown(KeyCode.Keypad2) ||
                     Input.GetKeyDown(KeyCode.DownArrow))
            {
                titleScreenUI.ModifiySelection(1);
            }

            // Check alphabetic input for changing your current selection within the inventory or executing options menu commands
            string alphabeticInput = game.InputSystem.GetAlphabeticInput();
            if (alphabeticInput != "")
            {
                if (alphabeticInput.ToLower() == "n")
                    titleScreenUI.ExecuteSelection(0);
                // TODO: OTHER ALPHA INPUT
            }

            if (Input.GetKeyDown(KeyCode.KeypadEnter) ||
                Input.GetKeyDown(KeyCode.Return) ||
                Input.GetKeyDown(KeyCode.Space))
            {
                titleScreenUI.ExecuteSelection();
            }
        }

        // Method for updating input
        private void UpdateInventoryUIKeyBoardInput()
        {
            if (!_inventoryDisplay)
            {
                // Check input for showing the inventory
                if (Input.GetKeyDown(KeyCode.I) || Input.GetKeyDown(KeyCode.C))
                {
                    StartCoroutine(InventoryDisplay());
                    return;
                }
            }
            else
            {
                // Check input for closing the inventory
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
                    {
                        StartCoroutine(InventoryDisplay());
                        return;
                    }
                    else
                    {
                        inventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                        return;
                    }
                }

                // Check input for changing your current selection within the inventory
                if (game.InputSystem.CheckKeyHeld(KeyCode.Keypad8) ||
                    game.InputSystem.CheckKeyHeld(KeyCode.UpArrow) ||
                    Input.GetKeyDown(KeyCode.Keypad8) ||
                    Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                        inventoryUI.ModifiyOptionsSelection(-1);
                    else if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
                        inventoryUI.ModifiySelection(-1);
                }

                else if (game.InputSystem.CheckKeyHeld(KeyCode.Keypad2) ||
                    game.InputSystem.CheckKeyHeld(KeyCode.DownArrow) ||
                    Input.GetKeyDown(KeyCode.Keypad2) ||
                    Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                        inventoryUI.ModifiyOptionsSelection(1);
                    else if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
                        inventoryUI.ModifiySelection(1);
                }

                // Check alphabetic input for changing your current selection within the inventory or executing options menu commands
                string alphabeticInput = game.InputSystem.GetAlphabeticInput();
                if (alphabeticInput != "")
                {
                    if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                    {
                        if (alphabeticInput.ToLower() == "a")
                            inventoryUI.UseSelectedItem();
                        else if (alphabeticInput.ToLower() == "u")
                            inventoryUI.UnequipSelectedItem();
                        else if (alphabeticInput.ToLower() == "e")
                            inventoryUI.EquipSelectedItem();
                        else if (alphabeticInput.ToLower() == "d")
                            inventoryUI.DropSelectedItem();
                        else if (alphabeticInput.ToLower() == "l")
                            inventoryUI.DropAllOfSelectedItem();
                        else if (alphabeticInput.ToLower() == "i")
                            inventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionItemInformation);
                    }
                    else if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
                        inventoryUI.ChangeSelection(game.Tools.GetAlphabeticIndex(alphabeticInput) + 10, true);

                }

                // Check for input for going to selected item's options menu or leaving it
                if (Input.GetKeyDown(KeyCode.KeypadEnter) ||
                    Input.GetKeyDown(KeyCode.Return) ||
                    Input.GetKeyDown(KeyCode.Space))
                {
                    if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                        inventoryUI.ExecuteOptionsSelection();
                    else if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
                        inventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionOptionsMenu);
                }
            }
        }

        // Method for checking mouse input that does not involve hoverable cells
        private void UpdateInventoryMouseInput()
        {
            if (!isHandlingAllInput)
                return;

            if (game.InputSystem.OverHoverableCell)
                return;

            if (Input.GetMouseButtonDown(1))
            {
                inventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                return;
            }
        }

        // Event listener for OnMouseDown
        private void OnMouseDown(int mouseX, int mouseY, int mouseButtonIndex)
        {
            if (mouseX == (int)quickBarInventoryPos.x &&
                mouseY == (int)quickBarInventoryPos.y)
            {
                InventoryDisplay();
                return;
            }

            // Check mousedown for inventory quit cell
            if (inventoryUI.quitCellInventory != null)
            {
                if (mouseX == (int)inventoryUI.quitCellInventory.position.x &&
                    mouseY == (int)inventoryUI.quitCellInventory.position.y)
                {
                    InventoryDisplay();
                    return;
                }
            }

            // Check mousedown for options menu quit cell
            if (inventoryUI.quitCellOptionsMenu != null)
            {
                if (mouseX == (int)inventoryUI.quitCellOptionsMenu.position.x &&
                    mouseY == (int)inventoryUI.quitCellOptionsMenu.position.y)
                {
                    inventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                    return;
                }
            }

            // Check mousedown for item information menu quit cell
            if (inventoryUI.quitCellItemInformation != null)
            {
                if (mouseX == (int)inventoryUI.quitCellItemInformation.position.x &&
                    mouseY == (int)inventoryUI.quitCellItemInformation.position.y)
                {
                    inventoryUI.SetState(InventoryUI.InventoryUIStates.Default);
                    return;
                }
            }

            // Check mousedown
            if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
            {
                if (mouseButtonIndex == 0)
                {
                    inventoryUI.ChangeSelection(inventoryUI.ConvertYPositionToIndex(mouseY));
                    inventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionOptionsMenu);
                }
                else if (mouseButtonIndex == 1)
                {
                    inventoryUI.ChangeSelection(inventoryUI.ConvertYPositionToIndex(mouseY));
                    inventoryUI.SetState(InventoryUI.InventoryUIStates.SelectionItemInformation);
                }
            }
            else if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
            {
                if (mouseButtonIndex == 0)
                    inventoryUI.ExecuteOptionsSelection();
            }

        }

        // Event listener for OnHoverEnter
        private void OnHoverEnter(int mouseX, int mouseY, int cellX, int cellY)
        {
            if (!isHandlingAllInput)
                return;

            if (mouseX > game.MapConsoleWidth)
                return;

            // Check on hover enter
            if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.Default)
                inventoryUI.ChangeSelection(inventoryUI.ConvertYPositionToIndex(cellY));
            else if (inventoryUI.inventoryUIState == InventoryUI.InventoryUIStates.SelectionOptionsMenu)
                inventoryUI.ChangeOptionsSelection((cellY - inventoryUI._optionsMinimumIndex) / 2);
        }

        // Event listener for OnHoverExit
        private void OnHoverExit(int mouseX, int mouseY)
        {
            if (!isHandlingAllInput)
                return;

            if (mouseX > game.MapConsoleWidth)
                return;
        }
    }
}