﻿using UnityEngine;
using Rogue2018.Core;
using Rogue2018.Interfaces;

using PhiOS;

namespace Rogue2018.UserInterface
{
    public class TitleScreenUI : IUserInterface
    {
        private static int _currentSelection = 0;
        private static int _selectionOffset = 69;
        private static int _maxSelection = 3;

        // IUserInterface properties
        public string[] handles { get; set; }
        public PhiCellPropertiesList[] phiCellPropertiesLists { get; set; }
        public void Draw()
        {
            // Clear title screen menu console
            game.Tools.ClearConsole(0, 20, 13, 130, 67, true);

            // Clear mouse properties
            // game.Tools.SetPhiCellRegionMouseProperties(0, 129, , game.MapConsoleHeight, false, false);

            game.Tools.DrawCellsFromPropertiesList(phiCellPropertiesLists[0]);
            game.Tools.DrawCellsFromPropertiesList(phiCellPropertiesLists[1], true, 0.25f);
            game.Tools.DrawCellsFromPropertiesList(phiCellPropertiesLists[2], true, 0.25f);

            game.Tools.DrawString(138, 70, "New Game", Color.white, Color.clear, 0, true, 0.25f);
            game.Tools.DrawString(138, 72, "Continue", Color.white, Color.clear, 0, true, 0.25f);
            game.Tools.DrawString(138, 74, "Options", Color.white, Color.clear, 0, true, 0.25f);
            game.Tools.DrawString(138, 76, "Quit", Color.white, Color.clear, 0, true, 0.25f);

            DrawSelection();
        }

        // Method for drawing current selection
        private void DrawSelection()
        {
            // Draw the selection box
            game.Tools.DrawUIBox(12, 3, 136, _selectionOffset + (_currentSelection * 2), 0, Color.clear, Color.yellow, Color.clear, "", "", false, true, 0.25f);
        }

        // Returns the current selection
        public int GetCurrentSelection()
        {
            return _currentSelection;
        }

        // Method for changing current selection using a modifier
        public void ModifiySelection(int modifier)
        {
            _currentSelection += modifier;
            if (_currentSelection < 0)
                _currentSelection = _maxSelection;
            else if (_currentSelection > _maxSelection)
                _currentSelection = 0;
            Draw();
        }

        // Method for changing current selection using an index
        public void ChangeSelection(int selectionIndex, bool goToOptions = false)
        {
            _currentSelection = selectionIndex;
            Draw();
        }

        public void ExecuteSelection(int selectionToExecute = -1)
        {
            if (selectionToExecute != -1)
            {
                if (selectionToExecute == 0)
                {
                    Game.Singleton.NewGame();
                }
                // TODO: OTHER SELECTIONS
            }
            else
            {
                if (_currentSelection == 0)
                {
                    Game.Singleton.NewGame();
                }
                // TODO: OTHER SELECTIONS
            }
        }

        // Constructor
        private Game game;
        private UIManager uiManager;
        public TitleScreenUI()
        {
            game = Game.Singleton;
            uiManager = UIManager.Singleton;

            handles = new string[] {"TitleScreen", "TitleScreenMenu", "Logo"};
            phiCellPropertiesLists = new PhiCellPropertiesList[handles.Length];

            for (int i = 0; i < handles.Length; i++)
            {
                foreach (Transform child in uiManager.transform)
                {
                    if (child.gameObject.name == handles[i] + "CellProperties")
                        phiCellPropertiesLists[i] = child.GetComponent<PhiCellPropertiesList>();
                }
            }
        }
    }
}