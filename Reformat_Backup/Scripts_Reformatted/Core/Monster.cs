﻿//-----------------------------------------------------------------------
// <copyright file="Monster.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using System;
using UnityEngine;
using RogueSharp;
using Rogue2018.Systems;
using Rogue2018.Behaviours;
using Rogue2018.Interfaces;

namespace Rogue2018.Core
{
    public class Monster : Actor
    {
        // Properties
        public int? TurnsAlerted { get; set; }
        public GoalMapMoveAttack moveAndAttackBehaviour;
        public HealthBar healthBar;

        public Monster()
        {
            moveAndAttackBehaviour = new GoalMapMoveAttack();
        }

        // Method for performing an action
        public void PerformAction(CommandSystem commandSystem)
        {
            // Decide on type of action here
            IBehaviour behaviour = moveAndAttackBehaviour;
            behaviour.Act(this, commandSystem);
        }

        // Method for drawing monster stats
        public void DrawHealthBar(int xOffset, int yOffset)
        {
            if (healthBar == null)
                healthBar = new HealthBar(Name);

            if (!healthBar.IsEnabled)
                healthBar.IsEnabled = true;

            healthBar.UpdateHealthBarPosition(X, Y, xOffset, yOffset);
            healthBar.UpdateHealthBarValues(Health, MaxHealth);
            healthBar.Draw();
        }
    }
}