﻿//-----------------------------------------------------------------------
// <copyright file="GlyphStrings.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core
{
    using Rogue2018.Core.MapCore;

    /// <summary>
    /// A class that defines a container that is responsible for holding strings for use as glyph strings
    /// </summary>
    public class GlyphStrings
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="GlyphStrings"/> class
        /// </summary>
        public GlyphStrings()
        {
            this.DoorClosed = "DOOR_CLOSED";
            this.DoorOpen = "DOOR_OPEN";
            this.Floor_Cave = "CAVE_FLOOR";
            this.Floor_Dungeon = "DUNGEON_FLOOR";
            this.Floor_Forest = "FOREST_FLOOR";
            this.Floor_Swamp = "SWAMP_FLOOR";
            this.Kobold = "KOBOLD";
            this.Player = "PLAYER";
            this.StairsDown = "STAIRS_DOWN";
            this.StairsUp = "STAIRS_UP";
            this.Tree = "TREE";
            this.Wall_Cave = "CAVE_WALL_BITMAP_";
            this.Wall_Dungeon = "DUNGEON_WALL_BITMAP_";
            this.Wall_Forest = "TREE";
            this.Wall_Swamp = "SWAMP_WALL_BITMAP_";
            this.Water = "WATER";
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or privately sets the <see cref="DoorClosed"/> property
        /// </summary>
        public string DoorClosed
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="DoorOpen"/> property
        /// </summary>
        public string DoorOpen
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Floor"/> property
        /// </summary>
        public string Floor
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Floor_Cave"/> property
        /// </summary>
        public string Floor_Cave
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Floor_Dungeon"/> property
        /// </summary>
        public string Floor_Dungeon
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Floor_Forest"/> property
        /// </summary>
        public string Floor_Forest
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Floor_Swamp"/> property
        /// </summary>
        public string Floor_Swamp
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Kobold"/> property
        /// </summary>
        public string Kobold
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Player"/> property
        /// </summary>
        public string Player
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="StairsDown"/> property
        /// </summary>
        public string StairsDown
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="StairsUp"/> property
        /// </summary>
        public string StairsUp
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Tree"/> property
        /// </summary>
        public string Tree
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Wall"/> property
        /// </summary>
        public string Wall
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Wall_Cave"/> property
        /// </summary>
        public string Wall_Cave
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Wall_Dungeon"/> property
        /// </summary>
        public string Wall_Dungeon
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Wall_Forest"/> property
        /// </summary>
        public string Wall_Forest
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Wall_Swamp"/> property
        /// </summary>
        public string Wall_Swamp
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Water"/> property
        /// </summary>
        public string Water
        {
            get; private set;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Changes the wall and floor glyph strings using the specified biome
        /// </summary>
        /// <param name="biome">A <see cref="Biomes"/> value defining the target biome</param>
        public void SetGlyphStringsForBiome(Biomes biome)
        {
            if (biome == Biomes.Cave)
            {
                this.Wall = this.Wall_Cave;
                this.Floor = this.Floor_Cave;
            }
            else if (biome == Biomes.Dungeon)
            {
                this.Wall = this.Wall_Dungeon;
                this.Floor = this.Floor_Dungeon;
            }
            else if (biome == Biomes.Forest)
            {
                this.Wall = this.Wall_Forest;
                this.Floor = this.Floor_Forest;
            }
            else if (biome == Biomes.Swamp)
            {
                this.Wall = this.Wall_Swamp;
                this.Floor = this.Floor_Swamp;
            }
        }

        #endregion
    }
}