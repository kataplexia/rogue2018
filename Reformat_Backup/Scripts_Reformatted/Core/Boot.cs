﻿//-----------------------------------------------------------------------
// <copyright file="Boot.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    /// <summary>
    /// A class that defines a type of <see cref="MonoBehaviour"/> that is responsible for booting the game
    /// </summary>
    public class Boot : MonoBehaviour
    {
        #region Methods

        /// <summary>
        /// Awake method
        /// </summary>
        private void Awake()
        {
            Screen.SetResolution(1600, 900, false);
            SceneManager.LoadScene("Game");
        }

        #endregion
    }
}