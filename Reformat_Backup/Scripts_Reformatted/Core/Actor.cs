﻿//-----------------------------------------------------------------------
// <copyright file="Actor.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core
{
    using PhiOS;
    using Rogue2018.Interfaces;
    using RogueSharp;
    using UnityEngine;

    /// <summary>
    /// A class that defines an actor entity with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IActor"/>, <see cref="IDrawable"/>, and <see cref="ISchedulable"/>
    /// </remarks>
    public abstract class Actor : IActor, IDrawable, ISchedulable
    {
        #region Fields
        private int attack;
        private int attackChance;
        private int awareness;
        private int defence;
        private int defenceChance;
        private int gold;
        private int health;
        private int maxHealth;
        private int speed;
        private string glyphString;
        private string name;

        private Color backgroundDrawColor;
        private Color backgroundDrawColorFOV;
        private Color drawColor;
        private Color drawColorFOV;
        private int x;
        private int y;
        private string symbol;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Attack"/> property
        /// </summary>
        public int Attack
        {
            get { return this.attack; } set { this.attack = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Awareness"/> property
        /// </summary>
        public int Awareness
        {
            get { return this.awareness; } set { this.awareness = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.BackgroundDrawColor"/> property
        /// </summary>
        public Color BackgroundDrawColor
        {
            get { return this.backgroundDrawColor; } set { this.backgroundDrawColor = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.BackgroundDrawColorFOV"/> property
        /// </summary>
        public Color BackgroundDrawColorFOV
        {
            get { return this.backgroundDrawColorFOV; } set { this.backgroundDrawColorFOV = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Defence"/> property
        /// </summary>
        public int Defence
        {
            get { return this.defence; } set { this.defence = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.DefenceChance"/> property
        /// </summary>
        public int DefenceChance
        {
            get { return this.defenceChance; } set { this.defenceChance = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.DrawColor"/> property
        /// </summary>
        public Color DrawColor
        {
            get { return this.drawColor; } set { this.drawColor = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.DrawColorFOV"/> property
        /// </summary>
        public Color DrawColorFOV
        {
            get { return this.drawColorFOV; } set { this.drawColorFOV = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.GlyphString"/> property
        /// </summary>
        public string GlyphString
        {
            get { return this.glyphString; } set { this.glyphString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Gold"/> property
        /// </summary>
        public int Gold
        {
            get { return this.gold; } set { this.gold = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Health"/> property
        /// </summary>
        public int Health
        {
            get { return this.health; } set { this.health = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Attack"/> property
        /// </summary>
        public int HitChance
        {
            get { return this.attackChance; } set { this.attackChance = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.MaxHealth"/> property
        /// </summary>
        public int MaxHealth
        {
            get { return this.maxHealth; } set { this.maxHealth = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Name"/> property
        /// </summary>
        public string Name
        {
            get { return this.name; } set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IActor"/>'s <see cref="IActor.Speed"/> property
        /// </summary>
        public int Speed
        {
            get { return this.speed; } set { this.speed = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.Symbol"/> property
        /// </summary>
        public string Symbol
        {
            get { return this.symbol; } set { this.symbol = value; }
        }

        /// <summary>
        /// Gets the <see cref="ISchedulable"/>'s <see cref="ISchedulable.Time"/> property
        /// </summary>
        public int Time
        {
            get { return this.Speed; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.X"/> property
        /// </summary>
        public int X
        {
            get { return this.x; } set { this.x = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IDrawable"/>'s <see cref="IDrawable.Y"/> position property
        /// </summary>
        public int Y
        {
            get { return this.y; } set { this.y = value; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Draws the <see cref="Actor"/> given offsets and an <see cref="IMap"/>
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        /// <param name="map"><see cref="IMap"/> to draw the <see cref="Actor"/> to</param>
        public void Draw(int xOffset, int yOffset, IMap map)
        {
            if (!Game.Singleton.Tools.CheckDrawable(map.GetCell(this.X, this.Y) as RogueSharp.Cell, xOffset, yOffset))
            {
                return;
            }

            if (!map.GetCell(this.X, this.Y).IsExplored)
            {
                return;
            }

            if (map.IsInFOV(this.X, this.Y))
            {
                this.BackgroundDrawColorFOV = Game.Singleton.CurrentMap.BackgroundDrawColorFOV(this.X, this.Y);
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.ActorLayer, this.X + xOffset, this.Y + yOffset);

                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? this.GlyphString : this.Symbol;
                    phiCell.SetContent(phiCellContent, this.BackgroundDrawColorFOV, this.DrawColorFOV);
                }
            }
            else
            {
                this.BackgroundDrawColor = Game.Singleton.CurrentMap.BackgroundDrawColor(this.X, this.Y);
                this.DrawColor = Game.Singleton.CurrentMap.DrawColor(this.X, this.Y);
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.ActorLayer, this.X + xOffset, this.Y + yOffset);

                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? Game.Singleton.GlyphStrings.Floor : Game.Singleton.CurrentMap.Symbol(this.X, this.Y);
                    phiCell.SetContent(phiCellContent, this.BackgroundDrawColor, this.DrawColor);
                }
            }
        }

        #endregion
    }
}