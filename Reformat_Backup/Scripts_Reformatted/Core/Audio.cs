﻿//-----------------------------------------------------------------------
// <copyright file="Audio.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using UnityEngine;

    /// <summary>
    /// A class that defines a type of <see cref="MonoBehaviour"/> that is responsible for handling audio playback
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "MonoBehaviour")]
    public class Audio : MonoBehaviour
    {
        #region Fields

        public static Audio Singleton;

        public AudioClip MonsterDeath;
        public AudioClip PlayerDeath;
        public AudioClip PlayerHit;
        public AudioClip PlayerMiss;
        public AudioClip PlayerMove;
        public AudioClip PlayerOpenDoor;
        public AudioClip PlayerStairs;
        public AudioClip PlayerStuck;
        public AudioClip PlayerTakeDamage;
        private AudioSource audioSource;
        private List<AudioClip> clipSchedule;
        private int maxClips = 1;

        #endregion

        #region Methods

        /// <summary>
        /// Queues an <see cref="AudioClip"/> to be played
        /// </summary>
        /// <param name="clip"><see cref="AudioClip"/> to play</param>
        public void QueueAudio(AudioClip clip)
        {
            if (this.clipSchedule.Count < this.maxClips)
            {
                this.clipSchedule.Add(clip);
            }
            else
            {
                this.clipSchedule.Clear();
                this.clipSchedule.Add(clip);
            }
        }

        /// <summary>
        /// <see cref="IEnumerator"/> run at start
        /// </summary>
        /// <returns><see cref="null"/></returns>
        public IEnumerator Start()
        {
            this.audioSource = this.GetComponent<AudioSource>();
            this.clipSchedule = new List<AudioClip>();
            yield return null;
        }

        /// <summary>
        /// Awake method with <see cref="Audio"/> as singleton
        /// </summary>
        private void Awake()
        {
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }

        /// <summary>
        /// Plays the next <see cref="AudioClip"/> in the queue
        /// </summary>
        /// <param name="clip"><see cref="AudioClip"/> to play</param>
        private void PlayAudio(AudioClip clip)
        {
            this.audioSource.clip = clip;
            this.audioSource.Play();
            this.clipSchedule.Remove(clip);
        }

        /// <summary>
        /// Update method called every frame
        /// </summary>
        private void Update()
        {
            if (this.clipSchedule.Count == 0)
            {
                return;
            }

            if (!this.audioSource.isPlaying)
            {
                this.PlayAudio(this.clipSchedule[0]);
            }
        }
        #endregion
    }
}