﻿//-----------------------------------------------------------------------
// <copyright file="MapGenerator.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using RogueSharp;
using RogueSharp.DiceNotation;
using RogueSharp.Algorithms;

using Rogue2018.Actors;
using Rogue2018.Actors.Monsters;

namespace Rogue2018.Core.MapCore
{
    public class MapGenerator
    {
        // Constructor shared properties
        private readonly string _mapName;
        private readonly string[] _exitStrings;
        private readonly GenerationStyles _generationStyle;
        private readonly Biomes _biome;
        private readonly int _mapWidth;
        private readonly int _mapHeight;

        // Constructor random room generation style properties
        private readonly int _maxRoomCount;
        private readonly int _maxRoomSize;
        private readonly int _minRoomSize;

        // Constructor cellular generation style properties
        private readonly int _fillProbability;
        private readonly int _iterationCount;
        private readonly int _bigAreaFillCutoff;
        private readonly bool _transparentWalls;

        // Constructor from string generation style properties
        private readonly string _mapRepresentationPath;
        private readonly string _mapRepresentation;
        private readonly string[,] _exitStringsAdvanced;
        private readonly Point _startingPoint;

        // Constructor
        private static Game game;
        private static MapCollection mapCollection;
        private readonly CurrentMap _map;
        private readonly int _actNumber;
        public MapGenerator(int actNumber, string mapName)
        {
            game = Game.Singleton;
            mapCollection = game.MapCollection;

            _actNumber = actNumber;

            // Get map generation properties from map collection using act level and map level
            MapProperties mapProperties = mapCollection.GetMapProperties(_actNumber, mapName);

            // Initialize shared properties
            _mapName = mapProperties.MapName;
            _exitStrings = mapProperties.ExitStrings;
            _generationStyle = mapProperties.GenerationStyle;
            _biome = mapProperties.Biome;
            _mapWidth = mapProperties.MapWidth;
            _mapHeight = mapProperties.MapHeight;

            if (_generationStyle == GenerationStyles.RandomRooms)
            {
                _maxRoomCount = mapProperties.MaxRoomCount;
                _maxRoomSize = mapProperties.MaxRoomSize;
                _minRoomSize = mapProperties.MinRoomSize;
            }
            else if (_generationStyle == GenerationStyles.Cellular)
            {
                _fillProbability = mapProperties.FillProbability;
                _iterationCount = mapProperties.IterationCount;
                _bigAreaFillCutoff = mapProperties.BigAreaFillCutoff;
                _transparentWalls = mapProperties.TransparentWalls;
            }
            else if (_generationStyle == GenerationStyles.FromString)
            {
                _mapRepresentationPath = mapProperties.MapRepresentationPath;
                TextAsset textAsset = Resources.Load(_mapRepresentationPath) as TextAsset;
                _mapRepresentation = textAsset.text;
                _exitStringsAdvanced = mapProperties.ExitStringsAdvanced;
                _startingPoint = mapProperties.StartingPoint;
            }

            _map = new CurrentMap();
        }

        // Method for creating a map
        private static int _padding;
        public CurrentMap GenerateMap()
        {
            // Generate map according to border only style
            if (_generationStyle == GenerationStyles.BorderOnly)
            {
                _map.Initialize(_mapWidth, _mapHeight);
                CreateMap_BorderOnlyStyle();
                SetBitmaskForMap();
                CreateExits_BorderOnlyStyle();
                SetCellDrawPropertiesForMap();
                PlacePlayer_BorderOnlyStyle();
                CreateGoalMaps();
            }

            // Generate map according to random rooms style
            else if (_generationStyle == GenerationStyles.RandomRooms)
            {
                _map.Initialize(_mapWidth, _mapHeight);
                CreateMap_RandomRoomsStyle();
                SetBitmaskForMap();
                CreateExits_RandomRoomsStyle();
                SetCellDrawPropertiesForMap();
                PlacePlayer_RandomRoomsStyle();
                PlaceMonsters_RandomRoomsStyle();
                CreateGoalMaps();
            }

            // Generate map according to cellular style
            else if (_generationStyle == GenerationStyles.Cellular)
            {
                if (_transparentWalls)
                    if (game.Player != null) _padding = 2 * game.Player.Awareness; else _padding = 50;
                _map.Initialize(_mapWidth + _padding, _mapHeight + _padding);
                CreateMap_CellularStyle();
                SetBitmaskForMap();
                CreateExits_CellularStyle();
                SetCellDrawPropertiesForMap();
                PlacePlayer_CellularStyle();
                // PlaceMonsters_CellularStyle();
                CreateGoalMaps();
            }

            // Generate map according to from string style
            else if (_generationStyle == GenerationStyles.FromString)
            {
                _map.Initialize(_mapWidth, _mapHeight);
                CreateMap_FromStringStyle();
                PlacePlayer_FromStringStyle();
                CreateGoalMaps();
            }

            // Store the created map in the MapCollection
            mapCollection.StoreMap(_actNumber, _mapName, _map);

            // Return the map to the Game class
            return _map;
        }

        private void CreateGoalMaps()
        {
            _map.PlayerGoalMap = new GoalMap(_map, true);
            _map.MonsterGoalMap = new GoalMap(_map, true);
            _map.MonsterGoalMap.AddGoal(game.Player.X, game.Player.Y, 1);
            foreach (Monster monster in _map.Monsters)
                _map.MonsterGoalMap.AddObstacle(monster.X, monster.Y);
            _map.MonsterGoalMap.ComputeCellWeightsLimited(_map.GetCell(game.Player.X, game.Player.Y), 20);
        }

        // Method for creating a border only map
        private void CreateMap_BorderOnlyStyle()
        {
            _map.Clear(true, true);

            foreach (ICell cell in _map.GetCellsInRows(0, _mapHeight - 1))
                _map.SetCellProperties(cell.X, cell.Y, false, false);

            foreach (ICell cell in _map.GetCellsInColumns(0, _mapWidth - 1))
                _map.SetCellProperties(cell.X, cell.Y, false, false);
        }

        // Method for creating random rooms map
        private void CreateMap_RandomRoomsStyle()
        {
            // Generate rooms
            for (int r = _maxRoomCount; r > 0; r--)
            {
                // Set size and position of room
                int roomWidth = game.Random.Next(_minRoomSize, _maxRoomSize);
                int roomHeight = game.Random.Next(_minRoomSize, _maxRoomSize);
                int roomXPosition = game.Random.Next(0, _mapWidth - roomWidth - 1);
                int roomYPosition = game.Random.Next(0, _mapHeight - roomHeight - 1);

                // Create rectangle
                var newRoom = new Rectangle(roomXPosition, roomYPosition, roomWidth, roomHeight);

                // Check for overlap
                bool newRoomIntersects = _map.Rooms.Any(room => newRoom.Intersects(room));

                // Add to list
                if (!newRoomIntersects)
                    _map.Rooms.Add(newRoom);
            }

            // Create rooms
            foreach (Rectangle room in _map.Rooms)
                CreateRoom(room);

            // Iterate through rooms and create tunnels
            for (int r = 1; r < _map.Rooms.Count; r++)
            {
                int previousRoomCenterX = _map.Rooms[r - 1].Center.X;
                int previousRoomCenterY = _map.Rooms[r - 1].Center.Y;
                int currentRoomCenterX = _map.Rooms[r].Center.X;
                int currentRoomCenterY = _map.Rooms[r].Center.Y;

                // 50% chance to generate connecting hallway
                if (game.Random.Next(1, 2) == 1)
                {
                    CreateHorizontalTunnel(previousRoomCenterX, currentRoomCenterX, previousRoomCenterY);
                    CreateVerticalTunnel(previousRoomCenterY, currentRoomCenterY, currentRoomCenterX);
                }
                else
                {
                    CreateHorizontalTunnel(previousRoomCenterX, currentRoomCenterX, currentRoomCenterY);
                    CreateVerticalTunnel(previousRoomCenterY, currentRoomCenterY, previousRoomCenterX);
                }
            }

            // Create doors
            foreach (Rectangle room in _map.Rooms)
                CreateDoors(room);
        }

        // Method for creating a room
        private void CreateRoom(Rectangle room)
        {
            for (int x = room.Left + 1; x < room.Right; x++)
            {
                for (int y = room.Top + 1; y < room.Bottom; y++)
                    _map.SetCellProperties(x, y, true, true, false);
            }
        }

        // Method for creating a horizontal tunnel between rooms
        private void CreateHorizontalTunnel(int xStart, int xEnd, int yPos)
        {
            for (int x = Math.Min(xStart, xEnd); x <= Math.Max(xStart, xEnd); x++)
                _map.SetCellProperties(x, yPos, true, true);
        }

        // Method for creating a vertical tunnel between rooms
        private void CreateVerticalTunnel(int yStart, int yEnd, int xPos)
        {
            for (int y = Math.Min(yStart, yEnd); y <= Math.Max(yStart, yEnd); y++)
                _map.SetCellProperties(xPos, y, true, true);
        }

        // Method for creating doors for a room
        private void CreateDoors(Rectangle room)
        {
            // Room boundaries
            int xMin = room.Left;
            int xMax = room.Right;
            int yMin = room.Top;
            int yMax = room.Bottom;

            // Put border cells into a list
            List<ICell> borderRogueCells = _map.GetCellsAlongLine(xMin, yMin, xMax, yMin).ToList();
            borderRogueCells.AddRange(_map.GetCellsAlongLine(xMin, yMin, xMin, yMax));
            borderRogueCells.AddRange(_map.GetCellsAlongLine(xMin, yMax, xMax, yMax));
            borderRogueCells.AddRange(_map.GetCellsAlongLine(xMax, yMin, xMax, yMax));

            // Check border cells for door placement
            foreach (RogueSharp.Cell rogueCell in borderRogueCells)
            {
                if (IsPotentialDoor(rogueCell))
                    CreateDoor(rogueCell.X, rogueCell.Y);
            }
        }

        private void CreateDoor(int x, int y)
        {
            _map.SetCellProperties(x, y, false, true);
            _map.Doors.Add(new Door { X = x, Y = y, IsOpen = false });
        }

        // Check rogueCell to see if able to place door
        private bool IsPotentialDoor(RogueSharp.Cell rogueCell)
        {
            if (!rogueCell.IsWalkable)
                return false;

            // Store references to neighbouring rogueCells
            RogueSharp.Cell rogueCellRight = _map.GetCell(rogueCell.X + 1, rogueCell.Y) as RogueSharp.Cell;
            RogueSharp.Cell rogueCellLeft = _map.GetCell(rogueCell.X - 1, rogueCell.Y) as RogueSharp.Cell;
            RogueSharp.Cell rogueCellUp = _map.GetCell(rogueCell.X, rogueCell.Y - 1) as RogueSharp.Cell;
            RogueSharp.Cell rogueCellDown = _map.GetCell(rogueCell.X, rogueCell.Y + 1) as RogueSharp.Cell;

            // Check neighbouring rogueCells for door
            if (_map.GetDoorAt(rogueCell.X, rogueCell.Y) != null ||
                _map.GetDoorAt(rogueCellRight.X, rogueCellRight.Y) != null ||
                _map.GetDoorAt(rogueCellLeft.X, rogueCellLeft.Y) != null ||
                _map.GetDoorAt(rogueCellUp.X, rogueCellUp.Y) != null ||
                _map.GetDoorAt(rogueCellDown.X, rogueCellDown.Y) != null)
                return false;

            // Place door on left or right of room
            if (rogueCellRight.IsWalkable &&
                rogueCellLeft.IsWalkable &&
                !rogueCellUp.IsWalkable &&
                !rogueCellDown.IsWalkable)
                return true;

            // Place door on top or bottom of room
            if (!rogueCellRight.IsWalkable &&
                !rogueCellLeft.IsWalkable &&
                rogueCellUp.IsWalkable &&
                rogueCellDown.IsWalkable)
                return true;

            return false;
        }

        // Method for creating cellular map
        private void CreateMap_CellularStyle()
        {
            RandomlyFillCells();

            for (int i = 0; i < _iterationCount; i++)
            {
                if (i < _bigAreaFillCutoff)
                    CellularAutomataBigAreaAlgorithm();
                else if (i >= _bigAreaFillCutoff)
                    CellularAutomaNearestNeighborsAlgorithm();
            }

            ConnectCellularAreas();
        }

        private void RandomlyFillCells()
        {
            foreach (ICell cell in _map.GetAllCells())
            {
                if (_map.IsBorderCell(cell, _padding))
                    _map.SetCellProperties(cell.X, cell.Y, _transparentWalls, false);
                else if (game.Random.Next(1, 100) < _fillProbability)
                    _map.SetCellProperties(cell.X, cell.Y, true, true);
                else
                    _map.SetCellProperties(cell.X, cell.Y, _transparentWalls, false);
            }
        }

        private void CellularAutomataBigAreaAlgorithm()
        {
            var updatedMap = _map.Clone();

            foreach (ICell cell in _map.GetAllCells())
            {
                if (_map.IsBorderCell(cell, _padding))
                    continue;
                if ((_map.CountWallsNear(cell, 1) >= 5) || (_map.CountWallsNear(cell, 2) <= 2))
                    updatedMap.SetCellProperties(cell.X, cell.Y, _transparentWalls, false);
                else
                    updatedMap.SetCellProperties(cell.X, cell.Y, true, true);
            }

            _map.UpdateMap(updatedMap);
        }

        private void CellularAutomaNearestNeighborsAlgorithm()
        {
            var updatedMap = _map.Clone();

            foreach (ICell cell in _map.GetAllCells())
            {
                if (_map.IsBorderCell(cell, _padding))
                    continue;
                if (_map.CountWallsNear(cell, 1) >= 5)
                    updatedMap.SetCellProperties(cell.X, cell.Y, _transparentWalls, false);
                else
                    updatedMap.SetCellProperties(cell.X, cell.Y, true, true);
            }

            _map.UpdateMap(updatedMap);
        }

        private void ConnectCellularAreas()
        {
            var floodFillAnalyzer = new FloodFillAnalyzer(_map);
            List<MapSection> mapSections = floodFillAnalyzer.GetMapSections();
            var unionFind = new UnionFind(mapSections.Count);
            while (unionFind.Count > 1)
            {
                for (int i = 0; i < mapSections.Count; i++)
                {
                    int closestMapSectionIndex = FindNearestMapSection(mapSections, i, unionFind);
                    MapSection closestMapSection = mapSections[closestMapSectionIndex];

                    IEnumerable<ICell> tunnelCells
                        = _map.GetCellsAlongLine(
                            mapSections[i].Bounds.Center.X,
                            mapSections[i].Bounds.Center.Y,
                            closestMapSection.Bounds.Center.X,
                            closestMapSection.Bounds.Center.Y);

                    ICell previousCell = null;
                    foreach (ICell cell in tunnelCells)
                    {
                        _map.SetCellProperties(cell.X, cell.Y, true, true);
                        if (previousCell != null)
                        {
                            if (cell.X != previousCell.X || cell.Y != previousCell.Y)
                                _map.SetCellProperties(cell.X + 1, cell.Y, true, true);
                        }
                        previousCell = cell;
                    }
                    unionFind.Union(i, closestMapSectionIndex);
                }
            }
        }

        private static int FindNearestMapSection(IList<MapSection> mapSections, int mapSectionIndex, UnionFind unionFind)
        {
            MapSection start = mapSections[mapSectionIndex];
            int closestIndex = mapSectionIndex;
            int distance = Int32.MaxValue;
            for (int i = 0; i < mapSections.Count; i++)
            {
                if (i == mapSectionIndex)
                    continue;
                if (unionFind.Connected(i, mapSectionIndex))
                    continue;
                int distanceBetween = DistanceBetween(start, mapSections[i]);
                if (distanceBetween < distance)
                {
                    distance = distanceBetween;
                    closestIndex = i;
                }
            }
            return closestIndex;
        }

        private static int DistanceBetween(MapSection startMapSection, MapSection destinationMapSection)
        {
            return Math.Abs(
                startMapSection.Bounds.Center.X - destinationMapSection.Bounds.Center.X) +
                Math.Abs(startMapSection.Bounds.Center.Y - destinationMapSection.Bounds.Center.Y);
        }

        private void CreateMap_FromStringStyle()
        {
            string[] lines = _mapRepresentation.Replace(" ", "").Replace("\r", "").Split('\n');

            for (int y = 0; y < _mapHeight; y++)
            {
                float colorRandomModifier = 0.825f;
                float differenceModifier = 1.0f;

                string line = lines[y];
                for (int x = 0; x < _mapWidth; x++)
                {
                    Color DrawColor = Color.clear;
                    Color BackgroundDrawColor = Color.clear;
                    Color DrawColorFOV = Color.clear;
                    Color BackgroundDrawColorFOV = Color.clear;

                    Color MiniMapDrawColor = Color.clear;
                    Color MiniMapDrawColorFOV = Color.clear;

                    string Symbol = string.Empty;
                    string GlyphString = string.Empty;

                    if (line[x] == '.')
                    {
                        _map.SetCellProperties(x, y, true, true);

                        DrawColor = game.Swatch.Floor_Dungeon;
                        BackgroundDrawColor = game.Swatch.FloorBackground_Dungeon;
                        DrawColorFOV = game.Swatch.FloorFOV_Dungeon;
                        BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV_Dungeon;

                        MiniMapDrawColor = game.Swatch.MiniMapFloor_Dungeon;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapFloorFOV_Dungeon;

                        int randomIndex = game.Random.Next(0, game.Symbols.Dungeon_Floors.Length - 1);
                        Symbol = game.Symbols.Dungeon_Floors[randomIndex];

                        GlyphString = game.GlyphStrings.Floor_Dungeon;
                    }

                    else if (line[x] == ',')
                    {
                        _map.SetCellProperties(x, y, true, true);

                        DrawColor = game.Swatch.Floor_Forest;
                        BackgroundDrawColor = game.Swatch.FloorBackground_Forest;
                        DrawColorFOV = game.Swatch.FloorFOV_Forest;
                        BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV_Forest;

                        MiniMapDrawColor = game.Swatch.MiniMapFloor_Forest;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapFloorFOV_Forest;

                        int randomIndex = game.Random.Next(0, game.Symbols.Forest_Floors.Length - 1);
                        Symbol = game.Symbols.Forest_Floors[randomIndex];

                        GlyphString = game.GlyphStrings.Floor_Forest;
                    }

                    else if (line[x] == '+')
                    {
                        CreateDoor(x, y);

                        // DrawColor = game.swatch.Floor_Dungeon;
                        BackgroundDrawColor = game.Swatch.FloorBackground_Dungeon;
                        // DrawColorFOV = game.swatch.FloorFOV_Dungeon;
                        BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV_Dungeon;

                        MiniMapDrawColor = game.Swatch.MiniMapFloor_Dungeon;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapFloorFOV_Dungeon;
                    }

                    else if (line[x] == '≈')
                    {
                        _map.SetCellProperties(x, y, true, false);

                        DrawColor = game.Swatch.Wall_Swamp;
                        BackgroundDrawColor = game.Swatch.WallBackground_Swamp;
                        DrawColorFOV = game.Swatch.WallFOV_Swamp;
                        BackgroundDrawColorFOV = game.Swatch.WallBackgroundFOV_Swamp;

                        MiniMapDrawColor = game.Swatch.MiniMapWall_Swamp;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapWallFOV_Swamp;

                        Symbol = game.Symbols.Water;

                        differenceModifier = 0.25f;
                    }

                    else if (line[x] == '#')
                    {
                        _map.SetCellProperties(x, y, false, false);

                        DrawColor = game.Swatch.Wall_Dungeon;
                        BackgroundDrawColor = game.Swatch.WallBackground_Dungeon;
                        DrawColorFOV = game.Swatch.WallFOV_Dungeon;
                        BackgroundDrawColorFOV = game.Swatch.WallBackgroundFOV_Dungeon;

                        MiniMapDrawColor = game.Swatch.MiniMapWall_Dungeon;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapWallFOV_Dungeon;

                        Symbol = game.Symbols.Wall;

                        differenceModifier = 0.25f;
                    }

                    else if (line[x] == 'T')
                    {
                        _map.SetCellProperties(x, y, false, false);

                        DrawColor = game.Swatch.Wall_Forest;
                        BackgroundDrawColor = game.Swatch.WallBackground_Forest;
                        DrawColorFOV = game.Swatch.WallFOV_Forest;
                        BackgroundDrawColorFOV = game.Swatch.WallBackgroundFOV_Forest;

                        MiniMapDrawColor = game.Swatch.MiniMapWall_Forest;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapWallFOV_Forest;

                        int randomIndex = game.Random.Next(0, game.Symbols.Trees.Length - 1);
                        Symbol = game.Symbols.Trees[randomIndex];

                        differenceModifier = 0.25f;
                    }

                    else if (line[x] == '<')
                    {
                        _map.SetCellProperties(x, y, true, true);
                        _map.Exits.Add(CreateExit(x, y, _exitStringsAdvanced[x, y], true));

                        // DrawColor = game.swatch.Floor_Dungeon;
                        BackgroundDrawColor = game.Swatch.FloorBackground_Dungeon;
                        // DrawColorFOV = game.swatch.FloorFOV_Dungeon;
                        BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV_Dungeon;

                        MiniMapDrawColor = game.Swatch.MiniMapFloor_Dungeon;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapFloorFOV_Dungeon;
                    }

                    else if (line[x] == '>')
                    {
                        _map.SetCellProperties(x, y, true, true);
                        _map.Exits.Add(CreateExit(x, y, _exitStringsAdvanced[x, y], false));

                        // DrawColor = game.swatch.Floor_Dungeon;
                        BackgroundDrawColor = game.Swatch.FloorBackground_Dungeon;
                        // DrawColorFOV = game.swatch.FloorFOV_Dungeon;
                        BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV_Dungeon;

                        MiniMapDrawColor = game.Swatch.MiniMapFloor_Dungeon;
                        MiniMapDrawColorFOV = game.Swatch.MiniMapFloorFOV_Dungeon;
                    }

                    float randomDCFOV_R = UnityEngine.Random.Range(DrawColorFOV.r, DrawColorFOV.r * colorRandomModifier);
                    float randomDCFOV_G = UnityEngine.Random.Range(DrawColorFOV.g, DrawColorFOV.g * colorRandomModifier);
                    float randomDCFOV_B = UnityEngine.Random.Range(DrawColorFOV.b, DrawColorFOV.b * colorRandomModifier);

                    float randomDCFOV_R_Dif = (DrawColorFOV.r - randomDCFOV_R) * differenceModifier;
                    float randomDCFOV_G_Dif = (DrawColorFOV.g - randomDCFOV_G) * differenceModifier;
                    float randomDCFOV_B_Dif = (DrawColorFOV.b - randomDCFOV_B) * differenceModifier;

                    DrawColorFOV = new Color(randomDCFOV_R, randomDCFOV_G, randomDCFOV_B);

                    float randomBGDCFOV_R = UnityEngine.Random.Range(BackgroundDrawColorFOV.r, BackgroundDrawColorFOV.r * colorRandomModifier);
                    float randomBGDCFOV_G = UnityEngine.Random.Range(BackgroundDrawColorFOV.g, BackgroundDrawColorFOV.g * colorRandomModifier);
                    float randomBGDCFOV_B = UnityEngine.Random.Range(BackgroundDrawColorFOV.b, BackgroundDrawColorFOV.b * colorRandomModifier);

                    float randomBGDCFOV_R_Dif = (BackgroundDrawColorFOV.r - randomBGDCFOV_R) * differenceModifier;
                    float randomBGDCFOV_G_Dif = (BackgroundDrawColorFOV.g - randomBGDCFOV_G) * differenceModifier;
                    float randomBGDCFOV_B_Dif = (BackgroundDrawColorFOV.b - randomBGDCFOV_B) * differenceModifier;

                    BackgroundDrawColorFOV = new Color(randomBGDCFOV_R, randomBGDCFOV_G, randomBGDCFOV_B);

                    float randomDC_R = DrawColor.r - randomDCFOV_R_Dif;
                    float randomDC_G = DrawColor.g - randomDCFOV_G_Dif;
                    float randomDC_B = DrawColor.b - randomDCFOV_B_Dif;
                    DrawColor = new Color(randomDC_R, randomDC_G, randomDC_B);

                    float randomBGDC_R = BackgroundDrawColor.r - randomBGDCFOV_R_Dif;
                    float randomBGDC_G = BackgroundDrawColor.g - randomBGDCFOV_G_Dif;
                    float randomBGDC_B = BackgroundDrawColor.b - randomBGDCFOV_B_Dif;
                    BackgroundDrawColor = new Color(randomBGDC_R, randomBGDC_G, randomBGDC_B);

                    _map.SetDrawColor(x, y, DrawColor);
                    _map.SetBackgroundDrawColor(x, y, BackgroundDrawColor);
                    _map.SetDrawColorFOV(x, y, DrawColorFOV);
                    _map.SetBackgroundDrawColorFOV(x, y, BackgroundDrawColorFOV);

                    _map.SetMiniMapDrawColor(x, y, MiniMapDrawColor);
                    _map.SetMiniMapDrawColorFOV(x, y, MiniMapDrawColorFOV);

                    if (GlyphString != string.Empty)
                        _map.SetGlyphString(x, y, GlyphString);

                    _map.SetSymbol(x, y, Symbol);
                }
            }

            int padding = 0;
            foreach (RogueSharp.Cell rogueCell in _map.GetAllCells())
                _map.SetBitmaskValue(rogueCell.X, rogueCell.Y, CreateBitmaskValue(rogueCell.X, rogueCell.Y, padding));

            for (int y = 0; y < _mapHeight; y++)
            {
                

                string line = lines[y];
                for (int x = 0; x < _mapWidth; x++)
                {
                    string GlyphString = string.Empty;

                    if (line[x] == '≈')
                    {
                        GlyphString = game.GlyphStrings.Wall_Swamp;
                        if (GlyphString[GlyphString.Length - 1] == '_')
                            GlyphString += _map.BitmaskValue(x, y).ToString();
                    }
                    else if (line[x] == '#')
                    {
                        GlyphString = game.GlyphStrings.Wall_Dungeon;
                        if (GlyphString[GlyphString.Length - 1] == '_')
                            GlyphString += _map.BitmaskValue(x, y).ToString();
                    }
                    else if (line[x] == 'T')
                    {
                        GlyphString = game.GlyphStrings.Wall_Forest;
                        if (GlyphString[GlyphString.Length - 1] == '_')
                            GlyphString += _map.BitmaskValue(x, y).ToString();
                    }

                    if (GlyphString != string.Empty)
                        _map.SetGlyphString(x, y, GlyphString);
                }
            }
        }

        // Returns a new exit given x, y and exit string
        private Exit CreateExit(int x, int y, string exitString, bool isUp)
        {
            // Debug.Log("Created exit to: " + exitString + " at [" + x + ", " + y + "]");
            return new Exit { game = game, X = x, Y = y, ExitToName = exitString, IsUp = isUp };
        }

        // Method for creating exits for border only style
        private void CreateExits_BorderOnlyStyle()
        {
            if (_exitStrings.Length > 4)
            {
                Debug.LogError("Too many exits for border only creation style.");
                return;
            }

            List<Point> cornerPoints = new List<Point>(4);
            cornerPoints.Add(new Point(2, 2));
            cornerPoints.Add(new Point(_mapWidth - 3, 2));
            cornerPoints.Add(new Point(_mapWidth - 3, _mapHeight - 3));
            cornerPoints.Add(new Point(2, _mapHeight - 3));

            int exitIndex = 0;
            foreach (string exitString in _exitStrings)
            {
                if (exitIndex == 0 && exitString != "DEADEND")
                    _map.Exits.Add(CreateExit(cornerPoints.First().X, cornerPoints.First().Y, exitString, true));

                else if (exitIndex == _exitStrings.Length - 1 && exitString != "DEADEND")
                    _map.Exits.Add(CreateExit(cornerPoints.Last().X, cornerPoints.Last().Y, exitString, false));
                else if (exitString != "DEADEND")
                    _map.Exits.Add(CreateExit(cornerPoints[exitIndex].X, cornerPoints[exitIndex].Y, exitString, false));
                exitIndex++;
            }
        }

        // Method for creating exits for random rooms style
        private void CreateExits_RandomRoomsStyle()
        {
            int exitIndex = 0;
            foreach (string exitString in _exitStrings)
            {
                if (exitIndex == 0 && exitString != "DEADEND")
                    _map.Exits.Add(CreateExit(_map.Rooms.First().Center.X, _map.Rooms.First().Center.Y, exitString, true));

                else if (exitIndex == _exitStrings.Length - 1 && exitString != "DEADEND")
                    _map.Exits.Add(CreateExit(_map.Rooms.Last().Center.X, _map.Rooms.Last().Center.Y, exitString, false));
                else if (exitString != "DEADEND")
                {
                    int roomIndex = (_map.Rooms.Count - 1) - ((_exitStrings.Length - 1) - exitIndex);
                    _map.Exits.Add(CreateExit(_map.Rooms[roomIndex].Center.X, _map.Rooms[roomIndex].Center.Y, exitString, false));
                }
                exitIndex++;
            }
        }

        // Method for creating exits for random rooms style
        public List<MapSection> divisionMapSections = new List<MapSection>();
        private void CreateExits_CellularStyle()
        {
            DivisionAnalyzer divisionAnalyzer = new DivisionAnalyzer(_map, _padding, 5, 5);
            divisionMapSections = divisionAnalyzer.GetMapSections();

            /*
            int sectionIndex = 0;
            foreach (MapSection mapSection in divisionMapSections)
            {
                foreach (ICell cell in mapSection.Cells)
                {
                    // Debug.Log(string.Format("MapSection: {0}, Cell: [{1}, {2}]", sectionIndex, cell.X, cell.Y));
                }

                Debug.LogWarning(string.Format("MapSection: {0}, CellCount: {1}", sectionIndex, mapSection.Cells.Count));
                Debug.Log(string.Format("Top: {0}, Bottom: {1},  Left: {2}, Right: {3}",
                    mapSection.Bounds.Top,
                    mapSection.Bounds.Bottom,
                    mapSection.Bounds.Left,
                    mapSection.Bounds.Right));
                sectionIndex++;
            }
            */

            int exitIndex = 0;
            foreach (string exitString in _exitStrings)
            {
                // Create first exit (exit back)
                if (exitIndex == 0 && exitString != "DEADEND")
                {
                    bool exitCreated = false;
                    for (int i = 0; i < divisionMapSections.Count; i++)
                    {
                        if (exitCreated)
                            break;

                        if (!divisionMapSections[i].HasExit)
                        {
                            foreach (ICell cell in divisionMapSections[i].Cells)
                            {
                                if (cell.IsWalkable && _map.CountWallsNear(cell, 1) == 0)
                                {
                                    _map.Exits.Add(CreateExit(cell.X, cell.Y, exitString, true));
                                    divisionMapSections[i].HasExit = true;
                                    // divisionMapSections[i].ExitPoint = new Point(cell.X, cell.Y);
                                    divisionMapSections[i].ExitPointX = cell.X;
                                    divisionMapSections[i].ExitPointY = cell.Y;
                                    exitCreated = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                // Create second exit (exit forward)
                else if (exitIndex == _exitStrings.Length - 1 && exitString != "DEADEND")
                {
                    bool exitCreated = false;
                    for (int i = divisionMapSections.Count - 1; i >= 0; i--)
                    {
                        if (exitCreated)
                            break;

                        if (!divisionMapSections[i].HasExit)
                        {
                            foreach (ICell cell in divisionMapSections[i].Cells)
                            {
                                if (cell.IsWalkable && _map.CountWallsNear(cell, 1) == 0)
                                {
                                    _map.Exits.Add(CreateExit(cell.X, cell.Y, exitString, false));
                                    divisionMapSections[i].HasExit = true;
                                    // divisionMapSections[i].ExitPoint = new Point(cell.X, cell.Y);
                                    divisionMapSections[i].ExitPointX = cell.X;
                                    divisionMapSections[i].ExitPointY = cell.Y;
                                    exitCreated = true;
                                    break;
                                }
                            }
                        } 
                    }
                }

                // create side exits
                else if (exitString != "DEADEND")
                {
                    MapSection randomSuitableMapLocation = _map.GetRandomSuitableMapSection(divisionMapSections);
                    if (randomSuitableMapLocation != null)
                    {
                        foreach (ICell cell in randomSuitableMapLocation.Cells)
                        {
                            if (cell.IsWalkable && _map.CountWallsNear(cell, 1) == 0)
                            {
                                _map.Exits.Add(CreateExit(cell.X, cell.Y, exitString, false));
                                randomSuitableMapLocation.HasExit = true;
                                // randomSuitableMapLocation.ExitPoint = new Point(cell.X, cell.Y);
                                randomSuitableMapLocation.ExitPointX = cell.X;
                                randomSuitableMapLocation.ExitPointY = cell.Y;
                                break;
                            }
                        }
                    }
                }

                exitIndex++;
            }
        }

        // Method for setting bitmask values for the map's cells
        private void SetBitmaskForMap(int padding = 0)
        {
            foreach (RogueSharp.Cell rogueCell in _map.GetAllCells())
                _map.SetBitmaskValue(rogueCell.X, rogueCell.Y, CreateBitmaskValue(rogueCell.X, rogueCell.Y, padding));
        }

        // Method for setting cell draw properties for the map
        private void SetCellDrawPropertiesForMap()
        {
            float colorRandomModifier = 0.825f;
            foreach (RogueSharp.Cell rogueCell in _map.GetAllCells())
            {
                if (rogueCell.IsWalkable)
                {
                    Color DrawColorFOV = game.Swatch.FloorFOV;
                    float randomDCFOV_R = UnityEngine.Random.Range(DrawColorFOV.r, DrawColorFOV.r * colorRandomModifier);
                    float randomDCFOV_G = UnityEngine.Random.Range(DrawColorFOV.g, DrawColorFOV.g * colorRandomModifier);
                    float randomDCFOV_B = UnityEngine.Random.Range(DrawColorFOV.b, DrawColorFOV.b * colorRandomModifier);

                    float randomDCFOV_R_Dif = DrawColorFOV.r - randomDCFOV_R;
                    float randomDCFOV_G_Dif = DrawColorFOV.g - randomDCFOV_G;
                    float randomDCFOV_B_Dif = DrawColorFOV.b - randomDCFOV_B;

                    DrawColorFOV = new Color(randomDCFOV_R, randomDCFOV_G, randomDCFOV_B);

                    Color BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV;
                    float randomBGDCFOV_R = UnityEngine.Random.Range(BackgroundDrawColorFOV.r, BackgroundDrawColorFOV.r * colorRandomModifier);
                    float randomBGDCFOV_G = UnityEngine.Random.Range(BackgroundDrawColorFOV.g, BackgroundDrawColorFOV.g * colorRandomModifier);
                    float randomBGDCFOV_B = UnityEngine.Random.Range(BackgroundDrawColorFOV.b, BackgroundDrawColorFOV.b * colorRandomModifier);

                    float randomBGDCFOV_R_Dif = BackgroundDrawColorFOV.r - randomBGDCFOV_R;
                    float randomBGDCFOV_G_Dif = BackgroundDrawColorFOV.g - randomBGDCFOV_G;
                    float randomBGDCFOV_B_Dif = BackgroundDrawColorFOV.b - randomBGDCFOV_B;

                    BackgroundDrawColorFOV = new Color(randomBGDCFOV_R, randomBGDCFOV_G, randomBGDCFOV_B);

                    Color DrawColor = game.Swatch.Floor;
                    float randomDC_R = DrawColor.r - randomDCFOV_R_Dif;
                    float randomDC_G = DrawColor.g - randomDCFOV_G_Dif;
                    float randomDC_B = DrawColor.b - randomDCFOV_B_Dif;
                    DrawColor = new Color(randomDC_R, randomDC_G, randomDC_B);

                    Color BackgroundDrawColor = game.Swatch.FloorBackground;
                    float randomBGDC_R = BackgroundDrawColor.r - randomBGDCFOV_R_Dif;
                    float randomBGDC_G = BackgroundDrawColor.g - randomBGDCFOV_G_Dif;
                    float randomBGDC_B = BackgroundDrawColor.b - randomBGDCFOV_B_Dif;
                    BackgroundDrawColor = new Color(randomBGDC_R, randomBGDC_G, randomBGDC_B);

                    string Symbol = game.Symbols.Floor;
                    string GlyphString = game.GlyphStrings.Floor;

                    if (_biome == Biomes.Cave)
                    {
                        int randomIndex = game.Random.Next(0, game.Symbols.Cave_Floors.Length - 1);
                        Symbol = game.Symbols.Cave_Floors[randomIndex];
                    }
                    else if (_biome == Biomes.Dungeon)
                    {
                        int randomIndex = game.Random.Next(0, game.Symbols.Dungeon_Floors.Length - 1);
                        Symbol = game.Symbols.Dungeon_Floors[randomIndex];
                    }
                    else if (_biome == Biomes.Forest)
                    {
                        int randomIndex = game.Random.Next(0, game.Symbols.Forest_Floors.Length - 1);
                        Symbol = game.Symbols.Forest_Floors[randomIndex];
                    }
                    else if (_biome == Biomes.Swamp)
                    {
                        int randomIndex = game.Random.Next(0, game.Symbols.Swamp_Floors.Length - 1);
                        Symbol = game.Symbols.Swamp_Floors[randomIndex];
                    }

                    _map.SetDrawColor(rogueCell.X, rogueCell.Y, DrawColor);
                    _map.SetBackgroundDrawColor(rogueCell.X, rogueCell.Y, BackgroundDrawColor);
                    _map.SetDrawColorFOV(rogueCell.X, rogueCell.Y, DrawColorFOV);
                    _map.SetBackgroundDrawColorFOV(rogueCell.X, rogueCell.Y, BackgroundDrawColorFOV);

                    _map.SetGlyphString(rogueCell.X, rogueCell.Y, GlyphString);
                    _map.SetSymbol(rogueCell.X, rogueCell.Y, Symbol);

                    _map.SetMiniMapDrawColor(rogueCell.X, rogueCell.Y, game.Swatch.MiniMapFloor);
                    _map.SetMiniMapDrawColorFOV(rogueCell.X, rogueCell.Y, game.Swatch.MiniMapFloorFOV);
                }
                else
                {
                    Color DrawColorFOV = game.Swatch.WallFOV;
                    float randomDCFOV_R = UnityEngine.Random.Range(DrawColorFOV.r, DrawColorFOV.r * colorRandomModifier);
                    float randomDCFOV_G = UnityEngine.Random.Range(DrawColorFOV.g, DrawColorFOV.g * colorRandomModifier);
                    float randomDCFOV_B = UnityEngine.Random.Range(DrawColorFOV.b, DrawColorFOV.b * colorRandomModifier);

                    float randomDCFOV_R_Dif = (DrawColorFOV.r - randomDCFOV_R) * 0.25f;
                    float randomDCFOV_G_Dif = (DrawColorFOV.g - randomDCFOV_G) * 0.25f;
                    float randomDCFOV_B_Dif = (DrawColorFOV.b - randomDCFOV_B) * 0.25f;

                    DrawColorFOV = new Color(randomDCFOV_R, randomDCFOV_G, randomDCFOV_B);

                    Color BackgroundDrawColorFOV = game.Swatch.WallBackgroundFOV;
                    float randomBGDCFOV_R = UnityEngine.Random.Range(BackgroundDrawColorFOV.r, BackgroundDrawColorFOV.r * colorRandomModifier);
                    float randomBGDCFOV_G = UnityEngine.Random.Range(BackgroundDrawColorFOV.g, BackgroundDrawColorFOV.g * colorRandomModifier);
                    float randomBGDCFOV_B = UnityEngine.Random.Range(BackgroundDrawColorFOV.b, BackgroundDrawColorFOV.b * colorRandomModifier);

                    float randomBGDCFOV_R_Dif = (BackgroundDrawColorFOV.r - randomBGDCFOV_R) * 0.25f;
                    float randomBGDCFOV_G_Dif = (BackgroundDrawColorFOV.g - randomBGDCFOV_G) * 0.25f;
                    float randomBGDCFOV_B_Dif = (BackgroundDrawColorFOV.b - randomBGDCFOV_B) * 0.25f;

                    BackgroundDrawColorFOV = new Color(randomBGDCFOV_R, randomBGDCFOV_G, randomBGDCFOV_B);

                    Color DrawColor = game.Swatch.Wall;
                    float randomDC_R = DrawColor.r - randomDCFOV_R_Dif;
                    float randomDC_G = DrawColor.g - randomDCFOV_G_Dif;
                    float randomDC_B = DrawColor.b - randomDCFOV_B_Dif;
                    DrawColor = new Color(randomDC_R, randomDC_G, randomDC_B);

                    Color BackgroundDrawColor = game.Swatch.WallBackground;
                    float randomBGDC_R = BackgroundDrawColor.r - randomBGDCFOV_R_Dif;
                    float randomBGDC_G = BackgroundDrawColor.g - randomBGDCFOV_G_Dif;
                    float randomBGDC_B = BackgroundDrawColor.b - randomBGDCFOV_B_Dif;
                    BackgroundDrawColor = new Color(randomBGDC_R, randomBGDC_G, randomBGDC_B);

                    string Symbol = game.Symbols.Wall;
                    Biomes biome = game.MapCollection.GetBiome(game.ActNumber, game.MapName);
                    if (biome == Biomes.Forest)
                    {
                        int randomIndex = game.Random.Next(0, game.Symbols.Trees.Length - 1);
                        Symbol = game.Symbols.Trees[randomIndex];
                    }
                    else if (biome == Biomes.Swamp)
                        Symbol = game.Symbols.Water;

                    string GlyphString = game.GlyphStrings.Wall;
                    if (GlyphString[GlyphString.Length - 1] == '_')
                        GlyphString += _map.BitmaskValue(rogueCell.X, rogueCell.Y).ToString();

                    _map.SetDrawColor(rogueCell.X, rogueCell.Y, DrawColor);
                    _map.SetBackgroundDrawColor(rogueCell.X, rogueCell.Y, BackgroundDrawColor);
                    _map.SetDrawColorFOV(rogueCell.X, rogueCell.Y, DrawColorFOV);
                    _map.SetBackgroundDrawColorFOV(rogueCell.X, rogueCell.Y, BackgroundDrawColorFOV);

                    _map.SetGlyphString(rogueCell.X, rogueCell.Y, GlyphString);
                    _map.SetSymbol(rogueCell.X, rogueCell.Y, Symbol);

                    _map.SetMiniMapDrawColor(rogueCell.X, rogueCell.Y, game.Swatch.MiniMapWall);
                    _map.SetMiniMapDrawColorFOV(rogueCell.X, rogueCell.Y, game.Swatch.MiniMapWallFOV);
                }
            }
        }

        // Method for making all walls transparent
        private void SetWallsTransparent()
        {
            foreach (RogueSharp.Cell cell in _map.GetAllCells())
            {
                if (!cell.IsWalkable)
                    _map.SetCellProperties(cell.X, cell.Y, true, false);
            }
        }

        // Returns a binary bitmask value using the cell's neighbours given the cell's position
        private int CreateBitmaskValue(int X, int Y, int padding)
        {
            bool aNotWalkable = true;
            bool bNotWalkable = true;
            bool cNotWalkable = true;
            bool dNotWalkable = true;

            if (Y > 0)
                aNotWalkable = !_map.IsWalkable(X, Y - 1);
            if (X < _mapWidth + padding - 1)
                bNotWalkable = !_map.IsWalkable(X + 1, Y);
            if (Y < _mapHeight + padding - 1)
                cNotWalkable = !_map.IsWalkable(X, Y + 1);
            if (X > 0)
                dNotWalkable = !_map.IsWalkable(X - 1, Y);

            int a = 1 * aNotWalkable.GetHashCode();
            int b = 2 * bNotWalkable.GetHashCode();
            int c = 4 * cNotWalkable.GetHashCode();
            int d = 8 * dNotWalkable.GetHashCode();

            return d + c + b + a;
        }

        // Method for placing the player at the first exit
        private void PlacePlayer_BorderOnlyStyle()
        {
            Player player = game.Player;
            if (player == null)
                player = new Player();

            player.X = _map.Exits[0].X;
            player.Y = _map.Exits[0].Y;

            _map.AddPlayer(player);
        }

        // Method for placing the player at the first room's center point
        private void PlacePlayer_RandomRoomsStyle()
        {
            Player player = game.Player;
            if (player == null)
                player = new Player();

            player.X = _map.Rooms[0].Center.X;
            player.Y = _map.Rooms[0].Center.Y;

            _map.AddPlayer(player);
        }

        // Method for placing the player at the first map section's center point
        private void PlacePlayer_CellularStyle()
        {
            Player player = game.Player;
            if (player == null)
                player = new Player();

            for (int i = 0; i < divisionMapSections.Count; i++)
            {
                if (divisionMapSections[i].HasExit)
                {
                    // player.X = divisionMapSections[i].ExitPoint.X;
                    // player.Y = divisionMapSections[i].ExitPoint.Y;
                    player.X = divisionMapSections[i].ExitPointX;
                    player.Y = divisionMapSections[i].ExitPointY;
                    break;
                }
            }

            _map.AddPlayer(player);
        }

        private void PlacePlayer_FromStringStyle()
        {
            Player player = game.Player;
            if (player == null)
                player = new Player();

            player.X = _startingPoint.X;
            player.Y = _startingPoint.Y;

            _map.AddPlayer(player);
        }

        // Method for placing monsters in the map
        private void PlaceMonsters_RandomRoomsStyle()
        {
            foreach (var room in _map.Rooms)
            {
                // 60% Chance for room to have monsters
                if (Dice.Roll("1D10") < 7)
                {
                    // Generate between 1 and 4 monsters
                    var numberOfMonsters = Dice.Roll("1D4");
                    for (int i = 0; i < numberOfMonsters; i++)
                    {
                        // Find random walkable location
                        Point randomRoomLocation = _map.GetRandomWalkableLocationInRoom(room);
                        if (randomRoomLocation != null)
                        {
                            // Temp spawn monster at level 1
                            var monster = Kobold.Create(1);
                            monster.X = randomRoomLocation.X;
                            monster.Y = randomRoomLocation.Y;
                            _map.AddMonster(monster);
                        }
                    }
                }
            }
        }

        private void PlaceMonsters_CellularStyle()
        {
            int sectionIndex = 0;
            foreach (MapSection mapSection in divisionMapSections)
            {
                // 40% Chance for map section to have monsters
                if (Dice.Roll("1D10") < 5)
                {
                    // Generate between 1 and 8 monsters
                    var numberOfMonsters = Dice.Roll("1D8");
                    for (int i = 0; i < numberOfMonsters; i++)
                    {
                        // Find random walkable location
                        Point randomMapSectionLocation 
                            = _map.GetRandomWalkableLocationInMapSection(mapSection);
                        if (randomMapSectionLocation != null)
                        {
                            // Temp spawn monster at level 1
                            var monster = Kobold.Create(1);
                            monster.X = randomMapSectionLocation.X;
                            monster.Y = randomMapSectionLocation.Y;
                            _map.AddMonster(monster);
                        }
                    }
                }
                sectionIndex++;
            }
        }
    }
}