﻿//-----------------------------------------------------------------------
// <copyright file="MapSection.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using RogueSharp;

    public class MapSection
    {
        #region Fields

        private int exitPointX;
        private int exitPointY;
        private int top;
        private int bottom;
        private int right;
        private int left;
        private bool hasExit;

        #endregion

        #region Constructor

        public MapSection()
        {
            this.Cells = new HashSet<ICell>();
            this.top = int.MaxValue;
            this.left = int.MaxValue;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HasExit"/> property
        /// </summary>
        public bool HasExit
        {
            get { return hasExit; } set { hasExit = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ExitPointX"/> property
        /// </summary>
        public int ExitPointX
        {
            get { return exitPointX; } set { exitPointX = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ExitPointY"/> property
        /// </summary>
        public int ExitPointY
        {
            get { return exitPointY; } set { exitPointY = value; }
        }

        /// <summary>
        /// Gets the <see cref="Bounds"/> property
        /// </summary>
        public Rectangle Bounds
        {
            get { return new Rectangle(left, top, right - left + 1, bottom - top + 1); }
        }

        /// <summary>
        /// Gets or sets the <see cref="Cells"/> property
        /// </summary>
        public HashSet<ICell> Cells
        {
            get; private set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds the specified <see cref="ICell"/> to the <see cref="MapSection"/>
        /// </summary>
        /// <param name="cell">An <see cref="ICell"/> to be added to the <see cref="MapSection"/></param>
        public void AddCell(ICell cell)
        {
            this.Cells.Add(cell);
            this.UpdateBounds(cell);
        }

        /// <summary>
        /// Updates the boundaries of the <see cref="MapSection"/> using the specified <see cref="ICell"/>
        /// </summary>
        /// <param name="cell">An <see cref="ICell"/> to be used when updating the boundaries</param>
        private void UpdateBounds(ICell cell)
        {
            if (cell.X > right)
            {
                this.right = cell.X;
            }

            if (cell.X < left)
            {
                this.left = cell.X;
            }

            if (cell.Y > bottom)
            {
                this.bottom = cell.Y;
            }

            if (cell.Y < top)
            {
                this.top = cell.Y;
            }
        }

        /// <summary>
        /// Returns a <see cref="string"/> representation of the <see cref="MapSection"/>'s boundaries
        /// </summary>
        /// <returns>A <see cref="string"/> defining the <see cref="MapSection"/>'s boundaries</returns>
        public override string ToString()
        {
            return string.Format("Bounds: {0}", this.Bounds);
        }

        #endregion
    }
}