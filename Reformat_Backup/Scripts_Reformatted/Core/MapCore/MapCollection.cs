﻿//-----------------------------------------------------------------------
// <copyright file="MapCollection.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;

using UnityEngine;

using RogueSharp;

using Rogue2018.Actors;

namespace Rogue2018.Core.MapCore
{
    public class MapPropertiesList
    {
        public List<MapProperties> mapPropertiesList;
    }

    public class MapCollection
    {
        public readonly List<MapPropertiesList> actPropertiesList;

        public MapCollection()
        {
            // Setup filepath
            string xmlPath = "MapCollectionData";

            // Load Xml data
            var xmlRawFile = Resources.Load<TextAsset>(xmlPath);
            string xmlData = xmlRawFile.text;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(new StringReader(xmlData));

            // Setup seperating chars
            string[] seperatingChars = { ", " };
            string[] advancedSeperatingChars = { "[", "]" };

            // Get list of acts
            XmlNodeList nodeListActs = xmlDocument.SelectNodes("/data/act");

            // Create actsMapGenerationPropertiesLists
            this.actPropertiesList = new List<MapPropertiesList>(nodeListActs.Count);

            // Iterate over act nodes within nodeListActs
            int actCount = 0;
            foreach (XmlNode nodeAct in nodeListActs)
            {
                // Get list of maps
                XmlNodeList nodeListMaps = nodeAct.SelectNodes("map");

                // Create actsMapGenerationPropertiesLists[actCount]
                this.actPropertiesList.Add(new MapPropertiesList());

                // Create actsMapGenerationPropertiesLists[actCount].mapGenerationPropertiesList
                this.actPropertiesList[actCount].mapPropertiesList = new List<MapProperties>(nodeListMaps.Count);

                int mapCount = 0;
                foreach (XmlNode nodeMap in nodeListMaps)
                {
                    // Get nodes for shared properties
                    XmlNode nodeMapName             = nodeMap.SelectSingleNode("mapname");
                    XmlNode nodeExits               = nodeMap.SelectSingleNode("exits");
                    XmlNode nodeGenerationStyle     = nodeMap.SelectSingleNode("generationstyle");
                    XmlNode nodeBiome               = nodeMap.SelectSingleNode("biome");
                    XmlNode nodeMapWidth            = nodeMap.SelectSingleNode("width");
                    XmlNode nodeMapHeight           = nodeMap.SelectSingleNode("height");

                    // Get nodes for random room generation style properties
                    XmlNode nodeMaxRoomCount        = nodeMap.SelectSingleNode("maxroomcount");
                    XmlNode nodeMaxRoomSize         = nodeMap.SelectSingleNode("maxroomsize");
                    XmlNode nodeMinRoomSize         = nodeMap.SelectSingleNode("minroomsize");

                    // Get nodes for cellular generation style properties
                    XmlNode nodeFillProbability     = nodeMap.SelectSingleNode("fillprobability");
                    XmlNode nodeIterationCount      = nodeMap.SelectSingleNode("iterationcount");
                    XmlNode nodeBigAreaFillCutoff   = nodeMap.SelectSingleNode("bigareafillcutoff");
                    XmlNode nodeTransparentWalls    = nodeMap.SelectSingleNode("transparentwalls");

                    // Get nodes for from string generation style properties
                    XmlNode nodeExitsAdvanced       = nodeMap.SelectSingleNode("exitsadvanced");
                    XmlNode nodeStartingPoint       = nodeMap.SelectSingleNode("startingpoint");
                    XmlNode nodePath                = nodeMap.SelectSingleNode("path");

                    // Create actsMapGenerationPropertiesLists[actCount].mapGenerationPropertiesList[mapCount]
                    this.actPropertiesList[actCount].mapPropertiesList.Add(new MapProperties());

                    // Create shared properties
                    string mapName = nodeMapName.InnerXml;

                    string[] exits;
                    if (nodeExitsAdvanced == null)
                        exits = nodeExits.InnerXml.Split(seperatingChars, StringSplitOptions.RemoveEmptyEntries);
                    else
                        exits = nodeExitsAdvanced.InnerXml.Split(seperatingChars, StringSplitOptions.RemoveEmptyEntries);

                    GenerationStyles generationStyle = GenerationStyles.None;
                    if (nodeGenerationStyle.InnerXml == "Border Only")
                        generationStyle = GenerationStyles.BorderOnly;
                    else if (nodeGenerationStyle.InnerXml == "Cellular")
                        generationStyle = GenerationStyles.Cellular;
                    else if (nodeGenerationStyle.InnerXml == "Random Rooms")
                        generationStyle = GenerationStyles.RandomRooms;
                    else if (nodeGenerationStyle.InnerXml == "From String")
                        generationStyle = GenerationStyles.FromString;

                    Biomes biome = Biomes.None;
                    if (nodeBiome.InnerXml == "Dungeon")
                        biome = Biomes.Dungeon;
                    else if (nodeBiome.InnerXml == "Cave")
                        biome = Biomes.Cave;
                    else if (nodeBiome.InnerXml == "Forest")
                        biome = Biomes.Forest;
                    else if (nodeBiome.InnerXml == "Swamp")
                        biome = Biomes.Swamp;

                    int mapWidth = Int32.Parse(nodeMapWidth.InnerXml);
                    int mapHeight = Int32.Parse(nodeMapHeight.InnerXml);

                    // Create border only generation style properties and construct map properties class
                    if (generationStyle == GenerationStyles.BorderOnly)
                    {
                        this.actPropertiesList[actCount].mapPropertiesList[mapCount].Construct_BorderOnlyStlyeProperties(
                            mapName,
                            exits,
                            generationStyle,
                            biome,
                            mapWidth,
                            mapHeight);
                    }

                    // Create random room generation style properties and construct map properties class
                    else if (generationStyle == GenerationStyles.RandomRooms)
                    {
                        int maxRoomCount = Int32.Parse(nodeMaxRoomCount.InnerXml);
                        int maxRoomSize = Int32.Parse(nodeMaxRoomSize.InnerXml);
                        int minRoomSize = Int32.Parse(nodeMinRoomSize.InnerXml);

                        this.actPropertiesList[actCount].mapPropertiesList[mapCount].Construct_RandomRoomsStyleProperties(
                            mapName,
                            exits,
                            generationStyle,
                            biome,
                            mapWidth,
                            mapHeight,
                            maxRoomCount,
                            maxRoomSize,
                            minRoomSize);
                    }

                    // Create cellular generation style properties and construct map properties class
                    else if (generationStyle == GenerationStyles.Cellular)
                    {
                        int fillProbability = Int32.Parse(nodeFillProbability.InnerXml);
                        int iterationCount = Int32.Parse(nodeIterationCount.InnerXml);
                        int bigAreaFillCutoff = Int32.Parse(nodeBigAreaFillCutoff.InnerXml);
                        bool transparentWalls = false;
                        if (nodeTransparentWalls != null)
                            if (nodeTransparentWalls.InnerXml == "True") transparentWalls = true;

                        this.actPropertiesList[actCount].mapPropertiesList[mapCount].Construct_CellularStyleProperties(
                            mapName,
                            exits,
                            generationStyle,
                            biome,
                            mapWidth,
                            mapHeight,
                            fillProbability,
                            iterationCount,
                            bigAreaFillCutoff,
                            transparentWalls);
                    }

                    else if (generationStyle == GenerationStyles.FromString)
                    {
                        string[,] exitsAdvanced = new string[mapWidth, mapHeight];

                        foreach (string exit in exits)
                        {
                            string[] exitData = exit.Split(advancedSeperatingChars, StringSplitOptions.RemoveEmptyEntries);
                            int x = Int32.Parse(exitData[0]);
                            int y = Int32.Parse(exitData[1]);
                            string exitString = exitData[2];
                            exitsAdvanced[x, y] = exitString;
                        }
                        string mapRepresentationPath = nodePath.InnerXml;

                        string[] startingPointData = nodeStartingPoint.InnerXml.Split(advancedSeperatingChars, StringSplitOptions.RemoveEmptyEntries);
                        Point startingPoint = new Point(Int32.Parse(startingPointData[0]), Int32.Parse(startingPointData[1]));

                        this.actPropertiesList[actCount].mapPropertiesList[mapCount].Construct_FromStringStyleProperties(
                            mapName,
                            exitsAdvanced,
                            generationStyle,
                            biome,
                            mapWidth,
                            mapHeight,
                            mapRepresentationPath,
                            startingPoint);
                    }

                    mapCount++;
                }
                actCount++;
            }
        }

        public string GetFirstMapNameOfAct(int actNumber)
        {
            return actPropertiesList[actNumber - 1].mapPropertiesList[0].MapName;
        }

        public MapProperties GetMapProperties(int actNumber, string mapName)
        {
            foreach (MapProperties mapProperties in actPropertiesList[actNumber - 1].mapPropertiesList)
            {
                if (mapProperties.MapName == mapName)
                    return mapProperties;
            }
            Debug.LogError(string.Format("MapProperties for actNumber {0} and mapName {1} was not found.", actNumber, mapName));
            return null;
        }

        public int GetMapCountInAct(int actNumber)
        {
            return actPropertiesList[actNumber - 1].mapPropertiesList.Count;
        }

        public void StoreMap(int actNumber, string mapName, CurrentMap mapToStore)
        {
            MapProperties mapProperties = GetMapProperties(actNumber, mapName);
            mapProperties.MapGenerated = true;
            mapProperties.StoredMap = mapToStore;
        }

        public void StoreMap(int actNumber, string mapName, CurrentMap mapToStore, Player player)
        {
            MapProperties mapProperties = GetMapProperties(actNumber, mapName);
            mapProperties.MapGenerated = true;
            mapProperties.StoredMap = mapToStore;
            mapProperties.StoredPlayerPosition = new Point(player.X, player.Y);
        }

        public bool CheckForStoredMap(int actNumber, string mapName)
        {
            MapProperties mapProperties = GetMapProperties(actNumber, mapName);
            if (mapProperties.MapGenerated && mapProperties.StoredMap != null)
                return true;
            else
                return false;
        }

        public CurrentMap GetStoredMap(int actNumber, string mapName)
        {
            MapProperties mapProperties = GetMapProperties(actNumber, mapName);
            return mapProperties.StoredMap;
        }

        public Point GetStoredPlayerPosition(int actNumber, string mapName)
        {
            MapProperties mapProperties = GetMapProperties(actNumber, mapName);
            return mapProperties.StoredPlayerPosition;
        }

        public Biomes GetBiome(int actNumber, string mapName)
        {
            MapProperties mapProperties = GetMapProperties(actNumber, mapName);
            return mapProperties.Biome;
        }
    }
}