﻿//-----------------------------------------------------------------------
// <copyright file="FloodFillAnalyzer.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using System.Collections.Generic;
using RogueSharp;

namespace Rogue2018.Core.MapCore
{
    public class FloodFillAnalyzer
    {
        private readonly IMap _map;
        private readonly List<MapSection> _mapSections;

        private readonly int[][] _offsets =
        {
            new[] { 0, -1 }, new[] { -1, 0 }, new[] { 1, 0 }, new[] { 0, 1 }
         };

        private readonly bool[][] _visited;

        public FloodFillAnalyzer(IMap map)
        {
            _map = map;
            _mapSections = new List<MapSection>();
            _visited = new bool[_map.Height][];
            for (int i = 0; i < _visited.Length; i++)
                _visited[i] = new bool[_map.Width];
        }

        public List<MapSection> GetMapSections()
        {
            IEnumerable<ICell> cells = _map.GetAllCells();
            foreach (ICell cell in cells)
            {
                MapSection section = Visit(cell);
                if (section.Cells.Count > 0)
                    _mapSections.Add(section);
            }

            return _mapSections;
        }

        private MapSection Visit(ICell cell)
        {
            Stack<ICell> stack = new Stack<ICell>(new List<ICell>());
            MapSection mapsection = new MapSection();
            stack.Push(cell);
            while (stack.Count != 0)
            {
                cell = stack.Pop();
                if (_visited[cell.Y][cell.X] || !cell.IsWalkable)
                    continue;
                mapsection.AddCell(cell);
                _visited[cell.Y][cell.X] = true;
                foreach (ICell neighbor in GetNeighbors(cell))
                {
                    if (cell.IsWalkable == neighbor.IsWalkable && !_visited[neighbor.Y][neighbor.X])
                        stack.Push(neighbor);
                }
            }
            return mapsection;
        }

        private ICell GetCell(int x, int y)
        {
            if (x < 0 || y < 0)
                return null;
            if (x >= _map.Width || y >= _map.Height)
                return null;
            return _map.GetCell(x, y);
        }

        private IEnumerable<ICell> GetNeighbors(ICell cell)
        {
            List<ICell> neighbors = new List<ICell>(8);
            foreach (int[] offset in _offsets)
            {
                var neighbor = GetCell(cell.X + offset[0], cell.Y + offset[1]);
                if (neighbor == null)
                    continue;
                neighbors.Add(neighbor);
            }

            return neighbors;
        }
    }
}