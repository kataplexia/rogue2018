﻿//-----------------------------------------------------------------------
// <copyright file="Exit.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using UnityEngine;
using RogueSharp;
using Rogue2018.Interfaces;

using PhiOS;

namespace Rogue2018.Core.MapCore
{
    public class Exit : IDrawable
    {
        // Properties
        public Game game { get; set; }
        public string ExitToName { get; set; }
        public bool IsUp { get; set; }

        // IDrawable properties
        public Color DrawColor              { get; set; }
        public Color BackgroundDrawColor    { get; set; }
        public Color DrawColorFOV           { get; set; }
        public Color BackgroundDrawColorFOV { get; set; }
        public string Symbol                { get; set; }
        public string GlyphString           { get; set; }
        public int X                        { get; set; }
        public int Y                        { get; set; }

        // Method for drawing
        public void Draw(int xOffset, int yOffset, IMap map)
        {
            // Check if drawable
            if (!game.Tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
                return;

            // Check if unexplored
            if (!map.GetCell(X, Y).IsExplored)
                return;

            // Setup symbol and glyphstring
            Symbol = IsUp ? game.Symbols.StairsUp : game.Symbols.StairsDown;
            GlyphString = IsUp ? game.GlyphStrings.StairsUp : game.GlyphStrings.StairsDown;

            // Check FOV
            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = IsUp ? game.Swatch.FloorBackgroundFOV * 0.8f : Color.clear;
                if (!Options.GET.DrawTiles)
                    BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV * 0.8f;
                DrawColorFOV = game.Swatch.StairsFOV;
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }
            }
            else
            {
                BackgroundDrawColor = IsUp ? game.Swatch.FloorBackground * 0.8f : Color.clear;
                if (!Options.GET.DrawTiles)
                    BackgroundDrawColorFOV = game.Swatch.FloorBackground * 0.8f;
                DrawColor = game.Swatch.Stairs;
                PhiCell phiCell = PhiDisplay.PhiCellAt(Game.Singleton.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }        
        }
    }
}