﻿//-----------------------------------------------------------------------
// <copyright file="CurrentMap.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.MapCore
{
    using System.Collections.Generic;
    using System.Linq;
    using PhiOS;
    using Rogue2018.Actors;
    using RogueSharp;
    using UnityEngine;

    /// <summary>
    /// A class defining a type of <see cref="Map"/> (Current map) with all of it's associated properties
    /// </summary>
    public class CurrentMap : Map
    {
        #region Fields

        private GoalMap monsterGoalMap;
        private int playerGoalExitIndex = 0;
        private GoalMap playerGoalMap;
        private List<Cell> unexploredWalkableCells;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentMap"/> class
        /// </summary>
        public CurrentMap()
        {
            this.Monsters = new List<Monster>();
            this.Rooms = new List<Rectangle>();
            this.Doors = new List<Door>();
            this.Exits = new List<Exit>();
            Game.Singleton.SchedulingSystem.Clear();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="Doors"/> property
        /// </summary>
        public List<Door> Doors
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the <see cref="Exits"/> property
        /// </summary>
        public List<Exit> Exits
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the <see cref="MonsterGoalMap"/> property
        /// </summary>
        public GoalMap MonsterGoalMap
        {
            get { return this.monsterGoalMap; } set { this.monsterGoalMap = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Monsters"/> property
        /// </summary>
        public List<Monster> Monsters
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the <see cref="playerGoalCell"/> property
        /// </summary>
        public Cell PlayerGoalCell
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the <see cref="PlayerGoalMap"/> property
        /// </summary>
        public GoalMap PlayerGoalMap
        {
            get { return this.playerGoalMap; } set { this.playerGoalMap = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Rooms"/> property
        /// </summary>
        public List<Rectangle> Rooms
        {
            get; set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a <see cref="Monster"/> to the map
        /// </summary>
        /// <param name="monster">A <see cref="Monster"/> defining the monster to be added to the map</param>
        public void AddMonster(Monster monster)
        {
            this.Monsters.Add(monster);
            this.SetIsWalkable(monster.X, monster.Y, false);
            Game.Singleton.SchedulingSystem.Add(monster);
        }

        /// <summary>
        /// Adds all <see cref="Monster"/>'s to the scheduling system's schedule
        /// </summary>
        public void AddMonstersToSchedule()
        {
            foreach (Monster monster in this.Monsters)
            {
                Game.Singleton.SchedulingSystem.Add(monster);
            }
        }

        /// <summary>
        /// Adds the <see cref="Player"/> to the map
        /// </summary>
        /// <param name="player">A <see cref="Player"/> defining the player to be added to the map</param>
        public void AddPlayer(Player player)
        {
            Game.Singleton.Player = player;
            this.SetIsWalkable(player.X, player.Y, false);
            this.UpdatePlayerFOV();
            Game.Singleton.SchedulingSystem.Add(player);
        }

        /// <summary>
        /// Returns a boolean depending on whether or not <see cref="Monster"/>'s are alerted
        /// </summary>
        /// <returns>A <see cref="bool"/> which is True if <see cref="Monster"/>'s are alerted else False</returns>
        public bool AreMonstersAlerted()
        {
            foreach (Monster monster in this.Monsters)
            {
                if (monster.TurnsAlerted.HasValue)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns a boolean depending on whether or not <see cref="Monster"/>'s are inside the <see cref="Player"/>'s <see cref="FieldOfView"/>
        /// </summary>
        /// <returns>A <see cref="bool"/> which is True if <see cref="Monster"/>'s are in view else False</returns>
        public bool AreMonstersInFOV()
        {
            foreach (Monster monster in this.Monsters)
            {
                if (this.IsInFOV(monster.X, monster.Y))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns a boolean depending on whether or not the <see cref="Player"/> can move down stairs
        /// </summary>
        /// <returns>A <see cref="bool"/> which is True if the <see cref="Player"/> can move down stairs else False</returns>
        public bool CanMoveDownStairs()
        {
            if (this.OnExit() == null)
            {
                return false;
            }

            if (this.OnExit().IsUp)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Returns a boolean depending on whether or not the <see cref="Player"/> can move up stairs
        /// </summary>
        /// <returns>A <see cref="bool"/> which is True if the <see cref="Player"/> can move up stairs else False</returns>
        public bool CanMoveUpStairs()
        {
            if (this.OnExit() == null)
            {
                return false;
            }

            if (this.OnExit().IsUp)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a count of all unwalkable <see cref="Cell"/>s around the specified <see cref="Cell"/> using the specified distance
        /// </summary>
        /// <param name="cell">A <see cref="Cell"/> that defines the target cell to count around</param>
        /// <param name="distance">An <see cref="int"/> that defines the distance to use when counting</param>
        /// <returns>An <see cref="int"/> defining the number of unwalkable <see cref="Cell"/>s</returns>
        public int CountWallsNear(ICell cell, int distance)
        {
            int count = 0;
            foreach (ICell nearbyCell in this.GetCellsInSquare(cell.X, cell.Y, distance))
            {
                if (nearbyCell.X == cell.X && nearbyCell.Y == cell.Y)
                {
                    continue;
                }

                if (!nearbyCell.IsWalkable)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Destroys all <see cref="HealthBar"/>s
        /// </summary>
        public void DestroyHealthBars()
        {
            foreach (Monster monster in this.Monsters)
            {
                if (monster.healthBar != null)
                {
                    monster.healthBar.DestroyHealthBar();
                    monster.healthBar = null;
                }
            }
        }

        /// <summary>
        /// Returns a boolean which is True if a map section has walkable space else False
        /// </summary>
        /// <param name="mapSection">A <see cref="MapSection"/> to check for walkable space</param>
        /// <returns>A <see cref="bool"/> defining whether or not the <see cref="MapSection"/> has walkable space</returns>
        public bool DoesMapSectionHaveWalkableSpace(MapSection mapSection)
        {
            for (int x = mapSection.Bounds.Left; x < mapSection.Bounds.Right - 1; x++)
            {
                for (int y = mapSection.Bounds.Top; y < mapSection.Bounds.Bottom - 1; y++)
                {
                    if (this.IsWalkable(x, y))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns a boolean which is True if a room has walkable space else False
        /// </summary>
        /// <param name="room">A <see cref="Rectangle"/> defining the room to check for walkable space</param>
        /// <returns>A <see cref="bool"/> defining whether or not the room has walkable space</returns>
        public bool DoesRoomHaveWalkableSpace(Rectangle room)
        {
            for (int x = 1; x <= room.Width - 2; x++)
            {
                for (int y = 1; y <= room.Height - 2; y++)
                {
                    if (this.IsWalkable(x + room.X, y + room.Y))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Draws the <see cref="CurrentMap"/>
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        public void Draw(int xOffset, int yOffset)
        {
            int drawWidth = (int)PhiDisplay.GET.zoomDisplayRects[PhiDisplay.GET.currentZoomLevel].width;
            int drawHeight = (int)PhiDisplay.GET.zoomDisplayRects[PhiDisplay.GET.currentZoomLevel].height;
            IEnumerable<ICell> cellsToDraw = GetCellsInRect(Game.Singleton.Player.X, Game.Singleton.Player.Y, drawWidth, drawHeight, true);

            foreach (ICell cell in cellsToDraw)
            {
                this.DrawMapCell(cell as RogueSharp.Cell, xOffset, yOffset);
            }

            foreach (Door door in this.Doors)
            {
                door.Draw(xOffset, yOffset, this);
            }

            foreach (Exit exit in this.Exits)
            {
                exit.Draw(xOffset, yOffset, this);
            }

            foreach (Monster monster in this.Monsters)
            {
                monster.Draw(xOffset, yOffset, this);
            }
        }

        /// <summary>
        /// Draws the <see cref="HealthBar"/>s
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        public void DrawHealthBars(int xOffset, int yOffset)
        {
            foreach (Monster monster in this.Monsters)
            {
                if (this.IsInFOV(monster.X, monster.Y))
                {
                    monster.DrawHealthBar(xOffset, yOffset);
                }
                else if (monster.healthBar != null)
                {
                    if (monster.healthBar.IsEnabled)
                    {
                        monster.healthBar.IsEnabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// Finds and sets the <see cref="PlayerGoalCell"/> for goal map path finding
        /// </summary>
        public void FindPlayerGoal()
        {
            this.PlayerGoalMap.ClearGoals();

            Point playerPoint = new Point(Game.Singleton.Player.X, Game.Singleton.Player.Y);
            int bestDistance = int.MaxValue;

            int distanceCheckCount = 20 > this.unexploredWalkableCells.Count ? this.unexploredWalkableCells.Count : 20;
            for (int i = 0; i < distanceCheckCount; i++)
            {
                RogueSharp.Cell cell = this.unexploredWalkableCells[i];
                Point cellPoint = new Point(cell.X, cell.Y);
                int cellDistance = (int)Point.Distance(cellPoint, playerPoint);
                if (cellDistance < bestDistance)
                {
                    bestDistance = cellDistance;
                    this.PlayerGoalCell = cell;
                }
            }

            /*

            // foreach (RogueSharp.Cell cell in GetAllUnexploredWalkableCells())
            foreach (RogueSharp.Cell cell in unexploredWalkableCells)
            {
                Point cellPoint = new Point(cell.X, cell.Y);
                int cellDistance = (int)Point.Distance(cellPoint, playerPoint);
                if (cellDistance < bestDistance)
                {
                    bestDistance = cellDistance;
                    playerGoalCell = cell;
                }
            }

            */

            if (this.PlayerGoalCell != null)
            {
                this.PlayerGoalMap.AddGoal(this.PlayerGoalCell.X, this.PlayerGoalCell.Y, 1);
                this.PlayerGoalMap.ComputeCellWeights();
            }
        }

        /// <summary>
        /// Finds and sets the <see cref="PlayerGoalCell"/> for goal map path finding when the intended goal is an exit
        /// </summary>
        public void FindPlayerGoalExit()
        {
            this.PlayerGoalMap.ClearGoals();

            if (this.playerGoalExitIndex == 0)
            {
                this.playerGoalExitIndex = this.Exits.Count - 1;
            }
            else if (this.playerGoalExitIndex == this.Exits.Count - 1)
            {
                this.playerGoalExitIndex--;
            }
            else
            {
                this.playerGoalExitIndex--;
            }

            this.PlayerGoalCell = GetCell(this.Exits[this.playerGoalExitIndex].X, this.Exits[this.playerGoalExitIndex].Y) as RogueSharp.Cell;

            if (this.PlayerGoalCell != null)
            {
                this.PlayerGoalMap.AddGoal(this.PlayerGoalCell.X, this.PlayerGoalCell.Y, 1);
                this.PlayerGoalMap.ComputeCellWeights();
            }
        }

        /// <summary>
        /// Returns a <see cref="Door"/> using the specified X and Y position
        /// </summary>
        /// <param name="x">An <see cref="int"/> defining the X position</param>
        /// <param name="y">An <see cref="int"/> defining the Y position</param>
        /// <returns>A <see cref="Door"/> corresponding to the specified position</returns>
        public Door GetDoorAt(int x, int y)
        {
            return this.Doors.SingleOrDefault(d => d.X == x && d.Y == y);
        }

        /// <summary>
        /// Returns a <see cref="Exit"/> using the specified X and Y position
        /// </summary>
        /// <param name="x">An <see cref="int"/> defining the X position</param>
        /// <param name="y">An <see cref="int"/> defining the Y position</param>
        /// <returns>A <see cref="Exit"/> corresponding to the specified position</returns>
        public Exit GetExitAt(int x, int y)
        {
            return this.Exits.SingleOrDefault(e => e.X == x && e.Y == y);
        }

        /// <summary>
        /// Returns a <see cref="Monster"/> using the specified X and Y position
        /// </summary>
        /// <param name="x">An <see cref="int"/> defining the X position</param>
        /// <param name="y">An <see cref="int"/> defining the Y position</param>
        /// <returns>A <see cref="Monster"/> corresponding to the specified position</returns>
        public Monster GetMonsterAt(int x, int y)
        {
            return this.Monsters.FirstOrDefault(m => m.X == x && m.Y == y);
        }

        /// <summary>
        /// Recursively returns a random suitable <see cref="MapSection"/> to place an exit
        /// </summary>
        /// <param name="mapSections">A <see cref="List{T}(MapSection)"/> to iterate through</param>
        /// <returns>A <see cref="MapSection"/> which is suitable for placing an exit within</returns>
        public MapSection GetRandomSuitableMapSection(List<MapSection> mapSections)
        {
            int randomIndex = Game.Singleton.Random.Next(0, mapSections.Count - 1);

            if (mapSections[randomIndex].HasExit)
            {
                return this.GetRandomSuitableMapSection(mapSections);
            }

            foreach (ICell cell in mapSections[randomIndex].Cells)
            {
                if (cell.IsWalkable && this.CountWallsNear(cell, 1) == 0)
                {
                    return mapSections[randomIndex];
                }
            }

            return this.GetRandomSuitableMapSection(mapSections);
        }

        /// <summary>
        /// Returns a random walkable <see cref="Point"/> within the map
        /// </summary>
        /// <returns>A random <see cref="Point"/> within the map that is walkable</returns>
        public Point GetRandomWalkableLocationInMap()
        {
            for (int i = 0; i < 100; i++)
            {
                int x = Game.Singleton.Random.Next(1, Width - 2);
                int y = Game.Singleton.Random.Next(1, Height - 2);

                if (this.IsWalkable(x, y))
                {
                    return new Point(x, y);
                }
            }

            return null;
        }

        /// <summary>
        /// Returns a random walkable <see cref="Point"/> within a specified <see cref="MapSection"/>
        /// </summary>
        /// <param name="mapSection">A <see cref="MapSection"/> to return a random walkable point from within</param>
        /// <returns>A random <see cref="Point"/> within the <see cref="MapSection"/> that is walkable</returns>
        public Point GetRandomWalkableLocationInMapSection(MapSection mapSection)
        {
            if (this.DoesMapSectionHaveWalkableSpace(mapSection))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = Game.Singleton.Random.Next(mapSection.Bounds.Left, mapSection.Bounds.Right - 1);
                    int y = Game.Singleton.Random.Next(mapSection.Bounds.Top, mapSection.Bounds.Bottom - 1);

                    if (this.IsWalkable(x, y))
                    {
                        return new Point(x, y);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns a random walkable <see cref="Point"/> within a specified <see cref="Rectangle"/> defining a room
        /// </summary>
        /// <param name="room">A <see cref="Rectangle"/> defining the room to return a random walkable point from within</param>
        /// <returns>A random <see cref="Point"/> within the room that is walkable</returns>
        public Point GetRandomWalkableLocationInRoom(Rectangle room)
        {
            if (this.DoesRoomHaveWalkableSpace(room))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = Game.Singleton.Random.Next(1, room.Width - 2) + room.X;
                    int y = Game.Singleton.Random.Next(1, room.Height - 2) + room.Y;

                    if (this.IsWalkable(x, y))
                    {
                        return new Point(x, y);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Hides all <see cref="HealthBar"/>s
        /// </summary>
        public void HideHealthBars()
        {
            foreach (Monster monster in this.Monsters)
            {
                if (monster.healthBar != null)
                {
                    if (monster.healthBar.IsEnabled)
                    {
                        monster.healthBar.IsEnabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// Returns a boolean defining whether or not a cell is a border cell (takes into account padding)
        /// </summary>
        /// <param name="cell">A <see cref="Cell"/> to check if it is a border cell</param>
        /// <param name="padding">An <see cref="int"/> defining the unwalkable padding around the map</param>
        /// <returns>A <see cref="bool"/> defining whether or not the cell is a border cell, True if it is else False</returns>
        public bool IsBorderCell(ICell cell, int padding)
        {
            return cell.X <= (int)(padding / 2) ||
                   cell.X >= this.Width - 1 - (int)(padding / 2) ||
                   cell.Y <= (int)(padding / 2) ||
                   cell.Y >= this.Height - 1 - (int)(padding / 2);
        }

        /// <summary>
        /// Returns a boolean defining whether or not the map is fully explored
        /// </summary>
        /// <returns>A <see cref="bool"/> defining whther or not the map is fully explored, True if it is else False</returns>
        public bool IsMapExplored()
        {
            foreach (RogueSharp.Cell cell in this.GetAllCells())
            {
                if (cell.IsWalkable && !cell.IsExplored)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns the <see cref="Exit"/> the <see cref="Player"/> is standing on when the <see cref="Player"/> is standing on an <see cref="Exit"/>
        /// </summary>
        /// <returns>An <see cref="Exit"/> defining the <see cref="Exit"/> the player is standing on</returns>
        public Exit OnExit()
        {
            Player player = Game.Singleton.Player;
            foreach (Exit exit in this.Exits)
            {
                if (exit.X == player.X && exit.Y == player.Y)
                {
                    return exit;
                }
            }

            return null;
        }

        /// <summary>
        /// Opens a <see cref="Door"/>
        /// </summary>
        /// <param name="actor">An <see cref="Actor"/> defining the <see cref="Actor"/> which is opening the door</param>
        /// <param name="x">An <see cref="int"/> defining the X position</param>
        /// <param name="y">An <see cref="int"/> defining the Y position</param>
        public void OpenDoor(Actor actor, int x, int y)
        {
            Door door = this.GetDoorAt(x, y);
            if (door != null && !door.IsOpen)
            {
                door.IsOpen = true;
                ICell rogueCell = this.GetCell(x, y);

                this.SetCellProperties(x, y, true, rogueCell.IsWalkable, rogueCell.IsExplored);

                Audio.Singleton.QueueAudio(Audio.Singleton.PlayerOpenDoor);
            }
        }

        /// <summary>
        /// Removes a <see cref="Monster"/> from <see cref="Monsters"/> and from the <see cref="SchedulingSystem"/>
        /// </summary>
        /// <param name="monster">A <see cref="Monster"/> to be removed</param>
        public void RemoveMonster(Monster monster)
        {
            this.Monsters.Remove(monster);
            this.SetIsWalkable(monster.X, monster.Y, true);
            Game.Singleton.SchedulingSystem.Remove(monster);
        }

        /// <summary>
        /// Removes all <see cref="Monster"/>s in <see cref="Monsters"/> from the <see cref="SchedulingSystem"/>
        /// </summary>
        public void RemoveMonstersFromSchedule()
        {
            foreach (Monster monster in this.Monsters)
            {
                Game.Singleton.SchedulingSystem.Remove(monster);
            }
        }

        /// <summary>
        /// Returns a boolean defining whether or not the specified <see cref="Actor"/> is able to be placed at the specified position and if so, moves it
        /// </summary>
        /// <param name="actor">An <see cref="Actor"/> to check if able to be moved</param>
        /// <param name="x">An <see cref="int"/> defining the X position</param>
        /// <param name="y">An <see cref="int"/> defining the Y position</param>
        /// <returns>A <see cref="bool"/> defining whether or not the movement is possible</returns>
        public bool SetActorPosition(Actor actor, int x, int y)
        {
            if (actor is Player && x == actor.X && y == actor.Y)
            {
                this.UpdateMonsterGoalMap();
                return true;
            }

            if (this.GetCell(x, y).IsWalkable)
            {
                this.SetIsWalkable(actor.X, actor.Y, true);

                actor.X = x;
                actor.Y = y;

                if (actor is Player)
                {
                    this.UpdateMonsterGoalMap();
                }

                this.SetIsWalkable(actor.X, actor.Y, false);
                this.OpenDoor(actor, x, y);

                if (actor is Player)
                {
                    Audio.Singleton.QueueAudio(Audio.Singleton.PlayerMove);
                    this.UpdatePlayerFOV();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets a <see cref="Cell"/>'s walkable state
        /// </summary>
        /// <param name="x">An <see cref="int"/> defining the X position</param>
        /// <param name="y">An <see cref="int"/> defining the Y position</param>
        /// <param name="isWalkable">A <see cref="bool"/> defining the walkable state to set</param>
        public void SetIsWalkable(int x, int y, bool isWalkable)
        {
            ICell rogueCell = this.GetCell(x, y);
            this.SetCellProperties(rogueCell.X, rogueCell.Y, rogueCell.IsTransparent, isWalkable, rogueCell.IsExplored);
        }

        /// <summary>
        /// Updates the <see cref="CurrentMap"/>'s cell properties using the specified <see cref="IMap"/> as a source
        /// </summary>
        /// <param name="source">An <see cref="IMap"/> to be used as a source</param>
        public void UpdateMap(IMap source)
        {
            foreach (ICell sourceCell in source.GetAllCells())
            {
                this.SetCellProperties(sourceCell.X, sourceCell.Y, sourceCell.IsTransparent, sourceCell.IsWalkable, sourceCell.IsExplored);
            }
        }

        /// <summary>
        /// Updates the <see cref="MonsterGoalMap"/> by setting goals and obstacles then recomputing
        /// </summary>
        public void UpdateMonsterGoalMap()
        {
            this.MonsterGoalMap.ClearGoals();
            this.MonsterGoalMap.AddGoal(Game.Singleton.Player.X, Game.Singleton.Player.Y, 1);

            this.MonsterGoalMap.ClearObstacles();
            foreach (Monster monster in this.Monsters)
            {
                this.MonsterGoalMap.AddObstacle(monster.X, monster.Y);
            }

            this.MonsterGoalMap.ComputeCellWeightsLimited(GetCell(Game.Singleton.Player.X, Game.Singleton.Player.Y), 20);
        }

        /// <summary>
        /// Updates the <see cref="Player"/>'s <see cref="FieldOfView"/> by recomputing
        /// </summary>
        public void UpdatePlayerFOV()
        {
            if (this.unexploredWalkableCells == null)
            {
                this.unexploredWalkableCells = new List<Cell>();
                foreach (Cell cell in this.GetAllUnexploredWalkableCells())
                {
                    this.unexploredWalkableCells.Add(cell);
                }
            }

            Player player = Game.Singleton.Player;
            this.ComputeFOV(player.X, player.Y, player.Awareness, true);

            IEnumerable<ICell> awarenessCells = this.GetCellsInDiamond(player.X, player.Y, player.Awareness);
            foreach (ICell cell in awarenessCells)
            {
                if (this.IsInFOV(cell.X, cell.Y))
                {
                    this.SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
                    if (cell.IsWalkable)
                    {
                        this.unexploredWalkableCells.Remove(this.unexploredWalkableCells.FirstOrDefault(c => c.X == cell.X && c.Y == cell.Y));
                    }
                }
            }
        }

        /// <summary>
        /// Draws a <see cref="Cell"/> from the map by setting the contents of the corresponding <see cref="PhiCell"/> using the specified <see cref="Cell"/> and position
        /// </summary>
        /// <param name="rogueCell">A <see cref="Cell"/> to draw</param>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        private void DrawMapCell(RogueSharp.Cell rogueCell, int xOffset, int yOffset)
        {
            if (!rogueCell.IsExplored)
            {
                return;
            }

            if (this.IsInFOV(rogueCell.X, rogueCell.Y))
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(0, rogueCell.X + xOffset, rogueCell.Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString(rogueCell.X, rogueCell.Y) : Symbol(rogueCell.X, rogueCell.Y);
                    Color drawColor = DrawColorFOV(rogueCell.X, rogueCell.Y);
                    Color backgroundDrawColor = BackgroundDrawColorFOV(rogueCell.X, rogueCell.Y);
                    if (!rogueCell.IsWalkable && !Options.GET.DrawTiles)
                    {
                        string symbol = Symbol(rogueCell.X, rogueCell.Y);
                        if (symbol == "#" || symbol == "≈")
                        {
                            backgroundDrawColor = drawColor * 0.5f;
                        }
                    }

                    phiCell.SetContent(phiCellContent, backgroundDrawColor, drawColor);
                }
            }
            else
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(0, rogueCell.X + xOffset, rogueCell.Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString(rogueCell.X, rogueCell.Y) : Symbol(rogueCell.X, rogueCell.Y);
                    Color drawColor = DrawColor(rogueCell.X, rogueCell.Y);
                    Color backgroundDrawColor = BackgroundDrawColor(rogueCell.X, rogueCell.Y);
                    if (!rogueCell.IsWalkable && !Options.GET.DrawTiles)
                    {
                        string symbol = Symbol(rogueCell.X, rogueCell.Y);
                        if (symbol == "#" || symbol == "≈")
                        {
                            backgroundDrawColor = drawColor * 0.5f;
                        }
                    }

                    phiCell.SetContent(phiCellContent, backgroundDrawColor, drawColor);
                }
            }
        }

        #endregion
    }
}