﻿//-----------------------------------------------------------------------
// <copyright file="MapProperties.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.MapCore
{
    using RogueSharp;

    /// <summary>
    /// A class defining a type of container responsible for holding a map's properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IMapProperties"/>
    /// </remarks>
    public class MapProperties : IMapProperties
    {
        #region Fields

        private GenerationStyles _generationStyle;
        private Biomes _biome;

        #endregion

        #region Properties

        public GenerationStyles GenerationStyle
        {
            get { return _generationStyle; } set { _generationStyle = value; }
        }

        public Biomes Biome
        {
            get { return _biome; } set { _biome = value; }
        }

        public string MapName
        {
            get; set;
        }

        public string[] ExitStrings
        {
            get; set;
        }

        public int MapWidth
        {
            get; set;
        }

        public int MapHeight
        {
            get; set;
        }

        public int MaxRoomCount
        {
            get; set;
        }

        public int MaxRoomSize
        {
            get; set;
        }

        public int MinRoomSize
        {
            get; set;
        }

        public int FillProbability
        {
            get; set;
        }

        public int IterationCount
        {
            get; set;
        }

        public int BigAreaFillCutoff
        {
            get; set;
        }

        public bool TransparentWalls
        {
            get; set;
        }

        public string MapRepresentationPath
        {
            get; set;
        }

        public string[,] ExitStringsAdvanced
        {
            get; set;
        }

        public Point StartingPoint
        {
            get; set;
        }

        public bool MapGenerated
        {
            get; set;
        }

        public CurrentMap StoredMap
        {
            get; set;
        }

        public Point StoredPlayerPosition
        {
            get; set;
        }

        #endregion

        #region Methods

        public void Construct_BorderOnlyStlyeProperties(
            string mapName,
            string[] exitStrings,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight)
        {
            MapName = mapName;
            ExitStrings = exitStrings;
            GenerationStyle = generationStyle;
            Biome = biome;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
        }

        public void Construct_RandomRoomsStyleProperties(
            string mapName,
            string[] exitStrings,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight,
            int maxRoomCount,
            int maxRoomSize,
            int minRoomSize)
        {
            MapName = mapName;
            ExitStrings = exitStrings;
            GenerationStyle = generationStyle;
            Biome = biome;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            MaxRoomCount = maxRoomCount;
            MaxRoomSize = maxRoomSize;
            MinRoomSize = minRoomSize;
        }

        public void Construct_CellularStyleProperties(
            string mapName,
            string[] exitStrings,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight,
            int fillProbability,
            int iterationCount,
            int bigAreaFillCutoff,
            bool transparentWalls)
        {
            MapName = mapName;
            ExitStrings = exitStrings;
            GenerationStyle = generationStyle;
            Biome = biome;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            FillProbability = fillProbability;
            IterationCount = iterationCount;
            BigAreaFillCutoff = bigAreaFillCutoff;
            TransparentWalls = transparentWalls;
        }

        public void Construct_FromStringStyleProperties(
            string mapName,
            string[,] exitStringsAdvanced,
            GenerationStyles generationStyle,
            Biomes biome,
            int mapWidth,
            int mapHeight,
            string mapRepresentationPath,
            Point startingPoint)
        {
            MapName = mapName;
            ExitStringsAdvanced = exitStringsAdvanced;
            GenerationStyle = generationStyle;
            Biome = biome;
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            MapRepresentationPath = mapRepresentationPath;
            StartingPoint = startingPoint;
        }

        #endregion
    }
}