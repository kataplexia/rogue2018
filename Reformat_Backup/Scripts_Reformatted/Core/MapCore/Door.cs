﻿//-----------------------------------------------------------------------
// <copyright file="Door.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using UnityEngine;
using RogueSharp;
using Rogue2018.Interfaces;

using PhiOS;

namespace Rogue2018.Core.MapCore
{
    public class Door : IDrawable
    {
        // Constructor
        private static Game game;
        public Door()
        {
            game = Game.Singleton;
            Symbol = game.Symbols.DoorClosed;
            DrawColor = game.Swatch.Door;
            BackgroundDrawColor = game.Swatch.DoorBackground;
        }

        // Properties
        public bool IsOpen                  { get; set; }

        // IDrawable properties
        public Color DrawColor              { get; set; }
        public Color BackgroundDrawColor    { get; set; }
        public Color DrawColorFOV           { get; set; }
        public Color BackgroundDrawColorFOV { get; set; }
        public string Symbol                { get; set; }
        public string GlyphString           { get; set; }
        public int X                        { get; set; }
        public int Y                        { get; set; }

        // Method for drawing
        public void Draw(int xOffset, int yOffset, IMap map)
        {
            // Check if drawable
            if (!game.Tools.CheckDrawable(map.GetCell(X, Y) as RogueSharp.Cell, xOffset, yOffset))
                return;

            // Check if unexplored
            if (!map.GetCell(X, Y).IsExplored)
                return;
            
            // Set symbol and glyph string
            Symbol = IsOpen ? game.Symbols.DoorOpen : game.Symbols.DoorClosed;
            GlyphString = IsOpen ? game.GlyphStrings.DoorOpen : game.GlyphStrings.DoorClosed;

            // Check FOV
            if (map.IsInFOV(X, Y))
            {
                BackgroundDrawColorFOV = game.Swatch.FloorBackgroundFOV * 0.8f;
                DrawColorFOV = game.Swatch.AlternateDarker;
                PhiCell phiCell = PhiDisplay.PhiCellAt(game.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColorFOV, DrawColorFOV);
                }
            }
            else
            {
                BackgroundDrawColor = game.Swatch.FloorBackground * 0.8f;
                DrawColor = game.Swatch.AlternateDarkest;
                PhiCell phiCell = PhiDisplay.PhiCellAt(game.MapLayer, X + xOffset, Y + yOffset);
                if (phiCell != null)
                {
                    string phiCellContent = Options.GET.DrawTiles ? GlyphString : Symbol;
                    phiCell.SetContent(phiCellContent, BackgroundDrawColor, DrawColor);
                }
            }
        }
    }
}