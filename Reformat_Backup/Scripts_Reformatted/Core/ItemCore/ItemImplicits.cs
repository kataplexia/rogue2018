﻿//-----------------------------------------------------------------------
// <copyright file="ItemImplicits.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore.Implicits;

    /// <summary>
    /// A class that defines a container that is responsible for holding and returning a collection of item implicit properties
    /// </summary>
    public class ItemImplicits
    {
        #region Fields

        private List<Implicit> implicits;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemImplicits"/> class
        /// </summary>
        public ItemImplicits()
        {
            this.implicits = new List<Implicit>();

            this.implicits.Add(new HitChanceImplicit());
            this.implicits.Add(new FlatPhysicalDamageImplicit());
            this.implicits.Add(new AwarenessImplicit());
            this.implicits.Add(new CriticalChanceImplicit());
            this.implicits.Add(new CriticalDamageImplicit());
            this.implicits.Add(new DefenceChanceImplicit());
            this.implicits.Add(new ElementalDamageImplicit());
            this.implicits.Add(new MaxHealthImplicit());
            this.implicits.Add(new MaxManaImplicit());
            this.implicits.Add(new SpeedImplicit());
            this.implicits.Add(new SpellCriticalChanceImplicit());
            this.implicits.Add(new SpellCriticalDamageImplicit());
            this.implicits.Add(new SpellDamageImplicit());
            this.implicits.Add(new StunChanceImplicit());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a random item implicit property from the list of all item implicit properties
        /// </summary>
        /// <returns>An <see cref="Implicit"/> selected randomly from the list of all item implicit properties</returns>
        public Implicit GetRandomImplicit()
        {
            int randomIndex = Game.Singleton.Random.Next(0, this.implicits.Count - 1);
            Implicit returnImplicit = Game.Singleton.Tools.DeepCopy<Implicit>(this.implicits[randomIndex]);
            returnImplicit.Construct();
            return returnImplicit;
        }

        #endregion
    }
}