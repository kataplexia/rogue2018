﻿//-----------------------------------------------------------------------
// <copyright file="DefenceChancePrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Defence chance prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class DefenceChancePrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DefenceChancePrefix"/> class
        /// </summary>
        public DefenceChancePrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);

            this.MinimumItemLevel = 3;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 8;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="DefenceChancePrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="DefenceChancePrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Defence Chance", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="DefenceChancePrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(17);
            this.TierMinimumItemLevels.Add(29);
            this.TierMinimumItemLevels.Add(42);
            this.TierMinimumItemLevels.Add(60);
            this.TierMinimumItemLevels.Add(72);
            this.TierMinimumItemLevels.Add(84);
            this.TierMinimumItemLevels.Add(86);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Reinforced");
            this.TierNameStrings.Add("Layered");
            this.TierNameStrings.Add("Lobstered");
            this.TierNameStrings.Add("Buttressed");
            this.TierNameStrings.Add("Thickened");
            this.TierNameStrings.Add("Girded");
            this.TierNameStrings.Add("Impregnable");
            this.TierNameStrings.Add("Impenetrable");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(15, 26));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(27, 42));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(43, 55));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(56, 67));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(68, 79));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(80, 91));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(92, 100));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(101, 110));
        }

        #endregion
    }
}