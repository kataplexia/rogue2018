﻿//-----------------------------------------------------------------------
// <copyright file="FlatDefenceValuePrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Flat defence value prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class FlatDefenceValuePrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FlatDefenceValuePrefix"/> class
        /// </summary>
        public FlatDefenceValuePrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Belt);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 6;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="FlatDefenceValuePrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="FlatDefenceValuePrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1} to Defence Value", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="FlatDefenceValuePrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(18);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(46);
            this.TierMinimumItemLevels.Add(59);
            this.TierMinimumItemLevels.Add(73);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Lacquered");
            this.TierNameStrings.Add("Studded");
            this.TierNameStrings.Add("Ribbed");
            this.TierNameStrings.Add("Fortified");
            this.TierNameStrings.Add("Plated");
            this.TierNameStrings.Add("Carapaced");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(3, 10));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(11, 35));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(36, 60));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(61, 138));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(139, 322));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(323, 400));
        }

        #endregion
    }
}