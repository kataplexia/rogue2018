﻿//-----------------------------------------------------------------------
// <copyright file="ElementalDamagePrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Elemental damage prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class ElementalDamagePrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementalDamagePrefix"/> class
        /// </summary>
        public ElementalDamagePrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 4;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 5;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="ElementalDamagePrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="ElementalDamagePrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Elemental Damage", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="DefenceChancePrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(15);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(60);
            this.TierMinimumItemLevels.Add(81);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            this.TierNameStrings.Add("Catalyzing");
            this.TierNameStrings.Add("Infusing");
            this.TierNameStrings.Add("Empowering");
            this.TierNameStrings.Add("Unleashed");
            this.TierNameStrings.Add("Overpowering");

            // Setup tierEffectValueProperties list
            this.TierEffectValues.Add(Game.Singleton.Random.Next(5, 10));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(11, 20));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(21, 30));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(31, 36));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(37, 42));
        }

        #endregion
    }
}
