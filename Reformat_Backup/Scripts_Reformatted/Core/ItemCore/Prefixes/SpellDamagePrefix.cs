﻿//-----------------------------------------------------------------------
// <copyright file="SpellDamagePrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Spell damage prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class SpellDamagePrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SpellDamagePrefix"/> class
        /// </summary>
        public SpellDamagePrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 2;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 8;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="SpellDamagePrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="SpellDamagePrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Spell Damage", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="SpellDamagePrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(11);
            this.TierMinimumItemLevels.Add(23);
            this.TierMinimumItemLevels.Add(35);
            this.TierMinimumItemLevels.Add(46);
            this.TierMinimumItemLevels.Add(58);
            this.TierMinimumItemLevels.Add(64);
            this.TierMinimumItemLevels.Add(84);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Apprentice's");
            this.TierNameStrings.Add("Adept's");
            this.TierNameStrings.Add("Scholar's");
            this.TierNameStrings.Add("Professor's");
            this.TierNameStrings.Add("Occultist's");
            this.TierNameStrings.Add("Incanter's");
            this.TierNameStrings.Add("Glyphic");
            this.TierNameStrings.Add("Runic");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(20, 29));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(30, 39));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(70, 74));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(75, 79));
        }

        #endregion
    }
}
