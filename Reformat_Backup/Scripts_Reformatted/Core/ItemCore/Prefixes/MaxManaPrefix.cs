﻿//-----------------------------------------------------------------------
// <copyright file="MaxManaPrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Max mana prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class MaxManaPrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxManaPrefix"/> class
        /// </summary>
        public MaxManaPrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 12;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="MaxManaPrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="MaxManaPrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1} to Maximum Mana", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="MaxManaPrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(11);
            this.TierMinimumItemLevels.Add(17);
            this.TierMinimumItemLevels.Add(23);
            this.TierMinimumItemLevels.Add(29);
            this.TierMinimumItemLevels.Add(35);
            this.TierMinimumItemLevels.Add(42);
            this.TierMinimumItemLevels.Add(51);
            this.TierMinimumItemLevels.Add(60);
            this.TierMinimumItemLevels.Add(69);
            this.TierMinimumItemLevels.Add(75);
            this.TierMinimumItemLevels.Add(81);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Beryl");
            this.TierNameStrings.Add("Cobalt");
            this.TierNameStrings.Add("Azure");
            this.TierNameStrings.Add("Sapphire");
            this.TierNameStrings.Add("Cerulean");
            this.TierNameStrings.Add("Aqua");
            this.TierNameStrings.Add("Opalescent");
            this.TierNameStrings.Add("Gentian");
            this.TierNameStrings.Add("Chalybeous");
            this.TierNameStrings.Add("Mazarine");
            this.TierNameStrings.Add("Blue");
            this.TierNameStrings.Add("Zaffre");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(3, 9));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(20, 29));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(30, 39));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(70, 79));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(70, 79));
        }

        #endregion
    }
}