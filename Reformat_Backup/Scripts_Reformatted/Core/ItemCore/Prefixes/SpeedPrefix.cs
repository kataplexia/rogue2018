﻿//-----------------------------------------------------------------------
// <copyright file="SpeedPrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Speed prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class SpeedPrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeedPrefix"/> class
        /// </summary>
        public SpeedPrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 6;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="SpeedPrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="SpeedPrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Speed", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="SpeedPrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(15);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(40);
            this.TierMinimumItemLevels.Add(55);
            this.TierMinimumItemLevels.Add(86);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Runner's");
            this.TierNameStrings.Add("Sprinter's");
            this.TierNameStrings.Add("Stallion's");
            this.TierNameStrings.Add("Gazelle's");
            this.TierNameStrings.Add("Cheetah's");
            this.TierNameStrings.Add("Hellion's");

            this.TierEffectValues.Add(10);
            this.TierEffectValues.Add(15);
            this.TierEffectValues.Add(20);
            this.TierEffectValues.Add(25);
            this.TierEffectValues.Add(30);
            this.TierEffectValues.Add(35);
        }

        #endregion
    }
}