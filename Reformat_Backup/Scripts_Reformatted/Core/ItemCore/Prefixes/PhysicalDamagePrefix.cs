﻿//-----------------------------------------------------------------------
// <copyright file="PhysicalDamagePrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Physical damage prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class PhysicalDamagePrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PhysicalDamagePrefix"/> class
        /// </summary>
        public PhysicalDamagePrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 8;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="PhysicalDamagePrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="PhysicalDamagePrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Physical Damage", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="PhysicalDamagePrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(11);
            this.TierMinimumItemLevels.Add(23);
            this.TierMinimumItemLevels.Add(35);
            this.TierMinimumItemLevels.Add(46);
            this.TierMinimumItemLevels.Add(60);
            this.TierMinimumItemLevels.Add(73);
            this.TierMinimumItemLevels.Add(83);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Heavy");
            this.TierNameStrings.Add("Serrated");
            this.TierNameStrings.Add("Wicked");
            this.TierNameStrings.Add("Vicious");
            this.TierNameStrings.Add("Bloodthirsty");
            this.TierNameStrings.Add("Cruel");
            this.TierNameStrings.Add("Tyrannical");
            this.TierNameStrings.Add("Merciless");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(50, 64));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(65, 84));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(85, 109));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(110, 134));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(135, 154));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(155, 169));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(170, 179));
        }

        #endregion
    }
}
