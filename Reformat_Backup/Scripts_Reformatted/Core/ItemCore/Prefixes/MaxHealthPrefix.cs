﻿//-----------------------------------------------------------------------
// <copyright file="MaxHealthPrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Max health prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class MaxHealthPrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxHealthPrefix"/> class
        /// </summary>
        public MaxHealthPrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 8;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="MaxHealthPrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="MaxHealthPrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1} to Maximum Health", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="MaxHealthPrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(5);
            this.TierMinimumItemLevels.Add(11);
            this.TierMinimumItemLevels.Add(18);
            this.TierMinimumItemLevels.Add(24);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(36);
            this.TierMinimumItemLevels.Add(44);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            // Setup tierNamePropeties list
            this.TierNameStrings.Add("Hale");
            this.TierNameStrings.Add("Healthy");
            this.TierNameStrings.Add("Sanguine");
            this.TierNameStrings.Add("Stalwart");
            this.TierNameStrings.Add("Stout");
            this.TierNameStrings.Add("Robust");
            this.TierNameStrings.Add("Rotund");
            this.TierNameStrings.Add("Virile");

            // Setup tierEffectValueProperties list
            this.TierEffectValues.Add(Game.Singleton.Random.Next(3, 9));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(20, 29));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(30, 39));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(40, 49));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(50, 59));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(60, 69));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(70, 79));
        }

        #endregion
    }
}