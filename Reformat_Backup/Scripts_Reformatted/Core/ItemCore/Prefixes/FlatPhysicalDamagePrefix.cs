﻿//-----------------------------------------------------------------------
// <copyright file="FlatPhysicalDamagePrefix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Prefixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Prefix"/> (Flat physical damage prefix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class FlatPhysicalDamagePrefix : Prefix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FlatPhysicalDamagePrefix"/> class
        /// </summary>
        public FlatPhysicalDamagePrefix()
        {
            this.PrefixBaseTypes = new List<EquipableItemBaseTypes>();
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.PrefixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            this.MinimumItemLevel = 5;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 6;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="FlatPhysicalDamagePrefix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="FlatPhysicalDamagePrefix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the prefix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1} to Physical Damage", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="FlatDefenceValuePrefix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(13);
            this.TierMinimumItemLevels.Add(19);
            this.TierMinimumItemLevels.Add(28);
            this.TierMinimumItemLevels.Add(35);
            this.TierMinimumItemLevels.Add(44);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("Glinting");
            this.TierNameStrings.Add("Burnished");
            this.TierNameStrings.Add("Polished");
            this.TierNameStrings.Add("Honed");
            this.TierNameStrings.Add("Gleaming");
            this.TierNameStrings.Add("Annealed");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(1, 2));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(2, 5));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(3, 7));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(4, 10));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(5, 12));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(6, 15));
        }

        #endregion
    }
}