﻿//-----------------------------------------------------------------------
// <copyright file="ElementalDamageImplicit.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    /// <summary>
    /// A class defining a type of <see cref="Implicit"/> (Elemental damage implicit) with all of its associated properties
    /// </summary>
    [System.Serializable]
    public class ElementalDamageImplicit : Implicit
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementalDamageImplicit"/> class
        /// </summary>
        public ElementalDamageImplicit()
        {
            this.Construct();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="ElementalDamageImplicit"/>'s effect(s) 
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            // TODO
        }

        /// <summary>
        /// Constructs the <see cref="ElementalDamageImplicit"/>
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct()
        {
            this.EffectValue = Dice.Roll("1D10");
            this.EffectInfoString = string.Format("+{0}% to Elemental Damage", this.EffectValue);
        }

        #endregion
    }
}