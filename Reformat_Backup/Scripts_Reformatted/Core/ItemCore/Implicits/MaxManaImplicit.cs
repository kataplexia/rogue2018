﻿//-----------------------------------------------------------------------
// <copyright file="MaxManaImplicit.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    /// <summary>
    /// A class defining a type of <see cref="Implicit"/> (Max mana implicit) with all of its associated properties
    /// </summary>
    [System.Serializable]
    public class MaxManaImplicit : Implicit
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxManaImplicit"/> class
        /// </summary>
        public MaxManaImplicit()
        {
            this.Construct();
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Applies the <see cref="MaxManaImplicit"/>'s effect(s) 
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="MaxManaImplicit"/>
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct()
        {
            this.EffectValue = Dice.Roll("1D10");
            this.EffectInfoString = string.Format("+{0} to Maximum Mana", this.EffectValue);
        }

        #endregion
    }
}
