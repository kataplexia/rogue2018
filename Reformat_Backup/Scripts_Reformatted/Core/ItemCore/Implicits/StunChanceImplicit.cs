﻿//-----------------------------------------------------------------------
// <copyright file="StunChanceImplicit.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    /// <summary>
    /// A class defining a type of <see cref="Implicit"/> (Spell damage implicit) with all of its associated properties
    /// </summary>
    [System.Serializable]
    public class StunChanceImplicit : Implicit
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StunChanceImplicit"/> class
        /// </summary>
        public StunChanceImplicit()
        {
            this.Construct();
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Applies the <see cref="StunChanceImplicit"/>'s effect(s) 
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="StunChanceImplicit"/>
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct()
        {
            this.EffectValue = Dice.Roll("1D5");
            this.EffectInfoString = string.Format("+{0}% to Stun Chance", this.EffectValue);
        }

        #endregion
    }
}