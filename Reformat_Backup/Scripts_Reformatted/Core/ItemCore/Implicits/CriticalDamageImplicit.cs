﻿//-----------------------------------------------------------------------
// <copyright file="CriticalDamageImplicit.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Implicits
{
    using RogueSharp.DiceNotation;

    /// <summary>
    /// A class defining a type of <see cref="Implicit"/> (Critical damage implicit) with all of its associated properties
    /// </summary>
    [System.Serializable]
    public class CriticalDamageImplicit : Implicit
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CriticalDamageImplicit"/> class
        /// </summary>
        public CriticalDamageImplicit()
        {
            this.Construct();
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Applies the <see cref="CriticalDamageImplicit"/>'s effect(s) 
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="CriticalDamageImplicit"/>
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct()
        {
            this.EffectValue = Dice.Roll("1D5");
            this.EffectInfoString = string.Format("+{0} to Critical Damage", this.EffectValue);
        }

        #endregion
    }
}