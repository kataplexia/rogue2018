﻿//-----------------------------------------------------------------------
// <copyright file="Suffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;
    using Rogue2018.Interfaces.ItemInterfaces;

    /// <summary>
    /// A class defining a suffix quality to be applied to an item with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="ISuffix"/>
    /// </remarks>
    [System.Serializable]
    public abstract class Suffix : ISuffix
    {
        #region Fields

        private string effectInfoString;
        private int effectValue;
        private int maximumItemLevel;
        private int minimumItemLevel;
        private int numberOfTiers;
        private List<EquipableItemBaseTypes> suffixBaseTypes;
        private string suffixNameString;
        private List<int> tierEffectValues;
        private string tierInfoString;
        private List<int> tierMinimumItemLevels;
        private List<string> tierNameStrings;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="EffectInfoString"/> property
        /// </summary>
        public string EffectInfoString
        {
            get { return this.effectInfoString; } set { this.effectInfoString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EffectValue"/> property
        /// </summary>
        public int EffectValue
        {
            get { return this.effectValue; } set { this.effectValue = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="MaximumItemLevel"/> property
        /// </summary>
        public int MaximumItemLevel
        {
            get { return this.maximumItemLevel; } set { this.maximumItemLevel = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="MinimumItemLevel"/> property
        /// </summary>
        public int MinimumItemLevel
        {
            get { return this.minimumItemLevel; } set { this.minimumItemLevel = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="NumberOfTiers"/> property
        /// </summary>
        public int NumberOfTiers
        {
            get { return this.numberOfTiers; } set { this.numberOfTiers = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="SuffixBaseTypes"/> property
        /// </summary>
        public List<EquipableItemBaseTypes> SuffixBaseTypes
        {
            get { return this.suffixBaseTypes; } set { this.suffixBaseTypes = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="SuffixNameString"/> property
        /// </summary>
        public string SuffixNameString
        {
            get { return this.suffixNameString; } set { this.suffixNameString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="TierEffectValues"/> property
        /// </summary>
        public List<int> TierEffectValues
        {
            get { return this.tierEffectValues; } set { this.tierEffectValues = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="TierInfoString"/> property
        /// </summary>
        public string TierInfoString
        {
            get { return this.tierInfoString; } set { this.tierInfoString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="TierMinimumItemLevels"/> property
        /// </summary>
        public List<int> TierMinimumItemLevels
        {
            get { return this.tierMinimumItemLevels; } set { this.tierMinimumItemLevels = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="TierNameStrings"/> property
        /// </summary>
        public List<string> TierNameStrings
        {
            get { return this.tierNameStrings; } set { this.tierNameStrings = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="Suffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void ApplyEffect();

        /// <summary>
        /// Constructs the <see cref="Suffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void Construct(int itemLevel);

        /// <summary>
        /// Constructs the <see cref="Suffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void ConstructTiers();

        /// <summary>
        /// Initializes the suffix's data lists
        /// </summary>
        public void SetupLists()
        {
            this.TierMinimumItemLevels = new List<int>(this.NumberOfTiers + 1);
            this.TierNameStrings = new List<string>(this.NumberOfTiers);
            this.TierEffectValues = new List<int>(this.NumberOfTiers);
        }

        /// <summary>
        /// Sets up the suffix's data lists with names and values using the specified item level
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when creating the name and value for the suffix</param>
        public void SetupNameAndValue(int itemLevel)
        {
            for (int i = 0; i < this.NumberOfTiers; i++)
            {
                if (itemLevel >= this.TierMinimumItemLevels[i] && itemLevel < this.TierMinimumItemLevels[i + 1])
                {
                    if (i > 0)
                    {
                        int randomTierIndex = Game.Singleton.Random.Next(0, i);
                        this.SuffixNameString = this.TierNameStrings[randomTierIndex];
                        this.EffectValue = this.TierEffectValues[randomTierIndex];
                        this.TierInfoString = string.Format("[Tier {0}]", this.NumberOfTiers - randomTierIndex);
                    }
                    else
                    {
                        this.SuffixNameString = this.TierNameStrings[i];
                        this.EffectValue = this.TierEffectValues[i];
                        this.TierInfoString = string.Format("[Tier {0}]", this.NumberOfTiers - i);
                    }
                }
            }
        }

        #endregion
    }
}