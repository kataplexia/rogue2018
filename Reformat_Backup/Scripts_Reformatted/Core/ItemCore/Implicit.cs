﻿//-----------------------------------------------------------------------
// <copyright file="Implicit.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces.ItemInterfaces;

    /// <summary>
    /// A class defining an item's <see cref="Implicit"/> quality with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IImplicit"/>
    /// </remarks>
    [System.Serializable]
    public abstract class Implicit : IImplicit
    {
        #region Fields

        private string effectInfoString;
        private int effectValue;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="EffectInfoString"/> property
        /// </summary>
        public string EffectInfoString
        {
            get { return this.effectInfoString; } set { this.effectInfoString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EffectValue"/> property
        /// </summary>
        public int EffectValue
        {
            get { return this.effectValue; } set { this.effectValue = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="Implicit"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void ApplyEffect();

        /// <summary>
        /// Constructs the <see cref="Implicit"/>
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void Construct();

        #endregion
    }
}