﻿//-----------------------------------------------------------------------
// <copyright file="ItemNameGenerator.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.IO;
    using System.Xml;
    using Rogue2018.Interfaces;
    using UnityEngine;

    /// <summary>
    /// A class defining a generator responsible for parsing from item name data and constructing item names
    /// </summary>
    public class ItemNameGenerator
    {
        #region Fields

        private string[] preNamesArray;
        private string[] soulShieldsPostNamesArray;
        private string[] otherShieldsPostNamesArray;
        private string[] chestArmoursPostNamesArray;
        private string[] headArmoursPostNamesArray;
        private string[] handsArmoursPostNamesArray;
        private string[] feetArmoursPostNamesArray;
        private string[] legsArmoursPostNameArray;
        private string[] ringsPostNameArray;
        private string[] beltsPostNameArray;
        private string[] quiversPostNameArray;
        private string[] axesPostNameArray;
        private string[] macesPostNameArray;
        private string[] sceptresPostNameArray;
        private string[] stavesPostNameArray;
        private string[] swordsPostNameArray;
        private string[] daggersPostNameArray;
        private string[] bowsPostNameArray;
        private string[] wandsPostNameArray;
        private string[] fociPostNameArray;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemNameGenerator"/> class
        /// </summary>
        public ItemNameGenerator()
        {
            string xmlPath = "ItemNameData";
            var xmlRawFile = Resources.Load<TextAsset>(xmlPath);
            string xmlData = xmlRawFile.text;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(new StringReader(xmlData));

            string[] seperatingChars = { ", " };

            XmlNode preNamesNode = xmlDocument.SelectSingleNode("/data/prenames");
            string preNamesString = preNamesNode.InnerXml;
            this.preNamesArray = preNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode soulShieldPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/soulshields");
            string soulShieldPostNamesString = soulShieldPostNamesNode.InnerXml;
            this.soulShieldsPostNamesArray = soulShieldPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode otherShieldPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/othershields");
            string otherShieldPostNamesString = otherShieldPostNamesNode.InnerXml;
            this.otherShieldsPostNamesArray = otherShieldPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode chestArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/chestarmours");
            string chestArmoursPostNamesString = chestArmoursPostNamesNode.InnerXml;
            this.chestArmoursPostNamesArray = chestArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode headArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/headarmours");
            string headArmoursPostNamesString = headArmoursPostNamesNode.InnerXml;
            this.headArmoursPostNamesArray = headArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode handsArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/handsarmours");
            string handsArmoursPostNamesString = handsArmoursPostNamesNode.InnerXml;
            this.handsArmoursPostNamesArray = handsArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode feetArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/feetarmours");
            string feetArmoursPostNamesString = feetArmoursPostNamesNode.InnerXml;
            this.feetArmoursPostNamesArray = feetArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode legsArmoursPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/legsarmours");
            string legsArmoursPostNamesString = legsArmoursPostNamesNode.InnerXml;
            this.legsArmoursPostNameArray = legsArmoursPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode ringsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/rings");
            string ringsPostNamesString = ringsPostNamesNode.InnerXml;
            this.ringsPostNameArray = ringsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode beltsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/belts");
            string beltsPostNamesString = beltsPostNamesNode.InnerXml;
            this.beltsPostNameArray = beltsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode quiversPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/quivers");
            string quiversPostNamesString = quiversPostNamesNode.InnerXml;
            this.quiversPostNameArray = quiversPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode axesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/axes");
            string axesPostNamesString = axesPostNamesNode.InnerXml;
            this.axesPostNameArray = axesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode macesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/maces");
            string macesPostNamesString = macesPostNamesNode.InnerXml;
            this.macesPostNameArray = macesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode sceptresPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/sceptres");
            string sceptresPostNamesString = sceptresPostNamesNode.InnerXml;
            this.sceptresPostNameArray = sceptresPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode stavesPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/staves");
            string stavesPostNamesString = stavesPostNamesNode.InnerXml;
            this.stavesPostNameArray = stavesPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode swordsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/swords");
            string swordsPostNamesString = swordsPostNamesNode.InnerXml;
            this.swordsPostNameArray = swordsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode daggersPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/daggers");
            string daggersPostNamesString = daggersPostNamesNode.InnerXml;
            this.daggersPostNameArray = daggersPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode bowsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/bows");
            string bowsPostNamesString = bowsPostNamesNode.InnerXml;
            this.bowsPostNameArray = bowsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode wandsPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/wands");
            string wandsPostNamesString = wandsPostNamesNode.InnerXml;
            this.wandsPostNameArray = wandsPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);

            XmlNode fociPostNamesNode = xmlDocument.SelectSingleNode("/data/postnames/foci");
            string fociPostNamesString = fociPostNamesNode.InnerXml;
            this.fociPostNameArray = fociPostNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a random name using the specified <see cref="EquipableItem"/>
        /// </summary>
        /// <param name="equipableItem">An <see cref="EquipableItem"/> to randomly name</param>
        /// <returns>A <see cref="string"/> defining the random name of the item</returns>
        public string CreateRandomName(EquipableItem equipableItem)
        {
            string preName = this.preNamesArray[Game.Singleton.Random.Next(0, this.preNamesArray.Length - 1)];
            string postName = string.Empty;

            if (equipableItem is IWeapon)
            {
                IWeapon equipableWeapon = equipableItem as IWeapon;
                if (equipableWeapon.WeaponType == WeaponTypes.Axe)
                {
                    postName = this.axesPostNameArray[Game.Singleton.Random.Next(0, this.axesPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Bow)
                {
                    postName = this.bowsPostNameArray[Game.Singleton.Random.Next(0, this.bowsPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Dagger)
                {
                    postName = this.daggersPostNameArray[Game.Singleton.Random.Next(0, this.daggersPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Mace)
                {
                    postName = this.macesPostNameArray[Game.Singleton.Random.Next(0, this.macesPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Sceptre)
                {
                    postName = this.sceptresPostNameArray[Game.Singleton.Random.Next(0, this.sceptresPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Staff)
                {
                    postName = this.stavesPostNameArray[Game.Singleton.Random.Next(0, this.stavesPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Sword)
                {
                    postName = this.swordsPostNameArray[Game.Singleton.Random.Next(0, this.swordsPostNameArray.Length - 1)];
                }
                else if (equipableWeapon.WeaponType == WeaponTypes.Wand)
                {
                    postName = this.wandsPostNameArray[Game.Singleton.Random.Next(0, this.wandsPostNameArray.Length - 1)];
                }
            }
            else if (equipableItem is IArmour)
            {
                if (equipableItem.EquipmentLocation == EquipmentLocations.Chest)
                {
                    postName = this.chestArmoursPostNamesArray[Game.Singleton.Random.Next(0, this.chestArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Feet)
                {
                    postName = this.feetArmoursPostNamesArray[Game.Singleton.Random.Next(0, this.feetArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Hands)
                {
                    postName = this.handsArmoursPostNamesArray[Game.Singleton.Random.Next(0, this.handsArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Head)
                {
                    postName = this.headArmoursPostNamesArray[Game.Singleton.Random.Next(0, this.headArmoursPostNamesArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.Legs)
                {
                    postName = this.legsArmoursPostNameArray[Game.Singleton.Random.Next(0, this.legsArmoursPostNameArray.Length - 1)];
                }
                else if (equipableItem.EquipmentLocation == EquipmentLocations.OffHand)
                {
                    IArmour equipableArmor = equipableItem as IArmour;
                    if (equipableArmor.ArmourStyle == ArmourStyles.Light)
                    {
                        postName = this.soulShieldsPostNamesArray[Game.Singleton.Random.Next(0, this.soulShieldsPostNamesArray.Length - 1)];
                    }
                    else
                    {
                        postName = this.otherShieldsPostNamesArray[Game.Singleton.Random.Next(0, this.otherShieldsPostNamesArray.Length - 1)];
                    }
                }
            }
            else if (equipableItem is IAccessory)
            {
                IAccessory equipableAccessory = equipableItem as IAccessory;
                if (equipableAccessory.AccessoryType == AccessoryTypes.Belt)
                {
                    postName = this.beltsPostNameArray[Game.Singleton.Random.Next(0, this.beltsPostNameArray.Length - 1)];
                }
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Focus)
                {
                    postName = this.fociPostNameArray[Game.Singleton.Random.Next(0, this.fociPostNameArray.Length - 1)];
                }
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Quiver)
                {
                    postName = this.quiversPostNameArray[Game.Singleton.Random.Next(0, this.quiversPostNameArray.Length - 1)];
                }
                else if (equipableAccessory.AccessoryType == AccessoryTypes.Ring)
                {
                    postName = this.ringsPostNameArray[Game.Singleton.Random.Next(0, this.ringsPostNameArray.Length - 1)];
                }
            }

            return preName + " " + postName;
        }

        #endregion
    }
}