﻿//-----------------------------------------------------------------------
// <copyright file="ItemSuffixes.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore.Suffixes;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a container that is responsible for holding and returning a collection of <see cref="Suffix"/>es
    /// </summary>
    public class ItemSuffixes : ItemAffixes
    {
        #region Fields

        private List<Suffix> suffixesList;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSuffixes"/> class
        /// </summary>
        public ItemSuffixes()
        {
            this.suffixesList = new List<Suffix>();

            this.suffixesList.Add(new AwarenessSuffix());
            this.suffixesList.Add(new ColdResistanceSuffix());
            this.suffixesList.Add(new CriticalChanceSuffix());
            this.suffixesList.Add(new CriticalDamageSuffix());
            this.suffixesList.Add(new ElementalResistanceSuffix());
            this.suffixesList.Add(new FireResistanceSuffix());
            this.suffixesList.Add(new HitChanceSuffix());
            this.suffixesList.Add(new LightningResistanceSuffix());
            this.suffixesList.Add(new PlagueResistanceSuffix());
            this.suffixesList.Add(new SpellCriticalChanceSuffix());

            this.SetupEBTSuffixLists(this.suffixesList);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a random <see cref="Suffix"/> using the specified item's current <see cref="List{T}(Suffix)"/>, item level and equipable base type
        /// </summary>
        /// <param name="currentSuffixList">A <see cref="List{T}(Suffix)"/> defining the item's current suffixes</param>
        /// <param name="itemLevel">An <see cref="int"/> defining the item's item level</param>
        /// <param name="ebt">An <see cref="EquipableItem"/> defining the item's eqipable base type</param>
        /// <returns>A <see cref="Suffix"/> randomly selected from the list of suitable prefixes</returns>
        public Suffix GetRandomSuffix(List<Suffix> currentSuffixList, int itemLevel, EquipableItemBaseTypes ebt)
        {
            List<Suffix> suffixListForEBT = GetSuffixForEBT(ebt);
            List<Suffix> modifiedSuffixesList = new List<Suffix>();

            for (int i = 0; i < suffixListForEBT.Count; i++)
            {
                bool currentListContainsMatch = false;
                foreach (Suffix currentsuffix in currentSuffixList)
                {
                    if (currentsuffix.GetType().Name == suffixListForEBT[i].GetType().Name)
                    {
                        currentListContainsMatch = true;
                    }
                }

                if (!currentListContainsMatch)
                {
                    modifiedSuffixesList.Add(suffixListForEBT[i]);
                }
            }

            int randomIndex = Game.Singleton.Random.Next(0, modifiedSuffixesList.Count - 1);
            Suffix selectedSuffix = modifiedSuffixesList[randomIndex];

            Suffix returnSuffix = Game.Singleton.Tools.DeepCopy<Suffix>(modifiedSuffixesList[randomIndex]);
            returnSuffix.Construct(itemLevel);
            return returnSuffix;
        }

        #endregion
    }
}