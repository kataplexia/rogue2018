﻿//-----------------------------------------------------------------------
// <copyright file="ItemPrefixes.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Core.ItemCore.Prefixes;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a container that is responsible for holding and returning a collection of <see cref="Prefix"/>es
    /// </summary>
    public class ItemPrefixes : ItemAffixes
    {
        #region Fields

        private List<Prefix> prefixesList;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemPrefixes"/> class
        /// </summary>
        public ItemPrefixes()
        {
            this.prefixesList = new List<Prefix>();

            this.prefixesList.Add(new DefenceChancePrefix());
            this.prefixesList.Add(new ElementalDamagePrefix());
            this.prefixesList.Add(new FlatDefenceValuePrefix());
            this.prefixesList.Add(new FlatPhysicalDamagePrefix());
            this.prefixesList.Add(new MaxHealthPrefix());
            this.prefixesList.Add(new MaxManaPrefix());
            this.prefixesList.Add(new PhysicalDamagePrefix());
            this.prefixesList.Add(new SpeedPrefix());
            this.prefixesList.Add(new SpellDamagePrefix());

            this.SetupEBTPrefixLists(this.prefixesList);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a random <see cref="Prefix"/> using the specified item's current <see cref="List{T}(Prefix)"/>, item level and equipable base type
        /// </summary>
        /// <param name="currentPrefixList">A <see cref="List{T}(Prefix)"/> defining the item's current prefixes</param>
        /// <param name="itemLevel">An <see cref="int"/> defining the item's item level</param>
        /// <param name="ebt">An <see cref="EquipableItem"/> defining the item's eqipable base type</param>
        /// <returns>A <see cref="Prefix"/> randomly selected from the list of suitable prefixes</returns>
        public Prefix GetRandomPrefix(List<Prefix> currentPrefixList, int itemLevel, EquipableItemBaseTypes ebt)
        {
            List<Prefix> prefixListForEBT = GetPrefixForEBT(ebt);
            List<Prefix> modifiedPrefixesList = new List<Prefix>();

            for (int i = 0; i < prefixListForEBT.Count; i++)
            {
                bool currentListContainsMatch = false;
                foreach (Prefix currentPrefix in currentPrefixList)
                {
                    if (currentPrefix.GetType().Name == prefixListForEBT[i].GetType().Name)
                    {
                        currentListContainsMatch = true;
                    }
                }

                if (!currentListContainsMatch)
                {
                    modifiedPrefixesList.Add(prefixListForEBT[i]);
                }
            }

            int randomIndex = Game.Singleton.Random.Next(0, modifiedPrefixesList.Count - 1);
            Prefix selectedSuffix = modifiedPrefixesList[randomIndex];

            Prefix returnPrefix = Game.Singleton.Tools.DeepCopy<Prefix>(modifiedPrefixesList[randomIndex]);
            returnPrefix.Construct(itemLevel);
            return returnPrefix;
        }

        #endregion
    }
}