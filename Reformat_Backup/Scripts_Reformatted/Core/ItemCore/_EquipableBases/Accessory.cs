﻿//-----------------------------------------------------------------------
// <copyright file="Accessory.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="EquipableItem"/> (Accessory) with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IAccessory"/>
    /// </remarks>
    [System.Serializable]
    public class Accessory : EquipableItem, IAccessory
    {
        #region Fields

        private AccessoryTypes accessoryType;
        private string accessoryTypeString;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="AccessoryType"/> property
        /// </summary>
        public AccessoryTypes AccessoryType
        {
            get { return this.accessoryType; } set { this.accessoryType = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="AccessoryTypeString"/> property
        /// </summary>
        public string AccessoryTypeString
        {
            get { return this.accessoryTypeString; } set { this.accessoryTypeString = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Constructs the <see cref="Accessory"/>
        /// </summary>
        public override void Construct()
        {
            this.EquipmentLocationString = this.SetupEquipmentLocationString(this.EquipmentLocation);
            this.AccessoryTypeString = this.SetupAccessoryTypeString(this.AccessoryType);
            this.SetupAffixes();
        }

        #endregion
    }
}