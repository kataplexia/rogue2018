﻿//-----------------------------------------------------------------------
// <copyright file="Weapon.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="EquipableItem"/> (Weapon) with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IWeapon"/>
    /// </remarks>
    [System.Serializable]
    public class Weapon : EquipableItem, IWeapon
    {
        #region Fields

        private WeaponStyles weaponStyle;
        private WeaponTypes weaponType;
        private DamageTypes damageType;
        private string weaponStyleString;
        private string weaponTypeString;
        private string damageTypeString;
        private int physicalDamage;
        private int hitChance;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="WeaponStyle"/> property
        /// </summary>
        public WeaponStyles WeaponStyle
        {
            get { return this.weaponStyle; } set { this.weaponStyle = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="WeaponType"/> property
        /// </summary>
        public WeaponTypes WeaponType
        {
            get { return this.weaponType; } set { this.weaponType = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="DamageType"/> property
        /// </summary>
        public DamageTypes DamageType
        {
            get { return this.damageType; } set { this.damageType = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="WeaponStyleString"/> property
        /// </summary>
        public string WeaponStyleString
        {
            get { return this.weaponStyleString; } set { this.weaponStyleString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="WeaponTypeString"/> property
        /// </summary>
        public string WeaponTypeString
        {
            get { return this.weaponTypeString; } set { this.weaponTypeString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="DamageTypeString"/> property
        /// </summary>
        public string DamageTypeString
        {
            get { return this.damageTypeString; } set { this.damageTypeString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="PhysicalDamage"/> property
        /// </summary>
        public int PhysicalDamage
        {
            get { return this.physicalDamage; } set { this.physicalDamage = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="HitChance"/> property
        /// </summary>
        public int HitChance
        {
            get { return this.hitChance; } set { this.hitChance = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Constructs the <see cref="Weapon"/>
        /// </summary>
        public override void Construct()
        {
            this.EquipmentLocationString = this.SetupEquipmentLocationString(this.EquipmentLocation);
            this.WeaponStyleString = this.SetupWeaponStyleString(this.WeaponStyle);
            this.WeaponTypeString = this.SetupWeaponTypeString(this.WeaponType);
            this.DamageTypeString = this.SetupDamageTypeString(this.DamageType);
            this.SetupAffixes();
        }

        #endregion
    }
}