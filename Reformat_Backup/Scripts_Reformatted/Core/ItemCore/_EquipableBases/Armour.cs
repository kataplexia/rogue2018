﻿//-----------------------------------------------------------------------
// <copyright file="Armour.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="EquipableItem"/> (Armour) with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IArmour"/>
    /// </remarks>
    [System.Serializable]
    public class Armour : EquipableItem, IArmour
    {
        #region Fields

        private ArmourStyles armourStyle;
        private string armourStyleString;
        private int defenceValue;
        private int defenceChance;
        private int speedModifier;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="ArmourStyle"/> property
        /// </summary>
        public ArmourStyles ArmourStyle
        {
            get { return this.armourStyle; } set { this.armourStyle = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ArmourStyleString"/> property
        /// </summary>
        public string ArmourStyleString
        {
            get { return this.armourStyleString; } set { this.armourStyleString = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="DefenceValue"/> property
        /// </summary>
        public int DefenceValue
        {
            get { return this.defenceValue; } set { this.defenceValue = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="DefenceChance"/> property
        /// </summary>
        public int DefenceChance
        {
            get { return this.defenceChance; } set { this.defenceChance = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="SpeedModifier"/> property
        /// </summary>
        public int SpeedModifier
        {
            get { return this.speedModifier; } set { this.speedModifier = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Constructs the <see cref="Armour"/>
        /// </summary>
        public override void Construct()
        {
            this.EquipmentLocationString = this.SetupEquipmentLocationString(this.EquipmentLocation);
            this.ArmourStyleString = this.SetupArmourStyleString(this.ArmourStyle);
            this.SetupAffixes();
        }

        #endregion
    }
}
