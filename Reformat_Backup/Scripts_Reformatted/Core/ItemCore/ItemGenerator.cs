﻿//-----------------------------------------------------------------------
// <copyright file="ItemGenerator.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class that defines a generator responsible for constructing and returning items
    /// </summary>
    public class ItemGenerator
    {
        #region Methods

        /// <summary>
        /// Constructs and returns an <see cref="EquipableItem"/> using the specified item base, quality and item level
        /// </summary>
        /// <param name="itemBase">An <see cref="EquipableItem"/> defining the base item to construct and return</param>
        /// <param name="itemQuality">The <see cref="ItemQualities"/> value definining the quality of the item to construct and return</param>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level of the item to construct and return</param>
        /// <returns>An <see cref="EquipableItem"/> using the specified parameters</returns>
        public EquipableItem CreateEquipableItem(
            EquipableItem itemBase,
            ItemQualities itemQuality,
            int itemLevel)
        {
            EquipableItem equipableItem = itemBase;
            equipableItem.ItemQuality = itemQuality;
            equipableItem.ItemLevel = itemLevel;

            equipableItem.Construct();
            return equipableItem;
        }

        /// <summary>
        /// Constructs and returns an <see cref="UsableItem"/> using the specified item base
        /// </summary>
        /// <param name="itemBase">A <see cref="UsableItem"/> defining the base item to construct and return</param>
        /// <returns>A <see cref="UsableItem"/> using the specified parameters</returns>
        public UsableItem CreateUsableItem(
            UsableItem itemBase)
        {
            UsableItem usableItem = itemBase;

            usableItem.Construct();
            return usableItem;
        }

        #endregion
    }
}