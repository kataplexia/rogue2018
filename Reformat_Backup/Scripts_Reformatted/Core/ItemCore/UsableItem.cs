﻿//-----------------------------------------------------------------------
// <copyright file="UsableItem.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using Rogue2018.Interfaces;
    using UnityEngine;

    /// <summary>
    /// A class defining a type of <see cref="UsableItem"/> with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IItem"/> and <see cref="IUsable"/>
    /// </remarks>
    public abstract class UsableItem : IItem, IUsable
    {
        #region Fields

        private int currentStackCount;
        private string description;
        private ItemQualities itemQuality;
        private int maxStackCount;
        private string name;
        private System.Type parentType;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="CurrentStackCount"/> property
        /// </summary>
        public int CurrentStackCount
        {
            get { return this.currentStackCount; } set { this.currentStackCount = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Description"/> property
        /// </summary>
        public string Description
        {
            get { return this.description; } set { this.description = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ItemQuality"/> property
        /// </summary>
        public ItemQualities ItemQuality
        {
            get { return this.itemQuality; } set { this.itemQuality = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="MaxStackCount"/> property
        /// </summary>
        public int MaxStackCount
        {
            get { return this.maxStackCount; } set { this.maxStackCount = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Name"/> property
        /// </summary>
        public string Name
        {
            get { return this.name; } set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ParentType"/> property
        /// </summary>
        public System.Type ParentType
        {
            get { return this.parentType; } set { this.parentType = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Constructs the <see cref="UsableItem"/>
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void Construct();

        /// <summary>
        /// Draws the <see cref="UsableItem"/>'s <see cref="Name"/> using the specified offsets and color
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        /// <param name="drawColor">A <see cref="Color"/> defining the foreground color used for drawing</param>
        public void DrawName(int xOffset, int yOffset, Color drawColor)
        {
            Color backgroundDrawColor = Game.Singleton.Tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            Game.Singleton.Tools.DrawString(xOffset, yOffset, this.Name, drawColor, backgroundDrawColor, 0);
        }

        /// <summary>
        /// Draws the <see cref="UsableItem"/>'s <see cref="CurrentStackCount"/> using the specified offsets and color
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        /// <param name="drawColor">A <see cref="Color"/> defining the foreground color used for drawing</param>
        public void DrawStacks(int xOffset, int yOffset, Color drawColor)
        {
            Color backgroundDrawColor = Game.Singleton.Tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            Game.Singleton.Tools.DrawString(xOffset, yOffset, string.Format("[{0}/{1}]", this.CurrentStackCount, this.MaxStackCount), drawColor, backgroundDrawColor, 0);
        }

        /// <summary>
        /// Drop this <see cref="IItem"/>
        /// </summary>
        /// <param name="dropAll">A <see cref="bool"/> defining whether or not to drop the whole stack</param>
        public void Drop(bool dropAll = false)
        {
            if (dropAll)
            {
                Game.Singleton.Player.InventoryList.Remove(this);

                //// TODO: Place item inside list for current cell's contents

                string droppedMessage = string.Format("You dropped {0}x {1} on the ground", this.CurrentStackCount, this.Name);
                Game.Singleton.MessageSystem.Add(droppedMessage, Color.cyan, false);
                Game.Singleton.SetDrawRequired(true);
            }
            else
            {
                this.CurrentStackCount--;
                if (this.CurrentStackCount == 0)
                {
                    Game.Singleton.Player.InventoryList.Remove(this);
                }

                //// Create instance for inside list for current cell's contents
                //// System.Object instance = System.Activator.CreateInstance(ParentType);
                //// UsableItem droppedUsable = Game.Singleton.itemGenerator.CreateUsableItem((UsableItem)instance);
                //// Debug.Log("Created instance of: " + droppedUsable.Name + ", which has [" + droppedUsable.CurrentStackCount + "/" + droppedUsable.MaxStackCount + "] stack property.");

                string droppedMessage = string.Format("You dropped 1x {0} on the ground", this.Name);
                Game.Singleton.MessageSystem.Add(droppedMessage, Color.cyan, false);
                Game.Singleton.SetDrawRequired(true);
            }
        }

        /// <summary>
        /// Uses the <see cref="UsableItem"/>
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void Use();

        #endregion
    }
}