﻿// <auto-generated/>
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class FellingHatchet : Weapon
    {
        public FellingHatchet()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.OneHandedAxe;
            Name = "Felling Hatchet";
            Description = "A felling hatchet.";
            WeaponStyle = WeaponStyles.OneHanded;
            WeaponType = WeaponTypes.Axe;
            DamageType = DamageTypes.Cut;
            EquipmentLocation = EquipmentLocations.EitherHand;
            PhysicalDamage = 6;
            HitChance = 25;
        }
    }
}