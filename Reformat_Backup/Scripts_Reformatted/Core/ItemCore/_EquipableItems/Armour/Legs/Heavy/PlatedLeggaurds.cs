﻿// <auto-generated/>
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PlatedLeggaurds : Armour
    {
        public PlatedLeggaurds()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.HeavyLegsArmour;
            Name = "Plated Legguards";
            Description = "A pair of plated legguards.";
            ArmourStyle = ArmourStyles.Heavy;
            EquipmentLocation = EquipmentLocations.Legs;
            DefenceValue = 8;
            DefenceChance = 5;
            SpeedModifier = -3;
        }
    }
}