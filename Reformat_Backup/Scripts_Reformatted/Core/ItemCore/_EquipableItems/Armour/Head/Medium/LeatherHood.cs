﻿// <auto-generated/>
using Rogue2018.Interfaces;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class LeatherHood : Armour
    {
        public LeatherHood()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumHeadArmour;
            Name = "Leather Hood";
            Description = "A leather hood.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.Head;
            DefenceValue = 4;
            DefenceChance = 3;
            SpeedModifier = -2;
        }
    }
}