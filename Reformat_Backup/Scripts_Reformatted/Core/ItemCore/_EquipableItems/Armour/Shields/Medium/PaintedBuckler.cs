﻿// <auto-generated/>
using Rogue2018.Interfaces;
using Rogue2018.Core.ItemCore.Implicits;

namespace Rogue2018.Core.ItemCore
{
    [System.Serializable]
    public class PaintedBuckler : Armour
    {
        public PaintedBuckler()
        {
            EquipableItemBaseType = EquipableItemBaseTypes.MediumShield;
            Name = "Painted Buckler";
            Description = "A painted buckler.";
            ArmourStyle = ArmourStyles.Medium;
            EquipmentLocation = EquipmentLocations.OffHand;
            DefenceValue = 12;
            DefenceChance = 10;
            SpeedModifier = -5;
            Implicit = new SpeedImplicit();
        }
    }
}