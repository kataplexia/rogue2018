﻿//-----------------------------------------------------------------------
// <copyright file="ItemCollection.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class that defines a container that is responsible for holding and returning a collection of items
    /// </summary>
    public class ItemCollection
    {
        #region Fields

        private List<IItem> allItems;

        private List<EquipableItem> rings;
        private List<EquipableItem> belts;
        private List<EquipableItem> quivers;
        private List<EquipableItem> foci;
        private List<EquipableItem> heavyChestArmours;
        private List<EquipableItem> mediumChestArmours;
        private List<EquipableItem> lightChestArmours;
        private List<EquipableItem> heavyFeetArmours;
        private List<EquipableItem> mediumFeetArmours;
        private List<EquipableItem> lightFeetArmours;
        private List<EquipableItem> heavyHandsArmours;
        private List<EquipableItem> mediumHandsArmours;
        private List<EquipableItem> lightHandsArmours;
        private List<EquipableItem> heavyHeadArmours;
        private List<EquipableItem> mediumHeadArmours;
        private List<EquipableItem> lightHeadArmours;
        private List<EquipableItem> heavyLegsArmours;
        private List<EquipableItem> mediumLegsArmours;
        private List<EquipableItem> lightLegsArmours;
        private List<EquipableItem> heavyShields;
        private List<EquipableItem> mediumShields;
        private List<EquipableItem> lightShields;
        private List<EquipableItem> oneHandedGenericSwords;
        private List<EquipableItem> oneHandedPierceSwords;
        private List<EquipableItem> twoHandedSwords;
        private List<EquipableItem> oneHandedAxes;
        private List<EquipableItem> twoHandedAxes;
        private List<EquipableItem> oneHandedMaces;
        private List<EquipableItem> twoHandedMaces;
        private List<EquipableItem> bows;
        private List<EquipableItem> daggers;
        private List<EquipableItem> sceptres;
        private List<EquipableItem> staves;
        private List<EquipableItem> wands;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemCollection"/> class
        /// </summary>
        public ItemCollection()
        {
            this.InitializeLists();

            this.AddRings();
            this.AddBelts();
            this.AddQuivers();
            this.AddFoci();
            this.AddChestArmours();
            this.AddFeetArmours();
            this.AddHandsArmours();
            this.AddHeadArmours();
            this.AddLegsArmours();
            this.AddShields();
            this.AddSwords();
            this.AddAxes();
            this.AddMaces();
            this.AddBows();
            this.AddDaggers();
            this.AddSceptres();
            this.AddStaves();
            this.AddWands();

            this.AddAllItems();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a random equipable item from the list of all items
        /// </summary>
        /// <returns>An <see cref="EquipableItem"/> selected randomly from the list of all items</returns>
        public EquipableItem GetRandomEquipableItem()
        {
            int randomIndex = Game.Singleton.Random.Next(0, this.allItems.Count - 1);
            if (!(this.allItems[randomIndex] is EquipableItem))
            {
                return this.GetRandomEquipableItem();
            }

            IItem returnItem = Game.Singleton.Tools.DeepCopy<IItem>(this.allItems[randomIndex]);
            EquipableItem returnEquipable = returnItem as EquipableItem;
            returnEquipable.Construct();
            return returnEquipable;
        }

        /// <summary>
        /// Initializes instances of the item lists
        /// </summary>
        private void InitializeLists()
        {
            this.rings = new List<EquipableItem>();
            this.belts = new List<EquipableItem>();
            this.quivers = new List<EquipableItem>();
            this.foci = new List<EquipableItem>();
            this.heavyChestArmours = new List<EquipableItem>();
            this.mediumChestArmours = new List<EquipableItem>();
            this.lightChestArmours = new List<EquipableItem>();
            this.heavyFeetArmours = new List<EquipableItem>();
            this.mediumFeetArmours = new List<EquipableItem>();
            this.lightFeetArmours = new List<EquipableItem>();
            this.heavyHandsArmours = new List<EquipableItem>();
            this.mediumHandsArmours = new List<EquipableItem>();
            this.lightHandsArmours = new List<EquipableItem>();
            this.heavyHeadArmours = new List<EquipableItem>();
            this.mediumHeadArmours = new List<EquipableItem>();
            this.lightHeadArmours = new List<EquipableItem>();
            this.heavyLegsArmours = new List<EquipableItem>();
            this.mediumLegsArmours = new List<EquipableItem>();
            this.lightLegsArmours = new List<EquipableItem>();
            this.heavyShields = new List<EquipableItem>();
            this.mediumShields = new List<EquipableItem>();
            this.lightShields = new List<EquipableItem>();
            this.oneHandedGenericSwords = new List<EquipableItem>();
            this.oneHandedPierceSwords = new List<EquipableItem>();
            this.twoHandedSwords = new List<EquipableItem>();
            this.oneHandedAxes = new List<EquipableItem>();
            this.twoHandedAxes = new List<EquipableItem>();
            this.oneHandedMaces = new List<EquipableItem>();
            this.twoHandedMaces = new List<EquipableItem>();
            this.bows = new List<EquipableItem>();
            this.daggers = new List<EquipableItem>();
            this.sceptres = new List<EquipableItem>();
            this.staves = new List<EquipableItem>();
            this.wands = new List<EquipableItem>();
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for rings
        /// </summary>
        private void AddRings()
        {
            this.rings.Add(new AgateRing());
            this.rings.Add(new RubyRing());
            this.rings.Add(new ThornedRing());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for belts
        /// </summary>
        private void AddBelts()
        {
            this.belts.Add(new ChainBelt());
            this.belts.Add(new LeatherBelt());
            this.belts.Add(new SilkThread());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for quivers
        /// </summary>
        private void AddQuivers()
        {
            this.quivers.Add(new BluntQuiver());
            this.quivers.Add(new SerratedQuiver());
            this.quivers.Add(new TwoPointQuiver());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for foci
        /// </summary>
        private void AddFoci()
        {
            this.foci.Add(new Censer());
            this.foci.Add(new Grimoire());
            this.foci.Add(new Tome());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for chest armour
        /// </summary>
        private void AddChestArmours()
        {
            this.heavyChestArmours.Add(new ChestPlate());
            this.heavyChestArmours.Add(new CopperPlate());
            this.heavyChestArmours.Add(new PlateVest());

            this.mediumChestArmours.Add(new PatchleatherTunic());
            this.mediumChestArmours.Add(new ShabbyJerkin());
            this.mediumChestArmours.Add(new StrappedLeather());

            this.lightChestArmours.Add(new ScholarsRobe());
            this.lightChestArmours.Add(new SilkVest());
            this.lightChestArmours.Add(new SimpleRobe());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for feet armour
        /// </summary>
        private void AddFeetArmours()
        {
            this.heavyFeetArmours.Add(new IronGreaves());
            this.heavyFeetArmours.Add(new PlatedGreaves());
            this.heavyFeetArmours.Add(new SteelGreaves());

            this.mediumFeetArmours.Add(new DeerskinBoots());
            this.mediumFeetArmours.Add(new PatchleatherBoots());
            this.mediumFeetArmours.Add(new RawhideBoots());

            this.lightFeetArmours.Add(new SilkSlippers());
            this.lightFeetArmours.Add(new StitchedSlippers());
            this.lightFeetArmours.Add(new VelvetSlippers());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for hand armour
        /// </summary>
        private void AddHandsArmours()
        {
            this.heavyHandsArmours.Add(new IronGauntlets());
            this.heavyHandsArmours.Add(new PlatedGauntlets());
            this.heavyHandsArmours.Add(new SteelGauntlets());

            this.mediumHandsArmours.Add(new DeerskinGloves());
            this.mediumHandsArmours.Add(new PatchleatherGloves());
            this.mediumHandsArmours.Add(new RawhideGloves());

            this.lightHandsArmours.Add(new SilkGloves());
            this.lightHandsArmours.Add(new StitchedGloves());
            this.lightHandsArmours.Add(new VelvetGloves());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for head armour
        /// </summary>
        private void AddHeadArmours()
        {
            this.heavyHeadArmours.Add(new BarbuteHelmet());
            this.heavyHeadArmours.Add(new ConeHelmet());
            this.heavyHeadArmours.Add(new IronHat());

            this.mediumHeadArmours.Add(new LeatherCap());
            this.mediumHeadArmours.Add(new LeatherHood());
            this.mediumHeadArmours.Add(new Tricorne());

            this.lightHeadArmours.Add(new IronCirclet());
            this.lightHeadArmours.Add(new TribalCirclet());
            this.lightHeadArmours.Add(new VineCirclet());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for leg armour
        /// </summary>
        private void AddLegsArmours()
        {
            this.heavyLegsArmours.Add(new IronLegguards());
            this.heavyLegsArmours.Add(new PlatedLeggaurds());
            this.heavyLegsArmours.Add(new SteelLeggaurds());

            this.mediumLegsArmours.Add(new DeerskinLeggings());
            this.mediumLegsArmours.Add(new PatchleatherLeggings());
            this.mediumLegsArmours.Add(new RawhideLeggings());

            this.lightLegsArmours.Add(new SilkPants());
            this.lightLegsArmours.Add(new StitchedPants());
            this.lightLegsArmours.Add(new VelvetPants());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for shields
        /// </summary>
        private void AddShields()
        {
            this.heavyShields.Add(new RawhideTowerShield());
            this.heavyShields.Add(new RustedTowerShield());
            this.heavyShields.Add(new SimpleTowerShield());

            this.mediumShields.Add(new PaintedBuckler());
            this.mediumShields.Add(new PineBuckler());
            this.mediumShields.Add(new RawhideBuckler());

            this.lightShields.Add(new BoneSoulShield());
            this.lightShields.Add(new TwigSoulShield());
            this.lightShields.Add(new YewSoulShield());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for swords
        /// </summary>
        private void AddSwords()
        {
            this.oneHandedGenericSwords.Add(new CopperSword());
            this.oneHandedGenericSwords.Add(new Cutlass());
            this.oneHandedGenericSwords.Add(new RustedSword());

            this.oneHandedPierceSwords.Add(new BoneRapier());
            this.oneHandedPierceSwords.Add(new DentedFoil());
            this.oneHandedPierceSwords.Add(new RustedSpike());

            this.twoHandedSwords.Add(new BastardSword());
            this.twoHandedSwords.Add(new LongSword());
            this.twoHandedSwords.Add(new RustedBlade());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for axes
        /// </summary>
        private void AddAxes()
        {
            this.oneHandedAxes.Add(new CopperHatchet());
            this.oneHandedAxes.Add(new FellingHatchet());
            this.oneHandedAxes.Add(new RustedHatchet());

            this.twoHandedAxes.Add(new CopperAxe());
            this.twoHandedAxes.Add(new SplittingAxe());
            this.twoHandedAxes.Add(new StoneAxe());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for maces
        /// </summary>
        private void AddMaces()
        {
            this.oneHandedMaces.Add(new SpikedClub());
            this.oneHandedMaces.Add(new TribalClub());
            this.oneHandedMaces.Add(new WoodenClub());

            this.twoHandedMaces.Add(new Mallet());
            this.twoHandedMaces.Add(new TribalMaul());
            this.twoHandedMaces.Add(new WoodenMaul());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for bows
        /// </summary>
        private void AddBows()
        {
            this.bows.Add(new CrudeBow());
            this.bows.Add(new ShortBow());
            this.bows.Add(new LongBow());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for daggers
        /// </summary>
        private void AddDaggers()
        {
            this.daggers.Add(new CarvingKnife());
            this.daggers.Add(new CrudeShank());
            this.daggers.Add(new SkinningKnife());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for sceptres
        /// </summary>
        private void AddSceptres()
        {
            this.sceptres.Add(new BronzeSceptre());
            this.sceptres.Add(new TribalSceptre());
            this.sceptres.Add(new WoodenSceptre());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for staves
        /// </summary>
        private void AddStaves()
        {
            this.staves.Add(new CarvedBranch());
            this.staves.Add(new LongStaff());
            this.staves.Add(new PrimitiveStaff());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for wands
        /// </summary>
        private void AddWands()
        {
            this.wands.Add(new CarvedWand());
            this.wands.Add(new QuartzWand());
            this.wands.Add(new TwigWand());
        }

        /// <summary>
        /// Fills the <see cref="List{T}(EquipableItem)"/> for all items
        /// </summary>
        private void AddAllItems()
        {
            this.allItems = new List<IItem>(
                this.rings.Count +
                this.belts.Count +
                this.quivers.Count +
                this.foci.Count +
                this.heavyChestArmours.Count +
                this.mediumChestArmours.Count +
                this.lightChestArmours.Count +
                this.heavyFeetArmours.Count +
                this.mediumFeetArmours.Count +
                this.lightFeetArmours.Count +
                this.heavyHandsArmours.Count +
                this.mediumHandsArmours.Count +
                this.lightHandsArmours.Count +
                this.heavyHeadArmours.Count +
                this.mediumHeadArmours.Count +
                this.lightHeadArmours.Count +
                this.heavyLegsArmours.Count +
                this.mediumLegsArmours.Count +
                this.lightLegsArmours.Count +
                this.heavyShields.Count +
                this.mediumShields.Count +
                this.lightShields.Count +
                this.oneHandedGenericSwords.Count +
                this.oneHandedPierceSwords.Count +
                this.twoHandedSwords.Count +
                this.oneHandedMaces.Count +
                this.twoHandedMaces.Count +
                this.bows.Count +
                this.daggers.Count +
                this.sceptres.Count +
                this.staves.Count +
                this.wands.Count);

            // Add equipable items to all items list
            this.allItems.AddRange(this.rings);
            this.allItems.AddRange(this.belts);
            this.allItems.AddRange(this.quivers);
            this.allItems.AddRange(this.foci);
            this.allItems.AddRange(this.heavyChestArmours);
            this.allItems.AddRange(this.mediumChestArmours);
            this.allItems.AddRange(this.lightChestArmours);
            this.allItems.AddRange(this.heavyFeetArmours);
            this.allItems.AddRange(this.mediumFeetArmours);
            this.allItems.AddRange(this.lightFeetArmours);
            this.allItems.AddRange(this.heavyHandsArmours);
            this.allItems.AddRange(this.mediumHandsArmours);
            this.allItems.AddRange(this.lightHandsArmours);
            this.allItems.AddRange(this.heavyHeadArmours);
            this.allItems.AddRange(this.mediumHeadArmours);
            this.allItems.AddRange(this.lightHeadArmours);
            this.allItems.AddRange(this.heavyLegsArmours);
            this.allItems.AddRange(this.mediumLegsArmours);
            this.allItems.AddRange(this.lightLegsArmours);
            this.allItems.AddRange(this.heavyShields);
            this.allItems.AddRange(this.mediumShields);
            this.allItems.AddRange(this.lightShields);
            this.allItems.AddRange(this.oneHandedGenericSwords);
            this.allItems.AddRange(this.oneHandedPierceSwords);
            this.allItems.AddRange(this.twoHandedSwords);
            this.allItems.AddRange(this.oneHandedAxes);
            this.allItems.AddRange(this.twoHandedAxes);
            this.allItems.AddRange(this.oneHandedMaces);
            this.allItems.AddRange(this.twoHandedMaces);
            this.allItems.AddRange(this.bows);
            this.allItems.AddRange(this.daggers);
            this.allItems.AddRange(this.sceptres);
            this.allItems.AddRange(this.staves);
            this.allItems.AddRange(this.wands);
        }

        #endregion
    }
}