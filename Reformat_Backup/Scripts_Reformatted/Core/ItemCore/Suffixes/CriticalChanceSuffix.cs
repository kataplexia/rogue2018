﻿//-----------------------------------------------------------------------
// <copyright file="CriticalChanceSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Critical chance suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class CriticalChanceSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CriticalChanceSuffix"/> class
        /// </summary>
        public CriticalChanceSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 6;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="CriticalChanceSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="CriticalChanceSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Critical Chance", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="CriticalChanceSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(20);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(44);
            this.TierMinimumItemLevels.Add(59);
            this.TierMinimumItemLevels.Add(73);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of Needling");
            this.TierNameStrings.Add("of Stinging");
            this.TierNameStrings.Add("of Piercing");
            this.TierNameStrings.Add("of Rupturing");
            this.TierNameStrings.Add("of Penetrating");
            this.TierNameStrings.Add("of Incision");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(10, 14));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(15, 19));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(20, 24));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(25, 29));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(30, 34));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(35, 38));
        }

        #endregion
    }
}