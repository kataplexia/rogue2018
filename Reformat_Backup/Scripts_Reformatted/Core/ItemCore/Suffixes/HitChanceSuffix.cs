﻿//-----------------------------------------------------------------------
// <copyright file="HitChanceSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Hit chance suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class HitChanceSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HitChanceSuffix"/> class
        /// </summary>
        public HitChanceSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 9;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="HitChanceSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="HitChanceSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Hit Chance", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="HitChanceSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(12);
            this.TierMinimumItemLevels.Add(20);
            this.TierMinimumItemLevels.Add(26);
            this.TierMinimumItemLevels.Add(33);
            this.TierMinimumItemLevels.Add(41);
            this.TierMinimumItemLevels.Add(50);
            this.TierMinimumItemLevels.Add(63);
            this.TierMinimumItemLevels.Add(76);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of Calm");
            this.TierNameStrings.Add("of Steadiness");
            this.TierNameStrings.Add("of Accuracy");
            this.TierNameStrings.Add("of Precision");
            this.TierNameStrings.Add("of the Sniper");
            this.TierNameStrings.Add("of the Marksman");
            this.TierNameStrings.Add("of the Deadeye");
            this.TierNameStrings.Add("of the Ranger");
            this.TierNameStrings.Add("of the Assassin");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(1, 4));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(5, 8));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(9, 12));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(13, 16));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(17, 20));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(21, 23));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(24, 26));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(27, 29));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(30, 32));
        }

        #endregion
    }
}