﻿//-----------------------------------------------------------------------
// <copyright file="ElementalResistanceSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Elemental resistance suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class ElementalResistanceSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementalResistanceSuffix"/> class
        /// </summary>
        public ElementalResistanceSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);

            this.MinimumItemLevel = 12;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 5;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="ElementalResistanceSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="ElementalResistanceSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Elemental Resistances", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="ElementalResistanceSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            // Setup tierMinimumItemLevels list
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(24);
            this.TierMinimumItemLevels.Add(36);
            this.TierMinimumItemLevels.Add(48);
            this.TierMinimumItemLevels.Add(60);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of the Crystal");
            this.TierNameStrings.Add("of the Prism");
            this.TierNameStrings.Add("of the Kaleidoscope");
            this.TierNameStrings.Add("of Variegation");
            this.TierNameStrings.Add("of the Rainbow");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(3, 5));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(6, 8));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(9, 11));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(12, 14));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(15, 16));
        }

        #endregion
    }
}