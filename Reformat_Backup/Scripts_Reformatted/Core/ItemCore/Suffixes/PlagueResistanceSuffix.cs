﻿//-----------------------------------------------------------------------
// <copyright file="PlagueResistanceSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Plague resistance suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class PlagueResistanceSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PlagueResistanceSuffix"/> class
        /// </summary>
        public PlagueResistanceSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 6;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="PlagueResistanceSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="PlagueResistanceSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Plague Resistance", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="PlagueResistanceSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(44);
            this.TierMinimumItemLevels.Add(56);
            this.TierMinimumItemLevels.Add(65);
            this.TierMinimumItemLevels.Add(81);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of the Lost");
            this.TierNameStrings.Add("of Banishment");
            this.TierNameStrings.Add("of Eviction");
            this.TierNameStrings.Add("of Expulsion");
            this.TierNameStrings.Add("of Exile");
            this.TierNameStrings.Add("of Bameth");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(5, 10));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(11, 15));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(16, 20));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(21, 25));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(26, 30));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(31, 35));
        }

        #endregion
    }
}