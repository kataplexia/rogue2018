﻿//-----------------------------------------------------------------------
// <copyright file="SpellCriticalChanceSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Spell critical chance suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class SpellCriticalChanceSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SpellCriticalChanceSuffix"/> class
        /// </summary>
        public SpellCriticalChanceSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 11;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 6;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="SpellCriticalChanceSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="SpellCriticalChanceSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Critical Chance for Spells", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="SpellCriticalChanceSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(21);
            this.TierMinimumItemLevels.Add(28);
            this.TierMinimumItemLevels.Add(41);
            this.TierMinimumItemLevels.Add(59);
            this.TierMinimumItemLevels.Add(76);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of Menace");
            this.TierNameStrings.Add("of Havoc");
            this.TierNameStrings.Add("of Disaster");
            this.TierNameStrings.Add("of Calamity");
            this.TierNameStrings.Add("of Ruin");
            this.TierNameStrings.Add("of Unmaking");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(10, 19));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(20, 39));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(40, 59));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(60, 79));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(80, 99));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(100, 109));
        }

        #endregion
    }
}