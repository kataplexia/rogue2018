﻿//-----------------------------------------------------------------------
// <copyright file="AwarenessSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Awareness suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class AwarenessSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AwarenessSuffix"/> class
        /// </summary>
        public AwarenessSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);

            this.MinimumItemLevel = 8;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 3;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="AwarenessSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="AwarenessSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1} to Awareness", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="AwarenessSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(15);
            this.TierMinimumItemLevels.Add(30);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of Shining");
            this.TierNameStrings.Add("of Light");
            this.TierNameStrings.Add("of Radiance");

            this.TierEffectValues.Add(5);
            this.TierEffectValues.Add(10);
            this.TierEffectValues.Add(15);
        }

        #endregion
    }
}
