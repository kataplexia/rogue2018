﻿//-----------------------------------------------------------------------
// <copyright file="LightningResistanceSuffix.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore.Suffixes
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;

    /// <summary>
    /// A class defining a type of <see cref="Suffix"/> (Lightning resistance suffix) with all of it's associated properties
    /// </summary>
    [System.Serializable]
    public class LightningResistanceSuffix : Suffix
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LightningResistanceSuffix"/> class
        /// </summary>
        public LightningResistanceSuffix()
        {
            this.SuffixBaseTypes = new List<EquipableItemBaseTypes>();
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordGeneric);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.OneHandedSwordPierce);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedAxe);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedMace);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.TwoHandedSword);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Bow);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Dagger);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Sceptre);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Staff);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyChestArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyFeetArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyLegsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumChestArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumFeetArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumLegsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightChestArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightFeetArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHandsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightHeadArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightLegsArmour);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.HeavyShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.MediumShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.LightShield);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Belt);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Ring);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Quiver);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Wand);
            this.SuffixBaseTypes.Add(EquipableItemBaseTypes.Focus);

            this.MinimumItemLevel = 1;
            this.MaximumItemLevel = 100;
            this.NumberOfTiers = 8;

            this.Construct(this.MinimumItemLevel);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies the <see cref="LightningResistanceSuffix"/>'s effect(s)
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ApplyEffect()
        {
            //// TODO
        }

        /// <summary>
        /// Constructs the <see cref="LightningResistanceSuffix"/>
        /// </summary>
        /// <param name="itemLevel">An <see cref="int"/> defining the item level to be used when constructing the suffix</param>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void Construct(int itemLevel)
        {
            this.SetupLists();
            this.ConstructTiers();
            this.SetupNameAndValue(itemLevel);
            this.EffectInfoString = string.Format("{0} +{1}% to Lightning Resistance", this.TierInfoString, this.EffectValue);
        }

        /// <summary>
        /// Constructs the <see cref="LightningResistanceSuffix"/>'s tier data
        /// </summary>
        /// <remarks>
        /// Overrides base abstract method
        /// </remarks>
        public override void ConstructTiers()
        {
            this.TierMinimumItemLevels.Add(this.MinimumItemLevel);
            this.TierMinimumItemLevels.Add(12);
            this.TierMinimumItemLevels.Add(24);
            this.TierMinimumItemLevels.Add(36);
            this.TierMinimumItemLevels.Add(48);
            this.TierMinimumItemLevels.Add(60);
            this.TierMinimumItemLevels.Add(72);
            this.TierMinimumItemLevels.Add(84);
            this.TierMinimumItemLevels.Add(this.MaximumItemLevel + 1);

            this.TierNameStrings.Add("of the Cloud");
            this.TierNameStrings.Add("of the Squall");
            this.TierNameStrings.Add("of the Storm");
            this.TierNameStrings.Add("of the Thunderhead");
            this.TierNameStrings.Add("of the Tempest");
            this.TierNameStrings.Add("of the Maelstrom");
            this.TierNameStrings.Add("of the Lightning");
            this.TierNameStrings.Add("of Ephij");

            this.TierEffectValues.Add(Game.Singleton.Random.Next(6, 11));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(12, 17));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(18, 23));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(24, 29));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(30, 35));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(36, 41));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(42, 45));
            this.TierEffectValues.Add(Game.Singleton.Random.Next(46, 48));
        }

        #endregion
    }
}