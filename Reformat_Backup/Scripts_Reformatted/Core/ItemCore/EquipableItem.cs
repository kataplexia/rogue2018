﻿//-----------------------------------------------------------------------
// <copyright file="EquipableItem.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core.ItemCore
{
    using System.Collections.Generic;
    using Rogue2018.Interfaces;
    using UnityEngine;

    /// <summary>
    /// A class defining a type of <see cref="EquipableItem"/> with all of its associated properties
    /// </summary>
    /// <remarks>
    /// Interfaces <see cref="IItem"/> and <see cref="IEquipable"/>
    /// </remarks>
    [System.Serializable]
    public abstract class EquipableItem : IItem, IEquipable
    {
        #region Fields

        private string description;
        private EquipableItemBaseTypes equipableItemBaseType;
        private EquipmentLocations equipmentLocation;
        private string equipmentLocationString;
        private bool equipped;
        private bool equippedLeftRing;
        private bool equippedMainHand;
        private bool equippedOffHand;
        private bool equippedRightRing;
        private Implicit itemImplicit;
        private int itemLevel;
        private List<Prefix> itemPrefixes;
        private ItemQualities itemQuality;
        private List<Suffix> itemSuffixes;
        private string name;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipableItem"/> class
        /// </summary>
        public EquipableItem()
        {
            this.Equipped = false;
            this.EquippedLeftRing = false;
            this.EquippedMainHand = false;
            this.EquippedOffHand = false;
            this.EquippedRightRing = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="Description"/> property
        /// </summary>
        public string Description
        {
            get { return this.description; } set { this.description = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref=""/> property
        /// </summary>
        public EquipableItemBaseTypes EquipableItemBaseType
        {
            get { return this.equipableItemBaseType; } set { this.equipableItemBaseType = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipmentLocation"/> property
        /// </summary>
        public EquipmentLocations EquipmentLocation
        {
            get { return this.equipmentLocation; } set { this.equipmentLocation = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipmentLocationString"/> property
        /// </summary>
        public string EquipmentLocationString
        {
            get { return this.equipmentLocationString; } set { this.equipmentLocationString = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="Equipped"/> is True or False
        /// </summary>
        public bool Equipped
        {
            get { return this.equipped; } set { this.equipped = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="EquippedLeftRing"/> is True or False
        /// </summary>
        public bool EquippedLeftRing
        {
            get { return this.equippedLeftRing; } set { this.equippedLeftRing = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="EquippedMainHand"/> is True or False
        /// </summary>
        public bool EquippedMainHand
        {
            get { return this.equippedMainHand; } set { this.equippedMainHand = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="EquippedOffHand"/> is True or False
        /// </summary>
        public bool EquippedOffHand
        {
            get { return this.equippedOffHand; } set { this.equippedOffHand = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="EquippedRightRing"/> is True or False
        /// </summary>
        public bool EquippedRightRing
        {
            get { return this.equippedRightRing; } set { this.equippedRightRing = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Implicit"/> property
        /// </summary>
        public Implicit Implicit
        {
            get { return this.itemImplicit; } set { this.itemImplicit = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ItemLevel"/> property
        /// </summary>
        public int ItemLevel
        {
            get { return this.itemLevel; } set { this.itemLevel = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="ItemQuality"/> property
        /// </summary>
        public ItemQualities ItemQuality
        {
            get { return this.itemQuality; } set { this.itemQuality = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Name"/> property
        /// </summary>
        public string Name
        {
            get { return this.name; } set { this.name = value; }
        }

        /// <summary>
        /// Gets or privately sets the <see cref="PrefixCount"/> property
        /// </summary>
        public int PrefixCount
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the <see cref="Prefixes"/> property
        /// </summary>
        public List<Prefix> Prefixes
        {
            get { return this.itemPrefixes; } set { this.itemPrefixes = value; }
        }

        /// <summary>
        /// Gets or privately sets the <see cref="SuffixCount"/> property
        /// </summary>
        public int SuffixCount
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the <see cref="Suffixes"/> property
        /// </summary>
        public List<Suffix> Suffixes
        {
            get { return this.itemSuffixes; } set { this.itemSuffixes = value; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Adds <see cref="Prefix"/>(es) to the <see cref="EquipableItem"/>
        /// </summary>
        /// <param name="prefixCount">An <see cref="int"/> defining the number of <see cref="Prefix"/>(es) to add</param>
        public void AddPrefixes(int prefixCount)
        {
            if (prefixCount == 0)
            {
                return;
            }

            for (int i = 0; i < prefixCount; i++)
            {
                this.Prefixes.Add(Game.Singleton.ItemPrefixes.GetRandomPrefix(this.Prefixes, this.ItemLevel, this.EquipableItemBaseType));
            }
        }

        /// <summary>
        /// Adds <see cref="Suffix"/>(es) to the <see cref="EquipableItem"/>
        /// </summary>
        /// <param name="suffixCount">An <see cref="int"/> defining the number of <see cref="Suffix"/>(es) to add</param>
        public void AddSuffixes(int suffixCount)
        {
            if (suffixCount == 0)
            {
                return;
            }

            for (int i = 0; i < suffixCount; i++)
            {
                this.Suffixes.Add(Game.Singleton.ItemSuffixes.GetRandomSuffix(this.Suffixes, this.ItemLevel, this.EquipableItemBaseType));
            }
        }

        /// <summary>
        /// Constructs the <see cref="EquipableItem"/>
        /// </summary>
        /// <remarks>
        /// Abstract method for derivatives to override
        /// </remarks>
        public abstract void Construct();

        /// <summary>
        /// Draws the <see cref="EquipableItem"/>'s <see cref="Name"/> using the specified offsets and color
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        /// <param name="drawColor">A <see cref="Color"/> defining the foreground color used for drawing</param>
        public void DrawName(int xOffset, int yOffset, Color drawColor)
        {
            Color backgroundDrawColor = Game.Singleton.Tools.GetPhiColorAt(xOffset, yOffset, true, 0);
            Game.Singleton.Tools.DrawString(xOffset, yOffset, this.Name, drawColor, backgroundDrawColor, 0);
        }

        /// <summary>
        /// Drop this <see cref="IItem"/>
        /// </summary>
        /// <param name="dropAll">A <see cref="bool"/> defining whether or not to drop the whole stack</param>
        public void Drop(bool dropAll = false)
        {
            if (this.Equipped)
            {
                this.RemoveItemAtEquipmentLocation(this.EquipmentLocation);
            }
            else
            {
                Game.Singleton.Player.InventoryList.Remove(this);
            }

            //// TODO: Place item inside list for current cell's contents

            string droppedMessage = string.Format("You dropped {0} on the ground", this.Name);
            Game.Singleton.MessageSystem.Add(droppedMessage, Color.cyan, false);
            Game.Singleton.SetDrawRequired(true);
        }

        /// <summary>
        /// Equip this <see cref="EquipableItem"/>
        /// </summary>
        public void Equip()
        {
            string checkString = this.CheckFreeSlotToEquipString(this.EquipmentLocation);
            if (checkString.Contains("False: "))
            {
                string cannotEquipMessage = checkString.Substring(7);
                Game.Singleton.MessageSystem.Add(cannotEquipMessage, Color.cyan, false);
                Game.Singleton.SetDrawRequired(true);
                return;
            }

            switch (this.EquipmentLocation)
            {
                case EquipmentLocations.Chest:

                    Game.Singleton.Player.EquipmentChest = this;

                    break;

                case EquipmentLocations.EitherHand:

                    if (Game.Singleton.Player.EquipmentMainHand == null)
                    {
                        Game.Singleton.Player.EquipmentMainHand = this;
                        this.EquippedMainHand = true;
                    }
                    else if (Game.Singleton.Player.EquipmentOffHand == null)
                    {
                        Game.Singleton.Player.EquipmentOffHand = this;
                        this.EquippedOffHand = true;
                    }

                    break;

                case EquipmentLocations.Feet:

                    Game.Singleton.Player.EquipmentFeet = this;

                    break;

                case EquipmentLocations.Hands:

                    Game.Singleton.Player.EquipmentHands = this;

                    break;

                case EquipmentLocations.Head:

                    Game.Singleton.Player.EquipmentHead = this;

                    break;

                case EquipmentLocations.Legs:

                    Game.Singleton.Player.EquipmentLegs = this;

                    break;

                case EquipmentLocations.MainHand:

                    Game.Singleton.Player.EquipmentMainHand = this;

                    break;

                case EquipmentLocations.OffHand:

                    Game.Singleton.Player.EquipmentOffHand = this;

                    break;

                case EquipmentLocations.Ring:

                    if (Game.Singleton.Player.EquipmentLeftRing == null)
                    {
                        Game.Singleton.Player.EquipmentLeftRing = this;
                        this.EquippedLeftRing = true;
                    }
                    else if (Game.Singleton.Player.EquipmentRightRing == null)
                    {
                        Game.Singleton.Player.EquipmentRightRing = this;
                        this.EquippedRightRing = true;
                    }

                    break;

                case EquipmentLocations.Waist:

                    Game.Singleton.Player.EquipmentWaist = this;

                    break;
            }

            this.Equipped = true;

            Game.Singleton.Player.InventoryList.Remove(this);

            string equippedMessage = string.Format("You equipped {0}", this.Name);
            Game.Singleton.MessageSystem.Add(equippedMessage, Color.cyan, false);
            Game.Singleton.SetDrawRequired(true);
        }

        /// <summary>
        /// Changes <see cref="Name"/> using the specified prefix count and suffix count
        /// </summary>
        /// <param name="prefixCount">An <see cref="int"/> defining the number of <see cref="Prefix"/>(es) on the <see cref="EquipableItem"/></param>
        /// <param name="suffixCount">An <see cref="int"/> defining the number of <see cref="Suffix"/>(es) on the <see cref="EquipableItem"/></param>
        public void RenameItem(int prefixCount, int suffixCount)
        {
            if (this.ItemQuality == ItemQualities.Magic)
            {
                if (prefixCount > 0 && suffixCount == 0)
                {
                    string preName = this.Prefixes[0].PrefixNameString;
                    this.Name = string.Format("{0} {1}", preName, this.Name);
                }
                else if (suffixCount > 0 && prefixCount == 0)
                {
                    string postName = this.Suffixes[0].SuffixNameString;
                    this.Name = string.Format("{0} {1}", this.Name, postName);
                }
                else if (suffixCount > 0 && prefixCount > 0)
                {
                    string preName = this.Prefixes[0].PrefixNameString;
                    string postName = this.Suffixes[0].SuffixNameString;
                    this.Name = string.Format("{0} {1} {2}", preName, this.Name, postName);
                }
            }
            else if (this.ItemQuality == ItemQualities.Rare)
            {
                string randomName = Game.Singleton.ItemNameGenerator.CreateRandomName(this);
                this.Name = string.Format("{0} ({1})", randomName, this.Name);
            }
        }

        /// <summary>
        /// Returns a string defining the specified <see cref="AccessoryTypes"/> value
        /// </summary>
        /// <param name="accessoryType">The <see cref="AccessoryTypes"/> value to be defined as a string</param>
        /// <returns>A <see cref="string"/> defining the specified <see cref="AccessoryTypes"/> value</returns>
        public string SetupAccessoryTypeString(AccessoryTypes accessoryType)
        {
            switch (accessoryType)
            {
                case AccessoryTypes.Belt:

                    return "Belt";

                case AccessoryTypes.Ring:

                    return "Ring";

                case AccessoryTypes.Quiver:

                    return "Quiver";

                case AccessoryTypes.None:
                default:

                    return string.Empty;
            }
        }

        /// <summary>
        /// Setup for the <see cref="EquipableItem"/>'s <see cref="Prefix"/>(es) and <see cref="Suffix"/>(es)
        /// </summary>
        public void SetupAffixes()
        {
            this.PrefixCount = 0;
            this.SuffixCount = 0;

            if (this.ItemQuality == ItemQualities.Magic)
            {
                int affixCount = Game.Singleton.Random.Next(1, 2);
                if (affixCount == 1)
                {
                    int prefixOrSuffix = Game.Singleton.Random.Next(0, 1);
                    if (prefixOrSuffix == 0)
                    {
                        this.PrefixCount = 1;
                    }
                    else
                    {
                        this.SuffixCount = 1;
                    }
                }
                else if (affixCount == 2)
                {
                    this.PrefixCount = 1;
                    this.SuffixCount = 1;
                }
            }
            else if (this.ItemQuality == ItemQualities.Rare)
            {
                int prefixOrSuffix = Game.Singleton.Random.Next(0, 1);
                if (prefixOrSuffix == 0)
                {
                    this.PrefixCount = 2 + Game.Singleton.Random.Next(0, 1);
                    this.SuffixCount = 1 + Game.Singleton.Random.Next(0, 2);
                }
                else
                {
                    this.PrefixCount = 1 + Game.Singleton.Random.Next(0, 2);
                    this.SuffixCount = 2 + Game.Singleton.Random.Next(0, 1);
                }
            }

            this.Prefixes = new List<Prefix>(this.PrefixCount);
            this.Suffixes = new List<Suffix>(this.SuffixCount);

            this.AddPrefixes(this.PrefixCount);
            this.AddSuffixes(this.SuffixCount);

            this.RenameItem(this.PrefixCount, this.SuffixCount);
        }

        /// <summary>
        /// Returns a string defining the specified <see cref="ArmourStyles"/> value
        /// </summary>
        /// <param name="armourStyle">The <see cref="ArmourStyles"/> value to be defined as a string</param>
        /// <returns>A <see cref="string"/> defining the specified <see cref="ArmourStyles"/> value</returns>
        public string SetupArmourStyleString(ArmourStyles armourStyle)
        {
            switch (armourStyle)
            {
                case ArmourStyles.Heavy:

                    return "Heavy";

                case ArmourStyles.Light:

                    return "Light";

                case ArmourStyles.Medium:

                    return "Medium";

                case ArmourStyles.None:
                default:

                    return string.Empty;
            }
        }

        /// <summary>
        /// Returns a string defining the specified <see cref="DamageTypes"/> value
        /// </summary>
        /// <param name="damageType">The <see cref="DamageTypes"/> value to be defined as a string</param>
        /// <returns>A <see cref="string"/> defining the specified <see cref="DamageTypes"/> value</returns>
        public string SetupDamageTypeString(DamageTypes damageType)
        {
            switch (damageType)
            {
                case DamageTypes.Blunt:

                    return "Blunt";

                case DamageTypes.Cut:

                    return "Cut";

                case DamageTypes.Pierce:

                    return "Pierce";

                case DamageTypes.None:
                default:

                    return string.Empty;
            }
        }

        /// <summary>
        /// Returns a string defining the specified <see cref="EquipmentLocations"/> value
        /// </summary>
        /// <param name="equipmentLocation">The <see cref="EquipmentLocations"/> value to be defined as a string</param>
        /// <returns>A <see cref="string"/> defining the specified <see cref="EquipmentLocations"/> value</returns>
        public string SetupEquipmentLocationString(EquipmentLocations equipmentLocation)
        {
            switch (equipmentLocation)
            {
                case EquipmentLocations.Chest:

                    return "Chest";

                case EquipmentLocations.EitherHand:

                    return "Either Hand";

                case EquipmentLocations.Feet:

                    return "Feet";

                case EquipmentLocations.Hands:

                    return "Hands";

                case EquipmentLocations.Head:

                    return "Head";

                case EquipmentLocations.Legs:

                    return "Legs";

                case EquipmentLocations.MainHand:

                    return "Main Hand";

                case EquipmentLocations.OffHand:

                    return "Off Hand";

                case EquipmentLocations.Ring:

                    return "Ring";

                default:

                    return string.Empty;
            }
        }

        /// <summary>
        /// Returns a string defining the specified <see cref="WeaponStyles"/> value
        /// </summary>
        /// <param name="weaponStyle">The <see cref="WeaponStyles"/> value to be defined as a string</param>
        /// <returns>A <see cref="string"/> defining the specified <see cref="WeaponStyles"/> value</returns>
        public string SetupWeaponStyleString(WeaponStyles weaponStyle)
        {
            switch (weaponStyle)
            {
                case WeaponStyles.OneHanded:

                    return "One Handed";

                case WeaponStyles.TwoHanded:

                    return "Two Handed";

                case WeaponStyles.Ranged:

                    return "Ranged";

                case WeaponStyles.None:
                default:

                    return string.Empty;
            }
        }

        /// <summary>
        /// Returns a string defining the specified <see cref="WeaponTypes"/> value
        /// </summary>
        /// <param name="weaponType">The <see cref="WeaponTypes"/> value to be defined as a string</param>
        /// <returns>A <see cref="string"/> defining the specified <see cref="WeaponTypes"/> value</returns>
        public string SetupWeaponTypeString(WeaponTypes weaponType)
        {
            switch (weaponType)
            {
                case WeaponTypes.Axe:

                    return "Axe";

                case WeaponTypes.Bow:

                    return "Bow";

                case WeaponTypes.Mace:

                    return "Mace";

                case WeaponTypes.Staff:

                    return "Staff";

                case WeaponTypes.Sword:

                    return "Sword";

                case WeaponTypes.Dagger:

                    return "Dagger";

                case WeaponTypes.Sceptre:

                    return "Sceptre";

                case WeaponTypes.Wand:

                    return "Wand";

                default:

                    return string.Empty;
            }
        }

        /// <summary>
        /// Unequip this <see cref="EquipableItem"/>
        /// </summary>
        public void Unequip()
        {
            if (!Game.Singleton.Player.CheckInventoryCapacity())
            {
                return;
            }

            this.RemoveItemAtEquipmentLocation(this.EquipmentLocation);
            Game.Singleton.Player.AddItemToInventory(this as IItem);

            string unequippedMessage = string.Format("You unequipped {0}", this.Name);
            Game.Singleton.MessageSystem.Add(unequippedMessage, Color.cyan, false);
            Game.Singleton.SetDrawRequired(true);
        }

        /// <summary>
        /// Checks for a suitable location for equipping using the specified <see cref="EquipmentLocation"/>
        /// </summary>
        /// <param name="equipmentLocation">An <see cref="EquipmentLocation"/> defining the target location for equipping</param>
        /// <returns>A <see cref="string"/> defining whether or not an equipment slot was found. Y for True, and N/Message for False</returns>
        private string CheckFreeSlotToEquipString(EquipmentLocations equipmentLocation)
        {
            switch (equipmentLocation)
            {
                case EquipmentLocations.Chest:

                    if (Game.Singleton.Player.EquipmentChest == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.EitherHand:

                    if (Game.Singleton.Player.EquipmentMainHand == null &&
                        Game.Singleton.Player.EquipmentOffHand != null)
                    {
                        IEquipable offHandEquipable = Game.Singleton.Player.EquipmentOffHand as IEquipable;
                        if (offHandEquipable is IAccessory)
                        {
                            IAccessory offHandAccessory = offHandEquipable as IAccessory;
                            if (offHandAccessory.AccessoryType == AccessoryTypes.Quiver)
                            {
                                return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", this.Name);
                            }

                            return "True";
                        }

                        return "True";
                    }
                    else if (Game.Singleton.Player.EquipmentMainHand != null &&
                             Game.Singleton.Player.EquipmentOffHand == null)
                    {
                        IWeapon mainHandWeapon = Game.Singleton.Player.EquipmentMainHand as IWeapon;
                        if (mainHandWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                        {
                            return string.Format("False: Cannot equip {0}. Main Hand equipment is Two Handed", Name);
                        }
                        else
                        {
                            if (mainHandWeapon.WeaponStyle == WeaponStyles.Ranged)
                            {
                                return string.Format("False: Cannot equip {0}. Main Hand equipment is of an incompatible type", Name);
                            }

                            return "True";
                        }
                    }
                    else if (Game.Singleton.Player.EquipmentMainHand != null &&
                             Game.Singleton.Player.EquipmentOffHand != null)
                    {
                        return string.Format("False: Cannot equip {0}. No equipment slot available", Name);
                    }

                    return "True";

                case EquipmentLocations.Feet:

                    if (Game.Singleton.Player.EquipmentFeet == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Hands:

                    if (Game.Singleton.Player.EquipmentHands == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Head:

                    if (Game.Singleton.Player.EquipmentHead == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Legs:

                    if (Game.Singleton.Player.EquipmentLegs == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.MainHand:

                    if (Game.Singleton.Player.EquipmentMainHand == null)
                    {
                        IWeapon thisWeapon = this as IWeapon;
                        if (thisWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                        {
                            if (Game.Singleton.Player.EquipmentOffHand != null)
                            {
                                return string.Format("False: Cannot equip {0}. No equipment slot available", this.Name);
                            }

                            return "True";
                        }
                        else if (thisWeapon.WeaponStyle == WeaponStyles.Ranged)
                        {
                            if (Game.Singleton.Player.EquipmentOffHand != null)
                            {
                                IEquipable offHandEquipable = Game.Singleton.Player.EquipmentOffHand as IEquipable;
                                if (thisWeapon.WeaponType == WeaponTypes.Bow)
                                {
                                    if (offHandEquipable is IAccessory)
                                    {
                                        IAccessory offHandAccessory = offHandEquipable as IAccessory;
                                        if (offHandAccessory.AccessoryType == AccessoryTypes.Focus)
                                        {
                                            return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", this.Name);
                                        }

                                        return "True";
                                    }

                                    return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", this.Name);
                                }
                                else if (thisWeapon.WeaponType == WeaponTypes.Wand)
                                {
                                    if (offHandEquipable is IAccessory)
                                    {
                                        IAccessory offHandAccessory = offHandEquipable as IAccessory;
                                        if (offHandAccessory.AccessoryType == AccessoryTypes.Quiver)
                                        {
                                            return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", this.Name);
                                        }

                                        return "True";
                                    }

                                    return "True";
                                }
                                else if (offHandEquipable is IWeapon)
                                {
                                    return string.Format("False: Cannot equip {0}. Off Hand equipment is of an incompatible type", this.Name);
                                }

                                return "True";
                            }

                            return "True";
                        }

                        return "True";
                    }

                    goto default;

                case EquipmentLocations.OffHand:

                    if (Game.Singleton.Player.EquipmentOffHand == null)
                    {
                        if (Game.Singleton.Player.EquipmentMainHand != null)
                        {
                            IWeapon mainHandWeapon = Game.Singleton.Player.EquipmentMainHand as IWeapon;
                            if (mainHandWeapon.WeaponStyle == WeaponStyles.TwoHanded)
                            {
                                return string.Format("False: Cannot equip {0}. Main Hand equipment is Two Handed", this.Name);
                            }

                            if (mainHandWeapon.WeaponType == WeaponTypes.Bow &&
                                this is IArmour)
                            {
                                return string.Format("False: Cannot equip {0}. Main Hand equipment is of an incompatible type", this.Name);
                            }
                            else if (mainHandWeapon.WeaponType != WeaponTypes.Bow &&
                                this is IAccessory)
                            {
                                IAccessory equipableAccessory = this as IAccessory;
                                if (equipableAccessory.AccessoryType == AccessoryTypes.Quiver)
                                {
                                    return string.Format("False: Cannot equip {0}. Main Hand equipment is of an incompatible type", this.Name);
                                }

                                return "True";
                            }

                            return "True";
                        }

                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Ring:

                    if (Game.Singleton.Player.EquipmentLeftRing == null ||
                        Game.Singleton.Player.EquipmentRightRing == null)
                    {
                        return "True";
                    }

                    goto default;

                case EquipmentLocations.Waist:

                    if (Game.Singleton.Player.EquipmentWaist == null)
                    {
                        return "True";
                    }

                    goto default;

                default:

                    return string.Format("False: Cannot equip {0}. No equipment slot available", this.Name);
            }
        }

        /// <summary>
        /// Removes an <see cref="EquipableItem"/> from the specified <see cref="EquipmentLocation"/>
        /// </summary>
        /// <param name="equipmentLocation">An <see cref="EquipmentLocation"/> to remove from</param>
        private void RemoveItemAtEquipmentLocation(EquipmentLocations equipmentLocation)
        {
            switch (equipmentLocation)
            {
                case EquipmentLocations.Chest:

                    Game.Singleton.Player.EquipmentChest = null;
                    this.Equipped = false;

                    break;

                case EquipmentLocations.EitherHand:

                    if (this.EquippedMainHand)
                    {
                        Game.Singleton.Player.EquipmentMainHand = null;
                        this.Equipped = false;
                        this.EquippedMainHand = false;
                    }
                    else if (this.EquippedOffHand)
                    {
                        Game.Singleton.Player.EquipmentOffHand = null;
                        this.Equipped = false;
                        this.EquippedOffHand = false;
                    }

                    break;

                case EquipmentLocations.Feet:

                    Game.Singleton.Player.EquipmentFeet = null;
                    this.Equipped = false;

                    break;

                case EquipmentLocations.Hands:

                    Game.Singleton.Player.EquipmentHands = null;
                    this.Equipped = false;

                    break;

                case EquipmentLocations.Head:

                    Game.Singleton.Player.EquipmentHead = null;
                    this.Equipped = false;

                    break;

                case EquipmentLocations.Legs:

                    Game.Singleton.Player.EquipmentLegs = null;
                    this.Equipped = false;

                    break;

                case EquipmentLocations.MainHand:

                    Game.Singleton.Player.EquipmentMainHand = null;
                    this.Equipped = false;
                    this.EquippedMainHand = false;

                    break;

                case EquipmentLocations.OffHand:

                    Game.Singleton.Player.EquipmentOffHand = null;
                    this.Equipped = false;
                    this.EquippedOffHand = false;

                    break;

                case EquipmentLocations.Ring:

                    if (this.EquippedLeftRing)
                    {
                        Game.Singleton.Player.EquipmentLeftRing = null;
                        this.Equipped = false;
                        this.EquippedLeftRing = false;
                    }
                    else if (this.EquippedRightRing)
                    {
                        Game.Singleton.Player.EquipmentRightRing = null;
                        this.Equipped = false;
                        this.EquippedRightRing = false;
                    }

                    break;

                case EquipmentLocations.Waist:

                    Game.Singleton.Player.EquipmentWaist = null;
                    this.Equipped = false;

                    break;
            }
        }

        #endregion
    }
}