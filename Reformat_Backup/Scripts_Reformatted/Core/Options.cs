﻿//-----------------------------------------------------------------------
// <copyright file="Options.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{
    public bool DrawTiles = true;

    // Awake with Game as singleton and do not destroy
    public static Options GET;
    private void Awake()
    {
        if (GET != null)
            GameObject.Destroy(GET);
        else
            GET = this;
    }
}
