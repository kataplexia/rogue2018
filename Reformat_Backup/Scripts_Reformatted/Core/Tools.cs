﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Rogue2018.Actors;

using PhiOS;

namespace Rogue2018.Core
{
    public class Tools
    {
        // Returns a new instance of the class type
        public T CreateNew<T>() where T : class, new()
        {
            return new T();
        }

        // Returns a deep copy of the class type passed in
        public T DeepCopy<T>(T other)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, other);
                memoryStream.Position = 0;
                return (T)binaryFormatter.Deserialize(memoryStream);
            }
        }

        // Method for setting a region of PhiCells mouse properties
        public void SetPhiCellRegionMouseProperties(int xOffset, int yOffset, int regionWidth, int regionHeight, bool isClickable, bool isHoverable, int layer = 0)
        {
            for (int x = 0; x < regionWidth; x++)
            {
                for (int y = 0; y < regionHeight; y++)
                    SetPhiCellMouseProperties(x + xOffset, y + yOffset, isClickable, isHoverable);
            }
        }

        // Method for setting a PhiCell's mouse properties
        public void SetPhiCellMouseProperties(int X, int Y, bool isClickable, bool isHoverable, int layer = 0)
        {
            PhiCell phiCell = PhiDisplay.PhiCellAt(layer, X, Y);
            if (isClickable)
                phiCell.clickAction = Game.Singleton.InputSystem;
            else
                phiCell.clickAction = null;
            if (isHoverable)
                phiCell.hoverAction = Game.Singleton.InputSystem;
            else
                phiCell.hoverAction = null;
        }

        // Method for drawing PhiCells using a properties list
        public void DrawCellsFromPropertiesList(PhiCellPropertiesList phiCellPropertiesList, bool useExistingBGDrawColor = false, float existingBGDrawColorModifier = 1.0f)
        {
            foreach (PhiCellProperties phiCellProperties in phiCellPropertiesList.propertiesList)
            {
                int X = phiCellProperties.X;
                int Y = phiCellProperties.Y;
                int layer = phiCellProperties.layer;
                string symbol = phiCellProperties.symbol.ToString();
                Color drawColor = phiCellProperties.drawColor;
                Color backgroundDrawColor = useExistingBGDrawColor
                    ? GetPhiColorAt(X, Y, true, layer) * existingBGDrawColorModifier
                    : phiCellProperties.backgroundDrawColor;
                DrawPhiCell(X, Y, symbol, drawColor, backgroundDrawColor, 0, 0, layer);
            }
        }

        // Method for drawing a PhiCell
        public void DrawPhiCell(int X, int Y, string symbol, Color fgPhiCellColor, Color bgPhiCellColor, int xOffset, int yOffset, int layer = 0)
        {
            PhiCell phiCell = PhiDisplay.PhiCellAt(layer, X + xOffset, Y);
            phiCell.SetContent(symbol, bgPhiCellColor, fgPhiCellColor);
        }

        // Method for drawing a string
        public void DrawString(int xOffset, int yOffset, string drawString, Color fgColor, Color bgColor, int layer = 0, bool useExistingBGDrawColor = false, float existingBGDrawColorModifier = 1.0f)
        {
            for (int x = 0; x < drawString.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset, yOffset);
                bgColor = useExistingBGDrawColor
                    ? GetPhiColorAt(x + xOffset, yOffset, true, layer) * existingBGDrawColorModifier
                    : bgColor;
                phiCell.SetContent(drawString.Substring(x, 1), bgColor, fgColor);
            }
        }

        // Method for drawing a string split into lines within a UIBox
        public void DrawInfoBox(
            int xOffset,
            int yOffset,
            int xMaxChars,
            string drawString,
            int propertyLineCount,
            int affixLineCount,
            int implicitLineCount,
            Color boxColor,
            Color fgColor,
            Color bgColor,
            string title,
            int layer = 0,
            string quitCellTag = "",
            bool setQuitCell = false)
        {
            // Setup default quality color
            Color qualityColor = Color.white;

            // Split draw string into an array of lines
            string[] seperatingChars = { "\n" };
            string[] linesArray = drawString.Split(seperatingChars, System.StringSplitOptions.None);

            // Create and add to a list of lines while shortening them
            List<string> linesList = new List<string>(linesArray.Length);
            foreach (string line in linesArray)
                AddLine(xMaxChars, line, linesList);

            // Check if final box height is off screen
            if (yOffset + linesList.Count + 4 > 86)
                yOffset = 86 - (linesList.Count + 4);

            // Draw the lines
            int lineIndex = 0;
            foreach (string line in linesList)
            {
                // Set color for quality line
                Color lineColor = fgColor;

                if (propertyLineCount > 0 && lineIndex == 0)
                {
                    qualityColor = GetQualityColor(line);
                    lineColor = qualityColor;
                }

                // Set color for properties lines
                if (lineIndex > 0 && lineIndex < propertyLineCount)
                    lineColor = Color.white;

                // Set color for implicit line
                if (lineIndex > propertyLineCount && lineIndex < propertyLineCount + implicitLineCount)
                    lineColor = Color.cyan;

                // Set color for affixes lines
                if (lineIndex > propertyLineCount + implicitLineCount && lineIndex < propertyLineCount + affixLineCount + implicitLineCount)
                    lineColor = qualityColor;

                DrawString(xOffset, lineIndex + yOffset + 1, line, lineColor, bgColor, layer);
                lineIndex++;
            }

            // Draw the UIBox around the info
            DrawUIBox(xMaxChars + 4, linesList.Count + 5, xOffset - 2, yOffset - 2, layer, bgColor, boxColor, qualityColor, title, quitCellTag, setQuitCell);
        }

        // Method for drawing a string split into lines within a UIBox
        public void DrawContextMenuBox(
            int xOffset,
            int yOffset,
            int xMaxChars,
            string drawString,
            Color boxColor,
            Color fgColor,
            Color bgColor,
            string title,
            Color titleColor,
            int layer = 0,
            string quitCellTag = "",
            bool setQuitCell = false)
        {
            // Split draw string into an array of lines
            string[] seperatingChars = { "\n" };
            string[] linesArray = drawString.Split(seperatingChars, System.StringSplitOptions.None);

            // Create and add to a list of lines while shortening them
            List<string> linesList = new List<string>(linesArray.Length);
            foreach (string line in linesArray)
                AddLine(xMaxChars, line, linesList);

            // Check if final box height is off screen
            if (yOffset + (linesList.Count * 2) + 3 > 86)
                yOffset = 86 - ((linesList.Count * 2) + 3);

            // Draw the backgrounds and borders
            Color bgBorderColor = Game.Singleton.Swatch.RexOptionBorder;
            for (int x = 0; x < xMaxChars + 2; x++)
            {
                for (int y = 0; y < linesList.Count + 1; y++)
                {
                    char character;
                    if (x == 0)
                        if (y == 0) character = '┌'; else if (y == linesList.Count) character = '└'; else character = '├';
                    else if (x == xMaxChars + 1)
                        if (y == 0) character = '┐'; else if (y == linesList.Count) character = '┘'; else character = '┤';
                    else if (x == 4)
                        if (y == 0) character = '┬'; else if (y == linesList.Count) character = '┴'; else character = '┼';
                    else character = '─';
                    if (x == 0 || x == 4 || x == xMaxChars + 1)
                    {
                        if (y < linesList.Count)
                            DrawString(xOffset - 1 + x, yOffset + (y * 2) + 1, "│", bgBorderColor, Color.black);
                    }
                    if (x > 4 && x < xMaxChars + 1)
                    {
                        if (y < linesList.Count)
                        {
                            Color optionBgColor;
                            if (y % 2 == 0)
                                optionBgColor = Game.Singleton.Swatch.RexOptionBackDark;
                            else
                                optionBgColor = Game.Singleton.Swatch.RexOptionBackDarkest;
                            DrawString(xOffset - 1 + x, yOffset + (y * 2) + 1, " ", Color.clear, optionBgColor);
                        }
                    }
                    DrawString(xOffset - 1 + x, yOffset + (y * 2), character.ToString(), bgBorderColor, Color.black);
                }
            }

            // Draw the lines
            int lineIndex = 0;
            foreach (string line in linesList)
            {
                DrawString(xOffset, yOffset + 1 + (lineIndex * 2), line.Substring(0, 2), Game.Singleton.Swatch.RexTextOption, bgColor, layer);
                DrawString(xOffset + 4, yOffset + 1 + (lineIndex * 2), line.Substring(2), Color.white, GetPhiColorAt(xOffset + 4, yOffset + 1 + (lineIndex * 2), true), layer);
                lineIndex++;
            }

            // Draw the UIBox around the options context menu
            DrawUIBox(xMaxChars + 4, (linesList.Count * 2) + 4, xOffset - 2, yOffset - 2, layer, bgColor, boxColor, titleColor, title, quitCellTag, setQuitCell);
        }

        // Method for drawing a UI box
        public void DrawUIBox(
            int boxWidth,
            int boxHeight,
            int xOffset,
            int yOffset,
            int layer,
            Color bgColor,
            Color fgColor,
            Color titleColor,
            string title = "",
            string quitCellTag = "",
            bool setQuitCell = false,
            bool useExistingBGDrawColor = false,
            float existingBGDrawColorModifier = 1.0f)
        {
            // Draw Top and Bottom
            for (int x = xOffset; x < boxWidth + xOffset; x++)
            {


                PhiCell phiCellTop = PhiDisplay.PhiCellAt(layer, x, yOffset);
                bgColor = useExistingBGDrawColor
                    ? GetPhiColorAt(x, yOffset, true, layer) * existingBGDrawColorModifier
                    : bgColor;
                if (x == xOffset)
                    phiCellTop.SetContent("┌", bgColor, fgColor);
                else if (x == boxWidth + xOffset - 1)
                    phiCellTop.SetContent("┐", bgColor, fgColor);
                else
                    phiCellTop.SetContent("─", bgColor, fgColor);

                PhiCell phiCellBottom = PhiDisplay.PhiCellAt(layer, x, boxHeight + yOffset - 1);
                bgColor = useExistingBGDrawColor
                    ? GetPhiColorAt(x, boxHeight + yOffset - 1, true, layer) * existingBGDrawColorModifier
                    : bgColor;
                if (x == xOffset)
                    phiCellBottom.SetContent("└", bgColor, fgColor);
                else if (x == boxWidth + xOffset - 1)
                    phiCellBottom.SetContent("┘", bgColor, fgColor);
                else
                    phiCellBottom.SetContent("─", bgColor, fgColor);
            }

            // Draw Sides
            for (int y = yOffset + 1; y < boxHeight + yOffset - 1; y++)
            {
                PhiCell phiCellLeft = PhiDisplay.PhiCellAt(layer, xOffset, y);
                bgColor = useExistingBGDrawColor
                    ? GetPhiColorAt(xOffset, y, true, layer) * existingBGDrawColorModifier
                    : bgColor;
                phiCellLeft.SetContent("│", bgColor, fgColor);

                PhiCell phiCellRight = PhiDisplay.PhiCellAt(layer, boxWidth + xOffset - 1, y);
                bgColor = useExistingBGDrawColor
                    ? GetPhiColorAt(boxWidth + xOffset - 1, y, true, layer) * existingBGDrawColorModifier
                    : bgColor;
                phiCellRight.SetContent("│", bgColor, fgColor);
            }

            // Draw Title
            if (title.Length > 0)
            {
                int titlePadding = 1;
                // Draw Top and Bottom of title box
                for (int x = titlePadding + xOffset - 1; x < titlePadding + title.Length + xOffset + 1; x++)
                {
                    PhiCell phiCellTop = PhiDisplay.PhiCellAt(layer, x, yOffset - 1);
                    if (x == titlePadding + xOffset - 1)
                        phiCellTop.SetContent("┌", bgColor, fgColor);
                    else if (x == titlePadding + title.Length + xOffset)
                        phiCellTop.SetContent("┐", bgColor, fgColor);
                    else
                        phiCellTop.SetContent("─", bgColor, fgColor);
                    PhiCell phiCellBottom = PhiDisplay.PhiCellAt(layer, x, yOffset + 1);
                    if (x == titlePadding + xOffset - 1)
                        phiCellBottom.SetContent("├", bgColor, fgColor);
                    else if (x == titlePadding + title.Length + xOffset)
                        phiCellBottom.SetContent("┘", bgColor, fgColor);
                    else
                        phiCellBottom.SetContent("─", bgColor, fgColor);
                }

                PhiCell phiCellLeft = PhiDisplay.PhiCellAt(layer, titlePadding + xOffset - 1, yOffset);
                phiCellLeft.SetContent("│", bgColor, fgColor);
                PhiCell phiCellRight = PhiDisplay.PhiCellAt(layer, titlePadding + title.Length + xOffset, yOffset);
                phiCellRight.SetContent("├", bgColor, fgColor);

                // Draw title
                for (int x = 0; x < title.Length; x++)
                {
                    PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + titlePadding + xOffset, yOffset);
                    phiCell.SetContent(title.Substring(x, 1), bgColor, titleColor);
                }

                // Draw quitbox
                DrawUIBox(3, 3, boxWidth + xOffset - 3, yOffset - 1, 0, bgColor, fgColor, titleColor);
                PhiCell phiCellQuitCenter = PhiDisplay.PhiCellAt(0, boxWidth + xOffset - 2, yOffset);
                phiCellQuitCenter.SetContent("X", bgColor, fgColor);
                if (setQuitCell)
                {
                    phiCellQuitCenter.SetTag(quitCellTag);
                    phiCellQuitCenter.clickAction = Game.Singleton.InputSystem;
                }

                PhiCell phiCellQuitBoxLeft = PhiDisplay.PhiCellAt(0, boxWidth + xOffset - 3, yOffset);
                phiCellQuitBoxLeft.SetContent("┤", bgColor, fgColor);

                PhiCell phiCellQuitBoxRight = PhiDisplay.PhiCellAt(0, boxWidth + xOffset - 1, yOffset + 1);
                phiCellQuitBoxRight.SetContent("┤", bgColor, fgColor);
            }
        }

        // Method for recursivley truncating a long line and adding the substring line to a list
        private void AddLine(int xMaxChars, string line, List<string> linesList)
        {
            if (line.Length > xMaxChars)
            {
                int charPos = xMaxChars;
                while (charPos > 0 && line[charPos] != ' ')
                    charPos--;
                linesList.Add(line.Substring(0, charPos));
                AddLine(xMaxChars, line.Substring(charPos + 1), linesList);
            }
            else
                linesList.Add(line);
        }

        // Returns a color given a string according to quality
        public Color GetQualityColor(string qualityString)
        {
            Swatch swatch = Game.Singleton.Swatch;
            if (qualityString.Contains("Normal"))
                return swatch.NormalQuality;
            else if (qualityString.Contains("Magic"))
                return swatch.MagicQuality;
            else if (qualityString.Contains("Rare"))
                return swatch.RareQuality;
            else if (qualityString.Contains("Unique"))
                return swatch.UniqueQuality;
            else
                return Color.white;
        }

        // Returns background or foreground color of PhiCell
        public Color GetPhiColorAt(int X, int Y, bool isBackgroundColor, int layer = 0)
        {
            PhiCell phiCell = PhiDisplay.PhiCellAt(layer, X, Y);
            if (isBackgroundColor)
                return phiCell.backgroundColor;
            else
                return phiCell.color;
        }

        // Returns a cell with a given tag
        public PhiCell GetPhiCellWithTag(string tag, int layer = 0)
        {
            for (int x = 0; x < PhiDisplay.GET.displayWidth; x++)
            {
                for (int y = 0; y < PhiDisplay.GET.displayHeight; y++)
                {
                    PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x, y);
                    if (phiCell != null)
                    {
                        if (phiCell.GetTag() == tag)
                            return phiCell;
                    }
                }
            }
            return null;
        }

        // Clears PhiCells within a console area
        public void ClearConsole(int layer, int consoleWidth, int consoleHeight, int xOffset, int yOffset, bool fgOnly = false)
        {
            for (int x = 0; x < consoleWidth; x++)
            {
                for (int y = 0; y < consoleHeight; y++)
                {
                    PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset, y + yOffset);
                    if (phiCell != null)
                    {
                        if (fgOnly)
                        {
                            phiCell.SetContent(" ", phiCell.backgroundColor, Color.clear);
                        }
                        else
                        {
                            phiCell.Clear();
                        }
                    }
                }
            }
        }

        // Returns an xOffset for drawing the map console centered on the player
        public int GetMapConsoleDrawXOffset(Player player, int mapConsoleWidth)
        {
            if (player.X > (mapConsoleWidth / 2))
                return -1 * (player.X - (mapConsoleWidth / 2));
            else if (player.X < (mapConsoleWidth / 2))
                return (mapConsoleWidth / 2) - player.X;
            return 0;
        }

        // Returns a yOffset for drawing the map console centered on the player
        public int GetMapConsoleDrawYOffset(Player player, int mapConsoleHeight)
        {
            // Must round down and add 1 when dealing withwith odd numbered height
            if (player.Y > (mapConsoleHeight / 2))
                return -1 * (player.Y - (mapConsoleHeight / 2));
            else if (player.Y < (mapConsoleHeight / 2))
                return (mapConsoleHeight / 2) - player.Y;
            return 0;
        }

        // Returns a boolean for determining if a RogueSharp.Cell should be drawn by PhiOS
        public bool CheckDrawable(RogueSharp.Cell rogueCell, int drawXOffset, int drawYOffset)
        {
            if (rogueCell.X + drawXOffset > PhiDisplay.GET.zoomDisplayRects[PhiDisplay.GET.currentZoomLevel].xMax - 1 ||
                rogueCell.X + drawXOffset < PhiDisplay.GET.zoomDisplayRects[PhiDisplay.GET.currentZoomLevel].xMin ||
                rogueCell.Y + drawYOffset > PhiDisplay.GET.zoomDisplayRects[PhiDisplay.GET.currentZoomLevel].yMax - 1 ||
                rogueCell.Y + drawYOffset < PhiDisplay.GET.zoomDisplayRects[PhiDisplay.GET.currentZoomLevel].yMin)
                return false;
            return true;
        }

        // Returns an index given an alphabetic string
        public int GetAlphabeticIndex(string alpha)
        {
            if      (alpha.ToLower() == "a") return 0;
            else if (alpha.ToLower() == "b") return 1;
            else if (alpha.ToLower() == "c") return 2;
            else if (alpha.ToLower() == "d") return 3;
            else if (alpha.ToLower() == "e") return 4;
            else if (alpha.ToLower() == "f") return 5;
            else if (alpha.ToLower() == "g") return 6;
            else if (alpha.ToLower() == "h") return 7;
            else if (alpha.ToLower() == "i") return 8;
            else if (alpha.ToLower() == "j") return 9;
            else if (alpha.ToLower() == "k") return 10;
            else if (alpha.ToLower() == "l") return 11;
            else if (alpha.ToLower() == "m") return 12;
            else if (alpha.ToLower() == "n") return 13;
            else if (alpha.ToLower() == "o") return 14;
            else if (alpha.ToLower() == "p") return 15;
            else if (alpha.ToLower() == "q") return 16;
            else if (alpha.ToLower() == "r") return 17;
            else if (alpha.ToLower() == "s") return 18;
            else if (alpha.ToLower() == "t") return 19;
            else if (alpha.ToLower() == "u") return 20;
            else if (alpha.ToLower() == "v") return 21;
            else if (alpha.ToLower() == "w") return 22;
            else if (alpha.ToLower() == "x") return 23;
            else if (alpha.ToLower() == "y") return 24;
            else if (alpha.ToLower() == "z") return 25;
            else return 26;
        }

        public void RevealMap()
        {
            foreach (RogueSharp.Cell rogueCell in Game.Singleton.CurrentMap.GetAllCells())
            {
                Game.Singleton.CurrentMap.SetCellProperties(
                    rogueCell.X,
                    rogueCell.Y,
                    rogueCell.IsTransparent,
                    rogueCell.IsWalkable,
                    true);
            }
        }
    }
}