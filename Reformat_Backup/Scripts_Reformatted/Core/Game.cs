﻿//-----------------------------------------------------------------------
// <copyright file="Game.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Core
{
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using PhiOS;
    using Rogue2018.Actors;
    using Rogue2018.Core.ItemCore;
    using Rogue2018.Core.MapCore;
    using Rogue2018.Systems;
    using Rogue2018.UserInterface;
    using RogueSharp;
    using RogueSharp.Random;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    /// <summary>
    /// A class that defines a type of <see cref="MonoBehaviour"/> that is responsible for handling the main game loop and functionality
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "MonoBehaviour")]
    [RequireComponent(typeof(Swatch))]
    [RequireComponent(typeof(MiniMap))]
    public class Game : MonoBehaviour
    {
        #region Fields

        public static int MapConsoleDrawXOffset;
        public static int MapConsoleDrawYOffset;
        public static Game Singleton;
        public readonly int ActorLayer = 0;
        public readonly int MapConsoleHeight = 90;
        public readonly int MapConsoleWidth = 120;
        public readonly int MapLayer = 0;
        public readonly int MessageConsoleHeight = 19;
        public readonly int MessageConsoleWidth = 36;
        public readonly int MessageLayer = 0;
        public readonly int StatsConsoleHeight = 30;
        public readonly int StatsConsoleWidth = 36;
        public readonly int StatsLayer = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or privately sets the <see cref="ActNumber"/> property
        /// </summary>
        public int ActNumber
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="CommandSystem"/> property
        /// </summary>
        public CommandSystem CommandSystem
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="RogueMap"/> property
        /// </summary>
        public CurrentMap CurrentMap
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="GlyphStrings"/> property
        /// </summary>
        public GlyphStrings GlyphStrings
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="InputSystem"/> property
        /// </summary>
        public InputSystem InputSystem
        {
            get; private set;
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="IsDrawingMapConsole"/> is True or False, or privately sets the <see cref="IsDrawingMapConsole"/> property
        /// </summary>
        public bool IsDrawingMapConsole
        {
            get; private set;
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="IsDrawRequired"/> is True or False, or privately sets the <see cref="IsDrawRequired"/> property
        /// </summary>
        public bool IsDrawRequired
        {
            get; private set;
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="IsGameStarted"/> is True or False, or privately sets the <see cref="IsGameStarted"/> property
        /// </summary>
        public bool IsGameStarted
        {
            get; private set;
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="IsInitialized"/> is True or False, or privately sets the <see cref="IsInitialized"/> property
        /// </summary>
        public bool IsInitialized
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="ItemCollection"/> property
        /// </summary>
        public ItemCollection ItemCollection
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="ItemGenerator"/> property
        /// </summary>
        public ItemGenerator ItemGenerator
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="ItemNameGenerator"/> property
        /// </summary>
        public ItemNameGenerator ItemNameGenerator
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="ItemPrefixes"/> property
        /// </summary>
        public ItemPrefixes ItemPrefixes
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="ItemSuffixes"/> property
        /// </summary>
        public ItemSuffixes ItemSuffixes
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="MapCollection"/> property
        /// </summary>
        public MapCollection MapCollection
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="MapName"/> property
        /// </summary>
        public string MapName
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="MessageSystem"/> property
        /// </summary>
        public MessageSystem MessageSystem
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="MiniMap"/> property
        /// </summary>
        public MiniMap MiniMap
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="PhiMouse"/> property
        /// </summary>
        public PhiMouse PhiMouse
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the <see cref="Player"/> property
        /// </summary>
        public Player Player
        {
            get; set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Random"/> property
        /// </summary>
        public IRandom Random
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="SchedulingSystem"/> property
        /// </summary>
        public SchedulingSystem SchedulingSystem
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Seed"/> property
        /// </summary>
        public int Seed
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Swatch"/> property
        /// </summary>
        public Swatch Swatch
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Symbols"/> property
        /// </summary>
        public Symbols Symbols
        {
            get; private set;
        }

        /// <summary>
        /// Gets or privately sets the <see cref="Tools"/> property
        /// </summary>
        public Tools Tools
        {
            get; private set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Generates a message when the player arrives to a map
        /// </summary>
        /// <returns>A <see cref="string"/> defining the arrived message</returns>
        public string ArrivedMessage()
        {
            string mapName = this.MapCollection.GetMapProperties(this.ActNumber, this.MapName).MapName;
            return string.Format("{0} arrives at {1}", this.Player.Name, mapName);
        }

        /// <summary>
        /// Changes the map using the specified map name
        /// </summary>
        /// <param name="newMapName">A <see cref="string"/> defining the new map name</param>
        public void ChangeMap(string newMapName)
        {
            this.CurrentMap.DestroyHealthBars();

            this.MapCollection.StoreMap(this.ActNumber, this.MapName, this.CurrentMap, this.Player);
            this.CurrentMap.RemoveMonstersFromSchedule();
            this.MapName = newMapName;

            if (this.MapCollection.CheckForStoredMap(this.ActNumber, this.MapName))
            {
                Biomes currentBiome = MapCollection.GetBiome(this.ActNumber, this.MapName);
                this.Swatch.SetSwatchForBiome(currentBiome);
                this.GlyphStrings.SetGlyphStringsForBiome(currentBiome);

                this.CurrentMap = this.MapCollection.GetStoredMap(this.ActNumber, this.MapName);
                this.CurrentMap.AddMonstersToSchedule();

                Point storedPlayerPosition = this.MapCollection.GetStoredPlayerPosition(this.ActNumber, this.MapName);
                this.Player.X = storedPlayerPosition.X;
                this.Player.Y = storedPlayerPosition.Y;

                this.CurrentMap.UpdatePlayerFOV();
            }
            else
            {
                this.GenerateMap();
            }

            this.MessageSystem = new MessageSystem();
            this.CommandSystem = new CommandSystem();
            this.MessageSystem.ConstructMessageSystem();
            this.CommandSystem.ConstructCommandSystem();

            this.MessageSystem.Add(this.ArrivedMessage(), Color.magenta);

            Audio.Singleton.QueueAudio(Audio.Singleton.PlayerStairs);
        }

        /// <summary>
        /// Clears the map console space
        /// </summary>
        public void ClearMapConsole()
        {
            this.Tools.ClearConsole(this.MapLayer, this.MapConsoleWidth, this.MapConsoleHeight, 0, 0);
        }

        /// <summary>
        /// Creates a new game
        /// </summary>
        public void NewGame()
        {
            this.Tools.ClearConsole(0, 160, 90, 0, 0);

            this.CommandSystem = new CommandSystem();
            this.MessageSystem = new MessageSystem();
            this.SchedulingSystem = new SchedulingSystem();

            this.ActNumber = 1;
            this.MapName = this.MapCollection.GetFirstMapNameOfAct(this.ActNumber);

            this.GenerateMap();

            this.CommandSystem.ConstructCommandSystem();
            this.MessageSystem.ConstructMessageSystem();

            this.MessageSystem.Add(this.ArrivedMessage(), Color.magenta);

            UIManager.Singleton.statsUI.Draw();

            this.IsDrawingMapConsole = true;
            this.IsDrawRequired = true;
            this.IsGameStarted = true;
        }

        /// <summary>
        /// Resets the Game
        /// </summary>
        public void ResetGame()
        {
            this.IsGameStarted = false;
            SceneManager.LoadScene(0);
        }

        /// <summary>
        /// Sets <see cref="drawMapConsole"/> using the specified value
        /// </summary>
        /// <param name="value">A <see cref="bool"/> defining the draw state</param>
        public void SetDrawMapConsole(bool value)
        {
            this.IsDrawingMapConsole = value;
        }

        /// <summary>
        /// Sets <see cref="drawRequired"/> using the specified value
        /// </summary>
        /// <param name="value">A <see cref="bool"/> defining the draw state</param>
        public void SetDrawRequired(bool value)
        {
            this.IsDrawRequired = value;
        }

        /// <summary>
        /// <see cref="IEnumerator"/> run at start
        /// </summary>
        /// <returns><see cref="null"/></returns>
        public IEnumerator Start()
        {
            while (!PhiDisplay.IsInitialized())
            {
                yield return null;
            }

            this.Seed = (int)DateTime.UtcNow.Ticks;
            this.Random = new DotNetRandom(this.Seed);

            this.GlyphStrings = new GlyphStrings();
            this.ItemCollection = new ItemCollection();
            this.ItemGenerator = new ItemGenerator();
            this.ItemNameGenerator = new ItemNameGenerator();
            this.ItemPrefixes = new ItemPrefixes();
            this.ItemSuffixes = new ItemSuffixes();
            this.MapCollection = new MapCollection();
            this.MiniMap = this.GetComponent<MiniMap>();
            this.PhiMouse = PhiMouse.Singleton;
            this.Swatch = this.GetComponent<Swatch>();
            this.Symbols = new Symbols();
            this.Tools = new Tools();

            this.InputSystem = new InputSystem();
            this.InputSystem.ConstructInputSystem();

            yield return new WaitForFixedUpdate();

            this.IsInitialized = true;
        }

        /// <summary>
        /// Starts <see cref="ZoomRoutine"/>
        /// </summary>
        /// <param name="zoomLevel">An <see cref="int"/> defining the target zoom level</param>
        public void StartZoomRoutine(int zoomLevel)
        {
            this.StartCoroutine(this.ZoomRoutine(zoomLevel));
        }

        /// <summary>
        /// Awake method with <see cref="Game"/> as singleton
        /// </summary>
        private void Awake()
        {
            if (Singleton != null)
            {
                GameObject.Destroy(Singleton);
            }
            else
            {
                Singleton = this;
            }
        }

        /// <summary>
        /// Generates the <see cref="CurrentMap"/> instance
        /// </summary>
        private void GenerateMap()
        {
            Biomes currentBiome = MapCollection.GetBiome(this.ActNumber, this.MapName);
            this.Swatch.SetSwatchForBiome(currentBiome);
            this.GlyphStrings.SetGlyphStringsForBiome(currentBiome);

            MapGenerator mapGenerator = new MapGenerator(this.ActNumber, this.MapName);
            this.CurrentMap = mapGenerator.GenerateMap();

            this.CurrentMap.UpdatePlayerFOV();
        }

        /// <summary>
        /// Update method called every frame
        /// </summary>
        private void Update()
        {
            if (!PhiDisplay.IsInitialized() || !this.IsInitialized)
            {
                return;
            }

            if (this.IsDrawRequired)
            {
                this.UpdateDrawable();
                this.IsDrawRequired = false;
            }

            if (!this.IsGameStarted)
            {
                return;
            }

            this.InputSystem.UpdateInputSystem();
            this.CommandSystem.UpdateCommandSystem();
        }

        /// <summary>
        /// Updates <see cref="IDrawable"/> entities
        /// </summary>
        private void UpdateDrawable()
        {
            if (this.IsDrawingMapConsole)
            {
                this.Tools.ClearConsole(this.MapLayer, this.MapConsoleWidth, this.MapConsoleHeight, 0, 0);
                MapConsoleDrawXOffset = this.Tools.GetMapConsoleDrawXOffset(this.Player, this.MapConsoleWidth);
                MapConsoleDrawYOffset = this.Tools.GetMapConsoleDrawYOffset(this.Player, this.MapConsoleHeight);

                this.CurrentMap.Draw(MapConsoleDrawXOffset, MapConsoleDrawYOffset);
                this.Player.Draw(MapConsoleDrawXOffset, MapConsoleDrawYOffset, this.CurrentMap);
            }

            this.Tools.ClearConsole(this.StatsLayer, this.StatsConsoleWidth, this.StatsConsoleHeight, this.MapConsoleWidth + 2, 3);
            this.Player.DrawStats(this.MapConsoleWidth + 2, 3);

            this.Tools.ClearConsole(this.MessageLayer, this.MessageConsoleWidth, this.MessageConsoleHeight, this.MapConsoleWidth + 2, this.StatsConsoleHeight + 9);
            this.MessageSystem.Draw(this.MapConsoleWidth + 2, this.StatsConsoleHeight + 9);

            this.CurrentMap.DrawHealthBars(MapConsoleDrawXOffset, MapConsoleDrawYOffset);
            this.MiniMap.Draw();
        }

        /// <summary>
        /// Routine for setting the target zoom level
        /// </summary>
        /// <param name="zoomLevel">An <see cref="int"/> defining the target zoom level</param>
        /// <returns><see cref="WaitForFixedUpdate"/></returns>
        private IEnumerator ZoomRoutine(int zoomLevel)
        {
            this.SetDrawRequired(true);

            yield return new WaitForFixedUpdate();

            PhiDisplay.GET.SetZoom(PhiDisplay.GET.currentZoomLevel);
        }
        #endregion
    }
}