﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Actors
{
    using System.Collections.Generic;
    using PhiOS;
    using Rogue2018.Core;
    using Rogue2018.Core.ItemCore;
    using Rogue2018.Interfaces;
    using UnityEngine;

    /// <summary>
    /// A class that defines a type of <see cref="Actor"/> (Player) with all of its associated properties
    /// </summary>
    public class Player : Actor
    {
        #region Fields

        private EquipableItem equipmentChest;
        private EquipableItem equipmentFeet;
        private EquipableItem equipmentHands;
        private EquipableItem equipmentHead;
        private EquipableItem equipmentLeftRing;
        private EquipableItem equipmentLegs;
        private EquipableItem equipmentMainHand;
        private EquipableItem equipmentOffHand;
        private EquipableItem equipmentRightRing;
        private EquipableItem equipmentWaist;
        private int inventoryCapacity = 26;
        private List<IItem> inventoryList;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class
        /// </summary>
        public Player()
        {
            this.Attack = 4;
            this.Awareness = 15;
            this.Defence = 2;
            this.DefenceChance = 40;
            this.Gold = 0;
            this.Health = 25;
            this.HitChance = 80;
            this.MaxHealth = 25;
            this.Name = "Kataplex";
            this.Speed = 4;

            this.DrawColorFOV = Game.Singleton.Swatch.Player;
            this.GlyphString = Game.Singleton.GlyphStrings.Player;
            this.Symbol = Game.Singleton.Symbols.Player;

            this.InitializeEquipment();
            this.InitializeInventory();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Chest
        /// </summary>
        public EquipableItem EquipmentChest
        {
            get { return this.equipmentChest; } set { this.equipmentChest = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Feet
        /// </summary>
        public EquipableItem EquipmentFeet
        {
            get { return this.equipmentFeet; } set { this.equipmentFeet = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Hands
        /// </summary>
        public EquipableItem EquipmentHands
        {
            get { return this.equipmentHands; } set { this.equipmentHands = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Head
        /// </summary>
        public EquipableItem EquipmentHead
        {
            get { return this.equipmentHead; } set { this.equipmentHead = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Left Ring
        /// </summary>
        public EquipableItem EquipmentLeftRing
        {
            get { return this.equipmentLeftRing; } set { this.equipmentLeftRing = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Legs
        /// </summary>
        public EquipableItem EquipmentLegs
        {
            get { return this.equipmentLegs; } set { this.equipmentLegs = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Main Hand
        /// </summary>
        public EquipableItem EquipmentMainHand
        {
            get { return this.equipmentMainHand; } set { this.equipmentMainHand = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Off Hand
        /// </summary>
        public EquipableItem EquipmentOffHand
        {
            get { return this.equipmentOffHand; } set { this.equipmentOffHand = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Right Ring
        /// </summary>
        public EquipableItem EquipmentRightRing
        {
            get { return this.equipmentRightRing; } set { this.equipmentRightRing = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="EquipableItem"/> assigned to the <see cref="Player"/>'s Waist
        /// </summary>
        public EquipableItem EquipmentWaist
        {
            get { return this.equipmentWaist; } set { this.equipmentWaist = value; }
        }

        /// <summary>
        /// Gets or sets the player's <see cref="List{}(IItem)"/>
        /// </summary>
        public List<IItem> InventoryList
        {
            get { return this.inventoryList; } set { this.inventoryList = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds an <see cref="IItem"/> to the player's inventory
        /// </summary>
        /// <param name="item"><see cref="IItem"/> to add</param>
        public void AddItemToInventory(IItem item)
        {
            if (item is IUsable)
            {
                IUsable usableItem = item as IUsable;
                if (usableItem.MaxStackCount > 1)
                {
                    bool stackFound = false;
                    foreach (IItem checkItem in this.InventoryList)
                    {
                        if (checkItem.Name == item.Name)
                        {
                            IUsable usableCheckItem = checkItem as IUsable;
                            if (usableCheckItem.CurrentStackCount < usableCheckItem.MaxStackCount)
                            {
                                usableCheckItem.CurrentStackCount++;
                                stackFound = true;
                            }
                        }
                    }

                    if (!stackFound)
                    {
                        this.InventoryList.Add(item);
                    }
                }
            }
            else
            {
                this.InventoryList.Add(item);
            }
        }

        /// <summary>
        /// Checks if there is space left in the player's inventory
        /// </summary>
        /// <returns>True if there is space left in the player's inventory else False</returns>
        public bool CheckInventoryCapacity()
        {
            if (this.InventoryList.Count > 25)
            {
                Game.Singleton.MessageSystem.Add("You cannot carry anymore", Color.red, false);
                Game.Singleton.SetDrawRequired(true);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Draws the player's stats in the side console given specified offsets
        /// </summary>
        /// <param name="xOffset">An <see cref="int"/> defining the X offset used for drawing</param>
        /// <param name="yOffset">An <see cref="int"/> defining the Y offset used for drawing</param>
        public void DrawStats(int xOffset, int yOffset)
        {
            int layer = Game.Singleton.StatsLayer;

            string statsName = string.Format("Name:    {0}", Name);
            string statsHealth = string.Format("Health:  {0}/{1}", Health, MaxHealth);
            string statsAttack = string.Format("Attack:  {0} ({1}%)", Attack, HitChance);
            string statsDefence = string.Format("Defence: {0} ({1}%)", Defence, DefenceChance);
            string statsGold = string.Format("Gold:    {0}", Gold);

            for (int x = 0; x < statsName.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 1);
                phiCell.SetContent(statsName.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsHealth.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 3);
                phiCell.SetContent(statsHealth.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsAttack.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 5);
                phiCell.SetContent(statsAttack.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsDefence.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 7);
                phiCell.SetContent(statsDefence.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Text);
            }

            for (int x = 0; x < statsGold.Length; x++)
            {
                PhiCell phiCell = PhiDisplay.PhiCellAt(layer, x + xOffset + 1, yOffset + 9);
                phiCell.SetContent(statsGold.Substring(x, 1), Color.clear, Game.Singleton.Swatch.Gold);
            }
        }

        /// <summary>
        /// Restores health to the player
        /// </summary>
        /// <param name="healValue">An <see cref="int"/> defining the amount of health to restore</param>
        public void HealPlayer(int healValue)
        {
            this.Health += healValue;

            if (this.Health > this.MaxHealth)
            {
                this.Health = this.MaxHealth;
            }
        }

        /// <summary>
        /// Handles the death of the <see cref="Player"/>
        /// </summary>
        public void PlayerDeath()
        {
            Audio.Singleton.QueueAudio(Audio.Singleton.PlayerDeath);

            Game.Singleton.MessageSystem.Add(string.Format("{0} was killed. Game over!", this.Name), Color.magenta, true);
        }

        /// <summary>
        /// TEST: Fills the inventory with random items
        /// </summary>
        public void TEST_FillInventoryWithRandomItems()
        {
            this.InventoryList = new List<IItem>();
            for (int i = 0; i < this.inventoryCapacity; i++)
            {
                ItemQualities itemQuality;
                int randomQualityValue = Game.Singleton.Random.Next(0, 1);
                itemQuality = randomQualityValue == 0
                    ? itemQuality = ItemQualities.Magic
                    : itemQuality = ItemQualities.Rare;

                int itemLevel = Game.Singleton.Random.Next(85, 90);
                IItem randomItem = Game.Singleton.ItemGenerator.CreateEquipableItem(
                    Game.Singleton.ItemCollection.GetRandomEquipableItem(),
                    itemQuality,
                    itemLevel);

                this.AddItemToInventory(randomItem);
            }
        }

        /// <summary>
        /// Initializes the Player's Equipment by creating Equipment for each location
        /// </summary>
        private void InitializeEquipment()
        {
            this.EquipmentHead = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronHat(), ItemQualities.Normal, 10);
            this.EquipmentHead.Equipped = true;

            this.EquipmentChest = Game.Singleton.ItemGenerator.CreateEquipableItem(new ChestPlate(), ItemQualities.Normal, 10);
            this.EquipmentChest.Equipped = true;

            this.EquipmentWaist = Game.Singleton.ItemGenerator.CreateEquipableItem(new LeatherBelt(), ItemQualities.Normal, 10);
            this.EquipmentWaist.Equipped = true;

            this.EquipmentLegs = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronLegguards(), ItemQualities.Normal, 10);
            this.EquipmentLegs.Equipped = true;

            this.EquipmentFeet = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronGreaves(), ItemQualities.Normal, 10);
            this.EquipmentFeet.Equipped = true;

            this.EquipmentHands = Game.Singleton.ItemGenerator.CreateEquipableItem(new IronGauntlets(), ItemQualities.Normal, 10);
            this.EquipmentHands.Equipped = true;

            this.EquipmentMainHand = Game.Singleton.ItemGenerator.CreateEquipableItem(new WoodenClub(), ItemQualities.Normal, 10);
            this.EquipmentMainHand.Equipped = true;
            this.EquipmentMainHand.EquippedMainHand = true;

            this.EquipmentOffHand = Game.Singleton.ItemGenerator.CreateEquipableItem(new SimpleTowerShield(), ItemQualities.Normal, 10);
            this.EquipmentOffHand.Equipped = true;
            this.EquipmentOffHand.EquippedOffHand = true;

            this.EquipmentLeftRing = Game.Singleton.ItemGenerator.CreateEquipableItem(new RubyRing(), ItemQualities.Normal, 10);
            this.EquipmentLeftRing.Equipped = true;
            this.EquipmentLeftRing.EquippedLeftRing = true;

            this.EquipmentRightRing = Game.Singleton.ItemGenerator.CreateEquipableItem(new ThornedRing(), ItemQualities.Normal, 10);
            this.EquipmentRightRing.Equipped = true;
            this.EquipmentRightRing.EquippedRightRing = true;
        }

        /// <summary>
        /// Initializes the Player's Inventory list
        /// </summary>
        private void InitializeInventory()
        {
            this.InventoryList = new List<IItem>();
            this.TEST_FillInventoryWithRandomItems();
        }

        #endregion
    }
}