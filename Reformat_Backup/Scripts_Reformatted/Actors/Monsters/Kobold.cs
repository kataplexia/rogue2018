﻿//-----------------------------------------------------------------------
// <copyright file="Kobold.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Actors.Monsters
{
    using Rogue2018.Core;
    using RogueSharp.DiceNotation;

    /// <summary>
    /// A class that defines a type of <see cref="Monster"/> (Kobold)
    /// </summary>
    public class Kobold : Monster
    {
        #region Methods

        /// <summary>
        /// Creates a type of <see cref="Monster"/> (<see cref="Kobold"/>) given a specified level
        /// </summary>
        /// <returns>
        /// A monster (kobold)
        /// </returns>
        /// <param name="level">An <see cref="int"/> defining the kobold's level</param>
        public static Kobold Create(int level)
        {
            int health = Dice.Roll("2D5");
            return new Kobold
            {
                Attack = Dice.Roll("1D3") + (level / 3),
                Awareness = 10,
                Defence = Dice.Roll("1D3") + (level / 3),
                DefenceChance = Dice.Roll("10D4"),
                Gold = Dice.Roll("5D5"),
                Health = health,
                HitChance = Dice.Roll("25D3"),
                MaxHealth = health,
                Name = "Kobold",
                Speed = 6,

                DrawColorFOV = Game.Singleton.Swatch.Kobold,
                GlyphString = Game.Singleton.GlyphStrings.Kobold,
                Symbol = Game.Singleton.Symbols.Kobold
            };
        }

        #endregion
    }
}