﻿using UnityEngine;

namespace Rogue2018.Interfaces
{
    public enum ItemQualities
    {
        None    = 0,
        Normal  = 1,
        Magic   = 2,
        Rare    = 3,
        Unique  = 4
    }

    public interface IItem
    {
        ItemQualities ItemQuality   { get; set; }
        string Name                 { get; set; }
        string Description          { get; set; }

        void DrawName(int xOffset, int yOffset, Color fgColor);
        void Drop(bool dropAll = false);
    }
}