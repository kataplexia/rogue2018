﻿namespace Rogue2018.Interfaces.ItemInterfaces
{
    public interface IImplicit
    {
        string EffectInfoString { get; set; }
        int EffectValue         { get; set; }

        void ApplyEffect();
    }
}