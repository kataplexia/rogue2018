﻿using System.Collections.Generic;

namespace Rogue2018.Interfaces.ItemInterfaces
{
    public interface ISuffix
    {
        List<EquipableItemBaseTypes> SuffixBaseTypes    { get; set; }
        string SuffixNameString                         { get; set; }
        string TierInfoString                           { get; set; }
        string EffectInfoString                         { get; set; }
        int EffectValue                                 { get; set; }
        int NumberOfTiers                               { get; set; }
        int MinimumItemLevel                            { get; set; }
        int MaximumItemLevel                            { get; set; }
        List<int> TierMinimumItemLevels                 { get; set; }
        List<string> TierNameStrings                    { get; set; }
        List<int> TierEffectValues                      { get; set; }

        void Construct(int itemLevel);
        void ConstructTiers();
        void ApplyEffect();
        void SetupLists();
        void SetupNameAndValue(int itemLevel);
    }
}