﻿using Rogue2018.Core;
using Rogue2018.Systems;

namespace Rogue2018.Interfaces
{
    public interface IBehaviour
    {
        // Properties
        bool Act(Monster monster, CommandSystem commandSystem);
    }
}