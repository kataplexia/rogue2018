﻿using PhiOS;

namespace Rogue2018.Interfaces
{
    public interface IUserInterface
    {
        string[] handles { get; set; }
        PhiCellPropertiesList[] phiCellPropertiesLists { get; set; }

        void Draw();
    }
}


