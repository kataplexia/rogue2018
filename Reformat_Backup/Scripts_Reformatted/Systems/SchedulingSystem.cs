﻿using System.Collections.Generic;
using System.Linq;
using Rogue2018.Interfaces;

namespace Rogue2018.Systems
{
    public class SchedulingSystem
    {
        // Constructor
        private int _time;
        private readonly SortedDictionary<int, List<ISchedulable>> _schedulables;
        public SchedulingSystem()
        {
            _time = 0;
            _schedulables = new SortedDictionary<int, List<ISchedulable>>();
        }

        // Add a new object to the schedule
        public void Add(ISchedulable schedulable)
        {
            int key = _time + schedulable.Time;
            if (!_schedulables.ContainsKey(key))
                _schedulables.Add(key, new List<ISchedulable>());
            _schedulables[key].Add(schedulable);
        }

        // Remove specific object from the schedule
        public void Remove(ISchedulable schedulable)
        {
            KeyValuePair<int, List<ISchedulable>> schedulableListFound = new KeyValuePair<int, List<ISchedulable>>(-1, null);

            foreach (var schedulableList in _schedulables)
            {
                if (schedulableList.Value.Contains(schedulable))
                {
                    schedulableListFound = schedulableList;
                    break;
                }
            }
            if (schedulableListFound.Value != null)
            {
                schedulableListFound.Value.Remove(schedulable);
                if (schedulableListFound.Value.Count <= 0)
                    _schedulables.Remove(schedulableListFound.Key);
            }
        }

        // Get the next object in the schedule and advance time if necessary
        public ISchedulable Get()
        {
            var firstSchedulableGroup = _schedulables.First();
            var firstSchedulable = firstSchedulableGroup.Value.First();
            Remove(firstSchedulable);
            _time = firstSchedulableGroup.Key;
            return firstSchedulable;
        }

        // Get the current time for the schedule
        public int GetTime()
        {
            return _time;
        }

        // Reset time and clear the schedule
        public void Clear()
        {
            _time = 0;
            _schedulables.Clear();
        }
    }
}