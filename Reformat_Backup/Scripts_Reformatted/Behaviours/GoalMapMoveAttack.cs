﻿//-----------------------------------------------------------------------
// <copyright file="GoalMapMoveAttack.cs" company="HiVE Interactive">
// Copyright (c) 2018 All Rights Reserved
// </copyright>
// <author>Blake Simpson</author>
// <year>2018</year>
//-----------------------------------------------------------------------
namespace Rogue2018.Behaviours
{
    using System.Collections.Generic;
    using Rogue2018.Core;
    using Rogue2018.Interfaces;
    using Rogue2018.Systems;
    using RogueSharp;

    /// <summary>
    /// A class that defines a type of <see cref="IBehaviour"/> (<see cref="GoalMapMoveAttack"/>) with all of its associated properties
    /// </summary>
    public class GoalMapMoveAttack : IBehaviour
    {
        #region Fields

        private FieldOfView monsterFOV;

        #endregion

        #region Methods

        /// <summary>
        /// Acts on the <see cref="GoalMapMoveAttack"/> behaviour given a specified <see cref="Monster"/> and <see cref="CommandSystem"/>
        /// </summary>
        /// <param name="monster"><see cref="Monster"/> to act</param>
        /// <param name="commandSystem"><see cref="CommandSystem"/> responsible for handling the act</param>
        /// <returns>True if acting on the <see cref="GoalMapMoveAttack"/> behaviour is possible else False</returns>
        public bool Act(Monster monster, CommandSystem commandSystem)
        {
            if (this.monsterFOV == null)
            {
                this.monsterFOV = new FieldOfView(Game.Singleton.CurrentMap);
            }

            if (!monster.TurnsAlerted.HasValue)
            {
                this.monsterFOV.ComputeFOV(monster.X, monster.Y, monster.Awareness, true);
                if (this.monsterFOV.IsInFOV(Game.Singleton.Player.X, Game.Singleton.Player.Y))
                {
                    monster.TurnsAlerted = 1;
                }
            }
            else
            {
                List<GoalMap.WeightedPoint> neighbours = Game.Singleton.CurrentMap.MonsterGoalMap.GetNeighbors(monster.X, monster.Y);
                int bestWeight = int.MaxValue;
                ICell bestNeighbourCell = null;

                foreach (GoalMap.WeightedPoint neighbour in neighbours)
                {
                    ICell weightedPointCell = Game.Singleton.CurrentMap.GetCell(neighbour.X, neighbour.Y);
                    bool isPlayerCell = weightedPointCell.X == Game.Singleton.Player.X && weightedPointCell.Y == Game.Singleton.Player.Y;

                    if (!weightedPointCell.IsWalkable && !isPlayerCell)
                    {
                        continue;
                    }
                    else if (neighbour.Weight < bestWeight)
                    {
                        bestWeight = neighbour.Weight;
                        bestNeighbourCell = weightedPointCell;
                    }
                }

                if (bestNeighbourCell != null)
                {
                    commandSystem.MoveMonster(monster, bestNeighbourCell);
                }

                monster.TurnsAlerted++;

                if (monster.TurnsAlerted > 10)
                {
                    monster.TurnsAlerted = null;
                }
            }

            return true;
        }

        #endregion
    }
}