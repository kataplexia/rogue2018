﻿using UnityEngine;
using System.Collections.Generic;

namespace PhiOS
{
    [System.Serializable]
    public class PhiCellPropertiesList : MonoBehaviour
    {
        public List<PhiCellProperties> propertiesList;
    }
}