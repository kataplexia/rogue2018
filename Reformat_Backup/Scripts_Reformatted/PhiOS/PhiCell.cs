﻿using UnityEngine;
using PhiOS.Interfaces;

namespace PhiOS
{

    public class PhiCellFades
    {
        public static string DEFAULT_REVERSE = "░▒▓█";
        public static string DEFAULT = "█▓▒░";
        public static string MOUSE_DEFAULT = "+";
    }

    public class PhiCell
    {

        public int layer;
        public Vector2 position;
        public MonoBehaviour owner;
        public string content = "";
        public Color backgroundColor;
        public Color color;
        public string tag;
        public IHoverAction hoverAction;
        public IClickAction clickAction;
        public IDragAction dragAction;
        public IScrollAction scrollAction;

        private string targetContent = "";
        private Color targetColor;
        private float fadeLeft = 0f;
        private float fadeMax = 0f;
        private Color fadeColor;
        private string fades = "";
        private bool fadeFinished = true;

        public void Clear()
        {
            SetContent("", Color.clear, Color.clear);
            tag = "";
        }

        public void Clear(float fadeTime, Color fadeColor)
        {
            SetContent("", Color.clear, Color.clear, fadeTime, fadeColor, PhiCellFades.DEFAULT_REVERSE);
            tag = "";
        }

        public void SetContent(string content, Color backgroundColor, Color color)
        {
            SetContent(content, backgroundColor, color, 0f, color, "");
        }

        public void SetContent(string content, Color backgroundColor, Color color, float fadeTime, Color fadeColor)
        {
            SetContent(content, backgroundColor, color, fadeTime, fadeColor, PhiCellFades.DEFAULT);
        }

        public void SetContent(
            string content,
            Color backgroundColor,
            Color color,
            float fadeMax,
            Color fadeColor,
            string fades)
        {

            // set target content and color
            targetContent = content;
            this.backgroundColor = backgroundColor;
            targetColor = color;

            // fade
            if (fadeMax > 0f)
            {
                this.fadeLeft = this.fadeMax = Random.Range(0f, fadeMax);
                this.color = this.fadeColor = fadeColor;
                this.fades = fades;
                fadeFinished = false;
            }

            // instant
            else {
                this.fadeLeft = 0f;
                this.fadeMax = 0f;
                fadeFinished = false;
            }

            // add cell to top layer
            if (targetContent != "")
            {
                PhiDisplay.GET.AddCellAsTopLayer(this);
            }
        }

        public void SetTag(string setTag)
        {
            tag = setTag;
        }

        public string GetTag()
        {
            return tag;
        }

        public void Update()
        {

            // display initialized
            if (PhiDisplay.GET.initialized)
            {

                // fade
                if (fadeLeft > 0f)
                {
                    content = targetContent.Trim().Length > 0 || content.Trim().Length > 0 ?
                        fades.Substring(Mathf.RoundToInt((fadeLeft / fadeMax) * (fades.Length - 1)), 1) :
                        targetContent;
                    color = Color.Lerp(
                        targetColor,
                        fadeColor,
                        PhiDisplay.GET.colorLerpCurve.Evaluate(fadeLeft / fadeMax));
                    fadeLeft -= Time.deltaTime;
                }

                // fade finished
                else {

                    // remove cell from top layer
                    if (!fadeFinished && targetContent == "")
                    {
                        PhiDisplay.GET.RemoveCellAsTopLayer(this);
                    }

                    fadeFinished = true;
                    fadeLeft = 0f;
                    content = targetContent;
                    color = targetColor;
                }
            }
        }
    }
}