﻿using UnityEngine;

namespace PhiOS
{
    [System.Serializable]
    public class PhiCellProperties
    {
        public int X;
        public int Y;
        public int layer;
        public string symbol;
        public Color drawColor;
        public Color backgroundDrawColor;
    }
}