﻿using UnityEngine;
using System.Collections;
using PhiOS.Interfaces;

namespace PhiOS
{
    public class PhiMouse : MonoBehaviour
    {
        public bool debugMouseCells;
        public bool debugMousePosition;
        public bool hideNativeCursor = true;
        public int reservedLayer = -1;
        public string cursorUp = "┼";
        public string cursorDown = "█";
        public Color cursorColor;
        public float cursorFadeTime = 0.25f;
        public bool fadeToClear = true;
        public float cursorHideTime = 3f;
        private float currentHideTimer;

        private bool initialized = false;
        [HideInInspector]
        public Vector3 mousePosition;
        [HideInInspector]
        public Vector2 cellPosition;
        private Vector2 lastCellPosition;
        private PhiCell currentCell;
        private PhiCell currentCellHover;
        private IHoverAction hoverAction;
        private bool dragging = false;
        private Vector2 dragStart;
        private IDragAction dragAction;

        public static PhiMouse Singleton;
        private void Awake()
        {
            if (hideNativeCursor)
                Cursor.visible = false;
            if (Singleton != null)
                GameObject.Destroy(Singleton);
            else
                Singleton = this;
        }

        public IEnumerator Start()
        {

            // wait until display is initialized
            PhiDisplay display = PhiDisplay.GET;
            while (!display.initialized)
            {
                yield return null;
            }

            yield return null;

            // initialized
            initialized = true;
        }

        public void Update()
        {

            // initialized
            if (initialized)
            {

                PhiDisplay display = PhiDisplay.GET;

                // Calculate omitted cells based on display zoom rects
                int ommittedCellCountX = (int)(display.zoomDisplayRects[0].width - display.zoomDisplayRects[display.currentZoomLevel].width);
                int ommittedCellCountY = (int)(display.zoomDisplayRects[0].height - display.zoomDisplayRects[display.currentZoomLevel].height);

                // Setup and clamp mouse position
                mousePosition = new Vector3(
                    Mathf.Clamp((int)(Input.mousePosition.x / 10), 0, display.displayWidth - 1),
                    Mathf.Clamp((int)(display.displayHeight - (Input.mousePosition.y / 10)), 0, display.displayHeight - 1));

                // Check if mouse is within main camera viewport
                if (mousePosition.x < display.zoomDisplayRects[display.currentZoomLevel].width + ommittedCellCountX)
                {
                    // Modify mouse input using scaling factor and omitted cell counts
                    mousePosition *= display.zoomDisplayRects[display.currentZoomLevel].width / display.zoomDisplayRects[0].width;
                    mousePosition = new Vector3(
                        (int)(mousePosition.x + (ommittedCellCountX / 2)),
                        (int)(mousePosition.y + (ommittedCellCountY / 2)));
                }

                // Setup and clamp cell position within display
                cellPosition = new Vector2(
                    Mathf.Clamp(mousePosition.x, 0f, display.displayWidth - 1),
                    Mathf.Clamp(mousePosition.y, 0f, display.displayHeight - 1));

                if (cellPosition == lastCellPosition)
                {
                    currentHideTimer += Time.deltaTime;
                    currentHideTimer = Mathf.Clamp(currentHideTimer, 0f, cursorHideTime);
                }
                else
                    currentHideTimer = 0;

                lastCellPosition = cellPosition;

                // clear current cell
                if (currentCell != null)
                {

                    // get background color to clear to
                    Color clearColor = display.GetBackgroundColorForCell(
                                           (int)currentCell.position.x,
                                           (int)currentCell.position.y,
                                           reservedLayer);

                    // clear cell content
                    currentCell.SetContent(
                        "",
                        clearColor,
                        fadeToClear ? display.clearColor : cursorColor,
                        cursorFadeTime,
                        cursorColor,
                        PhiCellFades.MOUSE_DEFAULT);
                    currentCell = null;
                }

                if (currentHideTimer == cursorHideTime)
                    return;

                // reset current cell hover
                currentCellHover = null;

                // new current cell
                currentCell = display.GetPhiCell(reservedLayer, cellPosition.x, cellPosition.y);

                // get background color for current cell
                Color currentCellBackgroundColor = display.GetBackgroundColorForCell(
                    (int)currentCell.position.x,
                    (int)currentCell.position.y,
                    reservedLayer);

                // highlight cell
                currentCell.SetContent(
                    Input.GetMouseButton(0) || Input.GetMouseButton(1) ? cursorDown : cursorUp,
                    currentCellBackgroundColor,
                    cursorColor,
                    0f,
                    cursorColor,
                    "");

                // hover
                if (!dragging)
                {
                    for (int i = display.GetNumLayers() - 1; i >= 0; i--)
                    {
                        PhiCell cellHover = display.GetPhiCell(i, currentCell.position.x, currentCell.position.y);

                        // hover on topmost layer
                        if (cellHover.content != "")
                        {

                            // set new hover cell
                            currentCellHover = cellHover;

                            // new hover cell has a hover action
                            if (currentCellHover.hoverAction != null)
                            {

                                // current hover action is different to new hover action
                                if (hoverAction != currentCellHover.hoverAction)
                                {

                                    // current hover exit
                                    if (hoverAction != null)
                                    {
                                        hoverAction.OnHoverExit();
                                    }

                                    // new hover enter
                                    hoverAction = currentCellHover.hoverAction;
                                    hoverAction.OnHoverEnter((int)cellHover.position.x, (int)cellHover.position.y);
                                }
                            }

                            // new hover cell has no hover action, just exit current hover action
                            else if (hoverAction != null)
                            {
                                hoverAction.OnHoverExit();
                                hoverAction = null;
                            }

                            break;
                        }
                    }
                }

                // click
                if (!dragging)
                {
                    if (Input.GetMouseButtonDown(0) &&
                        currentCellHover != null &&
                        currentCellHover.clickAction != null)
                    {
                        currentCellHover.clickAction.OnMouseDown(0);
                    }
                    else if (Input.GetMouseButtonDown(1) &&
                        currentCellHover != null &&
                        currentCellHover.clickAction != null)
                    {
                        currentCellHover.clickAction.OnMouseDown(1);
                    }
                }

                // drag start
                if (!dragging &&
                    Input.GetMouseButtonDown(0) &&
                    currentCellHover != null &&
                    currentCellHover.dragAction != null)
                {
                    dragging = true;
                    dragStart = currentCell.position;
                    dragAction = currentCellHover.dragAction;
                    dragAction.OnDragStart();
                }

                // drag end
                else if (dragging &&
                         Input.GetMouseButtonUp(0) &&
                         dragAction != null)
                {
                    dragging = false;
                    Vector2 dragDelta = currentCell.position - dragStart;
                    dragAction.OnDragDelta(dragDelta);
                    dragAction = null;
                }

                // drag delta
                else if (dragging &&
                         dragAction != null)
                {
                    Vector2 dragDelta = currentCell.position - dragStart;
                    dragAction.OnDragDelta(dragDelta);
                }

                // scroll
                if (!dragging)
                {
                    if (Input.mouseScrollDelta.y != 0f &&
                        currentCellHover != null &&
                        currentCellHover.scrollAction != null)
                    {
                        currentCellHover.scrollAction.OnScrollDelta(Mathf.RoundToInt(Input.mouseScrollDelta.y));
                    }
                }

                if (debugMousePosition)
                {
                    Debug.Log(string.Format("MousePos: [{0}, {1}]", mousePosition.x, mousePosition.y));
                }

                if (debugMouseCells && Input.GetKeyDown(KeyCode.F4))
                {
                    Debug.Log("TEST: SHOWING CELLS WHICH ARE INTERACTABLE WITH THE MOUSE");
                    for (int x = 0; x < PhiDisplay.GET.displayWidth; x++)
                    {
                        for (int y = 0; y < PhiDisplay.GET.displayHeight; y++)
                        {
                            PhiCell phiCell = PhiDisplay.PhiCellAt(0, x, y);
                            if (phiCell.clickAction != null || phiCell.hoverAction != null)
                                phiCell.SetContent(phiCell.content, Color.magenta, phiCell.color);
                        }
                    }
                }
            }
        }
    }
}