﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace PhiOS
{
    public class PhiDisplay : MonoBehaviour
    {
        [Header("COMPONENTS")]
        public BitmapFont font;
        public Camera mainCamera;
        public Camera secondCamera;
        public GameObject displayQuadPrefab;
        public Material backgroundMaterial;
        public DisplayMesh background;
        public DisplayMesh foreground;

        [Header("QUAD MODIFIERS")]
        public bool useModifiedScale = true;
        public float modifiedXScale = 1.05f;
        public float modifiedYScale = 0.9925f;
        public bool useModifiedPos = true;
        public float modifiedXPos = -84f;
        public float modifiedYPos = 49.95f;

        [Header("CAMERA")]
        public int displayWidth = 160;
        public bool autoDisplayHeight = false;
        public int displayHeight = 70;
        public float mainCameraXPos = -20f;
        public float secondCameraXPos = 60f;
        public bool useZoomOrthographicDefault = true;
        public float[] zoomOrthographicSizes;
        public Rect[] zoomDisplayRects;
        public int defaultZoomLevel = 2;
        [HideInInspector]
        public int currentZoomLevel = 0;

        [Header("TRANSITIONS")]
        public AnimationCurve colorLerpCurve;

        [HideInInspector]
        public bool initialized = false;

        [HideInInspector]
        public Color clearColor;

        private float quadWidth;
        private float quadHeight;
        private int reservedLayers = 1;
        private int initialNumLayers = 3;
        private int numLayers = 0;
        private Dictionary<int, PhiCell[,]> phiCells = new Dictionary<int, PhiCell[,]>();
        private LinkedList<PhiCell> cellList = new LinkedList<PhiCell>();
        private LinkedList<int>[,] topLayers;
        private Vector3 zero3 = Vector3.zero;
        private Vector2 zero2 = Vector2.zero;

        public static PhiDisplay GET;
        private void Awake()
        {
            if (GET != null)
                GameObject.Destroy(GET);
            else
                GET = this;
            // DontDestroyOnLoad(this);
            // cache clear color
            clearColor = Color.clear;
        }

        public IEnumerator Start()
        {

            // wait until font has loaded
            while (!font.fontLoaded)
            {
                yield return null;
            }

            // calculate quad size
            quadWidth = 1f;
            quadHeight =
                (font.GetGlyphHeight() / font.GetGlyphWidth()) *
                (font.useRexPaintFont ? font.rexPaintQuadHeightScale : font.quadHeightScale);

            // derive displayheight from desired display width
            int maxDisplayHeight = Mathf.RoundToInt((((float)Screen.height / (float)Screen.width) * (float)displayWidth) / quadHeight);
            if (autoDisplayHeight)
            {
                displayHeight = maxDisplayHeight;
            }

            // instantiate quads
            int quadMeshFilterIndex = 0;
            MeshFilter[] quadMeshFilters = new MeshFilter[displayWidth * displayHeight];
            for (int y = 0; y < displayHeight; y++)
            {
                for (int x = 0; x < displayWidth; x++)
                {

                    // instantiate from prefab
                    GameObject quad = (GameObject)GameObject.Instantiate(displayQuadPrefab);
                    quad.transform.parent = transform;
                    quad.transform.localScale = new Vector3(quadWidth, quadHeight, 1f);
                    quad.transform.position = new Vector3(x * quadWidth + quadWidth * 0.5f, -y * quadHeight - quadHeight * 0.5f, 0f);

                    // add to array for combining later
                    quadMeshFilters[quadMeshFilterIndex] = quad.GetComponent<MeshFilter>();
                    quadMeshFilterIndex++;
                }
            }

            // add quads to combine instances
            CombineInstance[] combineInstances = new CombineInstance[quadMeshFilters.Length];
            for (int i = 0; i < quadMeshFilters.Length; i++)
            {
                combineInstances[i].mesh = quadMeshFilters[i].sharedMesh;
                combineInstances[i].transform = quadMeshFilters[i].transform.localToWorldMatrix;
            }

            // combine quads to foreground and background
            background.CombineQuads(combineInstances, "background_quads", backgroundMaterial, 0.001f);
            foreground.CombineQuads(combineInstances, "foreground_quads", font.GetFontMaterial(), 0f);

            // update camera orthographic size
            mainCamera.orthographicSize = Mathf.Max(maxDisplayHeight * quadHeight * 0.5f, background.transform.position.y);

            yield return null;

            // destroy original quads
            for (int i = quadMeshFilters.Length - 1; i >= 0; i--)
            {
                GameObject.Destroy(quadMeshFilters[i].gameObject);
            }
            quadMeshFilters = null;

            // Debug.Log ("DISPLAY SIZE: " + displayWidth + "x" + displayHeight);

            // pre-populate top layers
            topLayers = new LinkedList<int>[displayWidth, displayHeight];
            for (int y = 0; y < displayHeight; y++)
            {
                for (int x = 0; x < displayWidth; x++)
                {
                    topLayers[x, y] = new LinkedList<int>();
                }
            }

            // pre-populate layers
            for (int layerIndex = -reservedLayers; layerIndex < initialNumLayers; layerIndex++)
            {
                PhiCell[,] layer = new PhiCell[displayWidth, displayHeight];
                for (int y = 0; y < displayHeight; y++)
                {
                    for (int x = 0; x < displayWidth; x++)
                    {
                        layer[x, y] = CreateCell(layerIndex, x, y);
                    }
                }
                phiCells.Add(layerIndex, layer);
            }
            numLayers = initialNumLayers;

            yield return null;

            // initialized
            initialized = true;

            // initialize cameras
            float xPosMain = mainCameraXPos;
            float yPosMain = mainCamera.transform.position.y;
            float zPosMain = mainCamera.transform.position.z;
            mainCamera.transform.position = new Vector3(xPosMain, yPosMain, zPosMain);

            float xPosSecond = secondCameraXPos;
            float yPosSecond = secondCamera.transform.position.y;
            float zPosSecond = secondCamera.transform.position.z;
            secondCamera.transform.position = new Vector3(xPosSecond, yPosSecond, zPosSecond);

            if (useZoomOrthographicDefault)
            {
                mainCamera.orthographicSize = zoomOrthographicSizes[0];
            }
            secondCamera.orthographicSize = mainCamera.orthographicSize;

            // modify quads
            if (useModifiedPos)
            {
                float xPosBackground = modifiedXPos;
                float yPosBackground = modifiedYPos;
                float zPosBackground = 1;
                Vector3 backgroundPosVector = new Vector3(xPosBackground, yPosBackground, zPosBackground);
                background.transform.position = backgroundPosVector;

                float xPosForeground = background.transform.position.x;
                float yPosForeground = modifiedYPos;
                float zPosForeground = 0;
                Vector3 foregroundPosVector = new Vector3(xPosForeground, yPosForeground, zPosForeground);
                foreground.transform.position = foregroundPosVector;
            }

            if (useModifiedScale)
            {
                float xScaleQuads = modifiedXScale;
                float yScaleQuads = modifiedYScale;
                float zScaleQuads = background.transform.localScale.z;
                Vector3 quadScaleVector = new Vector3(xScaleQuads, yScaleQuads, zScaleQuads);
                background.transform.localScale = quadScaleVector;
                foreground.transform.localScale = quadScaleVector;
            }

            // Set default zoom level
            currentZoomLevel = defaultZoomLevel;
            SetZoom(currentZoomLevel);
        }

        private PhiCell CreateCell(int layerIndex, int x, int y)
        {

            // create a new cell at layer and position
            PhiCell phiCell = new PhiCell();
            phiCell.layer = layerIndex;
            phiCell.position = new Vector2(x, y);
            cellList.AddLast(phiCell);
            return phiCell;
        }

        public int GetNumLayers()
        {
            if (initialized)
            {
                return numLayers;
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public PhiCell GetPhiCell(int layer, float x, float y)
        {

            if (initialized)
            {

                // get layer cells, add if not already exists
                PhiCell[,] layerPhiCells = null;
                if (!phiCells.TryGetValue(layer, out layerPhiCells))
                {
                    layerPhiCells = new PhiCell[displayWidth, displayHeight];
                    phiCells.Add(layer, layerPhiCells);
                    numLayers = Mathf.Max(layer + 1, numLayers);
                }

                // get cell, add if not already exists
                if (x >= 0 && y >= 0 && x < displayWidth && y < displayHeight)
                {
                    PhiCell phiCell = layerPhiCells[(int)x, (int)y];
                    if (phiCell == null)
                    {
                        phiCell = layerPhiCells[(int)x, (int)y] = CreateCell(layer, (int)x, (int)y);
                    }

                    return phiCell;
                }

                // position outside of display bounds
                else {
                    return null;
                }

            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public void AddCellAsTopLayer(PhiCell phiCell)
        {

            if (initialized)
            {

                // get top layers for cell
                LinkedList<int> topLayersForPhiCell = topLayers[(int)phiCell.position.x, (int)phiCell.position.y];

                // add layer to end
                if (topLayersForPhiCell.Count == 0 ||
                    (topLayersForPhiCell.Last.Value >= 0 && topLayersForPhiCell.Last.Value < phiCell.layer) ||
                    (topLayersForPhiCell.Last.Value >= 0 && phiCell.layer < 0) ||
                    (topLayersForPhiCell.Last.Value < 0 && phiCell.layer < 0 && phiCell.layer < topLayersForPhiCell.Last.Value))
                {
                    topLayersForPhiCell.AddLast(phiCell.layer);
                    return;
                }

                // add layer to beginning
                else if (
                    (topLayersForPhiCell.First.Value >= 0 && phiCell.layer >= 0 && topLayersForPhiCell.First.Value > phiCell.layer) ||
                    (topLayersForPhiCell.First.Value < 0 && phiCell.layer >= 0) ||
                    (topLayersForPhiCell.First.Value < 0 && phiCell.layer < 0 && topLayersForPhiCell.First.Value < phiCell.layer))
                {
                    topLayersForPhiCell.AddFirst(phiCell.layer);
                    return;
                }

                // insert layer
                LinkedListNode<int> current = topLayersForPhiCell.First;
                while (current.Next != null)
                {

                    // layer already exists
                    if (current.Value == phiCell.layer || current.Next.Value == phiCell.layer)
                    {
                        return;
                    }

                    // found a position to insert layer
                    if ((current.Value >= 0 && phiCell.layer >= 0 && current.Next.Value >= 0 && current.Value < phiCell.layer && current.Next.Value > phiCell.layer) ||
                        (current.Value >= 0 && phiCell.layer >= 0 && current.Next.Value < 0 && current.Value < phiCell.layer) ||
                        (current.Value >= 0 && phiCell.layer < 0 && current.Next.Value < 0 && current.Next.Value < phiCell.layer) ||
                        (current.Value < 0 && phiCell.layer < 0 && current.Value > phiCell.layer && current.Next.Value < phiCell.layer))
                    {
                        topLayersForPhiCell.AddAfter(current, phiCell.layer);
                        return;
                    }

                    current = current.Next;
                }
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public void RemoveCellAsTopLayer(PhiCell phiCell)
        {

            if (initialized)
            {

                // get top layers for cell
                LinkedList<int> topLayersForCell = topLayers[(int)phiCell.position.x, (int)phiCell.position.y];

                // remove layer
                if (topLayersForCell.Contains(phiCell.layer))
                {
                    topLayersForCell.Remove(phiCell.layer);
                }
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public LinkedList<int> GetTopLayersForCell(int x, int y)
        {
            if (initialized)
            {
                if (x >= 0 && y >= 0 && x < displayWidth && y < displayHeight)
                {
                    return topLayers[x, y];
                }
                else {
                    return new LinkedList<int>();
                }
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public Color GetBackgroundColorForCell(int x, int y, params int[] excludedLayers)
        {

            if (initialized)
            {
                List<int> excludedLayersList = new List<int>(excludedLayers);

                // get background color of cell previous to excluded layers
                LinkedList<int> topLayer = GetTopLayersForCell(x, y);
                if (topLayer.First != null && !excludedLayersList.Contains(topLayer.First.Value))
                {
                    LinkedListNode<int> topLayerNode = topLayer.Last;
                    while (excludedLayersList.Contains(topLayerNode.Value))
                    {
                        topLayerNode = topLayerNode.Previous;
                    }
                    PhiCell cell = GetPhiCell(topLayerNode.Value, x, y);
                    return (cell != null) ? cell.backgroundColor : clearColor;
                }

                return clearColor;
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public void Update()
        {

            // finished initializing
            if (initialized)
            {

                // update cells
                LinkedListNode<PhiCell> cellNode = cellList.First;
                while (cellNode != null)
                {
                    cellNode.Value.Update();
                    cellNode = cellNode.Next;
                }

                // update display meshes
                for (int y = 0; y < displayHeight; y++)
                {
                    for (int x = 0; x < displayWidth; x++)
                    {

                        // get top layer for cell
                        LinkedList<int> topLayersForCell = topLayers[x, y];

                        // get cell at top layer
                        PhiCell cell = null;
                        if (topLayersForCell.First != null)
                        {
                            cell = phiCells[topLayersForCell.Last.Value][x, y];
                        }

                        // empty cell
                        if (cell == null || cell.content == "")
                        {
                            for (int i = 0; i < 4; i++)
                            {

                                // update display mesh vertices, uvs and colors
                                foreground.meshVertices[(y * displayWidth + x) * 4 + i] = zero3;
                                foreground.meshUVs[(y * displayWidth + x) * 4 + i] = zero2;
                                foreground.meshColors[(y * displayWidth + x) * 4 + i] = clearColor;
                                background.meshColors[(y * displayWidth + x) * 4 + i] = cell != null ? cell.backgroundColor : clearColor;
                            }
                        }

                        // filled cell
                        else {
                            BitmapFontGlyph glyph = font.GetGlyph(cell.content);
                            for (int i = 0; i < 4; i++)
                            {

                                // update display mesh vertices, uvs and colors
                                foreground.meshVertices[(y * displayWidth + x) * 4 + i] = new Vector3(
                                    x * quadWidth + glyph.vertices[i].x * quadWidth,
                                    -y * quadHeight + glyph.vertices[i].y * quadHeight - quadHeight,
                                    0f);
                                foreground.meshUVs[(y * displayWidth + x) * 4 + i] = glyph.uvs[i];
                                foreground.meshColors[(y * displayWidth + x) * 4 + i] = cell.color;
                                background.meshColors[(y * displayWidth + x) * 4 + i] = cell.backgroundColor;
                            }
                        }
                    }
                }

                // apply display mesh updates
                background.UpdateMesh();
                foreground.UpdateMesh();

                // capture screenshot
                if (Input.GetKeyDown(KeyCode.P))
                {
                    ScreenCapture.CaptureScreenshot("ascii_" + Random.Range(0, int.MaxValue) + ".png");
                }
            }
        }

        public void SetZoom(int zoomLevel)
        {
            if (currentZoomLevel != zoomLevel)
                currentZoomLevel = zoomLevel;
            mainCamera.orthographicSize = zoomOrthographicSizes[zoomLevel];
        }

        public IEnumerator Quit(float delay)
        {
            yield return new WaitForSeconds(delay);
            Application.Quit();
        }

        public static bool IsInitialized()
        {
            return GET.initialized;
        }

        public static PhiCell PhiCellAt(int layer, float x, float y)
        {
            return GET.GetPhiCell(layer, x, y);
        }

        public static int GetDisplayWidth()
        {
            if (GET.initialized)
            {
                return GET.displayWidth;
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }

        public static int GetDisplayHeight()
        {
            if (GET.initialized)
            {
                return GET.displayHeight;
            }
            else {
                throw new UnityException("Display not yet initialized!");
            }
        }
    }
}