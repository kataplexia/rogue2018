﻿namespace PhiOS.Interfaces
{
    public interface IClickAction
    {
        void OnMouseDown(int mouseButtonIndex);
    }
}