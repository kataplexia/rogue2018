﻿using UnityEngine;

namespace PhiOS.Interfaces
{
    public interface IDragAction
    {
        void OnDragStart();
        void OnDragDelta(Vector2 delta);
    }
}