﻿using UnityEngine;
using UnityEditor;

namespace PhiOS
{
    [CustomEditor(typeof(Display))]
    public class DisplayEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                PhiDisplay display = (PhiDisplay)target;

                // force display height to 0 if auto calculating
                if (display.autoDisplayHeight && display.displayHeight != 0)
                {
                    display.displayHeight = 0;
                }
            }
        }
    }
}