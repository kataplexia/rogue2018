﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PhiOS;

public class TextureParse : MonoBehaviour
{
    // Method for parsing a RexPaintXmlFile and drawing PhiCells using the data
    public void ParseTextureFile(Texture2D texture, int xOffset, int yOffset, int layer = 0)
    {
        // Editor debug
        Debug.Log(string.Format("Parsing {0} with offset ({1}, {2}) and layer {3}.", texture.name, xOffset, yOffset, layer));

        // Create prefab object
        GameObject workingPrefab = new GameObject();

        // Initialize cell properties list
        PhiCellPropertiesList cellPropertiesList = workingPrefab.AddComponent<PhiCellPropertiesList>();
        cellPropertiesList.propertiesList = new List<PhiCellProperties>();

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                Color textureColor = texture.GetPixel(x, y);

                // Setup cell properties
                PhiCellProperties phiCellProperties = new PhiCellProperties();
                phiCellProperties.X = x + xOffset;
                phiCellProperties.Y = (texture.height - y - 1) + yOffset;
                phiCellProperties.layer = layer;
                phiCellProperties.symbol = " ";
                phiCellProperties.drawColor = Color.clear;
                phiCellProperties.backgroundDrawColor = textureColor;

                cellPropertiesList.propertiesList.Add(phiCellProperties);
            }
        }

        // Create prefab name and properties count
        string prefabName = texture.name + "CellProperties";
        int propertiesCount = cellPropertiesList.propertiesList.Count;

        // Create prefab
        UnityEngine.Object tempPrefab = PrefabUtility.CreateEmptyPrefab("Assets/Prefabs/TextureParse/CellPropertiesPrefabs/" + prefabName + ".prefab");
        PrefabUtility.ReplacePrefab(workingPrefab, tempPrefab, ReplacePrefabOptions.ConnectToPrefab);
        GameObject.DestroyImmediate(workingPrefab);

        // Editor debug
        Debug.Log(string.Format("Parse succesful. {0} created with {1} CellProperties components.", prefabName, propertiesCount));
    }
}