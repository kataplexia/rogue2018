﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Rogue2018.Systems;

[CustomEditor(typeof(TextureParse))]
public class TextureParseSystemEditor : Editor
{
    private Texture2D texture = null;
    private int xOffset = 0;
    private int yOffset = 0;
    private int layer = 0;

    // InspectorGUI
    public override void OnInspectorGUI()
    {
        // Setup GUI
        DrawDefaultInspector();
        TextureParse textureParse = (TextureParse)target;

        // GUI fields
        texture = (Texture2D)EditorGUILayout.ObjectField("Texture", texture, typeof(Texture2D), false);
        xOffset = EditorGUILayout.IntField("X Offset: ", xOffset);
        yOffset = EditorGUILayout.IntField("Y Offset: ", yOffset);
        layer = EditorGUILayout.IntField("Layer: ", layer);

        // GUI button
        if (GUILayout.Button("Create Cell Properties List"))
        {
            if (texture != null)
                textureParse.ParseTextureFile(texture, xOffset, yOffset, layer);
        }
    }
}
